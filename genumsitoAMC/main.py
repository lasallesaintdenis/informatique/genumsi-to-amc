from csv import DictReader
from html.parser import HTMLParser
import argparse


def lire_fichier() -> list:
    """
    Renvoie la totalité des questions du fichier csv
    """
    questions = []
    with open('data/export_questions_csv.csv') as fp:
        lignes = DictReader(fp)

        for question in lignes:
            questions.append(question)
    return questions


def get_id(question):
    """Renvoie le numéro de la question"""
    return question['numero de la question']



def get_entree(questions, num) -> int:
    for k,question in enumerate(questions):
        if get_id(question) == str(num):
            return k
    else:
        raise KeyError("L'exercice demandé n'est pas dans la base")


def get_domaine(question):
    """Renvoie le domaine de la question passée en argument"""
    return question[' domaine']


def unhtml(html):
    """Renvoie le code LaTeX simplifié d'un élément html"""
    class HTML2LaTeXParser(HTMLParser):
        latex_code = []
        opened_tags = []
        
        def handle_starttag(self, tag, attrs):
            if tag == 'p':
                self.opened_tags.append('p')
                self.latex_code.append("")
            elif tag == 'code':
                self.opened_tags.append('code')
                self.latex_code.append("\\mint{python}|")
            elif tag == 'br':
                if len(self.opened_tags) > 0 and self.opened_tags[-1] == 'pre':
                    self.latex_code.append('\n')
                else:
                    self.latex_code.append("\\newline ")
            elif tag == 'pre':
                self.opened_tags.append('pre')
                self.latex_code.append('\\begin{minted}{python}\n')
            elif tag == 'span' and 'math inline' in attrs[0][1]:
                self.latex_code.append('$')
            elif tag == "em":
                self.latex_code.append("")
            elif tag == 'sup':
                self.latex_code.append('^{')

        def handle_endtag(self, tag):
            if tag == 'p':
                self.latex_code.append("")
            elif tag == 'code':
                self.latex_code.append("|")
            elif tag == 'pre':
                self.latex_code.append('\n\\end{minted}')
            elif tag == 'span':
                self.latex_code.append('$')
            elif tag == "em":
                self.latex_code.append("")
            elif tag == 'sup':
                self.latex_code.append('}')

        def handle_data(self, data):
            self.latex_code.append(data)
    parser = HTML2LaTeXParser()
    parser.feed(html)
    return "".join(parser.latex_code)


def get_question(question):
    """Renvoie la transformée en tex de la question stockée dans la base"""
    return unhtml(question[' question'])


def get_reponses(question):
    """renvoie les 4 propositions dans l'ordre sous forme d'un tuple"""
    return (unhtml(question[' reponseA']),
            unhtml(question[' reponseB']),
            unhtml(question[' reponseC']),
            unhtml(question[' reponseD'])
            )


def get_bonne_reponse(question):
    """Renvoie le numéro de la bonne réponse"""
    lettre = question[' bonne_reponse']
    return ord(lettre) - 65


def ecrire_entete():
    with open('data/entete.tex', 'r') as fp:
        r = fp.readlines()
        for ligne in r:
            print(ligne, end="")


def ecrire_pied():
    with open('data/pied.tex', 'r') as fp:
        r = fp.readlines()
        for ligne in r:
            print(ligne, end="")


def ecrire_question(question):
    """Écrit la question, à grand coup de print()"""
    # print(f"\\element{{{get_domaine(question)}}}{{")
    print(f"\\begin{{question}}{{{get_id(question)}}}")
    print(get_question(question))
    print("\\begin{reponses}")
    for k, reponse in enumerate(get_reponses(question)):
        if get_bonne_reponse(question) == k:
            print(f"\\bonne{{{reponse}}}")
        else:
            print(f"\\mauvaise{{{reponse}}}")
    print("\\end{reponses}")
    print("\\end{question}")
    # print("}")


def generer():
    parser = argparse.ArgumentParser(description="Génère un fichier AMC")
    parser.add_argument('questions', metavar='N', type=int,
                        help="la liste des questions", action="append",
                        nargs="+"
                        )
    liste_question = parser.parse_args().questions[0]
    ecrire_entete()
    questions = lire_fichier()
    for q in liste_question:
        questions_choisies = get_entree(questions, q)
        ecrire_question(questions[questions_choisies])
    ecrire_pied()


if __name__ == '__main__':
    generer()
