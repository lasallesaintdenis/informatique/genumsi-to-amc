#!/usr/bin/env python3

"""
    Un module pour transformer le contenu de genumsi vers
    auto-multiple-choice
"""

__version__ = '0.0.1'

from genumsitoAMC import main

