from genumsitoAMC.main import unhtml

def test_html_p():
    assert unhtml('<p>Un texte</p>') == 'Un texte'

def test_html_code():
    assert unhtml('<code>Du code</code>') == '\\verb!Du code!'

def test_html_br():
    assert unhtml('<br>') == '\\newline '

def test_html_pre():
    reponse = '\\begin{verbatim}\nDu\ncode\n\\end{verbatim}'
    assert unhtml('<pre>Du<br>code</pre>') == reponse

def test_html_span_em():
    assert unhtml("<span class='math inline'><em>n</em></span>") == "$n$"

def test_html_span_sup():
    assert unhtml("<span class='math inline'>2<sup>2500</sup></span>") == "$2^{2500}$"
