.. genumsitoAMC documentation master file, created by
   sphinx-quickstart on Wed Nov 25 23:26:32 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to genumsitoAMC's documentation!
========================================

À installer avec : `pip install --extra-index-url https://framagit.org/api/v4/projects/69246/packages/pypi/simple genumsitoAMC`

Voir https://framagit.org/lasallesaintdenis/informatique/genumsi-to-amc/-/packages/20

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
