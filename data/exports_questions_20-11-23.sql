CREATE TABLE `questions` (
  `num_question` int NOT NULL AUTO_INCREMENT,
  `question` varchar(1000) DEFAULT NULL,
  `reponseA` varchar(500) DEFAULT NULL,
  `reponseB` varchar(500) DEFAULT NULL,
  `reponseC` varchar(500) DEFAULT NULL,
  `reponseD` varchar(500) DEFAULT NULL,
  `bonne_reponse` varchar(1) DEFAULT NULL,
  `num_domaine` int DEFAULT NULL,
  `num_sous_domaine` int DEFAULT NULL,
  `niveau` char(50) DEFAULT NULL,
  `num_util` int DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `date_ajout` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `auteur` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`num_question`),
  KEY `FK_domaine` (`num_domaine`),
  KEY `FK_num_util` (`num_util`),
  KEY `FK_qu_ss_dom` (`num_sous_domaine`),
  CONSTRAINT `FK_domaine` FOREIGN KEY (`num_domaine`) REFERENCES `domaines` (`num_domaine`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_num_util` FOREIGN KEY (`num_util`) REFERENCES `utilisateurs` (`num_util`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_qu_ss_dom` FOREIGN KEY (`num_sous_domaine`) REFERENCES `sous_domaines` (`num_sous_domaine`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1496 DEFAULT CHARSET=utf8;

/*!40000 ALTER TABLE `questions` DISABLE KEYS */;
REPLACE INTO questions VALUES
("1","<p> Quel est l'entier positif codé en base 2 sur 8 bits par le code 0011 1010 ? </p>","45","58","25","-128","B","2","2","1","1",NULL,"2019-11-04 21:58:52","nreveret"),
("2","<p> Quel est l'entier relatif codé en complément à 2 sur un octet par le code 1111 1111 ?</p>","255","127","-1","45","C","2","3","1","1",NULL,"2019-11-04 21:58:52","nreveret"),
("3","<p> Quel est le rôle de l'Unité Arithmétique et Logique dans un processeur?</p>","La gestion interne du processeur","Faire les calculs","Interfacer les différents périphériques","Définir la base arithmétique","B","6","23","1","1",NULL,"2019-11-04 21:58:52","nreveret"),
("4","<p>Parmi les éléments suivants, lequel n'est pas un périphérique d'entrée sortie ?</p>","La souris","L'imprimante","Le clavier","Le transistor","D","6","26","1","1",NULL,"2019-11-04 21:58:52","nreveret"),
("5","<p>Pour un réseau de classe B, on a un masque de sous-réseau qui est :</p>","<code>255.255.0.0</code>","<code>255.255.255.0 </code>","<code>255.0.0.0</code>","<code>255.255.255.255</code>","A","6","24","1","1",NULL,"2019-11-04 21:58:52","nreveret"),
("6","<p>Tom peut lire dans les propriétés de son smartphone  l’adresse MAC ci-dessous :</p><code>5E FF 56 A2 AF 15</code><p>Sachant que cette adresse est exprimée en hexadécimal.</p><p>Elle est donc codée sur : </p>","12 octets","48 bits","192 bits","24 octets","B","6","24","1","1",NULL,"2019-11-04 21:58:52","nreveret"),
("7","<p>Une trame ethernet vient d’être capturée à l’aide de logiciel wireShark. L’entête de cette trame contient : </p>","L’adresse MAC du destinataire","L’adresse IP de l’expéditeur","Les données","L’adresse MAC du destinataire puis l’adresse MAC de la source","D","6","24","1","1",NULL,"2019-11-04 21:58:52","nreveret"),
("8","<p>Parmi les affirmations suivantes, laquelle est vraie ?</p>","La mémoire RAM est une mémoire accessible en lecture seulement ","La mémoire RAM est une mémoire accessible en écriture seulement ","La mémoire RAM est une mémoire accessible en lecture et en écriture","La mémoire RAM permet de stocker des données après extinction de la machine ","C","6","23","1","1",NULL,"2019-11-04 21:58:52","nreveret"),
("9","<p>Dans un réseau d’entreprise l’adresse du réseau est décrite par :<br><code>Adresse  : 172.16.0.0<br>Netmask : 255.255.0.0</code><p>Combien d’hôtes réseaux peuvent être accueillis sur ce réseau ?</p>","255","510","65534","65536","C","6","24","1","1",NULL,"2019-11-04 21:58:52","nreveret"),
("10","<p>À partir du dossier <code>~/Doc/QCM</code> quelle commande permet de rejoindre le dossier <code>~/.Hack/Reponses</code> ?</p>","<code>cd ../../.Hack/Reponses</code> ","<code>cd .Hack/Reponses </code>","<code>cd /~/.Hack/Reponses</code>","<code>cd /.Hack/Reponses  </code>","A","6","25","1","1",NULL,"2019-11-04 21:58:52","nreveret"),
("11","<p>En logique (algèbre de Boole), l'expression: <code>non (A ou B)</code> est équivalente à  :</p>","<code>non A ou non B</code>","<code>A ou B</code>","<code>A et B</code>","<code>non A et non B</code>","D","2","5","1","1",NULL,"2019-11-04 21:58:52","nreveret"),
("12","<p>Le résultat de l'addition des deux nombres binaires 1101 et 0101 est:</p>","10010","10110","10011","11010","A","2","2","1","1",NULL,"2019-11-04 21:58:52","nreveret"),
("13","<p>Convertir  la valeur décimale 155 en binaire (sur un octet)</p>","10011011","11011011","01111111","10010111","A","2","2","1","1",NULL,"2019-11-05 11:35:11","nreveret"),
("14","<p>Convertir  la valeur décimale 195 en hexadécimal</p>","A5","B9","C3","C9","C","2","2","1","1",NULL,"2019-11-04 21:58:52","nreveret"),
("15","<p>Après avoir saisi dans son navigateur l'url de son forum favori, Jean reçoit comme réponse du serveur une erreur '404'. Une seule des réponses suivantes ne correspond pas à cette situation. Laquelle ?</p>","Une panne de sa connexion internet","Une mise à jour du serveur qu'elle consulte","Une erreur de saisie de sa part","Un changement du titre du forum qu'elle consulte","A","5","20","1","1",NULL,"2019-11-04 21:58:52","nreveret"),
("16","<p>Saisir l'url <code>http://monsite.com/monprogramme.py?id=25</code> dans la barre d'adresse du navigateur ne permet pas:</p>","De télécharger un programme Python","D'exécuter un programme Python sur le serveur","D'exécuter un programme Python sur le client","D'afficher une page html","C","5","22","1","1",NULL,"2019-11-04 21:58:52","nreveret"),
("17","<p>Qu'est ce qu'un algorithme ?</p>","Un organigramme","Un organigramme ou un pseudocode","Une Décision","Instructions pas à pas utilisées pour résoudre un problème","D","7","28","1","1",NULL,"2019-11-04 21:58:52","nreveret"),
("18","<p>On souhaite écrire un programme calculant le triple d'un nombre décimal et affichant le résultat. On a saisi le code suivant :</p><code>nombre = input('Saisir un nombre') <br>triple = nombre * 3 <br>print(triple) <br></code><p>Un utilisateur saisit le nombre 5 lorsque l'ordinateur lui demande. Quel va être le résultat affiché ?</p>","<code>nombrenombrenombre</code>","<code>555</code>","<code>15</code>","<code>15.0</code>","B","7","28","1","1",NULL,"2019-11-04 21:58:52","nreveret"),
("19","<p>On a saisi le code suivant :</p><code>nombre = int (input('Saisir un nombre') )<br>double = nombre * 2<br>print(double)<br></code><p>Quel message affiche l’ordinateur lorsque l'utilisateur saisit 8.5 ?</p>","<code>16</code>","<code>16.0</code>","<code>17</code>","L'ordinateur affiche une erreur","D","7","28","1","1",NULL,"2019-11-04 21:58:52","nreveret"),
("20","<p>On a saisi le code suivant :</p>
<pre><code class='pyhon'>a = '1 + 1'
b = 1 + 1
c = '2'
d = 2</code></pre>
<p>Quelle instruction permet d’afficher le message <code class='python'>1 + 1 = 2</code> ?</p>","<pre><code class='pyhon'>print(a + ' = ' + c)</code></pre>","<pre><code class='pyhon'>print(a + ' = ' + d)</code></pre>","<pre><code class='pyhon'>print(b + ' = ' + c)</code></pre>","<pre><code class='pyhon'>print(b + ' = ' + d)</code></pre>","A","7","28","1","1",NULL,"2020-09-14 16:28:46","nreveret"),
("21","<p>On a saisi le code suivant :</p><code>mot = 'première'</code><p>Quelle affirmation est vraie ?</p>","<code>mot[1]</code> vaut <code>p</code>","<code>len(mot)</code> vaut <code>7</code>","<code>len(mot)</code> vaut <code>6</code>","<code>mot[7]</code> vaut <code>e</code>","D","7","28","1","1",NULL,"2019-11-04 21:58:52","nreveret"),
("22","<p>On a saisi le code suivant :</p><code>a = 8<br>b = 5<br>a = a + b<br>b = a - b<br>a = a - b</code><p>Quelles sont les valeurs de a et b à la fin du programme ?</p>","<code>a = 8</code> et <code>b = 5</code>","<code>a = 8</code> et <code>b = 13</code>","<code>a = 5</code> et <code>b = 8</code>","<code>a = 13</code> et <code>b = 5</code>","C","7","28","1","1",NULL,"2019-11-04 21:58:52","nreveret"),
("23","<p>On a saisi le code suivant :</p><code>a = float( input('Saisir un nombre : ') )<br>if a < 1 :<br>  print('TIC')<br>elif a > 8.5 :<br>  print('TAC')<br>else :<br>  print('TOE')</code><p>Quel message affiche l’ordinateur lorsque l'utilisateur saisit 8.5 ?</p>","<code>TIC</code>","<code>TAC</code>","<code>TOE</code>","Le code n'affiche aucun message","C","7","28","1","1",NULL,"2019-11-27 18:37:53","nreveret"),
("24","<p>On a saisi le code suivant :</p><code>n = 8.0<br>while n > 1.0 :<br>&nbsp;&nbsp;n = n / 2</code><p>Quelle est la valeur de n après l’exécution du code ?</p>","<code>4.0</code>","<code>2.0</code>","<code>1.0</code>","<code>0.5</code>","C","7","28","1","1",NULL,"2019-11-04 21:58:52","nreveret"),
("25","<p>On souhaite que l’utilisateur saisisse une valeur entière comprise entre 1 et 20 (inclus).</p><p>Quel code est correct ?</p>","<code>a = int ( input('Saisir un nombre entre 1 et 20') )<br>while (a &lt; 1 and a &gt; 20) :<br>&nbsp;&nbsp;a = int ( input('Saisir un nombre entre 1 et 20') )</code>","<code>while (a &lt; 1 and a &gt; 20) :<br>&nbsp;&nbsp;a = int ( input('Saisir un nombre entre 1 et 20') )</code>","<code>a = int ( input('Saisir un nombre entre 1 et 20') )<br>while (a &lt; 1 or a &gt; 20) :<br>&nbsp;&nbsp;a = int ( input('Saisir un nombre entre 1 et 20') )</code>","<code>a = int ( input('Saisir un nombre entre 1 et 20') )<br>while (a &gt;= 1 and a &gt;= 20) :<br>&nbsp;&nbsp;a = int ( input('Saisir un nombre entre 1 et 20') )</code>","C","7","28","1","1",NULL,"2019-11-04 21:58:52","nreveret"),
("26","<p>On souhaite écrire un programme affichant tous les entiers multiples de 3 entre 6 et 288 inclus.</p><p>Quel code est correct ?</p>","<code>for n in range(6, 288, 3) :<br>&nbsp;&nbsp;print(n)</code>","<code>for n in range(6, 289) :<br>&nbsp;&nbsp;print(n / 3)</code>","<code>for n in range(6, 289) :<br>&nbsp;&nbsp;print(3 * n)</code>","<code>for n in range(6, 290, 3) :<br>&nbsp;&nbsp;print(n)</code>","D","7","28","1","1",NULL,"2019-11-04 21:58:52","nreveret"),
("27","<p>On a saisi le code suivant :</p><code>a = 12<br>for i in range(3) :<br>&nbsp;&nbsp;a = a * 2<br>&nbsp;&nbsp;a = a - 10</code><p>Quelle est la valeur de a après l’exécution du code ?</p>","<code>18</code>","<code>18.0</code>","<code>26</code>","<code>26.0</code>","C","7","28","1","1",NULL,"2019-11-04 21:58:52","nreveret"),
("28","<p>On considère le code suivant :</p><code>def f(tab):<br>  for i in range(len(tab)//2):<br>    tab[i],tab[-i-1] = tab[-i-1],tab[i]</code><p>Après les lignes suivantes :</p><code>tab = [2,3,4,5,7,8]<br>f(tab)</code><p>quelle est la valeur de tab ?</p>","<code>[2,3,4,5,7,8]</code>","<code>[5,7,8,2,3,4]</code>","<code>[8,7,5,4,3,2]</code>","<code>[4,3,2,8,7,5]</code>","C","3","10","1","1",NULL,"2019-11-09 09:59:36","nreveret"),
("29","<p>On dispose d'une liste de triplets: <code>t  = [(1,12,250),(1,12,251), (2,12,250),(2,13,250),(2,11,250),(2,12,249)]</code>.</p><p>On trie cette liste par ordre croissant des valeurs du second élément des triplets. En cas d'égalité, on trie par ordre croissant du troisième champ. Si les champs 2 et 3 sont égaux, on trie par ordre croissant du premier champ.</p><p>Après ce tri, quel est le contenu de la liste ?</p>","<code>[(1,12,249),(1,12,250),(1,12,251),(2,11,250),(2,12,250),(2,13,250)]</code>","<code>[(2,11,250),(1,12,249),(1,12,250),(2,12,250),(1,12,251),(2,13,250)]</code>","<code>[(2,11,250),(1,12,249),(1,12,250),(1,12,251),(2,12,250),(2,13,250)]</code>","<code>[(1,12,249),(2,11,250),(1,12,250),(2,12,250),(2,13,250),(1,12,251)]</code>","B","4","16","1","1",NULL,"2019-11-04 21:58:52","nreveret"),
("30","<p>Que permet la commande <code>ping</code> lancée sur ce poste ?</p> ","Vérifier que l'on a accès à internet ","Connaître l'adresse Ethernet d'une autre machine ","Tester l'accessibilité d'une autre machine à travers un réseau IP ","Saturer le réseau avec des requêtes ICMP ","C","6","24","1","1",NULL,"2019-11-04 21:58:52","nreveret"),
("31","<p>Voici une fonction Python de recherche d'un maximum :</p><code>def maxi(t):<br>&nbsp;&nbsp;m = -1 <br>&nbsp;&nbsp;for k in range(len(t)):<br>&nbsp;&nbsp;&nbsp;&nbsp;if t[k] > m:<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;m = t[k] <br>&nbsp;&nbsp;return m </code><p>Avec quelle précondition sur la liste <code>t</code>, la postcondition « <i>m est un élément maximum de la liste s</i> » n'est-elle pas assurée ?</p>","Tout élément de <code>t</code> est un entier positif ou nul ","Tout élément de <code>t</code> est un entier supérieur ou égal à -2 ","Tout élément de <code>t</code> est un entier supérieur ou égal à 11 ","Tout élément de <code>t</code> est un entier strictement supérieur à -2 ","B","7","28","1","1",NULL,"2019-11-04 21:58:52","nreveret"),
("32","<p>On dispose d'un tableau d'entiers, ordonné en ordre croissant. On désire connaître le nombre de valeurs distinctes contenues dans ce tableau.</p><p>Quelle est la fonction qui ne convient pas ?</p>","<code>def compte(t):<br>&nbsp;&nbsp;cpt = 1<br>&nbsp;&nbsp;for i in range(1,len(t)):<br>&nbsp;&nbsp;&nbsp;&nbsp;if t[i] != t[i-1]:<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cpt = cpt + 1<br>&nbsp;&nbsp;return cpt</code>","<code>def compte(t):<br>&nbsp;&nbsp;cpt = 1<br>&nbsp;&nbsp;for i in range(0,len(t)-1):<br>&nbsp;&nbsp;&nbsp;&nbsp;cpt = cpt + int(t[i] != t[i+1])<br>&nbsp;&nbsp;return cpt</code>","<code>def compte(t):<br>&nbsp;&nbsp;cpt = 0<br>&nbsp;&nbsp;for i in range(0,len(t)-1):<br>&nbsp;&nbsp;&nbsp;&nbsp;cpt = cpt + int(t[i] != t[i+1])<br>&nbsp;&nbsp;return cpt</code>","<code>def compte(t):<br>&nbsp;&nbsp;cpt = 0<br>&nbsp;&nbsp;for i in range(0,len(t)-1):<br>&nbsp;&nbsp;&nbsp;&nbsp;if t[i] != t[i+1]:<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cpt = cpt + 1<br>&nbsp;&nbsp;return cpt+1</code>","C","3","10","1","1",NULL,"2019-11-04 21:58:52","nreveret"),
("33","<p>Alan Turing était :</p>","un scientifique ayant développé la machine Enigma","l'inventeur du transistor","un ingénieur ayant utilisé des cartes perforées afin de programmer des métiers à tisser","l'inventeur du modèle théorique de l'ordinateur","D","1","1","1","1","33_turing.jpg","2019-11-11 09:52:53","nreveret"),
("34","<p>Alan Turing était :</p>","un scientifique ayant développé la machine Enigma","l'un des inventeurs du transistor","un ingénieur ayant utilisé des cartes perforées afin de programmer des métiers à tisser","l'inventeur du modèle théorique de l'ordinateur","D","11","40","T","1","34_turing.jpg","2019-11-11 09:53:09","nreveret"),
("35","<p>A quelle expression logique correspond la table de vérité ci-dessous ?</p><br><table class='table_verite' border='1' width='100px'><tr><th>a</th><th>b</th><th>?</th></tr><tr><th>F</th><th>F</th><th>V</th></tr><tr><th>F</th><th>V</th><th>V</th></tr><tr><th>V</th><th>F</th><th>V</th></tr><tr><th>V</th><th>V</th><th>F</th></tr></table>","NON (a ET b)","a OU b","NON (a OU b)","a ET b","A","2","5","1","1",NULL,"2019-11-04 21:58:52","osupk8"),
("36","A quelle expression logique correspond la table de vérité ci-dessous ?<table class='table_verite' border='1' width='100px'><tr><th>a</th><th>b</th><th>?</th></tr><tr><th>F</th><th>F</th><th>F</th></tr><tr><th>F</th><th>V</th><th>V</th></tr><tr><th>V</th><th>F</th><th>V</th></tr><tr><th>V</th><th>V</th><th>V</th></tr></table>","a OU b","NON (a OU b)","a ET b","NON (a ET b)","A","2","5","1","1",NULL,"2019-11-04 21:58:52","osupk8"),
("37","A quelle expression logique correspond la table de vérité ci-dessous ?<table class='table_verite' border='1' width='100px'><tr><th>a</th><th>b</th><th>?</th></tr><tr><th>F</th><th>F</th><th>F</th></tr><tr><th>F</th><th>V</th><th>F</th></tr><tr><th>V</th><th>F</th><th>F</th></tr><tr><th>V</th><th>V</th><th>V</th></tr></table>","a OU b","NON (a OU b)","a ET b","NON (a ET b)","C","2","5","1","1",NULL,"2019-11-06 18:01:56","osupk8"),
("38","Laquelle de ces propriétés est toujours vraie ?","a ET  (NON a) = V","a ET  (NON a) = NON  a","a ET  (NON a) = F","a ET  (NON a) = a","C","2","5","1","1",NULL,"2019-11-06 18:05:10","osupk8"),
("39","Quelle est le résultat de :  <code>'orange'[-3] ?</code>","'e'","'g'","'n'","Error : Negative index","C","7","28","1","1",NULL,"2019-11-04 21:58:52","osupk8"),
("40","Quelle est le résultat de :  <code>[ (a,b) for a in range(3) for b in range(a) ]</code> ?","[(1,0),(2,1),(2,1)]","[(1,0),(2,1),(3,2)]","[(1,0),(2,0),(2,1)]","[(0,0),(1,1),(2,2)]","C","7","28","1","1",NULL,"2019-11-06 18:15:55","osupk8"),
("41","Que contient la variable a si on execute ce script ?<pre>def diff(val1,val2):<br>    return val2 - val1 <br>a = diff(3.0,-2.0)</pre>","5.0","1.0","-1.0","-5.0","D","7","28","1","1",NULL,"2020-03-08 18:04:39","osupk8"),
("42","Que contient la variable <code>a</code> si on exécute ce script ?<br><br><pre>def carre(val):<br>  return val*val<br><br>def inc(val):<br>  return val + 1<br><br>a = carre(inc(3.0))</pre>","9.0","10.0","12.0","16.0","D","7","28","1","1",NULL,"2019-11-06 18:11:19","osupk8"),
("43","Que contiennent les variables a et b si on execute ce script ?<pre>def func(a):<br>    a += 2.0<br>    return a <br>a = 5.0 <br>b = func(a)</pre>","5.0 et 5.0","5.0 et 7.0","7.0 et 5.0","7.0 et 7.0","B","7","28","1","1",NULL,"2019-11-06 18:10:18","osupk8"),
("44","<code>Pour i allant de 0 à 9</code> s'écrit","<code>for i in range(8)</code>","<code>for i in range(9)</code>","<code>for i in range(10)</code>","<code>for i in range(11)</code>","C","7","28","1","1",NULL,"2019-11-06 18:16:31","osupk8"),
("45","<code>pour i allant de 1 à 10</code> s'écrit :","<code>for i in range(10)</code>","<code>for i in range(1,11)</code>","<code>for i in range(1,10)</code>","<code>for i in range(0,10)</code>","B","7","28","1","1",NULL,"2019-11-06 18:13:23","osupk8"),
("46","Que taper en Python pour obtenir 3<sup>8</sup> ?","3^8","3**8","3*8","3&8","B","7","28","1","1",NULL,"2019-11-05 11:32:35","osupk8"),
("47","Dans l'architecture de Von Neumann, le processeur contient :","L'unité de commande et l'unité arithmétique et logique","L'unité de commande et la RAM","Seulement l'unité arithmétique et logique.","Seulement l'unité de commande","A","6","23","1","1",NULL,"2019-11-04 21:58:52","osupk8"),
("48","<p>Avec la fonction donnée ci-dessous l'instruction <code>mystere(0,1)</code> retourne :</p><pre>def mystere(a,b):<br>    reponse=1<br>    if a==0:<br>        if b==0:<br>            reponse=0<br>    return reponse</pre>","0","1","True","False","B","7","28","1","1",NULL,"2019-11-06 18:17:19","osupk8"),
("49","Quelle est la valeur décimale de l'entier binaire 00011010 ?","26","22","51","24","A","2","2","1","1",NULL,"2019-11-04 21:58:52","osupk8"),
("50","L'ENIAC conçu par John William Mauchly et John Eckert,a été dévoilé au public en quelle année ?","1939","1946","1950","1955","B","6","23","1","1",NULL,"2019-11-06 18:07:39","osupk8"),
("51","Que représente l'unité FLOPS ?","Le nombre d'opérations sur des nombres  à virgule par seconde.","Le nombre d'opérations arithmétiques par seconde.","Le nombre d'instructions par seconde","La vitesse de l'horloge","A","6","23","1","1",NULL,"2019-11-04 21:58:52","osupk8"),
("52","Comment nomme-t-on une variable permettant de stocker des valeurs logiques ?","Une variable booléenne","Une variable logique","Une variable vrai - faux","Une variable binaire","A","2","5","1","1",NULL,"2019-11-04 21:58:52","osupk8"),
("53","Que dit la loi de MOORE ?","Que le nombre de transistors sur une puce va doubler tous les deux ans.","Que la vitesse des horloges va doubler tous les deux ans.","Que la puissance des ordinateurs va doubler tous les deux ans.","Que la taille des ordinateurs va être divisée par deux tous les deux ans.","A","6","23","1","1",NULL,"2019-11-04 21:58:52","osupk8"),
("54","<p>Soit le script Python suivant : :</p><pre>import random <br>pos = random.randint(2, 9)</pre><p>Quelle valeur est associée à la variable pos ?</p>","un nombre entier aléatoire dans l'intervalle [2 ; 9[","un nombre entier aléatoire dans l'intervalle [2 ; 9]","un nombre réel aléatoire dans l'intervalle ]2 ; 9[","un nombre réel aléatoire dans l'intervalle [2 ; 9[","B","7","28","1","1",NULL,"2019-11-06 18:15:12","osupk8"),
("55","A quoi sert l'horloge du processeur ?","Elle séquence les instructions à éxecuter.","Elle donne l'heure exacte.","Elle envoie des ordres de lecture de données.","Elle décide des instructions à éxecuter.","A","6","23","1","1",NULL,"2019-11-04 21:58:52","osupk8"),
("56","A quoi servent les mémoires cache ?","Elles contrôlent les droits d'écritures sur le disque dur.","Elles servent à accélerer les traitements.","Elles masquent les données avec des ET logiques.","Elles permettent de cacher les données inutiles.","B","6","23","1","1",NULL,"2019-11-06 18:06:18","osupk8"),
("57","En quelle année Georges Moore a-t-il énoncé sa loi ?","1965","1975","1982","1994","A","6","23","1","1",NULL,"2019-11-04 21:58:52","osupk8"),
("58","Donner le résultat de l'addition binaire : 1101 + 1001","10110","01001","00110","11010","A","2","2","1","1",NULL,"2019-11-04 21:58:52","osupk8"),
("59","Donner le résultat de l'addition binaire 101101 + 1011","111000","110110","101000","111100","A","2","2","1","1",NULL,"2019-11-04 21:58:52","osupk8"),
("60","Donner l'écriture décimale du nombre binaire 10011","19","17","23","21","A","2","2","1","1",NULL,"2019-11-04 21:58:52","osupk8"),
("61","Donner l'écriture décimale du nombre binaire 110101","13","53","47","51","B","2","2","1","1",NULL,"2019-11-06 17:56:11","osupk8"),
("62","Donner l'écriture hexadécimale du nombre binaire 1001011","3D","49","4B","5B","C","2","2","1","1",NULL,"2019-11-06 17:53:41","osupk8"),
("63","Donner l'écriture hexadécimale du nombre binaire 110101","35","6B","65","56","A","2","2","1","1",NULL,"2019-11-06 17:44:22","osupk8"),
("64","Donner l'écriture binaire du nombre 137","10001001","10111001","10001010","10010001","A","2","2","1","1",NULL,"2019-11-04 21:58:52","osupk8"),
("65","Donner l'écriture binaire du nombre 34.","010010","100010","100001","100110","B","2","2","1","1",NULL,"2019-11-06 17:57:43","osupk8"),
("66","Donner l'écriture binaire du nombre hexadécimal 6E","01101110","01110110","01101101","01110010","A","2","2","1","1",NULL,"2019-11-04 21:58:52","osupk8"),
("67","Donner l'écriture binaire du nombre hexadécimal B5","10110111","00110101","10110101","10111101","C","2","2","1","1",NULL,"2019-11-27 08:14:06","osupk8"),
("68","<p>Soit la liste : </p><br><pre>liste_pays = ['France','Allemagne','Italie','Belgique','Pays Bas']</pre><br><p> Que renvoie l'instruction :</p><br><pre>'Belgique' in liste_pays</pre>","True","False","liste_pays","['France','Allemagne','Italie','Belgique','Pays Bas']","A","7","28","1","1",NULL,"2019-12-08 18:25:02","osupk8"),
("69","Combien de chiffres binaires sont nécessaires pour coder le nombre 287","7","8","9","10","C","2","2","1","1",NULL,"2019-11-06 17:55:01","osupk8"),
("70","Combien de chiffres possède l'écriture binaire du nombre 75","5","6","7","8","C","2","2","1","1",NULL,"2019-11-06 17:51:40","osupk8"),
("71","<p>Soit la liste suivante :</p><br><pre>liste_pays = ['France','Allemagne','Italie','Belgique']</pre><br><p>Que renvoie :</p><br><pre>liste_pays[2]</pre>","France","Allemagne","Italie","Belgique","C","7","28","1","1",NULL,"2019-12-08 18:24:00","osupk8"),
("72","1 octet représente combien de bit(s) ?","2","3","6","8","D","2","2","1","1",NULL,"2019-11-06 17:41:24","osupk8"),
("73","Combien faut-il de bits minimum pour représenter le nombre décimal 16 ?","3","5","4","6","B","2","2","1","1",NULL,"2019-11-06 17:59:09","osupk8"),
("74","La figure ci-dessus représente l'architecture de Von Neumann. A quoi correspond l'élément n°2 ?","Mémoire","Unité de contrôle","Unité arithmétique et logique","Accumulateur","B","6","23",NULL,"1","74_von_neumann.png","2019-11-04 21:58:52","osupk8"),
("87","<p>Quelle est la valeur décimale de l'entier binaire 00011010 ?</p>","26","32","41","24","A","2","2",NULL,"1",NULL,"2019-11-04 21:58:52","admin"),
("88","Quelle est la représentation binaire du nombre 5D<sub>(16)</sub> ? <br>","01101101","01011110","01011101","10101101","C","2","2","1","1",NULL,"2019-11-04 21:58:52","osupk8"),
("89","<p>Quelle est la valeur hexadécimale de l'entier binaire 10110110</p>","C4","B6","B8","C6","B","2","2","1","1",NULL,"2019-11-04 21:58:52","osupk8"),
("90","<p>Avec 5 bits, on peut compter de .... à .... ? </p>","0 à 31","1 à 32","0 à 32","1 à 31","A","2","2","1","1",NULL,"2019-11-04 21:58:52","osupk8"),
("91","<p>Comment écrire 31<sub>(10)</sub> en écriture hexadécimale ?</p>","1F","1E","1D","1C","A","2","2","1","1",NULL,"2019-11-04 21:58:52","osupk8"),
("92","<p>Le nombre décimal 25 s'écrit en binaire :</p>","011010","00011000","0010101","011001","D","2","2","1","1",NULL,"2019-11-04 21:58:52","osupk8"),
("93","<p>Combien de mots binaires différents peut-on former avec 3 bits ?</p>","7","8","2<sup>3</sup>+1","3<sup>2</sup>+1","B","2","2","1","1",NULL,"2019-11-04 21:58:52","osupk8"),
("94","<p>Les entiers positifs ou nul dont l'écriture en base 16 (hexadécimal) est constituée  par un 1 suivi de 0 (par exemple 1, 10, 100, 1000, etc.) sont : </p><br>","les puissances de 2","les puissances de 8","les puissances de 10","les puissances de 16","D","2","2","1","1",NULL,"2019-11-04 21:58:52","osupk8"),
("95","<p>Le nombre hexadécimal A380 est codé sur combien d'octets ?</p>","2","4","8","16","A","2","2","1","1",NULL,"2019-11-04 21:58:52","osupk8"),
("96","<p>Que donne l’exécution de <code>foo(0x32,65)</code> ?</p><br><pre>def foo(n1,n2):<br>    return chr(n1)+chr(n2)</pre><br>","0x2A","'2A'","'2d'","0x3265","B","2","6","1","1",NULL,"2019-11-04 21:58:52","osupk8"),
("97","<p>Que donne l’exécution de <code>codage(‘25’)</code> ?</p><br><pre>def codage(nstr):<br>    n1= ord(nstr[0])-0x30<br>    n2= ord(nstr[1])-0x30<br>    return n2*10 + n1<br></pre>","25","'3235'","52","'3532'","C","2","6","1","1",NULL,"2019-11-04 21:58:52","osupk8"),
("98","<p>Combien de tables de vérités différentes peut-on définir avec un opérateur à 2 opérandes ? </p><br>","8","12","16","32","C","2","5",NULL,"1",NULL,"2019-11-04 21:58:52","osupk8"),
("99","<p>Laquelle de ces propriétés est toujours vraie ?</p>","a OU F = V","a OU F = F","a OU F = a","a OU F = NON a","C","2","5",NULL,"1",NULL,"2019-11-04 21:58:52","osupk8"),
("100","<p>Laquelle de ces propriétés est toujours vraie ?</p>","a OU a = V","a OU a = F","a OU a = NON a","a OU a = a","D","2","5",NULL,"1",NULL,"2019-11-04 21:58:52","osupk8"),
("101","<p>Laquelle de ces propositions est fausse ?</p>","<strong>(a ET b) ET c </strong><em> est équivalent à </em>  <strong>a ET (b ET c)</strong>","<strong>(a OU b) OU c </strong><em> est équivalent à  </em> <strong>a OU (b OU c)</strong>","<strong>(a OU b) ET c </strong><em>  est équivalent à  </em> <strong>(a OU b) ET (a OU c)</strong>","<strong>NON (a OU b) </strong><em>  est équivalent à </em>  <strong>(NON a) ET (NON b)</strong>","C","2","5",NULL,"1",NULL,"2019-11-04 21:58:52","osupk8"),
("102","<p>Soit l’expression logique NON (a ET b). Si a est Vrai que peut-on dire de cette expression ?</p><br>","Elle est forcément Vraie","Elle est forcément Fausse","Cela dépend de la valeur de b","On ne peut pas savoir","C","2","5",NULL,"1",NULL,"2019-11-04 21:58:52","osupk8"),
("103","<p>Soit l’expression logique NON (a OU b). Si a est Vrai que peut-on dire de cette expression ?</p><br>","Elle est forcément Vraie","Elle est forcément Fausse","Cela dépend de la valeur de b","On ne peut pas savoir","B","2","5",NULL,"1",NULL,"2019-11-04 21:58:52","osupk8"),
("109","<h4>La fonction ci-dessous calcule la température en °F à partir d’une température en °C. </h4><br><br><br><p>On a saisi le code suivant :</p><br><pre><br>def converti_temp(temp_c):    <br>    temp_f = temp_c *1.8+32<br>    return temp_f<br><br>a = .....................................<br>Print(“la température en °F est de “, a)<br></pre><br>Quelle instruction doit-on choisir pour appeler la fonction avec une température de 20 °C ?","a=converti_temp(20°C)","a=converti_temp('20')","a=int(input('saisir la temp en °C'))","a=converti_temp(20)","D","7","31",NULL,"1",NULL,"2019-11-04 21:58:52","planchet.d"),
("110","<h4>Suite au programme ci-dessous, il faut afficher le message suivant « je m’appelle prenom  et j’ai age ans ».</h4><br><br><br><pre><br>prenom=input('saisir votre prenom')<br>annee-naissance=int(input('saisir votre année de naissance'))<br>age= 2019 – annee_naissance<br></pre><br>Quelle instruction doit-on choisir ?","print('je m’appelle '+prenom+' et j’ai '+str(age)+ 'ans ')","print('je m’appelle '+int(prenom)+' et j’ai '+int(age)+ 'ans ')","print('je m’appelle '+prenom+' et j’ai '+ age+ 'ans ') ","print('je m’appelle '+prenom+' et j’ai '+int(age)+ 'ans ')","A","7","28",NULL,"1",NULL,"2019-11-04 21:58:52","planchet.d"),
("111","<h4>On a saisi le code suivant :</h4><br><br><br><pre><br>a = '8'<br>b = 5<br>a + b<br></pre><br>que retourne ce programme ?","'13'","TypeError : must be str, not int.","False","13","B","7","28",NULL,"1",NULL,"2019-11-04 21:58:52","planchet.d"),
("112","<h4>On entre les valeurs suivantes sur la console :</h4><br><br><br><pre><br>>>> a,b,c = 4,2,1<br></pre><br>Dans les expressions logiques suivantes, laquelle renvoie un booléen <b>True</b>.","a != c","a == b","a < b","a <= c","A","2","5",NULL,"1",NULL,"2019-11-04 21:58:52","planchet.d"),
("113","<h4>Le langage python a été élaboré par Guido Von Rossum en :</h4>","1981","1991","2001","2011","B","1","1",NULL,"1",NULL,"2019-11-04 21:58:52","planchet.d"),
("114","<h4>On souhaite écrire un programme calculant le triple d'un nombre décimal et affichant le résultat. </h4><br><br><br><p>On a saisi le code suivant :</p><br><pre><br>nombre = input('Saisir un nombre') <br>triple = nombre * 3 <br>print(triple) <br></pre><br>Un utilisateur saisit le nombre 5 lorsque l'ordinateur lui demande. Quel va être le résultat affiché ?<br>","555","nombrenombrenombre","15","15.0","A","7","28",NULL,"1",NULL,"2019-11-04 21:58:52","planchet.d"),
("115","<h4>Qu'affiche le programme python suivant ? </h4><br><br><br><pre><br>L=[3,2,2]<br>M=[1,5,1]<br>N=L+M<br>print(N)<br></pre><br><br>","N","[4,7,3]","[1,5,1,3,2,2]","[3,2,2,1,5,1]","D","3","10",NULL,"1",NULL,"2019-11-04 21:58:52","planchet.d"),
("116","<h4>Qu'affiche le programme python suivant ?</h4><br><br><br><pre><br>def f(x):<br>    return 2*x-4<br><br>def g(x):<br>     return x+3<br><br>print(f(g(0)))<br></pre><br>","f(g(0))","2","f(3)","-1","B","7","33",NULL,"1",NULL,"2019-11-04 21:58:52","planchet.d"),
("117","<h4>Soit a = [1,2,3,4,5], lequel des énoncés suivants est correct ?</h4>","print(a[:100]) #Affiche [1,2,3,4,5]","print(a[:]) #Affiche [1,2,3,4]","print(a[0:]) #Affiche [2,3,4,5]","print(a[-1:]) #Affiche [1,2]","A","3","10",NULL,"1",NULL,"2019-11-04 21:58:52","planchet.d"),
("118","<h4>On a saisi le code suivant :</h4><br><pre><br>a = [0.5 * x for x in range(0, 4)]<br></pre><br>donc a vaut :<br>","[0.0, 0.5, 1.0, 1.5, 2.0]","[0, 1, 2, 3, 4]","[0.0, 0.5, 1.0, 1.5]","[0, 1, 2, 3]  ","C","3","11",NULL,"1",NULL,"2019-11-04 21:58:52","planchet.d"),
("119","<h4>Qu'affiche le programme python suivant ? </h4><br><br><br><pre><br>L=[0,1,2]<br>M=[3,4,5]<br>N=[L[i]+M[i] for i in range(len(L))]<br>print(N)<br></pre><br>","[3,5,7]","[3,5,7,9]","[0,1,2,3,4,5]","Erreur","A","3","11",NULL,"1",NULL,"2019-11-04 21:58:52","planchet.d"),
("120","<h4>On a saisi le code suivant :</h4><br><br><br><pre><br>liste = [1,[2,7],3,[4,5,8],9]<br></pre><br>Lequel des énoncés suivants est correct ?<br>","print(liste[2]) #Affiche 2","print(liste[0][0]) #Affiche 1","print(liste[1][0]) #Affiche 2","print(liste[3][1]) #Affiche 4","C","3","10",NULL,"1",NULL,"2019-11-04 21:58:52","planchet.d"),
("121","<h4>On a saisi le code suivant :</h4><br><br><br><pre><br>s=0<br>for i in range(5):<br>    s=i<br>print(s)<br></pre><br>Qu'affiche le programme python ? <br>","5","4","10","0<br>1<br>2<br>3<br>4","B","7","28",NULL,"1",NULL,"2019-11-04 21:58:52","planchet.d"),
("122","<h4>Quelle est la sortie du code suivant ?</h4><br><br><br><pre><br>ma_liste = ['a','b','c','d']<br>print (''.join(ma_liste))<br></pre><br>","[abcd]","abcd","Null","a,b,c,d","B","2","6",NULL,"1",NULL,"2019-11-04 21:58:52","planchet.d"),
("123","<h4>Classer dans l’ordre chronologique les différentes machines</h4>","1 2 3 4","2 4 1 3","1 2 4 3","4 2 1 3","B","1","1",NULL,"1","123_machines.jpg","2019-11-04 21:58:52","planchet.d"),
("124","<h4>A quelle image correspond ce code ?</h4><br><br><br><pre><br>Image_inconnue = Image('09990:' '90909:' '99999:' '09990:' '09990')<br></pre>","Choix 1","Choix 2","Choix 3","Choix 4","A","5","22",NULL,"1","124_microbit.jpg","2019-11-04 21:58:52","planchet.d"),
("126","<h4>Dans le programme ci-dessous, vous souhaitez que la fonction choix_perso() retourne la valeur 3.</h4><br><br><br><p>On a saisi le code suivant :</p><br><pre><br>from microbit import *<br><br>def choix_perso():<br>    while True:<br>        if button_a.is_pressed() and not(button_b.is_pressed()):<br>            return 1<br>       elif not(button_a.is_pressed()) and button_b.is_pressed():<br>            return 2<br>        elif button_a.is_pressed() and button_b.is_pressed():<br>            return 3<br><br>retour = choix_perso()<br>display.show(retour)<br></pre><br>Sur quel(s) bouton(s) devez-vous appuyer ?","bouton A","bouton B","boutons A et B","ni bouton A ni bouton B","C","5","22",NULL,"1",NULL,"2019-11-04 21:58:52","planchet.d"),
("127","<p>Quelle commande linux permet de revenir à la racine de l'arborescence de fichiers ?</p><br>","cd .","cd ..","cd /","cd root","C","6","25",NULL,"1",NULL,"2019-11-07 06:47:02",NULL),
("128","<h4>Un peu d'HTML</h4><br><br><br><pre><br>         ‹form class='unform' action='maPage.php' method='get'›<br>               ‹input name='recherche' class='input' size='20' type='text' placeholder='Recherche...' /›<br>         ‹/form ›<br></pre><br><p>Dans ce formulaire par quel procédé les données saisies sont transmises au serveur ?</p><br><br>","Par la méthode POST","Par la méthode INPUT","Par la méthode GET","Par la méthode PLACEHOLDER","C","5","21",NULL,"1",NULL,"2019-11-07 11:41:56",NULL),
("129","<h4>Un peu d'HTML</h4><br><br><br><pre><br>         ‹form class='unform' action='maPage.php' method='get'›<br>               ‹input name='recherche' class='input' size='20' type='text' placeholder='Recherche...' /›<br>         ‹/form ›<br></pre><br><p>Quel est le nom de la page qui va récupérer les valeurs de ce formulaire ?</p><br><br>","inscription","formulaire.php","inscription.php","maPage.php","D","5","21",NULL,"1",NULL,"2019-11-07 11:42:40",NULL),
("130","<h4>Un peu d'HTML</h4><br><br><br><pre><br>         ‹form class='unform' action='maPage.php' method='get'›<br>               ‹input name='recherche' class='input' size='20' type='text' placeholder='Recherche...' /›<br>         ‹/form ›<br></pre><br><p>Dans ce formulaire, il manque un élément important du formulaire. Quel est-il ?</p><br><br>","Une image pour illustrer la demande.","Un libellé pour mieux définir la question.","Un bouton pour valider et soumettre les données du formulaire.","Un bouton Annuler pour effacer tous les champs en un clic.","C","5","21",NULL,"1",NULL,"2019-11-07 11:41:39",NULL),
("131","<h4>Couleur en Hexadécimal </h4><br><br><br><p>Dans une page HTML je veux un fond vert clair. Parmi ces quatre propositions laquelle est la plus proche du vert clair ?</p><br><br>","#0000FF","#151515","#22FF22","#FF1010","C","5","19",NULL,"1",NULL,"2019-11-07 16:50:05",NULL),
("132","<p>A quelle expression logique correspond cette table de vérité ?</p><br>","(a ET b) OU a","a ET (b ET a)","a OU (b OU a)","a OU (b ET b)","A","2","5",NULL,"1","132_tabverite1.png","2019-11-10 21:15:05",NULL),
("133","<p> Soit le code suivant :</p><br><pre>a = 12<br>b = a << 2<br></pre><br><p>Que vaut la variable b suite  à son exécution ?</p>","6","24","48","10","C","2","2",NULL,"1",NULL,"2019-11-10 13:46:57",NULL),
("134","<p>En python, que fait l'instruction ci-dessous ?</p><br><p> #print(a,b)</p>","elle affiche le texte 'a,b'","elle affiche les valeurs de a et b","elle génère une erreur","elle ne fait rien","D","7","28",NULL,"1",NULL,"2019-11-15 12:37:04",NULL),
("135","<br><p>En python, combien vaut : 12%5 ?</p><br>","1","2","3","ce calcul génère une erreur","B","7","28",NULL,"1",NULL,"2019-11-15 12:40:29",NULL),
("136","<br><p>Qu'affiche ce script ?</p><br><pre><br>n = 5<br>for _ in range (2, 7) :<br>    n = n + 2<br>print(n)<br></pre><br><br>","15","6","20","11","A","7","28",NULL,"1",NULL,"2019-11-15 12:42:57",NULL),
("137","<br><p>L'instruction 'if...elif...else' : :</p><br>","permet de diminuer le temps d'exécution du programme","ne présente pas plus d'avantages que plusieurs si... consécutifs","ne sert à rien. C'est juste une façon de présenter qui est plus lisible","aucune de ces réponses n'est correcte","A","7","28",NULL,"1",NULL,"2019-11-15 12:45:27",NULL),
("138","<br><p>On définit la fonction mystère suivante :</p><br><pre><br>def mystere (n) :<br>    if n % 3 == 0 or n % 5 == 0 :<br>        if n % 3 == 0 :<br>            resultat = 'A'<br>        else :<br>            resultat = 'B'<br>    else :<br>        if n % 5 == 0 :<br>            resultat = 'C'<br>        else :<br>            resultat = 'D'<br>    return resultat<br></pre><br>Quelle est la valeur de mystere(10) ?<br>","'A'","'B'","'C'","'D'","B","7","28",NULL,"1",NULL,"2019-11-15 12:49:35",NULL),
("139","<p>Combien de tests ('tab[i]==tab[j]') effectue la fonction Python suivante pour trier un tableau de 10 éléments :</p><br><pre><br>def recherche_doublons(tab):<br>    for i in range(1, len(tab)) :<br>         for j in range(len(tab)-i) :<br>              if tab[i] == tab[j] :<br>                    print('doublon aux rangs ', i, ' et', j)<br></pre><br>","45","100","10","55","A","3","10",NULL,"1",NULL,"2019-11-15 18:19:40",NULL),
("140","<br><p>Après le code Python qui suit, quelles sont les valeurs finales de x et de y ?</p><br><pre><br>x = 4<br>while x > 0 :<br>    y = 0<br>    while y < x :<br>        y = y + 1<br>        x = x - 1<br></pre>","La valeur finale de x est -1 et celle de y est 0","La valeur finale de x est 0 et celle de y est 0","La valeur finale de x est 0 et celle de y est 1","La boucle externe est une boucle infinie, le programme ne termine pas.","C","7","28",NULL,"1",NULL,"2019-11-15 18:23:22",NULL),
("141","<p>Quel est le résultat de l'évaluation de l'expression Python suivante ?</p><br><p> [x * x for x in range(10)]</p>","[0,1,4,9,16,25,36,49,64,81]","[0,1,4,9,16,25,36,49,64,81,100]","[0,2,4,8,16,32,64,128,256,512]","[0,2,4,8,16,32,64,128,256,512,1024]","A","3","11",NULL,"1",NULL,"2019-11-17 09:34:58",NULL),
("142","<p>Quelle est la valeur de la variable image après exécution du programme Python suivant :</p><br><pre><br>image = [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]<br>for i in range(4) :<br>     for j in range(4) :<br>          if (i+j) == 3 :<br>               image[i][j] = 1<br></pre><br>","[[0,0,0,1],[0,0,1,0],[0,1,0,0],[1,0,0,0]]","[[0,0,0,1],[0,0,0,1],[0,0,0,1],[0,0,0,1]]","[[0,0,0,1],[0,0,1,1],[0,1,1,1],[1,1,1,1]]","[[0,0,0,0],[0,0,0,0],[0,0,0,0],[1,1,1,1]]","A","4","14",NULL,"1",NULL,"2020-03-06 18:24:34",NULL),
("143","<p>Le programme Python suivant ne calcule pas toujours correctement le résultat de (x puissance y). Parmi les tests suivants, lequel va permettre de détecter l'erreur ?</p><br><pre><br>def puissance(x,y) :<br>     p = x<br>     for i in range(y-1):<br>          p = p * x<br>     return p<br></pre><br>","puissance(2,0)","puissance(2,1)","puissance(2,2)","puissance(2,10)","A","7","31",NULL,"1",NULL,"2019-11-17 20:58:59",NULL),
("144","<p>Soit une machine basée sur une architecture de Von Neumann, disposant d'un Accumulateur et du jeu d'instructions suivant :</p><br><pre><br>LDA xx  //LoaD (xx) in Accumulator<br>STA xx  //STore Accumulator value at address xx<br>ADD xx  //ADD (xx) to accumulator value<br>SUB xx  //SUBstract (xx) from accumulator value<br>BRZ xx  //BRanch to the instruction at address xx if Accumulator value equal Zero<br>BRP xx  //BRanch to the instruction at address xx if Accumulator value is Positive (or zero)<br>BRA xx  //BRanch Always to the instruction at address xx <br>END     //Stop program<br>où xx représente une adresse mémoire et (xx) son contenu.<br></pre><br><br><p>Que contient l'accumulateur une fois le programme ci-dessous exécuté ?</p><br><pre><br>01 LDA 09<br>02 SUB 08<br>03 BRP 06<br>04 LDA 08<br>05 BRA 07<br>06 LDA 09<br>07 HLT<br>08 25<br>09 41<br></pre>","08","09","41","25","C","6","23",NULL,"1",NULL,"2019-11-17 23:14:01",NULL),
("145","<p>Avec la variable <code>alphabet</code> définie par l'affectation suivante, quelle est l'expression Python permettant d'accéder à la lettre <code>E</code></p><br><pre><br>alphabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',<br> 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']</pre><br>","alphabet[4]","alphabet[5]","alphabet['E']","alphabet.E","A","3","10",NULL,"1",NULL,"2019-11-25 15:03:22",NULL),
("146","<p>Dans la définition suivante de la fonction <code>somme</code> en Python, quelle est l'instruction à ajouter pour que la valeur retournée par l'appel <code>somme([1, 2, 3, 4, 5])</code> soit <code>15</code>?</p><br><pre><br>def somme(tab):<br>    s = 0<br>    for i in range(len(tab)):<br>        ...<br>    return s<br></pre>","s = tab[i]","tab[i] = tab[i] + s","s = s + tab[i]","s = s + i","C","3","10",NULL,"1",NULL,"2019-11-18 14:14:12",NULL),
("147","<p>Quel est le résultat de l'évaluation de l'expression Python suivante ?</p><br><pre><br>[2**n for n in range(4)]<br></pre><br>","[0, 2, 4, 6, 8] ","[1, 2, 4, 8]","[0, 1, 4, 9]","[1, 2, 4, 8, 16]","B","3","11",NULL,"1",NULL,"2019-11-18 14:25:02",NULL),
("148","<p>Quelle est la valeur affichée par l'exécution du script suivant ?</p><br><pre><br>ports = {'ftp' : 21, 'http' : 80}<br>ports['imap'] = 142<br>print(ports['imap'])<br></pre>","Key not found","142","{'imap' : 142}","2","B","3","9",NULL,"1",NULL,"2019-11-18 14:30:22",NULL),
("149","<p>Quelle expression permet d'accéder au numéro de Tournesol, sachant que le répertoire a été défini par l'affectation suivante :</p><br><pre><br>repertoire = [{'nom' : 'Dupont', 'tel' : 5234}, {'nom' : 'Tournesol', 'tel' : 5248}, <br>    {'nom' : 'Dupond', 'tel' : 5237}]<br></pre>","repertoire[1]['tel']","repertoire['Tournesol']","repertoire['tel'][1]","repertoire['Tournesol']['tel']","A","4","14",NULL,"1",NULL,"2019-11-18 14:39:30",NULL),
("150","<p>Quelle est la valeur de la variable <code>image</code> après exécution du script Python suivant ?</p><br><pre><br>image = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]<br>for i in range(4):<br>    for j in range(4):<br>        if (i+j) == 3:<br>            image[i][j] = 1<br></pre>","<pre>[[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [1, 1, 1, 1]]</pre>","<pre>[[0, 0, 0, 1], [0, 0, 0, 1], [0, 0, 0, 1], [0, 0, 0, 1]]</pre>","<pre>[[0, 0, 0, 1], [0, 0, 1, 0], [0, 1, 0, 0], [1, 0, 0, 0]]</pre>","<pre>[[0, 0, 0, 1], [0, 0, 1, 1], [0, 1, 1, 1], [1, 1, 1, 1]]</pre>","C","3","12",NULL,"1",NULL,"2019-11-18 14:52:07",NULL),
("151","<p>Soit le circuit présenté ci-dessous, pour quels états des entrées <strong>a</strong> et <strong>b</strong> la sortie <strong>s</strong> est-elle à 0 ?</p>","a=0 et b=0","a=0 et b=1","a=1 et b=0","a=1 et b=1","B","6","23",NULL,"1","151_circuit1.png","2019-11-20 07:52:31",NULL),
("152","<p>Auxquels de ces circuits correspond la table de vérité ci-dessous ?</p><br><table class='table_verite' border='1' width='100px'><tr><th>a</th><th>b</th><th>s</th></tr><tr><th>0</th><th>0</th><th>1</th></tr><tr><th>0</th><th>1</th><th>0</th></tr><tr><th>1</th><th>0</th><th>1</th></tr><tr><th>1</th><th>1</th><th>0</th></tr></table>","Le circuit A","Le circuit B","Le circuit C","Le circuit D","A","6","23",NULL,"1","152_circuit2.png","2019-11-20 19:06:01",NULL),
("153","<p>Soit une machine basée sur une architecture de Von Neumann, disposant d'un Accumulateur et du jeu d'instructions suivant :</p><br><pre><br>LDA xx  //LoaD (xx) in Accumulator<br>STA xx  //STore Accumulator value at address xx<br>ADD xx  //ADD (xx) to accumulator value<br>SUB xx  //SUBstract (xx) from accumulator value<br>BRZ xx  //BRanch to the instruction at address xx if Accumulator value equal Zero<br>BRP xx  //BRanch to the instruction at address xx if Accumulator value is Positive (or zero)<br>BRA xx  //BRanch Always to the instruction at address xx <br>END     //Stop program<br>où xx représente une adresse mémoire et (xx) son contenu.<br></pre><br><br><p>Que contient l'accumulateur une fois le programme ci-dessous exécuté ?</p><br><pre><br>01 LDA 07<br>02 SUB 05<br>03 ADD 06<br>04 HLT<br>05 08<br>06 52<br>07 12<br></pre>","56","72","32","0","A","6","23",NULL,"1",NULL,"2019-11-20 19:11:01",NULL),
("154","<h4>Dans les pages Web</h4><br><br><br><pre> &#60;Title&#62;Bienvenue dans mon Blog&#60;/Title&#62;</pre><br><br><br><p>A quoi sert la balise TITLE ?</p><br><br>","Afficher un titre dans la page web en gras, centré et en police 32.","A rien du tout, cette balise n'existe pas.","Afficher dans l'onglet du navigateur le nom contenu dans la balise.","Appliquer un style décrit dans un autre document.","C","5","19",NULL,"1",NULL,"2019-11-20 19:20:03",NULL),
("155","<h4>Dans une page web</h4><br><br><br><p>Pour décorer la page HTML, on utilise des feuilles de styles qui décrivent la mise en forme à appliquer.</p><br><p>Parmi ces propositions laquelle permettra de créer un lien vers une feuille de style ?</p><br>","<pre>&#60;meta http-equiv='Content-Type' content='text/html; charset=UTF-8' /&#62;</pre>","<pre>&#60;link href='monstyle.css' rel='stylesheet' type='text/css'&#62;</pre>","<pre>&#60;link rel='manifest' href='monstyle.json' /&#62;</pre>","<pre>&#60;script type='text/javascript' src='monstyle.js'&#62;&#60;/script&#62;</pre>","B","5","19",NULL,"1",NULL,"2019-11-20 19:29:21",NULL),
("156","Selon la loi de Moore la puissance des microprocesseurs ...","Quadruple tous les trois ans environ.","Double tous les 9 mois environ.","Quintuple tous les 3 ans environ.","Double tous les 2 ans environ.","D","6","23",NULL,"1",NULL,"2019-11-20 20:44:55",NULL),
("157","En considérant le complément à 2 sur 4 bits, quel est le résultat du calcul :  2-5 ?<br>","0111","1101","1011","1111","B","2","3",NULL,"1",NULL,"2019-11-20 20:53:39",NULL),
("158","Que représente 11111<sub>(2)</sub> en complément à 2 sur 5 bits ?","-0<sub>(10)</sub>","-1<sub>(10)</sub>","-15<sub>(10)</sub>","+31<sub>(10)</sub>","B","2","3",NULL,"1",NULL,"2019-11-20 20:57:54",NULL),
("159","<h4>Dans un page web </h4><br><br><br><p>Quelle balise permet d'afficher un bouton ?</p><br>","<pre>&#60;input name='monBouton'  type='text' id='ok' required&#62;</pre>","<pre>&#60;input name='monBouton'  type='button' id='ok'  value='ok'&#62;</pre>","<pre>&#60;textarea id='monBouton' name='BtnOk'&#62;&#60;/textarea&#62;</pre>","<pre>&#60;label&#62;Ok&#60;/label&#62;</pre>","B","5","21",NULL,"1",NULL,"2019-11-20 21:08:47",NULL),
("160","<h4>HTML</h4><br><br><br><p>Au fait HTML signifie quoi ?</p><br>","HyperText Marked Langage","HypercalifragilisTique Meilleur Langage ","HyperText Mark-up Language","Hyperfile Textual and Mean Language","C","5","22",NULL,"1",NULL,"2019-11-20 21:14:06",NULL),
("161","<h4>Dans une page web</h4><br><br><br><p>Que ne permet pas de faire un formulaire ?</p><br>","Renvoyer des données à une autre page.","Créer une interaction avec l'utilisateur.","Proposer un choix parmi une liste déroulante.","Envoyer des données à un autre internaute sans intermédiaire. ","D","5","21",NULL,"1",NULL,"2019-11-20 21:18:58",NULL),
("162","<h4>En HTML</h4><br><br><br><p>Les balises H1, H2, H3, H4, H5 permettent de faire de la mise en forme de titre. Parmi les propositions suivantes laquelle est la plus vraisemblable ?</p><br><br>","La mise en forme de ces balises peut être redéfinie par le CSS.","Ces balises sont toujours écrites en MAJUSCULE.","Ces balises définissent des liens hypertexte.","Le texte contenu entre ces balises ne peut pas être modifié.","A","5","22",NULL,"1",NULL,"2019-11-20 21:32:17",NULL),
("163","<h4>Architecture Client/Serveur</h4><br><br><br><p>Le protocole crypté garantissant la sécurisation de la transaction d'un message depuis le navigateur du client vers le serveur WEB s'appelle comment ?</p><br><br>","FTPS","TCP/IP","HTTPS","TCPs/IPs","C","5","20",NULL,"1",NULL,"2019-11-20 21:38:19",NULL),
("164","<h4>Dans une page Web</h4><br><br><br><p>Du coté du client, pour programmer et décrire une page web, on peut utiliser quels langages ?</p><br>","HTML, CSS, PHP et JAVA","HTML, CSS, JAVASCRIPT","HTML, SCRATCH et C","HTML, CSS, RUBY et PYTHON","B","5","20",NULL,"1",NULL,"2019-11-20 21:44:53",NULL),
("165","<h4>Dans une page web</h4><br><br><br><p>Le Javascript est un langage de programmation utilisé pour dynamiser les pages web.</p><br><br>Que signifie 'dynamiser' dans ce contexte ?<br>","La page web permet une interaction forte avec l'utilisateur, permettant de contrôler les saisies ou d'animer les images en temps réel.","La page web écoute votre voix pour répondre à vos questions.","La page web vous demande l'autorisation de mémoriser vos informations dans des cookies.","La page web est en lien direct avec plusieurs bases de données.","A","5","20",NULL,"1",NULL,"2019-11-20 21:55:10",NULL),
("166","<h4>Architecture Client/Serveur</h4><br><br><br><p>Que peut-on déduire de la lecture de cet URL ?</p><br><br><br><pre>https://172.24.0.24/index.php?user=125<br></pre><br>","Le site internet dysfonctionne.","La connexion est sécurisée.","Le serveur est sur le dark-web.","Il y a 125 utilisateurs connectés en ce moment sur le site web.","B","5","20",NULL,"1",NULL,"2019-11-20 22:02:03",NULL),
("167","<h4>Dans une page Web</h4><br><br><br><p>A quoi sert la balise suivante ?</p><br><pre>&#60;script&#62;</pre><br>","A charger une feuille de style.","A mémoriser les cookies.","A ajouter des animations graphique.","A charger des programmes qui seront exécutés par le navigateur.","D","5","22",NULL,"1",NULL,"2019-11-20 22:14:51",NULL),
("168","20<sub>(10)</sub>  a pour écriture hexadécimale:<br>","14","41","5F","F5","A","2","2",NULL,"1",NULL,"2019-11-21 09:20:12",NULL),
("169","Si 1011<sub>(2)</sub> est un entier signé sur 4 bits alors sa valeur décimale est :","-7","-5","5","7","B","2","3",NULL,"1",NULL,"2019-11-21 09:24:28",NULL),
("170","Le complément à deux sur 8 bits de 1011 0100<sub>(2)</sub> est ","0100 1011<sub>(2)</sub>","0011 0100<sub>(2)</sub>","0100 1000<sub>(2)</sub>","0100 1100<sub>(2)</sub>","D","2","3",NULL,"1",NULL,"2019-11-21 09:27:19",NULL),
("171","1010<sub>(2)</sub> &#215; 100<sub>(2)</sub> =<br>","10<sub>(2)</sub>","1110<sub>(2)</sub>","1 0100<sub>(2)</sub>","10 1000<sub>(2)</sub>","D","2","2",NULL,"1",NULL,"2019-11-21 09:33:05",NULL),
("172","11 1010<sub>(2)</sub>+ 1101<sub>(2)</sub> =<br>","100 0111<sub>(2)</sub>","11 2111<sub>(2)</sub>(2)","un entier pair","59<sub>(10)</sub>","A","2","2",NULL,"1",NULL,"2019-11-21 09:39:17",NULL),
("173","<br><p>Dans la fonction Mystere ci-dessous, le contenu de la boucle for a été effacé.<br><br>Si Mystere([1, 3, 5, 2]) renvoie [4, 8, 7, 2], quelle est l'instruction manquante?<br><pre><br>def Mystere(b):<br>  for i in range(len(b)-1):<br>       ...<br>  return b<br></pre><br><br>","b[i]=b[i]+b[i+1]","b[i]=b[i]+b[i-1]","b[i]=b[i]*b[i-1]","b[i+1]=b[i]+b[i+1]","A","7","28",NULL,"1",NULL,"2019-11-21 10:20:37",NULL),
("174","<br>Voici un entier signé sur 8 bits:<br><br>101001<sub>(2)</sub>.<br><br>On peut alors dire que:<br>","le nombre est pair.","le nombre est négatif.","le nombre est positif.","le nombre vaut -23 en base 10.","C","2","3",NULL,"1",NULL,"2019-11-21 11:03:39",NULL),
("175","<p> Voici une fonction Mystere où il manque un bloc d'instructions:</p><br><pre><br>def Mystere(t,n,p):<br>   nb=0<br>   for i in range(len(t)):<br>     ....<br>   return nb<br></pre><br>Voici quelques résultats d'exécution:<br><br>  Mystere([1, 2, 3, 4, 5, 7], 2, 5)  renvoie 3.<br><br>  Mystere([1, 2, 3, 4, 6, 7], 2,4.1)  renvoie 3.<br><br>Parmi les blocs d'instructions suivants, lequel convient?<br>"," <pre><br>if n < t[i] and t[i] < p:<br>    nb=nb+1<br></pre>","<pre><br>if n < t[i] and t[i] <= p:<br>    nb=nb+1<br></pre>","<pre><br>if n <= t[i] and t[i] < p :<br>    nb=nb+1<br></pre>","<pre><br>if n <= t[i] and t[i] <= p:<br>   nb=nb+1<br></pre>","C","7","28",NULL,"1",NULL,"2020-01-22 13:33:45",NULL),
("176","for i in range(5):<br>signifie que i prend les valeurs:<br><br>","0, 1, 2, 3, 4.","1, 2, 3, 4, 5.","5, 4, 3, 2, 1","4, 3, 2, 1, 0","A","7","28",NULL,"1",NULL,"2019-11-21 11:55:19",NULL),
("177","<h4>Quelles sont les valeurs prises successivement par la variable i dans la boucle for ci-dessous ?</h4><br><br><br><pre><br>res = 1<br>for i in range(3) :<br>    res =res +i<br></pre><br><br>","0, 1, 2","0, 1, 2, 3","1, 2, 3","1, 2, 3, 4","A","7","28",NULL,"1",NULL,"2019-11-23 12:18:10",NULL),
("178","<p>On a saisi le code suivant :</p><br><pre><br>def mystere(nombre) :<br>    while nombre > 5 :<br>        nombre = nombre – 5<br>return nombre<br></pre><br>Quelle affirmation est vraie dans celles proposées ci-dessous ?<br>","On sort de la boucle while dès que nombre > 5","On sort de la boucle while dès que nombre < 5","On sort de la boucle while dès que nombre  <= 5","On continue la boucle tant que nombre  <= 5","C","7","28",NULL,"1",NULL,"2019-11-23 12:18:43",NULL),
("179","<p>On a saisi le code suivant :</p><br><pre><br>debut =  'sous le pont Mirabeau coule la Seine.'<br>lettre  = ' u '<br>fin =  ' '<br>for i in debut : #parcourt la chaine de caractère<br>#si la lettre sur laquelle on est est différente de celle <br>#stockée dans lettre, alors on concatène ce caractère à ce qui<br>#était stocké précédemment dans la variable fin.<br>    if i != lettre :<br>        fin = fin+i<br>print(fin)<br></pre><br>Qu'affiche ce script à la fin de son exécution ?<br>","sos le pont Mirabea cole la Seine","souus le pont Mirabeauu couule la Seine","sois le pont Mirabeai coile la Seine","souis le pont Mirabeaui couile la Seine","A","7","28",NULL,"1",NULL,"2019-11-23 12:19:19",NULL),
("180","<h4>Quel est le résultat de l’opération binaire suivante : 10101 +11 ?</h4><br><br><br>","11 100","11010","11001","11000","D","2","7",NULL,"1",NULL,"2019-11-23 12:17:21",NULL),
("181","<h4>Une fonction <font face='courier new'>test</font> a été programmée pour vérifiée une fonction f dont on a donné une spécification.<br> Quelle proposition est vraie ?</h4><br><br><br>","La fonction <font  face='courier new' >test </font>permet de trouver toutes les éventuelles erreurs de codage de la fonction f.","La fonction <font  face='courier new' >test </font> peut permettre à l’utilisateur de trouver une éventuelle erreur de codage de la fonction f.                                  ","La spécification sert à l’interpréteur pour vérifier les paramètres de la fonction f.","La spécification sert à l’interpréteur pour vérifier le résultat renvoyé par la fonction f. ","B","7","31",NULL,"1",NULL,"2019-11-23 12:29:50",NULL),
("182","<h4>Après le code Python qui suit, quelles sont les valeurs finales de x et de y ?</h4><br><br><br><pre><br>x = 6<br>while x > 3 :<br>    y = 3<br>    while y < x :<br>         y = y + 1 <br>         x = x - 1  <br></pre><br><br>","La valeur finale de x est 3 et celle de y est 3.       ","La valeur finale de x est 3 et celle de y est 4.","La valeur finale de x est 4 et celle de y est 3.       ","La boucle externe est une boucle infinie, le programme ne termine pas.","B","7","28",NULL,"1",NULL,"2019-11-23 14:00:30",NULL),
("183","<h4>Si a et b sont deux booléens tels que a=1 et b=0,</br> que vaut le booléen :<br>not(a or b) and (a or not(b)) ?</h4><br><br>","0","1","Type Error","7","A","2","5",NULL,"1",NULL,"2019-11-23 14:02:51",NULL),
("184","<h4>Si a et b sont deux booléens tels que a=1 et b=0,</br> que vaut le booléen :<br>not(a and b) or (a and b) </h4><br><br><br>","0","1","Type Error","7","B","2","5",NULL,"1",NULL,"2019-11-23 14:04:14",NULL),
("185","<h4>Avec une mémoire de 16 bits, on peut coder tous les entiers naturels de : </h4><br><br>","0 à 15","0 à 255","0 à 65 535","0 à 4 294 967 295","C","2","2",NULL,"1",NULL,"2019-11-23 14:07:21",NULL),
("186","<h4>On souhaite convertir <I>2F </I> écrit en hexadécimal en décimal. <br> La valeur en décimal est :</h4><br><br><br>","15","17","33","47","D","2","7",NULL,"1",NULL,"2019-11-23 14:11:07",NULL),
("187","<h4> On souhaite convertir <I>25</I> de base 10 en base 2. <br> On obtient en binaire :</h4><br><br><br>","11001","10110","10011","11000","A","2","7",NULL,"1",NULL,"2019-11-23 14:13:34",NULL),
("188","<h4> Que peut-on dire de la représentation des nombres réels, donc des flottants ?<br> Elle est : </h4><br><br><br>","Appropriée    ","Approximative","Approvisionnée","Approfondie","B","2","4",NULL,"1",NULL,"2019-11-23 14:15:46",NULL),
("189","<h4>Les variables a, b et c  ont respectivement pour valeur True, 0 et 1. <br>Quelle est la valeur de l’expression <font face='Courier New'> a and b or c </font> ?</h4><br><br><br>","True","1","0","False","B","2","5",NULL,"1",NULL,"2019-11-23 14:18:53",NULL),
("190","<h4> Qu’est ce qu’un transistor ? </h4><br><br>","Un composant électronique utilisé comme résistance de chauffage.","Un composant électronique utilisé comme émetteur de lumière.","Un composant électronique utilisé par les processeurs.","Un composant électronique utilisé comme le condensateur.","C","6","27",NULL,"1",NULL,"2020-01-03 19:15:28",NULL),
("191","<p>Quel est l'entier positif codé en base 2 sur 8 bits par le code 0010 1010 ?</p>","42","21","84","3","A","2","2",NULL,"1",NULL,"2019-11-24 09:49:01",NULL),
("192","<p>Quelle est la valeur affichée a l'exécution du programme Python suivant ? </p><br><pre><br>x = 1 <br>for i in range(10): <br>    x = x * 2 <br>print(x)<br></pre>","1024","2","2000000000","2048","A","2","2",NULL,"1",NULL,"2019-11-24 09:52:14",NULL),
("193","<p>Quel est l’entier relatif codé en complément à 2 sur un octet par le code 1111 1111 ?<p>","-1","-128","-127","255","A","2","3",NULL,"1",NULL,"2019-11-24 10:18:20",NULL),
("194","<p>Que peut-on dire du programme Python suivant de calcul sur les nombres ﬂottants ? </p><br><pre><br>x = 1.0<br>while x != 0.0:<br>    x = x - 0.1<br></pre>","L’exécution peut ne pas s’arrêter, si la variable x n’est jamais égale exactement à 0.0","A la ﬁn de l’exécution, la variable x vaut -0.000001","A la ﬁn de l’exécution, la variable x vaut 0.000001","L’exécution s’arrête sur une erreur FloatingPointError.","A","2","4",NULL,"1",NULL,"2019-11-24 10:14:36",NULL),
("195","<p>Si <strong>A</strong> et <strong>B</strong> sont des variables booléennes, quelle est l’expression booléenne équivalente a : <strong> (not A) or B</strong> ?</p>","(A and B) or (not A or B) or (not A or not B)","(A and B) or (not A or B)","(not A or B) or (not A or not B)","(A or B) or (not A or not B)","B","2","5",NULL,"1",NULL,"2019-11-24 10:14:03",NULL),
("196","<p>Quelle est l’afﬁrmation vraie concernant le codage UTF-8 des caractères ?</p>","Le codage UTF-8 est sur 1 à 4 octets.","Le codage UTF-8 est sur 8 bits.","Le codage UTF-8 est sur 8 octets.","Le codage UTF-8 est sur 7 bits.","A","2","6",NULL,"1",NULL,"2019-11-24 10:32:45",NULL),
("197","<p>La fonction suivante doit calculer la moyenne d'un tableau de nombres, passé en paramètre. Avec quelles expressions faut-il compléter l'écriture pour que la fonction soit correcte ? :</p><br><pre><br>def moyenne(tableau) :<br>     total = ...<br>     for valeur in tableau :<br>          total = total + valeur<br>     return total / .....<br></pre>","0 et len(tableau)","0 et len(tableau) + 1","1 et len(tableau)","1 et len(tableau) + 1","A","8","34",NULL,"1",NULL,"2019-11-24 16:51:08",NULL),
("198","<p>Quelle valeur retourne la fonction 'mystere' suivante ?</p><br><pre><br>def mystere(liste):<br>     valeur_de_retour = True<br>     indice = 0<br>     while indice < len(liste) - 1 :<br>          if liste[indice] > liste[indice + 1]:<br>               valeur_de_retour = False<br>          indice = indice + 1<br>     return valeur_de_retour<br></pre><br>","Une valeur booléenne indiquant si la liste liste passée en paramètre est triée","La valeur du plus grand élément de la liste passée en paramètre","La valeur du plus petit élément de la liste passée en paramètre.","Une valeur booléenne indiquant si la liste passée en paramètre contient plusieurs fois le même élément","A","8","35",NULL,"1",NULL,"2019-11-24 16:58:33",NULL),
("199","<p>A quelle catégorie appartient l'algorithme des k plus proches voisins ?</p><br><br>","Algorithmes de classification et d'apprentissage","Algorithmes de recherche de chemins","Algorithmes de tri","Algorithmes gloutons","A","8","36",NULL,"1",NULL,"2019-11-24 17:24:06",NULL),
("200","<p>Combien d'échanges effectue la fonction Python suivante pour trier un tableau de 10 éléments au pire des cas ?</p><br><pre><br>def tri(tab) :<br>     for i in range (1, len(tab)) :<br>          for j in range (len(tab) - i) :<br>               if tab[j] > tab[j+1] :<br>                    tab[j], tab[j+1] = tab[j+1], tab[j]<br></pre>","45","100","10","55","A","8","35",NULL,"1",NULL,"2019-11-24 17:28:56",NULL),
("201","<p>Avec un algorithme de recherche par dichotomie, combien d'étapes sont nécessaires pour déterminer que 35 est présent dans le tableau <code>[1, 7, 12, 16, 18, 20, 24, 28, 35, 43, 69]</code> ?</p>
","2 étapes","1 étape","9 étapes","11 étapes","A","8","37",NULL,"1",NULL,"2020-05-12 15:38:18",NULL),
("202","<p>Pour pouvoir utiliser un algorithme de recherche par dichotomie dans une liste, quelle précondition doit être vraie ?</p><br>","La liste doit être triée","La liste ne doit pas comporter de doublons","La liste doit comporter uniquement des entiers positifs","La liste doit être de longueur inférieure à 1024","A","8","37",NULL,"1",NULL,"2019-11-24 17:34:57",NULL),
("203","<p>A désignant un entier, lequel des codes suivants ne termine pas ?</p>","<pre><code class='python'>i = A + 1
while i &lt; A :
     i = i - 1
</code></pre>","<pre><code class='python'>i = A + 1
while i &lt; A :
     i = i + 1
</code></pre>","<pre><code class='python'>i = A - 1
while i &lt; A :
     i = i - 1
</code></pre>","<pre><code class='python'>i = A - 1
while i &lt; A :
     i = i + 1
</code></pre>","C","8","39",NULL,"1",NULL,"2020-05-12 15:36:59",NULL),
("204","<p>On considère la fonction suivante :</p><br><pre><br>def f(t,i) :<br>     im = i<br>     m = t[i]<br>     for k in range(i+1, len(t)) :<br>          if t[k] < m :<br>               im, m = k, t[k]<br>     return im<br></pre><br>Que vaut l'expression f([7, 3, 1, 8, 19, 9, 3, 5], 0) ?","1","2","3","4","B","8","35",NULL,"1",NULL,"2019-11-24 18:04:56",NULL),
("205","<p>a et m étant des entiers strictement positifs,la fonction suivante calcule a <sup>m</sup> :</p><br><pre><br>def puissance (a,m) :<br>     p = 1<br>     n = 0<br>     while n < m :<br>          p = p * a<br>          #<br>          n = n + 1<br>     return p<br></pre><br>A la ligne marquée d'un #, on a, après le # :","p = a<sup>n</sup>","p = a<sup>n-1</sup>","p = a<sup>n+1</sup>","p = a<sup>m</sup>","C","7","28",NULL,"1",NULL,"2019-12-11 20:34:23",NULL),
("206","<p>Voici une fonction Python de recherche d'un maximum :</p><br><pre><br>def maxi(t) :<br>     m = -1<br>     for k in range(len(t)) :<br>          if t[k] > m :<br>               m = t[k]<br>     return m<br></pre><br>Avec quelle précondition sur la liste t, la postcondition 'm est un élément maximum de la liste t' n'est-elle pas assurée ?<br>","Tout élément de t est un entier positif ou nul","Tout élément de t est un entier supérieur ou égal à -2","Tout élément de t est un entier supérieur ou égal à -1","Tout élément de t est un entier strictement supérieur à -2","B","7","30",NULL,"1",NULL,"2019-12-11 20:35:22",NULL),
("207","<p>La fonction indice_maxi ci-dessous doit rechercher l'indice de la valeur maximale présente dans une liste de nombres et le renvoyer. Dans le cas où cette valeur maximale est présente plusieurs fois, c'est le plus petit de ces indices qui doit être renvoyé.</p><br><pre><br>def indice_maxi(liste) :<br>     valeur_max = liste[0]<br>     indice = 0<br>     for i in range(len(liste)) :<br>          if liste[i] > valeur_max :<br>               indice = i<br>     return indice<br></pre><br>Cette fonction a mal été programmée. On souhaite mettre en évidence son incorrection par un test bien choisi. Quelle valeur de test mettra l'erreur en évidence ?<br>","[1, 2, 3, 4]","[4, 3, 2, 1]","[1, 3, 3, 2]","[1, 1, 1, 1]","C","7","31",NULL,"1",NULL,"2019-11-24 18:24:11",NULL),
("208","<p>Dans le programme JavaScript suivant, quelle est la notation qui délimite le bloc d'instructions exécuté à chaque passage dans la boucle while ?</p><br><pre><br>i = 0<br>while (i < 10) {<br>     alert(i)<br>     i = i + 1<br>}<br>alert('Fin')<br></pre>","Le fait que les instructions soient encadrées entre { et }","Le fait que les instructions soient indentées de 4 caractères comme en Python.","Le fait que les instructions suivent le mot clé while","Le fait que les instructions suivent la parenthèse )","A","7","29",NULL,"1",NULL,"2019-11-25 15:49:31",NULL),
("209","<p>La fonction suivante calcule la racine carrée du double d'un nombre flottant.</p><br><pre><br>from math import sqrt<br><br>def racine_du_double(x) :<br>     return sqrt(2*x)<br></pre><br>Quelle est la précondition sur les arguments de cette fonction ?","x >= 0","2 * x > 0","x < 0","sqrt(x) >= 0","A","7","30",NULL,"1",NULL,"2019-11-24 18:35:57",NULL),
("210","<p>Avec la définition de fonction capital_double suivante, que peut-on toujours affirmer à propos du résultat n retourné par la fonction ?</p><br><pre><br>def capital_double (capital, interet) :<br>     montant = capital<br>     n = 0<br>     while montant <= 2 * capital :<br>          montant = montant + interet<br>          n = n + 1<br>     return n<br></pre><br>","capital + n * interet > 2 * capital","n = capital / interet","capital * n * interet > 2 * capital","n = 2 * capital / interet","A","7","31",NULL,"1",NULL,"2019-11-24 18:41:28",NULL),
("211","<p>On a écrit une fonction qui prend en paramètre une liste non vide et qui renvoie son plus grand élément. Combien de tests faudrait-il écrire pour garantir que la fonction donne un résultat correct pour toute liste ?</p>","Il faudrait écrire une infinité de tests : on ne peut pas prouver que cette fonction est correcte, simplement en la testant.","Deux tests : pour une liste à un élément et pour une liste à deux éléments ou plus.","Trois tests : pour une liste vide, pour une liste à un élément et pour une liste à deux éléments ou pus.","Deux tests : pour le cas où le plus grand élément est en début de liste, et pour le cas où le plus grand élément n'est pas en début de liste.","A","7","31",NULL,"1",NULL,"2019-11-24 18:48:39",NULL),
("212","<p>La documentation de la bibliothèque random de Python précise :</p><br><pre><br>random.randint(a,b)<br>     renvoie un entier aléatoire N tel que a <= N <= b.<br></pre><br>Quelle est l'expression Python permettant de simuler le tirage d'un dé à 6 faces après avoir exécuté import random ?<br>","random.randint(1,6)","random.randint(1,7)","random.randint(6)","random.randint(0,6)","A","7","32",NULL,"1",NULL,"2019-11-24 18:51:48",NULL),
("213","<p>La documentation de la bibliothèque <code>random</code> de python précise :</p><br><pre><code class='python'>random.randint(a, b)<br>    Renvoie un entier aléatoire N tel que a &lt;= N &lt;= b.<br></code></pre><br><br><p>Quelle expression permet de simuler le tirage d'un dé à 6 faces après avoir exécuté <code>import random</code>?</p>","random.randint (0,6)","random.randint (0,7)","random.randint (1,6)","random.randint (1,7)","C","7","32",NULL,"1",NULL,"2019-11-24 21:10:14",NULL),
("214","<p>Dans le quadrillage ci-dessous 14 points sont dessinés, dont 7 de la classe C1, avec des ronds noirs •,et 7 de la classe C2, avec des losanges ◇. On introduit un nouveau point A, dont on cherche la classe à l'aide d'un algorithme des k plus proches voisins pour la distance géométrique habituelle, en faisant varier la valeur de k parmi 1, 3 et 5. Quelle est la bonne réponse (sous la forme d'un triplet de classes pour le triplet (1,3,5) des valeurs de k) ?","C1, C2, C3","C2, C1, C2","C2, C2, C2","C2, C1, C1","D","8","36",NULL,"1","214_knn.png","2019-11-25 08:01:39",NULL),
("215","<p>Avec la définition de fonction f suivante en Python, quelle est la valeur retournée par l’appel<br>f (42 , 21) ?<br></p><br><pre><br>def f(x,y):<br>   if x > y:<br>      return y,x<br>   else:<br>      return x,y<br></pre>","(21,42)","(21,21)","(42,42)","(42,21)","A","3","13",NULL,"1",NULL,"2019-11-25 11:29:55",NULL),
("216","<p>Dans la déﬁnition suivante de la fonction somme en Python, quelle est l’instruction à ajouter pour<br>que la valeur retournée par l’appel somme([10 , 11 , 12 , 13 , 14]) soit 60 ?<br></p><br><pre><br>def somme (tab):<br>   s = 0<br>   for ind in range(len(tab)):<br>      ...<br>   return s<br></pre>","s = tab[ind]","s = s + tab[ind]","tab [ind] = tab[ind] + s","s = s + ind","B","3","10",NULL,"1",NULL,"2019-12-05 17:52:25",NULL),
("217","<p>Quel est le résultat de l’évaluation de l’expression Python suivante?</p><br><pre><br>[n * n for n in range(10)]<br></pre><br>","[0, 1, 4, 9, 16, 25, 36, 49, 64, 81, 100]","[0, 2, 4, 8, 16, 32, 64, 128, 256, 512]","[0, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024]","[0, 1, 4, 9, 16, 25, 36, 49, 64, 81]","D","3","11",NULL,"1",NULL,"2019-11-25 11:36:14",NULL),
("218","<p>Comment peut-on accéder à la valeur associée à une clé dans un dictionnaire?</p>","Il faut parcourir le dictionnaire avec une boucle à la recherche de la clé.","On peut y accéder directement à partir de la clé.","On ne peut pas accéder à une valeur contenue dans un dictionnaire à partir d’une clé.","Il faut d’abord déchiffrer la clé pour accéder à un dictionnaire.","B","3","9",NULL,"1",NULL,"2019-11-25 11:57:14",NULL),
("219","<p>Quelle est la valeur affichée à l’exécution du programme Python suivant?</p><br><pre><br>ports = {'http' : 80, 'imap' : 142, 'smtp' : 25}<br>ports['ftp'] = 21<br>print(ports ['ftp'])<br></pre>","Key not found","{'ftp' : 21}","3","21","D","3","13",NULL,"1",NULL,"2019-11-25 12:02:28",NULL),
("220","<p>Combien d'entiers positifs ou nuls peut-on représenter en machine sur 32 bits ? </p>","2<sup>32</sup> − 1","2<sup>32</sup>","2 x 32","32<sup>2</sup>","B","2","2",NULL,"1",NULL,"2019-11-25 15:15:50",NULL),
("221","<p>Dans l’algorithme ci-dessous, qui prend en entrée un entier naturel non nul et renvoie son écriture<br>binaire, remplacer les pointillés par l’opérateur qui convient.</p><br><pre><br>def cascade(n):<br>    chiffres = ''<br>    while n != 0:<br>        chiffres = str(n … 2) + chiffres<br>        n = n //2<br>    return chiffres<br></pre>","//","+","*","%","D","2","2",NULL,"1",NULL,"2019-11-25 15:30:07",NULL),
("222","<p>On définit un tableau t rempli de 0 en langage Python. Ce tableau est une liste de listes, toutes les<br>sous-listes ayant le même nombre d'éléments.</p><br><pre><br>t = [ [0, 0, …, 0],<br>      [0, 0, …, 0],<br>      …<br>      [0, 0, …, 0] ]<br></pre><br><p>On appelle h le nombre de listes contenus dans t et l le nombre d'éléments appartenant à ces<br>listes. Parmi les propositions suivantes, laquelle permet de calculer h et l ?</p>","h, l = len(t[0]), len(t)","h, l = len(t[0]), len(t[1])","h, l = len(t), len(t[0])","h, l = len(t[1]), len(t[0])","C","3","10",NULL,"1",NULL,"2019-11-25 15:36:11",NULL),
("223","<p>On dispose d’une liste de triplets :</p><br><pre><br>t = [ (1,12,250), (1,12,251), (2,12,250),(2,13,250), (2,11,250), (1,12,249) ]<br></pre><br><p>On trie cette liste par ordre croissant des valeurs du second éléments des triplets. En cas d’égalité, on<br>trie par ordre croissant du troisième champ. Si les champs 2 et 3 sont égaux, on trie par ordre<br>croissant du premier champ. Après ce tri, quel est le contenu de la liste t ?</p><br>","[ (1,12,249), (1,12,250), (1,12,251),(2,11,250), (2,12,250), (2,13,250) ]","[ (2,11,250), (1,12,249), (1,12,250),(2,12,250), (1,12,251), (2,13,250) ]<br>","[ (2,11,250), (1,12,249), (1,12,250),(1,12,251), (2,12,250), (2,13,250) ]","[ (1,12,249), (2,11,250), (1,12,250),(2,12,250), (2,13,250), (1,12,251) ]","B","3","12",NULL,"1",NULL,"2019-11-25 15:43:25",NULL),
("224","<p>Quelle est l’expression manquante dans le programme Python suivant, pour que son exécution affiche le numéro de Dupond ?<br><pre><br>repertoire = [{‘nom' : ‘Dupont' , ‘tel‘ : '5234'},<br>              {'nom' : ‘Tournesol‘ , 'tel' : ‘5248‘},<br>              {‘nom‘ : ‘Dupond' , ‘tel‘ : '3452'}]<br> <br>for i in range(len(repertoire)):<br>    if . . . :<br>        print (repertoire [i] ['tel ' ])<br></pre>","répertoire [i] ['nom'] == 'Dupond‘","répertoire ['nom'] == 'Dupond'","répertoire [i] == ‘Dupond'","nom == ‘Dupond'","A","4","14",NULL,"1",NULL,"2020-03-06 16:58:43",NULL),
("225","<p>Laquelle de ces listes de chaînes de caractères est triée en ordre croissant ?</p>","['Chat' , 'Cheval' , 'Chien' , 'Cochon']","['Cochon' , 'Chat' , 'Cheval' , 'Chien']","['Cheval' , 'Chien‘ , 'Chat' , 'Cochon']","['Chat' , 'Cochon' , 'Cheval' , 'Chien']","A","4","16",NULL,"1",NULL,"2020-03-06 17:02:31",NULL),
("226","<p>Laquelle de ces listes de chaînes de caractères est triée en ordre croissant ?</p><br>","['12','142','21','8']","['8','12','142','21']","['8','12','21','142']","['12','21','8','142']","A","4","16",NULL,"1",NULL,"2020-03-06 17:03:06",NULL),
("227","<p>Quelle est la valeur de la variable <strong>table</strong> après exécution du programme Python suivant :</p><br><pre><br>table = [12, 43, 6, 22, 37]<br>for i in range(len(table) - 1):<br>    if table [i] > table [i+1] :<br>        table [i] ,table [i+1] = table [i+1] ,table [i]<br></pre>","[12, 6, 22, 37, 43]","[6, 12, 22, 37, 43]","[43, 12, 22, 37, 6]","[43, 37, 22, 12, 6]","A","4","16",NULL,"1",NULL,"2020-01-14 18:08:21",NULL),
("228","<p>Quel est le rôle de l’unité arithmétique et logique dans un processeur ?<p>","Effectuer les calculs.","Réaliser les branchements.","Définir la base des opérations arithmétiques (binaire, octale, hexadécimale).","Gérer le contrôle interne du processeur.","A","6","23",NULL,"1",NULL,"2019-11-27 17:40:25",NULL),
("229","<p>Dans un réseau informatique, que peut-on dire de la transmission de données par paquets ?</p>","Cela assure une utilisation efficace des liens de communication.","Cela nécessite la réservation d’un chemin entre l’émetteur et le récepteur.","Cela empêche l’interception des données transmises.","Cela garantit que toutes les données empruntent le même chemin.","A","6","24",NULL,"1",NULL,"2019-11-27 17:42:09",NULL),
("230","<p>Quelle est l’utilité de la commande ping dans un réseau informatique ?</p>","Tester si une machine distante peut être jointe.","Obtenir la route suivie par un paquet dans le réseau.","Mesurer les performances d’une machine distante.","Établir un réseau privé virtuel.","A","6","24",NULL,"1",NULL,"2019-11-27 17:44:34",NULL),
("231","<p>Quel est l’effet de la commande shell suivante ?</p><br><pre>cd ..</pre>","Changer le répertoire courant vers le répertoire supérieur.","Éjecter le CD.","Copier le contenu du répertoire courant dans un répertoire caché.","Supprimer le répertoire courant.","A","6","25",NULL,"1",NULL,"2019-12-05 15:58:16",NULL),
("232","<p>Que peut-on dire du système de fichiers, suite à l’exécution des commandes suivantes ?</p><br><pre><br>% ls<br>entier.py flottant.py readme.md<br>% mkdir foo<br>% mv *.py foo<br></pre>","Le répertoire foo contient deux fichiers d’extension .py.","Les fichiers entier.py, flottant.py, et foo ont été déplacés dans le répertoire de l’utilisateur.","L’utilisateur foo est propriétaire des fichiers entier.py et flottant.py.","Le répertoire foo contient le résultat de l’exécution des deux fichiers entier.py et flottant.py.","A","6","25",NULL,"1",NULL,"2019-11-27 17:49:11",NULL),
("233","<p>Parmi les dispositifs d’entrée et de sortie suivants, lequel est uniquement un capteur ?</p>","Le thermomètre.","La diode.","L’écran tactile.","Le moteur pas à pas.","A","6","26",NULL,"1",NULL,"2019-11-27 17:51:06",NULL),
("234","<p>À partir de fichiers remplis à l’aide de formulaires en ligne, on dispose d’un tableau t1 de numéro de client, nom, âge, ville et d’un tableau t2 de numéro de commande, numéro de client, prix :</p><br><pre><br>t1 = [ [3,'Pierre',25,'Ambérieu'], [5,'Gaspe',45,'Lagnieu'], [98,'Abraham',75,'Meximieux'],[24,'Zoé',34,'Lyon'] ]<br>t2 = [ [1287,5,1025], [13245,98,234], [23,5,42],[10001,24,53] ]<br></pre><br><p>Que devient t1 après l'exécution du code suivant :</p><br><pre><br>for i in range(len(t1)): <br>    for c in [ com for com in t2 if com[1]==t1[i][0] ]:<br>        t1[i].append( (c[0], c[2]) )<br></pre>","[ [5, 'Gaspe', 45, 'Lagnieu', (1287, 1025), (23, 42)]<br>[98, 'Abraham', 75, 'Meximieux', (13245, 234)]<br>[24, 'Zoé', 34, 'Lyon', (10001, 53)] ]","[ [3, 'Pierre', 25, 'Ambérieu']<br>[5, 'Gaspe', 45, 'Lagnieu', (1287, 1025), (23, 42)]<br>[98, 'Abraham', 75, 'Meximieux', (13245, 234)]<br>[24, 'Zoé', 34, 'Lyon', (10001, 53)] ]","[ [3, 'Pierre', 25, 'Ambérieu', (1287, 1025)]<br>[5, 'Gaspe', 45, 'Lagnieu', (13245, 234)]<br>[98, 'Abraham', 75, 'Meximieux', (23, 42)]<br>[24, 'Zoé', 34, 'Lyon', (10001, 53)] ]","[ [3, 'Pierre', 25, 'Ambérieu']<br>[5, 'Gaspe', 45, 'Lagnieu']<br>[98, 'Abraham', 75, 'Meximieux']<br>[24, 'Zoé', 34, 'Lyon'] ]","B","4","17",NULL,"1",NULL,"2019-11-27 19:11:48",NULL),
("235","<p>On considère le programme suivant :</p><br><pre><br>def maxi(tab):<br>   '''<br>   tab est une liste de couples (nom, note)<br>   où nom est de type str<br>   et où note est un entier entre 0 et 20.<br>   '''<br>   m = tab[0]<br>   for x in tab:<br>      if x[1] >= m[1]:<br>         m = x<br>   return m<br>L = [('Adrien', 17), ('Barnabé', 17), ('Casimir', 17),<br>('Dorian', 17), ('Emilien', 16), ('Fabien', 16)]<br></pre><br><p>Quelle est la valeur de maxi(L) ?</p>","('Adrien',17)","('Dorian',17)","('Fabien',16)","('Emilien',16)","B","4","15",NULL,"1",NULL,"2019-11-27 19:27:10",NULL),
("236","<h4>L’interactivité dans une page Web : </h4><br><br><br>","ne peut se faire que grâce à Javascript.","n’est pas possible.","ne peut se faire que grâce au CSS.","peut se faire avec la balise <font face='courier new'>&ltform&gt</font>.","D","5","19",NULL,"1",NULL,"2019-12-01 19:58:08",NULL),
("237","<h4>Qu’est ce qu’une page Web ?</h4><br>","un réseau mondial d’ordinateurs connectés.","un des services d’Internet.    ","un document généralement codé en HTML.","un moteur de recherche.","C","5","19",NULL,"1",NULL,"2019-12-01 20:01:47",NULL),
("238","<h4>Parmi les éléments HTML 5 de formulaires suivants, un intrus s’est glissé, lequel ?</h4><br><br><br>","<font face = 'courier new '>&lt input &gt </font>.","<font face = 'courier new '>&lt radio &gt </font>.","<font face = 'courier new '>&lt textarea &gt </font>.","<font face = 'courier new '>&lt select &gt </font>.","B","5","19",NULL,"1",NULL,"2019-12-01 20:07:01",NULL),
("239","<p>Laquelle des instructions suivantes définit une liste contenant les valeurs 1,2,3,4 ? </p>","[1,2,3,4]","(1,2,3,4)","{1,2,3,4}","#1,2,3,4","A","3","10",NULL,"1",NULL,"2020-03-26 13:45:43",NULL),
("240","<p> Soit les déclarations suivantes :<P><br><pre><br>animaux=['Chat','Cochon','Chien','Canard','Vache']<br>effectif=[3,8,5,9,1]<br>groupe=[animaux,effectif]<br></pre><br><p>Quel sera le résultat de groupe[1][3] ?</p>","'Chien'","9","5","'Canard'","B","3","10",NULL,"1",NULL,"2019-12-02 15:51:47",NULL),
("241","<p>Quel est le nom de l’événement généré lorsque l’utilisateur clique sur un bouton de type button<br>dans une page HTML ?</p><br>","click","submit","mouse","action","A","5","19",NULL,"1",NULL,"2019-12-03 18:34:31",NULL),
("242","<p>Lors de la consultation d’une page HTML, contenant un bouton auquel est associée la fonction suivante, que se passe-t-il quand on clique sur ce bouton?</p><br><pre><br>function action(event) {<br><br>this.style.color = 'blue'<br><br>}<br></pre><br><br>","Le texte de la page passe en bleu.","Le texte du bouton passe en bleu.","Le texte du bouton est changé et afﬁche maintenant le mot “bleu”.","Le pointeur de la souris devient bleu quand il arrive sur le bouton.","B","5","19",NULL,"1",NULL,"2019-12-03 18:37:02",NULL),
("243","<p>Quelle est la machine qui va exécuter un programme JavaScript inclus dans une page HTML ?</p>","La machine de l’utilisateur ou du serveur, selon celle qui est la plus disponible.","Le serveur web sur lequel est stockée la page HTML.","La machine de l’utilisateur sur laquelle s’exécute le navigateur web.","La machine de l’utilisateur ou du serveur, suivant la conﬁdentialité des données manipulées.","C","5","20",NULL,"1",NULL,"2019-12-03 18:39:57",NULL),
("244","<p>Soit la définition suivante :</p><br><pre>t = ('foo', 'bar', 'baz')</pre><br><p>Laquelle des propositions suivantes permet de remplacer l'élément 'bar' par 'qux' ? </p>","t[1] = 'qux'","t(1) = 'qux'","t[1:1] = 'qux'","Ce n'est pas possible de faire ce remplacement !","D","3","8",NULL,"1",NULL,"2019-12-03 19:28:34",NULL),
("245","<p>Laquelle des propositions suivantes permet de créer un p-uplet nommé <strong>p</strong> contenant la chaîne 'foo' ? </p>","p = puplet('foo')","p = ('foo',)","p = ('foo')","On ne peut pas créer un tuple comportant 1 seul élément !","B","3","8",NULL,"1",NULL,"2019-12-03 19:41:36",NULL),
("246","<p>Soit la suite d'instructions suivantes :</p><br><pre><br>fruit = {}<br><br>def addone(index):<br>    if index in fruit:<br>        fruit[index] += 1<br>    else:<br>        fruit[index] = 1<br>        <br>addone('Apple')<br>addone('Banana')<br>addone('Orange')<br>addone('Orange')<br>addone('Orange')<br></pre><br><p>Que contient <strong>fruit</strong> suite à l'exécution de ce code ?</p>","{'Apple': 1, 'Banana': 1, 'Orange': 3}","{'Apple', 'Banana' , 'Orange', 'Orange', 'Orange'}","{0:'Apple', 1:'Banana', 2:'Orange'}","{1:'Apple', 1:'Banana', 3:'Orange'}","A","3","8",NULL,"1",NULL,"2019-12-03 20:14:18",NULL),
("247","<br><p>Parmi les affirmations suivantes, laquelle est vraie ?</p><br>","Deux algorithmes de coûts identiques effectuent exactement le même nombre d'opérations.","Le coût d'un algorithme permet d'évaluer un temps d'exécution.","Plus le coût d'un algorithme est élevé, plus précis est le résultat.","Une recherche dichotomique a un coût deux fois plus élevé que celui d'une recherche linéaire puisque 'dichotomie' signifie 'division en deux parties'","B","8","39",NULL,"1",NULL,"2019-12-03 20:16:16",NULL),
("248","<br><p>On considère le code qui suit où la valeur de n est un entier naturel :</p><br><pre><br>x = 1<br>while x < n :<br>     x = 2 * x<br></pre><br>On s'intéresse au coût de cet algorithme en fonction de n. Parmi les affirmations suivantes, laquelle est vraie ?<br>","Le coût est semblable à celui d'une recherche dichotomique.","Le coût est linéaire en fonction de n.","Le coût est quadratique en fonction de n.","Il est impossible de connaître le coût.","A","8","39",NULL,"1",NULL,"2019-12-03 20:27:38",NULL),
("249","<p>Dans la fonction suivante, les valeurs des variables x et y sont des entiers naturels :</p><br><pre><br>def f(x, y) :<br>    s = x<br>    t = y<br>    while t > 0 :<br>        s = s + 1<br>        t = t - 1<br>    return s<br></pre><br>Quelle affirmation est fausse ?<br>","La propriété 's + t = x + y' est un invariant de boucle.","La valeur finale de t est 1","La propriété 't est supérieur ou égal à 0' est un invariant de la boucle while.","Le résultat renvoyé est égal à la somme a + b.","B","8","39",NULL,"1",NULL,"2019-12-11 20:37:02",NULL),
("250","<p>On écrit un programme pour chercher le mot 'an' dans une chaîne de caractères (cette chaîne contient au moins deux caractères). Pour cela on parcourt la chaîne et si on trouve un 'a', on vérifie si le caractère suivant est un 'n'. Si oui, le programme s'arrête. Il faut faire attention au dépassement d'indice qui provoquerait une erreur. Quelle affirmation est vraie ?</p><br>","La recherche de 'a' a un coût linéaire et le coût de l'algorithme est deux fois plus élevé donc il n'est pas linéaire.","Dans le pire des cas, le nombre de comparaisons est exactement 2n.","Dans le pire des cas, le nombre de comparaisons est exactement 2n-1.","Dans le pire des cas, le nombre de comparaisons est exactement 2n-2.","D","8","39",NULL,"1",NULL,"2019-12-08 20:59:28",NULL),
("251","<p>Un algorithme de recherche dichotomique dans une liste triée de taille n nécessite exactement k comparaisons dans le pire des cas. Combien de comparaisons sont nécessaires avec le même algorithme pour une liste de taille 2n ?</p><br>","2k + 1 comparaisons.","2k comparaisons.","k + 2 comparaisons.","k + 1 comparaisons.","D","8","37",NULL,"1",NULL,"2019-12-03 20:53:28",NULL),
("252","<p>Un algorithme cherche la valeur maximale d'une liste non triée de taille n. Combien de temps mettra cet algorithme sur une liste de taille 2n ?</p><br>","Le même temps que sur la liste de taille n si le maximum est dans la première moitié de la liste.","On a ajouté n valeurs, l'algorithme mettra donc n fois plus de temps que sur la liste de taille n.","Le temps sera simplement doublé par rapport au temps mis sur la liste de taille n.","On ne peut pas savoir, tout dépend de l'endroit où est le maximum.","C","8","35",NULL,"1",NULL,"2019-12-03 21:02:12",NULL),
("253","<p>Quel est le coût en temps dans le pire des cas du tri par insertion ?</p><br>","O(n)","O(n<sup>2</sup>)","O(2<sup>n</sup>)","O(log(n))","B","8","35",NULL,"1",NULL,"2019-12-03 21:10:55",NULL),
("254","<br><p>Soit les points de coordonnées suivantes :</p><br><pre><br>A(1, 6)<br>B(2, 6)<br>C(3, 1)<br>D(4, 2)<br>E(6, 0)<br>F(7, 5)<br>G(7, 3)<br>H(10, 3)<br></pre><br>En utilisant la distance euclidienne, quels sont les deux plus proches voisins du point P(5,5) ?<br>","les points A et B.","les points D et E.","les points F et G.","les points B et F.","C","8","36",NULL,"1",NULL,"2019-12-03 21:22:21",NULL),
("255","<br><p>Un système monétaire contient les pièces suivantes : 20, 15, 7, 4 et 2 unités. Le nombre de pièces de chaque sorte n'est pas limité. On souhaite rendre 38 unités. Quelle est la solution donnée par l'algorithme glouton de rendu de monnaie ?</p><br>","[15, 15, 4, 4]","[20, 7, 7, 4]","[7, 7, 7, 7, 4, 4, 2]","L'algorithme échouera à rendre la somme de 38 unités.","D","8","38",NULL,"1",NULL,"2019-12-03 21:33:49",NULL),
("256","<p>Voici un arbre, on le parcourt en partant du haut (la racine) et en descendant de branche en branche (les noeuds) jusqu'à arriver à une feuille.<br>Par exemple on peut faire le parcourt Racine 4 puis noeud 5 puis noeud 4 puis feuille 6.<br>Considérons un algorithme Glouton de parcours (racine vers feuille) de cet arbre, Sélectionnant le noeud le plus grand à chaque étape.</p><br><p>Quel chemin cet algorithme Glouton va-t-il parcourir ?</p>","4-->7-->3","4-->5-->8","9-->10-->8).","4-->5-->4-->9","A","8","38",NULL,"1","256_glouton1.png","2019-12-04 08:18:17",NULL),
("257","<p>On considère le problème où l’on doit rendre 8 euros de monnaie.</p><br><p>On dispose de pièces de 1,4,6 euros.</p><br><p>Indiquer le rendu de monnaie donné par un algorithme glouton.</p>","4;4","6;1;1","4;1;1;1;1","1;1;1;1;1;1;1;1","B","8","38",NULL,"1",NULL,"2019-12-04 08:24:03",NULL),
("258","<p>Quelle est la valeur de la variable <strong>sum</strong> à la fin de l’exécution de ce bloc d'instructions ?</p><br><pre><br>arr = {}<br>arr[1] = 1<br>arr['1'] = 2<br>arr[1] += 1<br><br>sum = 0<br>for k in arr:<br>    sum += arr[k]<br></pre>","1","2","3","4","D","3","9",NULL,"1",NULL,"2019-12-04 08:43:56",NULL),
("259","<p>Soit la fonction <strong>foo</strong> suivante :</p><br><pre>def foo(a):return (a[1],a[-1])</pre><br><p>Que renvoie ? </p><br><pre>foo(['chat','chien','cheval'])</pre> ","('chat','cheval')","('chat','chien')","('chien','cheval')","Error : index -1 out of range","C","3","10",NULL,"1",NULL,"2019-12-04 11:10:57",NULL),
("260","<p>Parmi les balises HTML ci-dessous quelle est celle qui permet à l’utilisateur de saisir son nom dans un formulaire en respectant la norme HTML ?<br></p><br>","&lt;input type='name'/&gt;","&lt;select/&gt;","&lt;input type='text'/&gt;","&lt;form/&gt;","C","5","21",NULL,"1",NULL,"2019-12-04 12:00:35",NULL),
("261","<p>Parmi les propriétés suivantes d’une balise &lt;button&gt; dans une page HTML, laquelle doit être rédigée en langage JavaScript ?</p><br>","La propriété onclick","La propriété name","La propriété type","La propriété id","A","5","19",NULL,"1",NULL,"2019-12-04 12:01:54",NULL),
("262","<br><p>Quelle méthode d’envoi des paramètres est-il préférable d’utiliser, pour un formulaire d’une page web, destiné à demander à l’utilisateur un mot de passe pour se connecter (le protocole utilisé est HTTPS) ?</p><br>","La méthode POST.","La méthode GET.","La méthode PASSWORD.","La méthode CRYPT.","A","5","21",NULL,"1",NULL,"2019-12-04 12:03:51",NULL),
("263","<br><p>Après avoir saisi dans son navigateur l’url de son forum favori, Clothilde reçoit comme réponse du serveur une erreur «404». Une seule des réponses suivantes ne correspond pas à cette situation, laquelle ?  </p>","Une panne de sa connexion internet","Une mise à jour du serveur qu’elle consulte","Une erreur de saisie de sa part","Un changement de titre du forum qu’elle consulte","A","5","22",NULL,"1",NULL,"2019-12-04 21:24:25",NULL),
("264","<p>Saisir l’url http://monsite.com/monprogramme.py?id=25 dans la barre d’adresse du navigateur ne permet à coup sûr pas :</p><br><br>","de télécharger un programme Python ","d'exécuter un programme Python sur le serveur","d'exécuter un programme Python sur le client","d'afficher une page html ","C","5","20",NULL,"1",NULL,"2019-12-04 21:26:31",NULL),
("265","<br><p>Un élément form d’une page html contient un élément button de type submit. Un clic sur ce bouton : </p><br>","efface les données entrées par l’utilisateur dans le formulaire ","envoie les données du formulaire vers la page définie par l’attribut method de l’élément form ","envoie les données du formulaire vers la page définie par l’attribut action de l’élément form ","ne fait rien si un script javascript n’est pas associé au bouton","C","5","21",NULL,"1",NULL,"2019-12-04 21:28:07",NULL),
("266","<p>On a importé un fichier <code>csv</code> contenant les notes d’élèves dans une liste de listes nommée <code>liste_eleves</code>.</p><br><p>Le premier élément de chaque ligne est le nom de l’élève, le deuxième est sa note au premier devoir et le troisième sa note au deuxième devoir.</p><br><p>Quel code python permet d’obtenir la liste des noms des élèves ayant eu strictement plus de 15 au deuxième devoir ?</p><br>","<code>[eleve for eleve in liste_eleves if eleve[2] > 15]</code>","<code>[eleve[0] for eleve in liste_eleves if eleve[2] > 15]</code>","<code>[eleve for eleve in liste_eleves if eleve[3] > 15]</code>","<code>[eleve[0] in liste_eleves if eleve[2] > 15]</code>","B","3","11",NULL,"1",NULL,"2019-12-05 16:42:37",NULL),
("267","<p>On souhaite écrire une fonction recherche_dichotomique(t, v), qui renvoie une position v dans le tableau t, supposé trié, et None si v ne s'y trouve pas : parmi les 4 fonctions ci-dessous, laquelle est correcte ?</p><br>","<pre><br>def recherche_dichotomique (t, v) :<br>     g = 0<br>     d = len(t) - 1<br>     while g <= d :<br>          m = (g + d) // 2<br>          if t[m] < v :<br>               g = m + 1<br>          elif t[m] > v :<br>               d = m - 1<br>          else :<br>               return m<br>     return None<br></pre><br>     ","<pre><br>def recherche_dichotomique (t, v) :<br>     g = 0<br>     d = len(t) - 1<br>     while g <= d :<br>          m = (g + d) // 2<br>          if t[m] > v :<br>               g = m + 1<br>          elif t[m] < v :<br>               d = m - 1<br>          else :<br>               return m<br>     return None<br></pre>","<pre><br>def recherche_dichotomique (t, v) :<br>     g = 0<br>     d = len(t)<br>     while g <= d :<br>          m = (g + d) // 2<br>          if t[m] < v :<br>               g = m + 1<br>          elif t[m] > v :<br>               d = m - 1<br>          else :<br>               return m<br>     return None<br></pre>","<pre><br>def recherche_dichotomique (t, v) :<br>     g = 0<br>     d = len(t) - 1<br>     while g < d :<br>          m = (g + d) // 2<br>          if t[m] < v :<br>               g = m + 1<br>          elif t[m] > v :<br>               d = m - 1<br>          else :<br>               return m<br>     return None<br></pre>","A","8","37",NULL,"1",NULL,"2019-12-08 19:23:21",NULL),
("268","<h4>Quel est le résultat de ce code en JavaScript ? </h4><br><br><br><pre><br>var number1 = '3', number2 = '7', reponse;<br>reponse = number1 + number2;<br>alert(reponse);<br></pre><br><br>","-1","10","37","Type Error","C","5","19",NULL,"1",NULL,"2019-12-08 17:01:51",NULL),
("269","<h4>Parmi les langages suivants, lequel n’est pas un langage de programmation ? </h4><br>","le langage C++","le langage JavaScript","le langage PHP","le langage HTML","D","7","29",NULL,"1",NULL,"2019-12-08 17:05:34",NULL),
("270","<p>On souhaite écrire une fonction tri_selection(t), qui trie le tableau t dans l'ordre croissant : parmi les 4 programmes suivants, lequel est correct ?</p>","<pre><br>def tri_selection(t) :<br>     for i in range (len(t)-1) :<br>          min = i        <br>          for j in range(i+1,len(t)):<br>               if t[j] < t[min]:<br>                    min = j<br>          tmp = t[i]<br>          t[i] = t[min]<br>          t[min] = tmp<br></pre>","<pre><br>def tri_selection(t) :<br>     for i in range (len(t)-1) :<br>          min = i        <br>          for j in range(i+1,len(t)-1):<br>               if t[j] < t[min]:<br>                    min = j<br>          tmp = t[i]<br>          t[i] = t[min]<br>          t[min] = tmp<br></pre>","<pre><br>def tri_selection(t) :<br>     for i in range (len(t)-1) :<br>          min = i        <br>          for j in range(i+1,len(t)):<br>               if t[j] < min:<br>                    min = j<br>          tmp = t[i]<br>          t[i] = t[min]<br>          t[min] = tmp<br></pre>","<pre><br>def tri_selection(t) :<br>     for i in range (len(t)-1) :<br>          min = i        <br>          for j in range(i+1,len(t)):<br>               if t[j] < t[min]:<br>                    min = j<br>          tmp = t[i]<br>          t[min] = t[i]<br>          t[i] = tmp<br></pre>","A","8","35",NULL,"1",NULL,"2019-12-08 19:24:58",NULL),
("271","<p>Rendu de monnaie : </p><br><p> euros = [0, 1, 2, 5, 10, 20, 50, 100] </p><br><p>On souhaite écrire un programme qui affiche la monnaie que le commerçant devra rendre. Parmi les 4 programmes suivants, lequel est correct ?</p><br>","<pre><br>def monnaie(s) :            <br>     i = len(euros) - 1                <br>     p = 0                             <br>     while s > 0 :<br>          if s >= euros[i] :            <br>               p +=1       <br>               s -= euros[i]         <br>          else :<br>               i = i - 1<br>     return p<br></pre>","<pre><br>def monnaie(s) :            <br>     i = len(euros)                <br>     p = 0                             <br>     while s > 0 :<br>          if s >= euros[i] :            <br>               p +=1       <br>               s -= euros[i]         <br>          else :<br>               i = i - 1<br>     return p<br></pre>","<pre><br>def monnaie(s) :            <br>     i = len(euros) - 1                <br>     p = 0                             <br>     while s >= 0 :<br>          if s >= euros[i] :            <br>               p +=1       <br>               s -= euros[i]         <br>          else :<br>               i = i - 1<br>     return p<br></pre>","<pre><br>def monnaie(s) :            <br>     i = len(euros) - 1                <br>     p = 0                             <br>     while s > 0 :<br>          if s > euros[i] :            <br>               p +=1       <br>               s -= euros[i]         <br>          else :<br>               i = i - 1<br>     return p<br></pre>","A","8","38",NULL,"1",NULL,"2019-12-11 20:39:01",NULL),
("272","<p>Soit la liste <strong>lst</strong> suivante :</p><br><pre>lst = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]</pre><br><p>Que va renvoyer le code suivant :</p><br><pre>[a[0] for a in lst if a[1] % 2 == 0]</pre>","[123, 456, 789]","[2, 8]","[1, 7]","[1, 4, 7]","C","3","11",NULL,"1",NULL,"2019-12-08 21:01:47",NULL),
("273","<p>Soit la liste <strong>lst</strong> suivante :</p><br><pre>lst = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]</pre><br><p>Que va renvoyer le code suivant :</p><br><pre>[a[1] + 10 for a in lst]</pre>","[[11, 12, 13], [14, 15, 16], [17, 18, 19]]","[[1, 2, 10, 3], [4, 5, 10, 6], [7, 8, 10, 9]]","[12, 15, 18]","[11, 14, 17]","C","3","11",NULL,"1",NULL,"2019-12-08 21:02:15",NULL),
("274","<p>Soit la liste <strong>lst</strong> suivante :</p><br><pre>lst = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]</pre><br><p>Que va renvoyer le code suivant :</p><br><pre>[lst[i][i] for i in [0, 1, 2]]</pre>","[1, 5, 9]","[[1], [5], [9]]","[2, 5, 8]","[[2], [5], [8]]","A","3","11",NULL,"1",NULL,"2019-12-08 21:11:33",NULL),
("275","<p>On considère le programme suivant :</p><br><pre><br>liste = [1, 4, 5, 2]<br>liste[2] = liste[2] + liste[0] + liste[1]<br>liste[0] = liste[1] + liste[2]<br></pre><br><p>Que vaut liste[0]?</p>","9","10","11","14","D","3","10",NULL,"1",NULL,"2019-12-09 19:23:15",NULL),
("276","<p>Quelle est la valeur de :</p><br><pre> [x+1 for x in range(10) if x%3 != 0] ?</pre>","[2, 3, 5, 6, 8, 9]","[2, 3, 5, 6, 8, 9, 11]","[1, 4, 7, 10]","[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]","A","3","11",NULL,"1",NULL,"2019-12-09 19:46:56",NULL),
("277","<p>On considère un n-uplet <code>t = (2, 5, 9)</code>. Quelle commande permet de remplacer la valeur 5 par 10 ?</p>","t[1] = 10","t(1) = 10","t[2] = 10","Aucune des commandes ci-dessus.","D","3","8",NULL,"1",NULL,"2019-12-09 19:51:20",NULL),
("278","<p>On considère la fonction suivante qui prend une liste de nombre et détermine si elle est ordonnée.</p><br><pre><br>def dans_l_ordre(liste):<br>    for i in range(len(liste)):<br>        if liste[i] > liste[i+1]:<br>            return False<br>    return True<br></pre><br><p>Laquelle de ces instructions provoque une erreur ?</p>","<code>dans_l_ordre([5, 4, 3, 2, 1])</code>","<code>dans_l_ordre([])</code>","<code>dans_l_ordre([1, 2, 3, 5, 4])</code>","<code>dans_l_ordre([1, 2, 3, 4, 5])</code>","D","3","10",NULL,"1",NULL,"2019-12-09 19:59:43",NULL),
("279","<p>On considère la fonction suivante, qui prend en entrée deux listes de même longueur.</p><br><pre><br><strong>def</strong> inconnu(liste1, liste2):<br>    nouvelle_liste = []<br>    <strong>for</strong> i in range(len(liste1)):<br>        if liste2[i] == 1:<br>            nouvelle_liste.append(liste1[i])<br>    <strong>return</strong> nouvelle_liste<br></pre><br><p>Lequel des appels suivants donne le résultat [3, 7]?</p>","<code>inconnu([3, 5, 1, 7, 2],[3, 5, 1, 7, 2])</code>","<code>inconnu([1, 0, 0, 1, 0],[3, 5, 1, 7, 2])</code>","<code>inconnu([3, 5, 1, 7, 2],[1, 0, 0, 1, 0])</code>","<code>inconnu([3, 5, 1, 7, 2],[1, 0, 7, 0, 0])</code>","C","3","10",NULL,"1",NULL,"2019-12-09 20:22:49",NULL),
("281","<p>On considère le programme ci-dessous. Quel est le résultat de mystere([2,1,1,4,1,5,2,4], [3,2,1,4,5]) ?</p><br><pre><br>def mystere(liste1, liste2):<br>    res = 0<br>    for e1 in liste1:<br>        for e2 in liste2:<br>            if e1 == e2:<br>                res += 1<br>    return res<br></pre>","0","4","8","40","C","8","34",NULL,"1",NULL,"2019-12-09 20:54:26",NULL),
("282","<p>Soit la fonction ci-dessous : </p><br><pre><br>def foo(lst1,lst2):<br>    lst=[(random.choice(lst1),random.choice(lst2))]<br>    n = 1<br>    while n <= 2:<br>        a = random.choice(lst1)<br>        b = random.choice(lst2)<br>        if (a,b) not in lst:<br>            lst.append((a,b))<br>            n = n + 1<br>    return lst<br></pre><br><p>On rappelle que random.choice(liste) renvoie un élément choisi aléatoirement dans liste.</p><br><p>Parmi les propositions suivantes laquelle est valide ?</p>","foo([1,2,3],['A','B','C'])  peut valoir [(3, 'C'), (3, 'A'), (1, 'A')]","foo([1,2,3],['A','B','C'])  peut valoir [(1, 'C'), (3, 'A'), (1, 'C')]","foo([1,2,3],['A','B','C'])  peut valoir [(3, 'C'), (3, 'A')]","foo([1,2,3],['A','B','C'])  peut valoir [(1, 'C'), (3, 'C')]","A","8","34",NULL,"1",NULL,"2019-12-09 23:18:01",NULL),
("283","<p>Soit le code suivant :</p><br><pre><br>prenom = ['Jean', 'Leon', 'Paul', 'Milo']<br>p = prenom[-1][-1]<br></pre><br><p>Quelle valeur est associée à la variable p ?</p><br>","J","n","M","o","D","3","10",NULL,"1",NULL,"2019-12-11 07:44:05",NULL),
("284","Comment s’appelle la mémoire qui permet l’opération de lecture et d’écriture simultanées ?<br>","ROM<br>","EPROM<br><br>","RAM<br>","EEPROM","C","6","27",NULL,"1",NULL,"2019-12-13 01:39:56",NULL),
("285","Que-ce qui n’est pas considéré comme un périphérique de l’ordinateur ?<br><br><br>","Disque dur<br>","Clavier<br>","CPU<br>","Moniteur","C","6","26",NULL,"1",NULL,"2019-12-13 01:41:31",NULL),
("286","La taille du registre accumulateur peut être de l’ordre de quelques ....<br>","bits<br>","ko<br>","Mo<br>","octets","D","6","23",NULL,"1",NULL,"2019-12-13 01:44:11",NULL),
("287","Parmi les 4 composants informatiques ci-dessous, lequel est le plus rapide ?<br>","RAM","disque dur","mémoire cache","registre","D","6","23",NULL,"1",NULL,"2019-12-13 01:45:28",NULL),
("288","Un programme source est généralement écrit en … <br>","langage binaire au niveau machine","langage de haut niveau","langage naturel","langage assembleur","B","6","27",NULL,"1",NULL,"2019-12-13 01:47:54",NULL),
("289","Pour stocker les résultats intermédiaires, l’ALU utilise ...<br><br>","l'accumulateur","le tas","la pile","les registres","A","6","23",NULL,"1",NULL,"2019-12-13 01:51:17",NULL),
("290","Le processeur est composé ...<br>","de l'unité de contrôle et de la mémoire principale","d'une mémoire principale et de l'unité arithmétique et logique","de l'unité de contrôle et de l'unité arithmétique et logique","d'un accumulateur et d'entrées/sorties","C","6","23",NULL,"1",NULL,"2019-12-13 02:07:59",NULL),
("291","Le composant du CPU chargé de comparer le contenu de deux données est ...<br><br><br><br>","l'unité arithmétique et logique","l'unité de contrôle","la mémoire","les registres","A","6","23",NULL,"1",NULL,"2019-12-13 02:11:24",NULL),
("292","L’unité qui permet de séquencer, décoder, traduire chaque instruction et générer les signaux d’activation nécessaires pour l'ALU et d’autres unités s'appelle ...<br>","l'unité arithmétique<br><br>","le CPU","l'unité logique","l'unité de contrôle","D","6","23",NULL,"1",NULL,"2019-12-13 02:23:47",NULL),
("293","Comment s'appelle la mémoire tampon (buffer) à grande vitesse insérée entre le processeur et la mémoire principale ?","les registres","la mémoire cache","la RAM","la ROM","B","6","23",NULL,"1",NULL,"2019-12-13 02:56:17",NULL),
("294","Aux origines de l’informatique, l'interface utilisateur des systèmes d’exploitation était composée :<br>","d’écrans, de claviers et de souris<br>","de cartes perforées<br>","d’interrupteurs et de lampes","de cables électriques","C","6","25",NULL,"1",NULL,"2019-12-13 03:29:29",NULL),
("295","<p>On a saisi le code suivant :</p><br><pre><br>class Account:<br>    def __init__(self, id):<br>        self.id = id<br>        id = 666 <br><br>acc = Account(123)<br>num = acc.id<br></pre><br>Que contient la variable num à la fin de l’exécution de ce script ?<br>","123","666","None","SyntaxError","A","12","42",NULL,"1",NULL,"2019-12-13 13:55:56",NULL),
("296","<p>On a saisi le code suivant :</p><br><pre><br>class account:<br>    def __init__(self, id, balance):<br>        self.id = id<br>        self.balance = balance<br>    def deposit(self, amount):<br>        self.balance += amount<br>    def withdraw(self, amount):<br>        self.balance -= amount<br><br>myac = account('123', 100)<br>myac.deposit(800)<br>myac.withdraw(500)<br>myac.deposit(200)<br></pre><br>Que vaut myac.balance à la fin de l’exécution de ce script ?<br>","600","100","800","200","A","12","42",NULL,"1",NULL,"2019-12-13 14:02:35",NULL),
("297","<h4>Voici un dictionnaire de langues :<br></h4><br><br><br><p>dico={'anglais':'english','allemand':'deutsch','breton':'brezhoneg'}</p><br><pre><br>Quelle est la valeur de dico[1] ?<br></pre>","rien car l'expression est invalide","english","allemand","deutch","A","3","9",NULL,"1",NULL,"2019-12-18 12:43:33",NULL),
("298","<h4>Voici un dictionnaire de langues :</h4><br><br><br><pre>dico={'anglais':'english','allemand':'deutsch','breton':'brezhoneg'}</pre><br><p>on souhaite ajouter une langue en plus et obtenir le dictionnaire suivant :</p><br><pre>dico={'anglais':'english','allemand':'deutsch','breton':'brezhoneg','espagnol':'español'}</pre><br>Quelle instruction permet d'ajouter le dernier élément 'espagnol':'español' dans le dictionnaire ?<br><br><br>","Ce n'est pas possible car un dictionnaire n'est pas modifiable","dico.append('espagnol':'español')","dico['espagnol']='español'","dico+=['espagnol':'español']","C","3","9",NULL,"1",NULL,"2019-12-18 12:43:06",NULL),
("299","<h4>Voici un dictionnaire de langues :</h4><br><br><br><p>dico={'anglais':'english','allemand':'deutsch','breton':'brezhoneg'}</p><br><pre><br>for elem in dico.items():<br>    print(elem, end=' ')<br></pre><br>Quel est l'affichage lors de l'exécution du code précédent ?<br>","anglais allemand breton","english deutsch brezhoneg","('anglais', 'english') ('allemand', 'deutsch') ('breton', 'brezhoneg') ","une erreur","C","3","9",NULL,"1",NULL,"2019-12-18 12:42:34",NULL),
("300","<p>Quelle valeur est renvoyée par l'appel <code>mystere(3, 5)</code> ? </p><br><pre><br>def mystere(a, b):<br>    if a < b:<br>        return a*b<br>    else:<br>        return 2*a<br></pre>","15","a*b","5","6","A","7","28",NULL,"1",NULL,"2019-12-20 18:04:06",NULL),
("301","<p>On considère le tableau <code>t</code> suivant.</p><br><pre> t = [('Béatrice', 15), ('Jean', 17), ('Marie', 16)]</pre><br><p> Que vaut l'expression <code>t[2][1]</code> ?</p>","'Jean'","('Jean', 17)","'Marie'","16","D","3","10",NULL,"1",NULL,"2019-12-20 18:10:24",NULL),
("302","<p>Quelle est la valeur de <code>[x+2 for x in range(10) if x%2 !=0]</code> ?</p>","[2, 3, 4, 5, 6, 7 ,8 , 9, 10, 11, 12]","[2, 4, 6, 8, 10]","[3, 5, 7, 9, 11]","[1, 3, 5, 7, 9]","C","3","11",NULL,"1",NULL,"2019-12-20 18:15:49",NULL),
("303","<p>On considère la fonction suivante.</p><br><pre><br>def mystere(tab):<br>    booleen = True<br>    for i in range(len(tab)-1):<br>        if tab[i] > tab[i+1]:<br>            booleen = False<br>    return booleen<br></pre><br><p>Que renvoie l'appel <code>mystere([1, 2, 7, 3, 10])</code> ?</p><br>","False","True","[1, 2, 3, 7, 10]","On ne peut pas savoir","A","8","34",NULL,"1",NULL,"2019-12-20 18:22:20",NULL),
("304","<p>Qu'affiche le programme suivant ?</p><br><pre><br>rep = {'Jean': 123, 'Jenifer': 124, 'Samuel': 125, 'Lisa': 126}<br>rep['Clara'] = 127<br>for cle in rep.keys():<br>    print(cle)<br></pre>","'Clara'","'Jean', 'Jenifer', 'Samuel', 'Lisa' et 'Clara'","127","123, 124, 125, 126 et 127","B","3","9",NULL,"1",NULL,"2020-01-13 19:28:28",NULL),
("305","<p>Quel est le tableau <code>t</code> construit par les instructions suivantes ?</p><br><pre><br>t = [0]*10<br>for i in range(10):<br>    t[i] = i<br></pre>","<pre>[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]</pre>","<pre>[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]</pre>","<pre>[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]</pre>","<pre>[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]</pre>","D","3","10",NULL,"1",NULL,"2020-01-13 19:27:00",NULL),
("306","<p>Quel est le tableau construit par l'instruction suivante ?</p><br><pre>t = [i%2 for i in range(20)]</pre>","<pre>[0, 1, 2, 3, ..., 20]</pre>","<pre>[0, 2, 4, 6, 8, ..., 18]</pre>","<pre>[0, 1, 0, 1, 0, 1, ..., 1]</pre>","<pre>[1, 0, 1, 0, 1, 0, ..., 0]</pre>","C","3","11",NULL,"1",NULL,"2020-01-13 19:30:28",NULL),
("307","<p>On considère la fonction suivante.</p><br><pre><br>def mystere(v, T):<br>    ''' T est un tableau d'entiers et v est un entier'''<br>    n = 0<br>    for e in T:<br>        if e == v:<br>            n = n + 1<br>    return n<br></pre><br><p>Que renvoie l'expression <code>mystere(1, [1, 2, 1, 3, 4, 6, 7, 2, 1, 2])</code> ?</p> <br>","3","2","10","1","A","8","34",NULL,"1",NULL,"2019-12-20 18:42:57",NULL),
("308","<p>On considère la fonction suivante.</p><br><pre><br>def hamming(t1, t2):<br>    ''' t1 et t2 sont deux tableaux de même taille, constitués d'entiers'''<br>    d = 0<br>    for i in range(len(t1)):<br>        if t1[i] != t2[i]:<br>            d = d + 1<br>    return d<br></pre><br><p> Que renvoie l'appel <code>hamming([1, 2, 3, 4, 5], [1, 3, 2, 4, 5])</code> ?</p>","True","False","3","2","D","8","34",NULL,"1",NULL,"2019-12-20 18:55:50",NULL),
("309","<h4>Qu’affiche le programme suivant ?</h4><br><br><br><p></p><br><pre><br>tuple=(1,5,7,9,10,15)<br>print('tuple[5] = ',tuple[5])<br></pre><br><br>","<p>tuple[5] = 5</p>","<p>tuple[5] = ‘5’</p>","<p>tuple[5] = 15</p>","<p>tuple[5] = 10</p>","C","3","8",NULL,"1",NULL,"2020-01-14 18:07:31",NULL),
("310","<h4>Qu’affiche le programme suivant?</h4><br><br><br><p></p><br><pre><br>tuple=(1,5,7,9,10,15)<br>print('tuple=',Tuple)<br></pre><br><br>","<p>(1,5,7,9,10,15)</p>","<p> NameError:name ‘Tuple’ is not defined </p>","<p> Tuple </p>","<p> 5 </p>","B","3","8",NULL,"1",NULL,"2019-12-26 08:45:54",NULL),
("311","<h4>Qu’affiche le programme suivant ?</h4><br><br><br><p></p><br><pre><br>tableau=[[0 for i in range(0,10)] for j in range(0,10)]<br>print(tableau)<br></pre><br><br>","<p> tableau </p>","<p> [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]] </p>","<p> [0, 0, 0, 0, 0, 0, 0, 0, 0, 0] </p>","<p> Aucune de ces propositions n’est correcte </p>","B","3","11",NULL,"1",NULL,"2019-12-26 08:49:08",NULL),
("312","<h4>Qu’affiche le programme suivant ?</h4><br><br><br><p></p><br><pre><br>tableau=[[0 for i in range(0,10)] for j in range(0,10)]<br>for i in range(0,10):<br>    for j in range(i,10):<br>        tableau[i][j]=i*j<br>print(tableau[3][1])<br></pre><br><br>","<p>6</p>","<p>3</p>","<p>0</p>","<p>1</p>","C","3","11",NULL,"1",NULL,"2019-12-26 08:51:38",NULL),
("313","<h4>Qu’affiche le programme suivant ?</h4><br><br><br><p></p><br><pre><br>tuple=(1,5,7,9,10,15)<br>tuple[5]=2<br>print('tuple=',tuple)<br></pre><br><br>","<p>(1,2,7,9,10,15)</p>","<p>(1,5,7,9,10,2)</p>","<p>TypeError :’tuple’ object does not support item assignment</p>","<p>(1,5,7,9,2,15)</p>","C","3","8",NULL,"1",NULL,"2019-12-26 08:54:45",NULL),
("314","<h4>Combien de valeurs affiche le programme suivant ?</h4><br><br><br><p></p><br><pre><br>tableau=[[0 for i in range(0,2)] for j in range(0,2)]<br>for i in range(0,2):<br>    for j in range(i,2):<br>        tableau[i][j]=i*j<br>for i in range(0,2):<br>    #for j in range(i,2):<br>    print(tableau[i][j])<br></pre><br>","<p>4</p>","<p>3</p>","<p>2</p>","<p>Aucune de ces propositions n’est correcte</p>","C","3","10",NULL,"1",NULL,"2019-12-26 08:57:48",NULL),
("315","<h4>Soit une liste nommée <font face='Courier New'>  bizarre = [1,[2,3],[4,5],6,7].</font><br><br>Que vaut <font face='Courier New'> len(bizarre)</font> ?</h4>","1","3","5","7","C","3","8",NULL,"1",NULL,"2020-01-05 16:14:44",NULL),
("316","<br><p>On a saisi le code suivant :</p><br><pre><br>lst =[3*n for n in range (4)] <br></pre><br>Que contient la liste lst à la fin de l’exécution de ce script ?<br>","[0,3,6,9,12,15]","[0,3,6,9,12]","[0,3,6,9]","[0,3,6]","C","3","13",NULL,"1",NULL,"2020-01-05 16:19:21",NULL),
("317","<br><p>On a saisi le code suivant :</p><br><pre><br>variable = [8,12,-7,52,-5,32]<br>lst = [n for n in variable if n <12] <br></pre><br>Que contient la liste lst à la fin de l’exécution de ce script ?<br>","[8, -7, -5] ","[8,12,-7,52,-5,32]       ","[True, False, True, False, True, False]     ","[8]","A","3","13",NULL,"1",NULL,"2020-01-05 16:23:02",NULL),
("318","<p>Soit le programme de tri suivant :</p><br><pre><br>def tri(lst):<br>    for i in range(1,len(lst)):<br>        valeur = lst[i]<br>        j = i<br>        while j>0 and lst[j-1]>valeur:<br>            lst[j]=lst[j-1]<br>            j = j-1<br>        lst[j]=valeur_a_inserer<br></pre><br><p>De quel type de tri s'agit-il ? </p><br>","Tri par insertion","Tri fusion","Tri par sélection","Tri à bulles","A","8","35",NULL,"1",NULL,"2020-03-08 21:36:57",NULL),
("319","<p>Soit le programme de tri suivant :</p><br><pre><br>def tri(lst):<br>    nb = len(lst)<br>    for i in range(0,nb):    <br>        ind_plus_petit = i<br>        for j in range(i+1,nb) :<br>            if lst[j] < lst[ind_plus_petit] :<br>                ind_plus_petit = j<br>        if ind_plus_petit is not i :<br>            temp = lst[i]<br>            lst[i] = lst[ind_plus_petit]<br>            lst[ind_plus_petit] = temp<br></pre><br><p>De quel type de tri s'agit-il ? </p>","Tri par insertion","Tri fusion","tri par selection","tri à bulles","C","8","35",NULL,"1",NULL,"2020-03-08 21:37:29",NULL),
("320","<p>L'ingénieur et pionnier de l'informatique <strong>Douglas Engelbart</strong>, chercheur au Stanford Research Institute est l'inventeur de la souris d'ordinateur. En quelle année a-t-il créé le premier prototype de souris ? </p><br>","1945","1963","1977","1986","B","1","1",NULL,"1",NULL,"2020-01-06 15:43:52",NULL),
("321","<p> En quelle année Georges Boole a-t-il publié un premier article sur l'algèbre qui porte son nom ? </p>","1844","1944","1994","2004","A","1","1",NULL,"1",NULL,"2020-01-06 15:46:47",NULL),
("322","Ada Lovelace est considérée :","comme l'inventrice du langage de programmation ADA","comme la première programmeuse au monde","comme l'inventrice du langage de programmation COBOL","comme une personne majeure de l’équipe qui conçoit l’ordinateur Harvard Mark I","B","1","1",NULL,"1","322_838_ada.jpg","2020-01-08 09:24:27",NULL),
("323","<p>Quel est le résultat de l'évaluation de l'expression suivante en python :</p><br><pre><br>[2 * n + 1 for n in range(10)]<br></pre><br><br>","[0, 2, 4, 6, 8, 10, 12, 14, 16, 18]","[1, 3, 5, 7, 9, 11, 13, 15, 17, 19]","[21, 31, 41, 51, 61, 71, 81, 101, 121]","[1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21]","B","3","11",NULL,"1",NULL,"2020-01-12 11:33:23",NULL),
("324","<p>Quelle est la valeur de la variable <code>tab</code> à l'issue de l'évaluation du code suivant :</p><br><pre>tab = ['a', 'b', 'c', 'd']<br>tab[2] = 'z'</pre><br>","['a', 'z', 'c', 'd']","['a', 'b', 'z', 'd']","['a', 'z', 'b', 'c', 'd']","['a', 'b', 'z', 'c', 'd']","B","3","10",NULL,"1",NULL,"2020-01-12 11:45:17",NULL),
("325","<p>Quel est le tableau construit par l'instruction suivante ?</p><br><pre><br>tab = [2**i for i in range(1,5)] <br></pre><br>","<pre>[0, 2, 4, 8, 16]</pre>","<pre>[2, 4, 6, 8]</pre>","<pre>[2, 4, 8, 16]</pre>","<pre>[2, 4, 8, 16, 32]</pre>","C","3","11",NULL,"1",NULL,"2020-02-03 18:13:24",NULL),
("326","<p>Quel est le tableau <code>t</code> construit par les instructions suivantes ?</p><br><pre><br>tab = [1, 2, -3, 7, 4, 10, -1, 0]<br>t = [e for e in tab if e >= 0]<br></pre><br>","<pre>t = [1, 2, 7, 4, 10, 0]</pre>","<pre>t = [e, e, e, e, e, e]</pre>","<pre>t = [1, 2, 7, 4, 10]</pre>","<pre>t = [-3, -1, 0]</pre>","A","3","11",NULL,"1",NULL,"2020-01-12 19:31:47",NULL),
("327","<p>On considère un fichier <code>CSV</code> contenant le prénom et la ville de naissance d'un certain nombre de personnes. On a mémorisé le contenu de ce fichier dans un tableau <code>personnes</code> dont on donne le début ci-dessous.</p><br><pre><br>personnes = [{'prénom': 'Marius', 'ville': 'Paris'}, <br>             {'prénom': 'Nassim', 'ville': 'Angers'}, <br>             {'prénom': 'Eléa', 'ville': 'Nantes'}, <br>             ...<br>            ]<br></pre><br>Quelles instructions permettent d'afficher le prénom de toutes les personnes nées à Rouen ?<br>","<pre><br>for p in personnes:<br>    if p['ville'] == p['Rouen']:<br>        print(p['prénom'])<br></pre>","<pre><br>for p in personnes:<br>    if p['ville'] == 'Rouen':<br>        print(prénom)<br></pre>","<pre><br>for p in personnes:<br>    if p['ville'] == 'Rouen':<br>        print(p['prénom'])<br></pre>","<pre><br>for p in personnes:<br>    if personnes['ville'] == personnes['Rouen']:<br>        print(personnes['prénom'])<br></pre>","C","4","15",NULL,"1",NULL,"2020-01-12 20:39:46",NULL),
("328","<p>On considère un fichier <code>CSV</code> contenant le prénom, la ville de naissance et l'année de naissance d'un groupe de personnes. On a mémorisé le contenu de ce fichier dans un tableau <code>personnes</code> dont on donne le début ci-dessous.</p><br><pre><br>personnes = [{'prénom': 'Marius', 'ville': 'Paris', 'année': '2004'}, <br>             {'prénom': 'Nassim', 'ville': 'Angers', 'année': '1972'}, <br>             {'prénom': 'Eléa', 'ville': 'Nantes', 'année': '1993'}, <br>             ...<br>            ]<br></pre><br><p>Quelle instruction permet de construire un tableau <code>t</code> contenant les prénoms de toutes les personnes nées en 2001 ?</p><br>","<pre><br>t = [personnes if p['année'] == '2001']<br></pre>","<pre><br>t = [p for p in personnes if p['année'] == '2001']<br></pre>","<pre><br>t = [if p['année'] == '2001': p['prénom']]<br></pre>","<pre><br>t = [p['prénom'] for p in personnes if p['année'] == '2001']<br></pre>","D","4","15",NULL,"1",NULL,"2020-01-12 20:53:24",NULL),
("329","<p>On considère un fichier <code>CSV</code> contenant le prénom, la ville de naissance et l'année de naissance d'un groupe de personnes. On a mémorisé le contenu de ce fichier dans un tableau <code>personnes</code> dont on donne le début ci-dessous.</p><br><pre><br>personnes = [{'prénom': 'Marius', 'ville': 'Paris', 'année': '2004'}, <br>             {'prénom': 'Nassim', 'ville': 'Angers', 'année': '1972'}, <br>             {'prénom': 'Eléa', 'ville': 'Nantes', 'année': '1993'}, <br>             ...<br>            ]<br></pre><br><p>Par quoi faut-il remplacer les pointillés dans l'instruction suivante pour construire le tableau <code>t</code> contenant uniquement les lignes du tableau <code>personnes</code> concernant les individus nés à partir de 2005 à Angers ?</p><br><pre><br>t = [p for p in personnes if ...]<br></pre><br>","<pre>'année' >= 2005 and 'ville' == Angers</pre>","<pre>p['ville'] == 'Angers' and int(p['année']) >= 2005 </pre>","<pre>p['année'] >= int(2005) and p['ville'] == 'Angers'</pre>","<pre>int(p['année']) >= 2005 or p['ville'] == 'Angers'</pre>","B","4","15",NULL,"1",NULL,"2020-01-12 21:11:25",NULL),
("330","Physiquement, les registres sont :","Des composants sur la carte mère de l'ordinateur","A l'intérieur du processeur","Une partie séparée de la RAM","De la mémoire cache","B","6","23",NULL,"1",NULL,"2020-01-12 21:58:21",NULL),
("331","Un programme qui convertit le langage assembleur en langage machine est appelé :","Assembleur","Interpréteur","Compilateur","Comparateur","A","6","23",NULL,"1",NULL,"2020-01-12 22:01:07",NULL),
("332","Laquelle des mémoires suivantes est non volatile, c'est à dire ne s'efface pas quand on éteint l'ordinateur ?","SRAM","DRAM","ROM","Toutes les réponses sont vraies","C","6","23",NULL,"1",NULL,"2020-01-12 22:03:11",NULL),
("333","« BUS » est le nom que l'on donne en informatique :","Au Board Unit System","Aux registres du processeur","Aux fils qui permettent de transmettre les données d'un composant à un autre","A l'horloge de la carte mère","C","6","23",NULL,"1",NULL,"2020-01-12 22:09:08",NULL),
("334","Dans quel langage les instructions sont formées de suites de 0 et de 1 ?","Le langage assembleur","Le langage machine","Un langage compilé","Un langage orienté objets","B","6","23",NULL,"1",NULL,"2020-01-12 22:13:28",NULL),
("335","Laquelle des instructions Python ci-dessous a un équivalent direct en assembleur ?","while","for","if","def","C","6","23",NULL,"1",NULL,"2020-01-12 22:15:02",NULL),
("336","Laquelle des instructions assembleur ci-dessous n'a pas d'équivalent en Python ?","ADD (additionne le contenu de deux registres)","MOV (Copie un nombre ou le contenu d'un registre dans un registre)","B (Saute à l'adresse indiquée)","AND (Fait un 'et' bit par bit entre deux registres)","C","6","23",NULL,"1",NULL,"2020-01-12 22:16:41",NULL),
("337","<p>On considère la fonction suivante.</p><br><pre><br>def mystere(tab):<br>    '''tab est un tableau non vide de nombres réels'''<br>    n = 0<br>    for x in tab:<br>        if x >= 10:<br>            n = n + 1<br>    return n/len(tab)<br></pre><br><p>Que renvoie l'expression <code>mystere([14.5, 8, 12, 10, 5, 15.5, 17, 7])</code> ?</p>","<pre>[14.5, 12, 10, 15.5, 17]</pre>","<pre>0.625</pre>","<pre>8.625</pre>","<pre>5</pre>","B","3","10",NULL,"1",NULL,"2020-01-13 18:49:26",NULL),
("338","<p>On considère le dictionnaire <code>repertoire</code> suivant.</p><br><pre>repertoire = {'Marc':'0101', 'Virginie':'0202', 'Justine':'0303'}</pre><br><p>Que renvoie l'instruction <code>'0202' in repertoire</code> ?</p><br><br><br>","<pre>False</pre>","<pre>True</pre>","<pre>'Virginie'</pre>","<pre>'Virginie':'0202'</pre>","A","3","9",NULL,"1",NULL,"2020-01-13 19:13:27",NULL),
("339","<p>On considère le tableau <code>t</code> suivant.</p><br><pre>t = [[1, 2, 3], [2, 3, 4], [3, 4, 5], [4, 5, 6]]</pre><br><p>Quelle est la valeur de <code>t[1][2]</code> ?</p><br>","1","3","4","2","C","3","12",NULL,"1",NULL,"2020-01-13 19:22:37",NULL),
("340","<h4>Identifier la porte logique ci-dessous :</h4><br><br><br>","<p> ET </p>","<p> OU </p>","<p> NON </p>","<p> OU EXCLUSIF </p>","A","6","27",NULL,"1","340_AND.png","2020-01-15 15:08:51",NULL),
("341","<h4>Identifier la porte logique ci-dessous:</h4><br>","<p> AND </p>","<p> OR </p>","<p> NO </p>","<p> XOR  </P>","B","6","27",NULL,"1","341_OR.png","2020-01-15 15:13:29",NULL),
("342","<h4> Quelle est cette porte logique ?</h4><br><br>","<p> AND </p>","<p> OR </p>","<p> NOT </p>","<p> XOR </p>","C","6","27",NULL,"1","342_NO.png","2020-01-15 16:18:01",NULL),
("343","<h4>Quelle est cette porte logique ?</h4><br><br>","<p> AND </p>","<p> OR </p>","<p> NO </p>","<p> XOR </p>","D","6","27",NULL,"1","343_xor.png","2020-01-15 15:19:27",NULL),
("344","<h4> Identifier cette porte logique :</h4><br>","AND","XAND","NAND","RAND","C","6","27",NULL,"1","344_NO AND.png","2020-01-17 19:44:24",NULL),
("345","<p> Quelle est cette porte logique ? </p>","<p> OR </p>","<p> XOR </p>","<p> NOR </p>","<p> FOR </p>","C","6","27",NULL,"1","345_NOR SANS LETTRE.png","2020-01-15 17:17:34",NULL),
("346","<h4>A qui appartient cette table de vérité ?</h4><br>","<p> XOR </P>","<p> XAND </P>","<p> NOR </P>","<p> NAND </P>","D","6","27",NULL,"1","346_NO AND table verite.png","2020-01-15 17:18:33",NULL),
("347","<h4>A qui appartient cette table de vérité ? </h4><br>","<p> XOR </p>","<p> XAND </p>","<p> NOR </p>","<p> NAND </p>","A","6","27",NULL,"1","347_table XOR.png","2020-01-15 17:19:34",NULL),
("348","<h4>Dans le circuit logique ci-dessous,</h4><br><h4> quelle valeur faut il entrer respectivement en A, B, C, et D pour obtenir 1 en sortie ?</h4><br>","A=0 B=0 C=0 D=0","A=0 B=1 C=0 D=1","A=0 B=0 C=1 D=1","A=1 B=1 C=1 D=1","C","6","27",NULL,"1","348_ex3.png","2020-01-15 17:20:24",NULL),
("349","<h4>Dans le circuit logique ci-dessous,</h4><br><h4>les entrées A=1 , B=1 et Cin=0 donnent en sortie ...</h4><br><br>","S=1 Cout=0","S=0 Cout=1","S=1 Cout=1","S=0 Cout=0","B","6","27",NULL,"1","349_additionneur.png","2020-01-17 19:42:32",NULL),
("350","<h4>Recherche dans un dictionnaire :</h4><br><br><br><p>On dispose d'un dictionnaire et d'un ensemble :</p><br><pre><br>T = {'Bill': '06 05 04 03 02', 'Roger': '06 12 11 13 20', 'Marilla': '07 00 01 02 03', 'James': '07 00 70 00 70'}<br>ns = {x for x in T if T[x][:2] == '06'}<br></pre><br><br><br>Que contient ns à la fin de l’exécution de ce script ?<br>","{'06'}","{'06 05 04 03 02', '06 12 11 13 20'}","{'Bill' : '06 05 04 03 02', 'Roger': '06 12 11 13 20'}","{'Bill', 'Roger'}","D","3","9",NULL,"1",NULL,"2020-01-25 10:42:10",NULL),
("351","<h4>Recherche dans un dictionnaire :</h4><br><br><br><p>On dispose du dictionnaire suivant :</p><br><pre><br>Tel = {'Bill': '06 05 04 03 02', 'Roger': '06 12 11 13 20'}<br></pre><br>Comment obtenir  la liste des numéros de téléphones ?<br>","<pre>list(Tel)</pre>","<pre>list(Tel.values())</pre>","<pre>list(Tel.keys())</pre>","<pre>list(Tel.items())</pre>","B","3","9",NULL,"1",NULL,"2020-01-25 10:49:35",NULL),
("352","<h4>Extraction d'une valeur :</h4><br><br><br><p>On a saisi le code suivant :</p><br><pre><br>Tel = {'Bill': '06 05 04 03 02', 'Roger': '06 12 11 13 20', 'Marilla': '07 01 02 03 04'}<br></pre><br>Quelle est la valeur de <pre>Tel[1]</pre> ?<br>","<pre>'Bill'</pre>","<pre>'06 05 04 03 02'</pre>","<pre>'B'</pre>","Rien","D","3","9",NULL,"1",NULL,"2020-01-25 10:54:56",NULL),
("353","<h4>Modification d'un dictionnaire :</h4><br><br><br><p>On dispose du dictionnaire <pre>dico = {'Bill': '06 05 04 03 02', 'Roger': '06 12 11 13 20'}</pre>. Quelle instruction permet de modifier le dictionnaire de façon à ce que sa nouvelle valeur soit <pre>{'Bill': '06 05 04 03 02', 'Roger': '06 12 11 13 20', 'Marilla': '07 01 02 03 04'}</pre> ? </p><br>","<pre>dico['Marilla'] = '07 01 02 03 04'</pre>","<pre>dico.append('Marilla', '07 01 02 03 04')</pre>","<pre>dico['Marilla'] += '07 01 02 03 04'</pre>","ce n'est pas possible car un dictionnaire n'est pas modifiable","A","3","9",NULL,"1",NULL,"2020-01-25 11:00:59",NULL),
("354","<h4>Boucle et dictionnaire :</h4><br><br><br><p>On dispose du dictionnaire suivant :</p><br><pre><br>dico = {'Bill': '06 05 04 03 02', 'Roger': '06 12 11 13 20'}<br></pre><br>Quels sont les affichages possibles lors de l'exécution de ce code ?<br><br><pre><br>for truc in dico.items():<br>    print(truc, end=' ')<br></pre><br>","<pre><br>Bill Roger<br></pre>","<pre><br>06 05 04 03 02 06 12 11 13 20<br></pre>","<pre><br>('Bill', '06 05 04 03 02') ('Roger', '06 12 11 13 20')<br></pre>","<pre><br>Bill end<br>Roger end<br></pre>","C","3","9",NULL,"1",NULL,"2020-01-25 12:03:30",NULL),
("355","<h4>Table de données :</h4><br><br><br><p>On dispose d'une table de  données <pre>Table</pre> représentée par une liste de dictionnaires. En entrant <pre>Table[0]</pre> on obtient:</p><br>","Une ligne","Une colonne","Une cellule","Une erreur","A","4","14",NULL,"1",NULL,"2020-01-25 12:07:46",NULL),
("356","<h4>Exemple de question :</h4><br><br><br><p>On a saisi le code suivant :</p><br><pre><br>t = (1, 2, 3, (2, 3, 4), 5, 6)<br>len(t)<br></pre><br>Que renvoie l’exécution de ce script ?<br>","6","8","5","3","A","3","8",NULL,"1",NULL,"2020-01-26 15:27:18",NULL),
("357","<p>La mise en forme d'une page web statique se fait grâce au :</p><br>","langage HTML","langage Javascript","langage CSS","langage PHP","A","5","19",NULL,"1",NULL,"2020-01-27 11:52:27",NULL),
("358","<p> Les délimiteurs html tels que <xmp><head>...</head></xmp> s'appellent :</p><br><br>","des bornes","des boutons","des valises","des balises","D","5","19",NULL,"1",NULL,"2020-01-27 11:52:11",NULL),
("359","<p>On considère la fonction <strong>xor</strong> dont la table de vérité est donnée ci-dessous :</p>
<table border='1'><tr><th>A</th>
  <th>B</th>
  <th>A xor B</th>
</tr><tr><th>0</th>
  <th>0</th>
  <th>0</th>
</tr><tr><th>0</th>
  <th>1</th>
  <th>1</th>
</tr><tr><th>1</th>
  <th>0</th>
  <th>1</th>
</tr><tr><th>1</th>
  <th>1</th>
  <th>0</th>
</tr></table><p>Laquelle des propositions suivantes est incorrecte ?</p>
","A xor A = 0","A xor 1 = A","A xor 1 = not A","A xor (not A) = 1","B","2","5",NULL,"1",NULL,"2020-10-19 12:50:51",NULL),
("360","Dans un fichier html, tout ce qui est affiché se trouve entre :<br>","<xmp> <head>...</head> </xmp>","<xmp> <title>...</title> </xmp>","<xmp> <html>...</html> </xmp>","<xmp> <body>...</body> </xmp>","D","5","19",NULL,"1",NULL,"2020-01-28 07:48:21",NULL),
("361","Dans un fichier html, pour aller à la ligne dans un paragraphe, on utilise :<br><br>","<xmp> <p>...</p> </xmp>","<xmp><br></xmp>","<xmp> <h1>...</h1> </xmp>","<xmp> <li>...</li> </xmp>","B","5","19",NULL,"1",NULL,"2020-01-28 07:51:17",NULL),
("362","Dans une page html, pour organiser le texte en tableau, on utilise :","<xmp> <table>...</table> </xmp>","<xmp> <li>...</li> </xmp>","<xmp> <ol>...</ol> </xmp>","<xmp> <p>...</p> </xmp>","A","5","19",NULL,"1",NULL,"2020-01-28 07:53:27",NULL),
("363","Combien y-a-t-il de couches dans le modèle OSI ?<br>","6","7","4","5","B","6","24",NULL,"1",NULL,"2020-03-23 15:38:20",NULL),
("364","Sur quelle couche du modèle OSI se trouve les protocoles TCP et UDP ?","Liaison de données","Application","Physique","Transport","D","6","24",NULL,"1",NULL,"2020-03-23 15:38:53",NULL),
("365","D'après l'architecture de von Neumann, un ordinateur est composé de :","Processeur, mémoire RAM, mémoire ROM","Unité centrale, écran, clavier","Processeur, mémoire RAM, périphériques","Processeur, mémoire, périphériques","D","6","23",NULL,"1",NULL,"2020-01-28 09:28:18",NULL),
("366","Qu'obtient-on avec le script suivant ?<br><pre><br>from turtle import *<br><br>up()<br>goto(-200,0)<br>down()<br>width(5)<br><br>for i in range(10)<br>   forward(10)<br>   up()<br>   forward(40)<br>   down()<br><br>mainloop()<br></pre><br>","un carré","des flèches","un rectangle","des pointillés","D","7","32",NULL,"1",NULL,"2020-01-28 09:52:37",NULL),
("367","Soit une machine basée sur le processeur M999 qui satisfait à l'architecture de Von Neumann.<br/><br>Elle dispose :<br/><br>* de deux registres d'entrée A et B<br/><br>* d'un registre de sortie R<br/><br><br/><br>Et du jeu d'instruction suivant :<br/><br><pre><br>LDA xx //LoaD la mémoire @xx dans le registre A<br>LDB xx //LoaD la mémoire @xx dans le registre B<br>STR xx //STore le registre R dans la mémoire @xx<br>ADD    //Ajoute le registre A et B, et place le résultat dans le registre R<br>SUB    //Soustrait le registre B à A (fait A-B en fait), et place le résultat dans le registre R<br>JMP xx //JuMP (saute) à l'adresse @xx<br>JPP xx //JumP (saute) à l'adresse @xx si R est strictement positif<br>HLT    //Arrête le programme<br></pre><br><br/><br>Que contient la mémoire d'adresse 07 (@07) une fois le programme suivant exécuté ?<br/><br><pre><br>00: LDA 08<br>01: LDB 09<br>02: JMP 04<br>03: LDA 10<br>04: ADD<br>05: STR 07<br>06: HLT<br>07: 000<br>08: 001<br>09: 010<br>10: 100<br></pre>","000","011","101","110","B","6","23",NULL,"1",NULL,"2020-01-31 18:21:36",NULL),
("368","Avec le codage ASCII, chaque caractère est codé sur 1 octet. Ce codage représente un jeu de :","256 caractères","127 caractères","128 caractères","255 caractères","C","2","6",NULL,"1",NULL,"2020-02-01 11:20:23",NULL),
("369","<p>Les systèmes informatiques (comme un ordinateur ou un téléphone portable) ont un fonctionnement très structuré. Lequel des schémas ci-dessous rend correctement compte des interactions entre les différents éléments d’un système informatique ?</p><br>","Schéma 1)","Schéma 2)","Schéma 3)","Schéma 4)","B","6","25",NULL,"1","369_img.png","2020-02-02 14:15:29",NULL),
("370","<h4>Liste en compréhension</h4><br><br><br><p>Quelle instruction permet d'obtenir une liste contenant les points de code des voyelles de la chaîne de caractères <code>chaine</code> ?<br>","<code><br>[append(ord(c)) for c in chaine if c in 'aeiouy']<br></code>","<code><br>[append(ord(c)) if c in 'aeiouy' for c in chaine]<br></code>","<code><br>[ord(c) for c in chaine if c in 'aeiouy']<br></code>","<code><br>[ord(c) if c in 'aeiouy' for c in chaine]<br></code>","C","3","11",NULL,"1",NULL,"2020-02-03 15:24:30",NULL),
("371","<h4>On considère la fonction suivante :</h4><br><br><br><p></p><br><pre><br>def recherche_max(liste):<br>      max=liste[0]<br>      for i in range(1,len(liste)-1):<br>          if liste[i]>liste[i-1]:<br>              max=liste[i]<br>      return max<br></pre><br>Quelle liste permet de montrer que cette fonction est erronée ?<br>","liste=[3,5,1,15]","liste=[3,19,18,17]","liste=[8,15,5,3]","liste=[3,15,2,7]","A","7","31",NULL,"1",NULL,"2020-03-25 08:33:23",NULL),
("372","<p>Quelle est la bibliothèque à importer pour utiliser des fonctions qui permettent de réaliser des tracés 2D ?</p>","matplotlib","numpy","math","random","A","7","32",NULL,"1",NULL,"2020-02-06 17:34:30",NULL),
("373","<p>Lorsque l'on crée une liste à partir d'un fichier grâce à ce programme</p><br><pre><br>with open('file.txt','r') as fichier :<br>        annuaire=[]<br>        for ligne in fichier:<br>            ligne=ligne.replace('\n','')<br>            annuaire.append(ligne)<br></pre><br>le code ligne=ligne.replace('\n','') permet :<br>","de séparer en liste les lignes","d'ajouter tous les caractères \n","de rendre visible tous les caractères \n du fichier","d'enlever tous les caractères \n","D","7","33",NULL,"1",NULL,"2020-02-05 08:59:25",NULL),
("374","<p>lorsque l'on ouvre un fichier à partir de l'instruction suivante :</p><br><pre>with open('file.txt','r') as fichier :</pre><br>","l'instruction close() est obligatoire pour éviter une erreur","l'instruction close() n'est pas à indiquer dans le code","l'instruction close() est obligatoire pour refermer l'instruction open","l'instruction close() est souhaitable pour éviter que le programme ne prenne trop de mémoire","B","7","33",NULL,"1",NULL,"2020-02-05 09:03:01",NULL),
("375","A partir de la liste chiffre=['22','29','35','44','56'],<br>comment créer le texte suivant : '22-29-35-44-56' ?<br>","'-'.join(chiffre)","tiret.join(chiffre)","chiffre.split('-')","chiffre.split(tiret)","A","7","33",NULL,"1",NULL,"2020-02-06 17:30:43",NULL),
("376","<h4>Que signifie le sigle CSV ? </h4>","Comma separated Values","Common Special Values","Clear Special Values","Check Specific Values","A","4","18",NULL,"1",NULL,"2020-02-06 19:24:15",NULL),
("377","<h4>A quoi correspond l'Open Data ?</h4>","Des données numériques dont l'accès et l'usage sont laissés libres aux usagers.","Des données numériques dont l'accès est réservé aux abonnés du service 'Open'","Une liste de date correspondant à l'ouverture de sites Internet.","Des données correspondants à l'ouverture d'un service en ligne.","A","4","18",NULL,"1",NULL,"2020-02-06 19:35:12",NULL),
("378","<p>Un algorithme est en complexité quadratique. Codé en python, son exécution pour des données de taille 100 prend 12 millisecondes</p><br><br><p>Si l'on fournit des données de taille 200 au programme, on peut s'attendre à un temps d'exécution d'environ :</p>","48 millisecondes","24 millisecondes","12 millisecondes","96 millisecondes","A","4","16",NULL,"1",NULL,"2020-02-07 06:56:54",NULL),
("379","<p>On a saisi le code suivant :</p><br><pre><br>liste = [0, 1, 2, 3]<br>compteur = 0<br>for i in range(len(liste)-1) :<br>    for j in range(i,len(liste)) :<br>        compteur += 1<br></pre><br>Que contient la variable compteur à la fin de l’exécution de ce script ?<br>","4","9","8","10","B","4","16",NULL,"1",NULL,"2020-03-06 18:05:52",NULL),
("380","Soit une machine basée sur le processeur M999 qui satisfait à l'architecture de Von Neumann.<br/><br>Elle dispose :<br/><br>* de deux registres d'entrée A et B<br/><br>* d'un registre de sortie R<br/><br><br/><br>Et du jeu d'instruction suivant :<br/><br><pre><br>LDA xx //LoaD la mémoire @xx dans le registre A<br>LDB xx //LoaD la mémoire @xx dans le registre B<br>STR xx //STore le registre R dans la mémoire @xx<br>ADD    //Ajoute le registre A et B, et place le résultat dans le registre R<br>SUB    //Soustrait le registre B à A (fait A-B en pratique), et place le résultat dans le registre R<br>JMP xx //JuMP (saute) à l'adresse @xx<br>JPP xx //JumP (saute) à l'adresse @xx si R est strictement positif<br>HLT    //Arrête le programme<br></pre><br><br/><br>Que contient la mémoire d'adresse 11 (@11) une fois le programme suivant exécuté ?<br/><br><pre><br>00: LDB 13<br>01: JMP 04<br>02: LDA 12<br>03: JMP 05<br>04: LDA 14<br>05: SUB<br>06: JPP 02<br>07: LDB 11<br>08: ADD<br>09: STR 11<br>10: HLT<br>11: 000<br>12: 001<br>13: 010<br>14: 100<br></pre>","000","001","010","111","B","6","23",NULL,"1",NULL,"2020-02-07 17:22:24",NULL),
("381","<h4>Quel est le résultat de ce code?</h4><br><br><br><p>On a saisi le code suivant :</p><br><pre><br>ingredients='oeufs'<br>print(ingredients*3)<br></pre><br><br>","Ingredientingredientingredient","'ingredientingredientingredient'","oeufsoeufsoeufs","aucune de ces trois propositions","C","7","28",NULL,"1",NULL,"2020-02-10 17:46:53",NULL),
("382","<h4>Quel résultat aura ce code ?</h4><br><br><br><pre><br>bonjour()<br>#bonjour()<br>def bonjour():<br>    print('Bonjour à tous!')<br></pre><br><br>","Bonjour à tous!","Traceback (most recent call last):  File '<input>', line 1, in <module>   ","' Bonjour à tous ! '","Aucune de ces trois propositions","B","7","28",NULL,"1",NULL,"2020-02-10 17:50:11",NULL),
("383","<h4>Quel est le résultat de ce code ?</h4><br><br><br><br><pre><br>num=7<br>if num>3:<br>    print('3')<br>    if num<5:<br>        print('5')<br>        if num==7:<br>            print('7')<br></pre><br><br>","3","7","'37'","Aucune des solutions n’est exacte","A","7","28",NULL,"1",NULL,"2020-02-10 17:52:22",NULL),
("384","<h4>Quel est le résultat de ce code ?</h4><br><br><br><br><pre><br>if not True:<br>    print('10')<br>elif not (10+10==3):<br>    print('20')<br>else:<br>    print('30')<br></pre><br><br>","File 'input', line 1, in <module>if not true:NameError: name 'true' is not defined","20","30","aucune de ces propositions n'est exacte","B","7","28",NULL,"1",NULL,"2020-02-10 17:56:14",NULL),
("385","<h4>Quel est le résultat de ce code ?</h4><br><br><br><br><pre><br>x=4<br>y=2<br>if not 1+1==y or x==4 and 7==8:<br>    print('OK')<br>elif x>y:<br>    print('Non')<br></pre><br><br>","Non","OK","File 'input', line 5 elif x>y ^SyntaxError: invalid syntax","aucune de ces propositions n'est exacte","A","7","28",NULL,"1",NULL,"2020-02-10 17:58:58",NULL),
("386","<h4>Sélectionnez le code permettant d’obtenir le résultat suivant</h4><br><br><br><p><br>5<br><br>4<br><br>3<br><br></p><br><pre><br></pre><br><br>","i=5<br>while True:<br>    print(i)<br>    i=i-1","i=5<br>while i>2:<br>    print(i)<br>    i=i-1","i=2<br>while not i>5:<br>    i=i+1<br>    print(i)","Aucune de ces propositions n'est exacte","B","7","28",NULL,"1",NULL,"2020-02-10 18:00:56",NULL),
("387","<h4>Je souhaite définir une fonction qui compare la longueur de deux chaînes de caractères et me renvoie la plus courte. Pour cela, je compléterai le code suivant avec : </h4><br><br><br><pre><br>def plus_court_string(x,y) :<br>    if len(x)<=len(y) :<br>        _________<br>    else :<br>        _________<br></pre><br><br>","print(x) et print(y)","print(x) et return(y)","return x et return y","return x et return","C","7","28",NULL,"1",NULL,"2020-02-10 18:05:48",NULL),
("388","<h4>Parmi les nombres suivants, écrits en base 10, lequel à une écriture infinie en base 2:</h4><br><br>","1,25","0,75","1,7","3,5","C","2","4",NULL,"1",NULL,"2020-02-10 18:07:10",NULL),
("389","<h4>Parmi les nombres suivants, écrits en base 10, lequel à une écriture infinie en base 2:</h4><br><br>","1,25","0,75","1,7","3,5","C","2","4",NULL,"1",NULL,"2020-02-10 18:07:31",NULL),
("390","<h4>L’UAL</h4><br><br>","Permet de gérer la mémoire","permet de gérer les entrées/sorties","c’est une adresse internet","est l’endroit où les calculs sont effectués","D","6","23",NULL,"1",NULL,"2020-02-10 18:08:46",NULL),
("391","<h4>Quelle affirmation est exacte:</h4><br><br><br>","La mémoire RAM ne fonctionne qu’en mode lecture","La mémoire RAM permet de stocker des données et des programmes","Une mémoire morte ne peut plus être utilisée","La mémoire RAM permet  de stocker définitivement des données","B","6","23",NULL,"1",NULL,"2020-02-10 18:10:08",NULL),
("392","<h4>On dispose d’une box reliée à Internet qui a une seule prise Ethernet libre et de 2 switchs munis de 5 prises Ethernet. Combien d’ordinateurs peuvent être connectés par câbles Ethernet pour avoir l’accès à Internet?</h4><br><br>","6","7","8","3","B","6","24",NULL,"1",NULL,"2020-02-10 18:11:26",NULL),
("393","<h4>on a l’adresse 192.168.1.12 et le masque réseau 255.255.255.240 Combien d’ordinateurs peuvent être connectés sur ce réseau?</h4><br><br>","4","8","14","16","C","6","24",NULL,"1",NULL,"2020-02-10 18:12:42",NULL),
("394","<p>La Small-Scale Experimental Machine (SSEM) (« machine expérimentale à petite échelle »), surnommée Baby (« Bébé »), était la première machine à architecture de von Neumann du monde. Construite à l’université Victoria de Manchester par Frederic Calland Williams, Tom Kilburn et Geoff Tootill, elle exécuta son premier programme le ...</p><br><br><br><p>(Source: wikipedia)</p><br><br>","18 juin 1940","21 juin 1948","18 septembre 1959","21 juillet 1969","B","1","1",NULL,"1","394_SSEM_Manchester_museum_close_up.300k.jpg","2020-03-12 09:47:17",NULL),
("395","<p>À quand remontent les plus anciens algorithmes ?</p><br>","Ils existent depuis l'Antiquité","Ils sont apparus au 9e siècle grace au mathématicien, géographe, astrologue et astronome persan Muḥammad ibn Mūsā al-Khwārizmī généralement appelé Al-Khwârismî","C'est Blaise Pascal qui a écrit les premiers au 16e siècle pour sa calculatrice mécanique dite 'la pascaline'","Ils ont été inventés au 19e siècle avec les travaux du mathématicien George Boole","A","1","1",NULL,"1",NULL,"2020-02-11 23:08:49",NULL),
("396","<br><br><br><p>On souhaite écrire une fonction Python qui admette comme argument une date et retourne le jour de la semaine (lundi,mardi,...,dimanche).</p><br><P>La date est un argument nommé <b>donnee</b> de type liste au format [JJ,MM,AAAA].</p><br><br><P>Parmi les instructions suivantes, laquelle est correcte pour décrire une pré-condition :</p><br><br>","<pre><br>assert donnee[0]>31,'Il n'y a que 31 jours au maximum dans un mois'<br></pre>","<pre><br>assert 0< donnee[0]<=31,'Erreur dans la saisie du jour'<br></pre>","<pre><br>assert donnee[1]<0,'Le numéro d'un mois est positif'<br></pre>","<pre><br>assert donnee[1]>12,'Il n'y a que 12 mois dans une année'<br></pre>","B","7","30",NULL,"1",NULL,"2020-02-12 13:49:32",NULL),
("397","<p><br>Quel est l'entier positif codé en base 2 sur 8 bits par le code 0100 1011 ?<br></p><br>","75","42","74","43","A","2","2",NULL,"1",NULL,"2020-02-12 13:52:09",NULL),
("398","<br><br><br><p>Quel est le résultat de l'instruction seuil(65) ?</p><br><pre><br>def seuil(n):<br>    i=0<br>    resultat=1<br>    while resultat < n:<br>         i=i+1<br>         resultat=resultat*2<br>    return i<br></pre><br><br>","6","5","8","7","D","7","28",NULL,"1",NULL,"2020-02-12 13:58:55",NULL),
("399","<p>On a saisi le code suivant :</p><br><pre><br>matrice = [3*[0] for i in range (3)]<br>for i in range(3):<br>    for j in range(3):<br>        matrice[i][j] = i + j<br>        if (i == j):<br>            matrice[i][j]=2 * matrice[i][j]<br></pre><br>Que contient la variable matrice à la fin de l’exécution de ce script ?<br>","<pre> [[0, 1, 2], [1, 2, 3], [2, 3, 4]] </pre>","<pre> [[1, 2, 3], [2, 3, 4], [3, 4, 5]] </pre>","<pre> [[0, 1, 2], [1, 4, 3], [2, 3, 8]] </pre>","<pre> [[1, 2, 3], [2, 9, 4], [3, 4, 25]] </pre>","C","3","12",NULL,"1",NULL,"2020-02-13 17:54:57",NULL),
("400","<p>On a saisi le code suivant :</p><br><pre><br>tab = [10, 8, 4, 3, 1, 5, 7, 2]<br>cpt = 0<br>for i in range(1, len(tab)):<br>    for j in range(len(tab) - i):<br>        cpt +=1<br></pre><br>Que contient la variable cpt à la fin de l’exécution de ce script ?<br>","64","8","36","28","D","8","39",NULL,"1",NULL,"2020-02-13 18:05:56",NULL),
("401","<p> L'écriture binaire du nombre décimal 49,78125 est : </p><br>","110001,11001","110001,01011","110001,01101","infinie","A","2","4",NULL,"1",NULL,"2020-02-13 18:08:42",NULL),
("402","<p>L'écriture décimale du nombre binaire 110,1011 est : </p><br>","6,625","6,6875","6,675","6,6825","B","2","4",NULL,"1",NULL,"2020-02-13 18:11:10",NULL),
("403","<p>On a saisi le code suivant :</p><br><pre><br>def mystere(texte):<br>    status = False<br>    for i in range(len(texte)-1):<br>        if (texte[i] == texte[i+1]):<br>            status = True<br>    return status<br></pre><br>Dans quel cas la fonction mystere renvoie le booléen False ? <br>","mystere('alphabet')","mystere('voyelle')","mystere('dictionnaire')","mystere('lettre')","A","8","39",NULL,"1",NULL,"2020-02-13 19:39:18",NULL),
("404","<p>On a saisi le code suivant :</p><br><pre><br>def mystere(texte,n):<br>    chaine = ''<br>    for i in range(len(texte)):<br>        if (i % 2 == 0): <br>            chaine = chaine + texte[i]*n<br>    return chaine<br></pre><br>Que retourne l'instruction mystere('chocolat',4) ? <br>","'cccchhhhoooollllaaaatttt'","'chocolatchocolatchocolatchocolat'","'ccccooooooooaaaa'","'hhhhcccclllltttt'","C","8","39",NULL,"1",NULL,"2020-02-13 19:48:13",NULL),
("405","L'écriture 10<sub>(16)</sub><br>est :","correcte","incorrecte",NULL,NULL,"A","2","2",NULL,"1",NULL,"2020-02-18 09:51:48",NULL),
("406","La conversion du code suivant : 11111110<sub>(2)</sub>, en base :","(10), donne 256","(16), donne F0","(16), donne FE","(10), donne 127","C","2","2",NULL,"1",NULL,"2020-02-18 10:56:48",NULL),
("407","Soit la combinaison suivante, écrite en norme IEEE754 (32 bits), pour un nombre à virgule flottante :<br />
1/10000000/11000000000000000000000<br />
(Les / sont positionnés pour faciliter la lecture). Avec le rappel de la formule de conversion suivant :<br />
N<sub>(10)</sub> = (-1)<sup>s</sup> x (1+M) x 2<sup>(e-127)</sup><br />
Cette combinaison vaut :","3,5","-75,75","-3,5","75,3","C","2","4",NULL,"1",NULL,"2020-11-02 17:04:20",NULL),
("408","Quelle est l’écriture en complément à 2 sur 8 bits du nombre -85 ?","01010111","01011101","11010101","10101011","D","2","3",NULL,"1",NULL,"2020-11-02 17:05:39",NULL),
("409","Est-ce que cette portion de code HTML est valide ?<br><br>&lt;p>&lt;i&gt;Du texte italique&lt;/p&gt;&lt;/i&gt;","oui","non",NULL,NULL,"B","5","19",NULL,"1",NULL,"2020-02-18 11:13:19",NULL),
("410","Choisir la balise pour le plus gros titre :","&lt;h1>","&lt;h6>","&lt;head>","&lt;title>","A","5","19",NULL,"1",NULL,"2020-04-10 07:09:47",NULL),
("411","Les différents dossiers et fichiers d’un site internet sont regroupés dans un dossier nommé [Mon_Site]. <br>L’arborescence de ce dossier est donnée ci-dessous.<br><br>Parmi les liens suivants, lequel permet d’appeler l’image 'Mon_image.png' depuis la feuille de style 'Mon_style.css' ?","images/Mon_image.png","medias/images/Mon_image.png","Mon_image.png","../medias/images/Mon_image.png","D","5","19",NULL,"1","411_Original_Arborescence_Mon_Site.png","2020-02-18 11:26:05",NULL),
("412","Un peu d'HTML :<br><br><br><code>‹form class='unform' action='maPage.php' method='get'›<br><br>‹input name='recherche' class='input' size='20' type='text' placeholder='Recherche...' /›<br><br>‹/form ›</code><br><br><br>Dans ce formulaire par quel procédé les données saisies sont transmises au serveur ?","Par la méthode INPUT","Par la méthode PLACEHOLDER","Par la méthode POST","Par la méthode GET","D","5","20",NULL,"1",NULL,"2020-02-18 14:24:59",NULL),
("413","Un navigateur cherche à se connecter à une URL sur un serveur web. Cette URL contient un fichier .html, un fichier .css et 2 images .png<br><br>Combien de requêtes HTTP (version 1.1), seront nécessaires ?","1","2","3","4","D","5","20",NULL,"1",NULL,"2020-02-18 13:04:01",NULL),
("414","Lequel de ces composants n’est pas une mémoire ?","Barrette de RAM","Graveur DVD-RW","Clé USB","Disque dur externe","B","6","23",NULL,"1",NULL,"2020-02-18 13:06:42",NULL),
("415","<h4>Une de ces séries d’octets ne correspond pas à un codage UTF-8, laquelle  ?</h4><br>","1101 0101 | 1011 1111","0011 1011","1111 0110 | 1011 0110 | 1010 1010 | 1001 0111","1110 1011 | 1101 1100 | 1011 0101","D","2","6",NULL,"1",NULL,"2020-02-18 14:33:08",NULL),
("416","<h4> Linux :</h4><br><br><br>Comment créer le répertoire rep2 ?<br><br>","mdir rep2","mkdir rep2","makedir rep2","rmdir rep2","B","6","25",NULL,"1",NULL,"2020-02-20 18:32:18",NULL),
("417","<h4>Linux</h4><br><br><br>Dans le monde Linux, pwd signifie<br>","password","power device","ping without destination","print working directory","D","6","25",NULL,"1",NULL,"2020-02-20 18:34:50",NULL),
("418","<h4> Linux </h4><br><br><br><br>Le droit de lecture r permet pour un fichier de : <br>","le consulter ou le copier","le consulter uniquement","le consulter, copier ou modifier","le consulter, copier ou exécuter","A","6","25",NULL,"1",NULL,"2020-02-20 18:37:38",NULL),
("419","<h4> Linux </h4><br><br><br><br>Si les permissions -rwxr-xr-- sont accordées au groupe gp1 pour un fichier celui-ci a les droits<br>","de lecture, écriture et exécution","de lecture et d'exécution","de lecture uniquement","d'exécution uniquement","B","6","25",NULL,"1",NULL,"2020-02-20 18:41:48",NULL),
("420","<h4> Linux </h4><br><br><br>Comment copier fich1 vers fich2 ?","copy fich1 fich2","duplicate fich1 fich2","cp fich1 > fich2","cp fich1 fich2","D","6","25",NULL,"1",NULL,"2020-02-20 18:44:37",NULL),
("421","<h4> Linux </h4><br><br><br><br>Comment se déconnecter du système en mode ligne de commandes ?<br>","exit","quit","end","bye","A","6","25",NULL,"1",NULL,"2020-02-20 18:47:15",NULL),
("422","<h4> Linux </h4><br><br><br><br>Si un fichier a les permissions -rw-r--r-- , son propriétaire a les droits","de lecture et d'exécution","de lecture et d'écriture","de lecture uniquement","d'exécution uniquement","B","6","25",NULL,"1",NULL,"2020-02-20 18:48:57",NULL),
("423","<h4> Linux</h4><br><br><br><br>A partir du répertoire courant, quelle commande faut-il taper pour retourner immédiatement à<br>son répertoire home?<br>","cd.","cd -","cd","cd home","C","6","25",NULL,"1",NULL,"2020-02-20 18:58:35",NULL),
("424","<h4> Linux </h4><br><br><br><br>Pour changer les droits d'accès à un fichier existant, je peux utiliser la commande<br>","touch","chmod","cat","echo","B","6","25",NULL,"1",NULL,"2020-02-20 19:05:36",NULL),
("425","<p>On considère la fonction suivante dont la fin est manquante.</p><br><pre><br>def mois(n) :<br>    '''<br>    Transforme un mois donné sous forme d'un entier en une chaîne de caractères où le mois est écrit en toutes lettres.<br>    param n : (int) entier compris entre 1 et 12 correspondant à un mois de l'année<br>    return (str) : chaîne de caractères correspondant au mois en toutes lettres<br>    '''<br>    dico1 = {1: 'Janv.', 2: 'Fév.', 3: 'Mars', 4: 'Avr.', 5: 'Mai', 6: 'Juin',<br>             7: 'Juil.', 8: 'Aout', 9: 'Sept.', 10: 'Oct.', 11: 'Nov.', 12: 'Déc.'}<br>    dico2 = {'Janv.': 1, 'Fév.': 2,'Mars': 3, 'Avr.': 4, 'Mai': 5, 'Juin': 6,<br>             'Juil.': 7, 'Aout': 8, 'Sept.': 9, 'Oct.': 10, 'Nov.': 11, 'Déc.':  12}<br>    return ...<br></pre><br><p>Par quel code faut-il remplacer les … après le <code>return</code> pour que la fonction soit correcte ?</p><br>","dico1(n)","mois","dico1[n]","dico2{n}","C","3","9",NULL,"1",NULL,"2020-02-23 18:33:34",NULL),
("426","<h4>Liste :</h4><br><br><br><p>Voici un programme Python :</p><br><pre><br>mat = [[6, 7], [8, 9], [10, 11]]<br>for i in range(len(mat)):<br>    print(mat[i])<br></pre><br>Quelle est la séquence imprimée ?<br>","6 &nbsp; &nbsp; 8 &nbsp; &nbsp; 10","7 &nbsp; &nbsp; 9 &nbsp; &nbsp; 11","[6, 7] &nbsp; &nbsp; [8, 9] &nbsp; &nbsp; [10, 11]","6 &nbsp; &nbsp; 7 &nbsp; &nbsp; 8 &nbsp; &nbsp; 9 &nbsp; &nbsp; 10 &nbsp; &nbsp; 11","C","3","12",NULL,"1",NULL,"2020-03-07 18:22:09",NULL),
("427","<h4>Assembleur :</h4><br><br><br><p>Voici quelques instructions de langage assembleur  :</p><br><ul><br><li><br><code>INP R0, 2</code> : entrer un nombre au clavier, et ce nombre sera stocké dans le registre 0.</li><br><li><code>MOV R1,R0</code> : Copie la valeur du registre R0 dans le registre R1.</li><br><li><code>ADD R2,R1,R0</code> : additionne les valeurs des registres R0 et R1 ; et place le résultat dans le registre R2</li><br><li><code>OUT R5,4</code> : affiche la valeur du R5 (Le 2ème paramètre 4 est la forme de sortie, 4 est la forme signée).</li><br><li><code>HALT</code> : Arrête l'exécution du programme</li><br></ul><br><p>Voici un programme assembleur:</p><br><pre><br>INP R3,2<br>MOV R4,R3<br>ADD R5,R4,R3<br>OUT R5,4<br>HALT<br></pre><br>Dans ce programme, le résultat affiché en sortie est :<br>","le double de celui entré au clavier","la somme de 4 et l'entier saisi au clavier","la somme de l'entier saisi au clavier et d'un nombre aléatoire<br>","Aucune de ces propositions","A","6","23",NULL,"1",NULL,"2020-03-29 20:52:07",NULL),
("428","<h4>Logique :</h4><br><br><br><p>On a saisi le code suivant :</p><br><pre><br>a = 5<br>b = 10<br>c = (a > 50 and b == 10)<br>d = (a == 3 or not(b != 15))<br>print('c vaut', c,'.  Et d vaut', d)<br></pre><br>Qu'affiche ce programme ?<br>","c vaut True .  Et d vaut True","c vaut False .  Et d vaut True","c vaut True .  Et d vaut False","c vaut False .  Et d vaut False","D","2","5",NULL,"1",NULL,"2020-03-07 19:13:20",NULL),
("429","<h4>Représentation d'un flottant :</h4><br><br><br><p>Soient x et y deux flottants codés sur 32 bits de cette façon :<br><br>signe : 1 bit ; &nbsp; &nbsp; exposant : 8 bits ; &nbsp; &nbsp; mantisse : 23 bits</p><br><p>x = <code>0</code>&nbsp;<code>00110000</code>&nbsp;<code>11000000000000000000000</code><br><br>y = <code>1</code>&nbsp;<code>11110000</code>&nbsp;<code>11000000000000000000000</code><br></p><br>Quatre propositions :<br><br>","x et y ont le même signe et x < y","x et y ont le même signe et x > y","x et y sont de signe différent et x < y ","x et y sont de signe différent et x > y","D","2","4",NULL,"1",NULL,"2020-03-07 18:47:56",NULL),
("430","<h4>Liste et drapeau :</h4><br><br><br><p>On a saisi le code suivant :</p><br><pre><br>def mystere(liste):<br>    drapeau = True<br>    for val in liste:<br>        if val % 2 == 0:<br>            drapeau = False<br>    return drapeau<br></pre><br>Quels booléens retournent les appels <code>mystere([11, 7, 13])</code> et <code>mystere([8, 6, 1])</code> ?<br><br>","<code>mystere([11, 7, 13])</code> retourne True et<br><code>mystere([8, 6, 1])</code> retourne True<br>","<code>mystere([11, 7, 13])</code> retourne True et<br><code>mystere([8, 6, 1])</code> retourne False","<code>mystere([11, 7, 13])</code> retourne False et<br><code>mystere([8, 6, 1])</code> retourne True","<code>mystere([11, 7, 13])</code> retourne False et<br><code>mystere([8, 6, 1])</code> retourne False","B","3","10",NULL,"1",NULL,"2020-03-07 19:13:57",NULL),
("431","<h4>Découverte des listes par compréhension</h4><br><br><br><p>Pour créer une liste de 100 éléments de valeur 0 il faut écrire :</p><br>","[0 for i in 100]]","[0 for i in range(100)]","[i for i in range(100)]","rien, en python il n'est pas nécessaire de déclarer un liste","B","3","11",NULL,"1",NULL,"2020-03-10 12:04:28",NULL),
("432","<h4>Que génère cette liste :</h4><br><br><br><p>[i+1 for i in range(0,10)]</p><br>","[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]","[1, 2, 3, 4, 5, 6, 7, 8, 9]","[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]","rien, car on ne peut pas modifier la valeur de i.","C","3","11",NULL,"1",NULL,"2020-03-10 12:07:53",NULL),
("433","<h4>Que génère cette liste : [i for i in range(0,10,2)] ?</h4><br>","[0, 2, 3, 4, 5, 6, 7, 8, 9]","[0, 2, 4, 6, 8]","[10, 8, 6, 4, 2]","[2, 3, 4, 5, 6, 7, 8, 9]","B","3","11",NULL,"1",NULL,"2020-03-10 12:11:37",NULL),
("434","<h4>[5, 6, 7, 8, 9] </h4><br><br><br><p>Pour générer cette liste que faut-il écrire ?</p><br><br>","[i for i in range(10,0,2)]","[i for i in range(5,10)]","[i for i in range(5,9)]","[i+4 for i in range(1,10)]","B","3","11",NULL,"1",NULL,"2020-03-10 12:15:47",NULL),
("435","<h4>[0, 5, 10, 15, 20]</h4><br><br><br><p>Parmi ces trois propositions, laquelle ne génère pas la liste ci-dessus ?</p>","[i for i in range(0,25,5)]","[i*4+i for i in range(0,5)]","[i*5 for i in range(0,5)]","[i//2 for i in range(0,25,10)]","D","3","11",NULL,"1",NULL,"2020-03-10 12:20:30",NULL),
("436","<h4>Si on saisit les lignes suivantes dans la console :</h4><br><br><br><br><pre><br>maListe = [10, 5, 8]<br>[i*4 for i in maListe]<br></pre><br>Que renvoie l’exécution de ce script ?<br>","[40, 20]","[20, 32, 40]","[40, 20, 32]","[[40, 20, 32]]","C","3","11",NULL,"1",NULL,"2020-03-10 12:24:29",NULL),
("437","<h4>[i for i in range (20) if i &gt; 10 and i % 2 == 1]</h4><br><br><br><p>Que génère cette liste ?</p><br><br>","[9, 11, 13, 15, 17, 19]","[10, 12, 14, 16, 18]","[11, 12, 13, 14, 15, 16, 17, 18, 19]","elle génère la même chose que [i for i in range (11,20,2)]","D","3","11",NULL,"1",NULL,"2020-03-10 12:28:45",NULL),
("438","<h4>Les Années Bissextiles</h4>
<p>Une année bissextile est divisible par 4 et non par 100 ou est divisible par 400.</p>
<p>Écrire une liste qui génère les années bissextiles de 1880 jusqu'en 2120.</p>
","<pre><code class='python'>[i for i in range(1890, 2121) if i%4==0 and not i%100==0 or i%400==0]</code></pre>","<pre><code class='python'>[i for i in range(1890, 2121) if i%4==0 and not i%100==0 and i%400==0]</code></pre>","<pre><code class='python'>[i for i in range(1890, 2121) if i%4==0 or not i%100==0 or i%400==0]</code></pre>","<pre><code class='python'>[i for i in range(1890, 2121, 4) if not i%100==0 or i%400==0]</code></pre>","A","3","11",NULL,"1",NULL,"2020-05-12 08:43:37",NULL),
("439","<pre>def myProg(n):<br>    return [d for d in range(1,n+1) if n%d==0]<br></pre><br><p>Que fait ce programme si l'on tape dans la console la commande (plusieurs réponses) :</p><br><pre>myProg(2020)</pre>","[1, 2, 2, 5, 101]","[1, 2, 4, 5, 10, 20, 101, 202, 404, 505, 1010, 2020]","[1, 2, 4, 5, 10, 20, 101, 202, 404, 505, 1010]","<br>[1, 2, 4, 5, 101]","B","3","11",NULL,"1",NULL,"2020-03-10 12:50:05",NULL),
("440","<p>On a saisi le code suivant :</p><br><pre><br>def myProg(aListe,anotherListe):<br>    return [x for x in aListe if x in anotherListe]<br></pre><br><p>Que retourne la console lorsqu'on saisit :</p><br><pre>myProg([i for i in range(10)],[2,3,5,7,11,13,17,19])</pre>","[]","[1, 2, 4, 5, 7]","[2, 3, 5, 7]","[2, 5, 10]","C","3","11",NULL,"1",NULL,"2020-03-10 13:00:18",NULL),
("441","<h4>[chr(i) for i in range(97,123)]</h4><br><br><br><p>Que génère cette liste ?</p><br><br>","[97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122]","['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']","[97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 123]","['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y']","B","3","11",NULL,"1",NULL,"2020-03-10 13:04:35",NULL),
("442","<h4>Réseau :</h4><br><br><br><p> Quel constituant permet de faire communiquer 2 postes n'appartenant pas au même réseau ? :</p><br>","Un commutateur.","Un switch.","Un routeur.","Un concentrateur.","C","6","24",NULL,"1",NULL,"2020-03-11 08:20:17",NULL),
("443","<h4>Soit l'arborescence d'un système Linux ci-dessous :</h4><br><br><br><p>Indiquez le chemin absolu permettant d'accéder au répertoire contenant le fichier DR.pdf :</p><br>","/home/Jacky/Cours","home/Jacky/Cours","../../../Cours","/Cours","A","6","25",NULL,"1","443_arborescence2.png","2020-03-11 08:55:05",NULL),
("444","<h4>Soit l'arborescence Linux ci dessous :</h4><br><br><br><p> Le répertoire courant est 'TP', indiquez le chemin relatif permettant d'accéder au répertoire 'bin':</p><br>","/../../../bin","../../../bin","Jacky/home/bin","../Jacky/home/bin","B","6","25",NULL,"1","444_arborescence2.png","2020-03-11 08:54:25",NULL),
("445","<p>Un programmeur Java peu scrupuleux a écrit ce code sans commentaires:</p><br><pre><br>class Main {<br>    public static void main(String[] args) {<br>        int i = 1, n = 10, t1 = 0, t2 = 1;<br>        while (i <= n) {<br>            System.out.print(t1 + ' ; ');<br>            int sum = t1 + t2;<br>            t1 = t2;<br>            t2 = sum;<br>            i = i + 1;<br>        }<br>    }<br>}<br></pre><br><p>Qu'affiche son programme ?</p><br>","0 ; 1 ; 2 ; 3 ; 4 ; 5 ; 6 ; 7 ; 8 ; 9 ; 10 ;    (c'est-à-dire tous les nombres allant de 0 à 10)<br>","55 ;           (c'est-à-dire la somme des nombres allant de 0 à 10) ","0 ; 1 ; 1 ; 2 ; 3 ; 5 ; 8 ; 13 ; 21 ; 34 ;        (c'est-à-dire les 10 premiers nombres de la suite de Fibonacci)","34 ;          (c'est-à-dire le 10ième nombre de la suite de Fibonacci)","C","7","29",NULL,"1",NULL,"2020-03-11 16:05:15",NULL),
("446","<p>En mathématiques, la factorielle d'un entier naturel <i>n</i>, notée <i>n!</i>, est le produit des nombres entiers strictement positifs inférieurs ou égaux à <i>n</i>.</p><br><p align='center'><i>n!</i> = 1 x 2 x 3 x .... x (<i>n-1</i>) x <i>n</i></p><br><p>Un programmeur a écrit en PHP le code suivant qui demande un nombre <i>n</i> et affiche <i>n!</i>, mais son programme ne fonctionne pas.</p><br><xmp><br>ligne    code<br>----     ----<br>1        <?php<br>2        echo 'Entrez un nombre\n';<br>3        $n = rtrim(fgets(STDIN));<br>4        $fact = 0;<br>5        for ($i = 1; $i <= $n; $i = $i+1) { <br>6          $fact = $fact * $i;<br>7        }<br>8        echo $n.'! = '.$fact;<br>9        ?><br></xmp><br><p>Quelle correction faut il apporter à son programme pour qu'il fonctionne correctement ?</p><br>","remplacer la ligne 4 par:<br>$fact = 1;","remplacer la ligne 5 par:<br>for ($i = 1; $i < $n; $i = $i+1) {<br>","remplacer la ligne 5 par:<br>for ($i = 0; $i <= $n; $i = $i+1) {","remplacer la ligne 6 par:<br>$fact = $i;","A","7","29",NULL,"1",NULL,"2020-03-11 18:34:52",NULL),
("447","<p>Un professeur de NSI demande à trois élèves d'écrire un programme Python qui calcule la somme des nombres entiers de 0 à 100. Maxime, Julie et Pablo font trois programmes qui fonctionnent parfaitement mais écrits dans des styles (ou paradigmes) de programmation vraiment différents.</p><br><h4>Programme de Maxime :</h4><br><pre><br>cent = 100<br>somme = 0<br>for i in range(cent+1):<br>    somme = somme + i<br>print(somme)<br></pre><br><h4>Programme de Julie:</h4><br><pre><br>def somme(n):<br>    if n == 0: return 0<br>    else: return n + somme(n-1)<br><br>cent = 100<br>print(somme(cent))<br></pre><br><h4>Programme de Pablo:</h4><br><pre><br>class Nombre():<br>    def __init__(self, n):<br>        self.value = n<br>    def somme(self):<br>        s = 0<br>        for i in range(self.value + 1):<br>            s = s + i<br>        return s<br><br>cent = Nombre(100)<br>print(cent.somme())<br></pre><br><p>Laquelle de ces affirmations est vraie ?</p><br>","Le paradigme de programmation utilisé par Maxime est appelé fonctionnel, celui de Julie est orienté objet et celui de Pablo est impératif.","Le paradigme de programmation utilisé par Maxime est appelé impératif, celui de Julie est fonctionnel et celui de Pablo est orienté objet.","Le paradigme de programmation utilisé par Maxime est appelé fonctionnel, celui de Julie est impératif et celui de Pablo est orienté objet.","Le paradigme de programmation utilisé par Maxime est appelé orienté objet, celui de Julie est impératif et celui de Pablo est fonctionnel.","B","7","29",NULL,"1",NULL,"2020-03-12 09:58:19",NULL),
("448","<h4>RÉSEAUX</h4><br><br><br><br>Sur un réseau local, le protocole qui permet d’associer l’adresse IP à l’adresse MAC d’une machine de ce réseau est :<br>","Le protocole IP","Le protocole ARP","Le protocole TCP","Le protocole HTTP","B","6","24",NULL,"1",NULL,"2020-03-19 13:29:19",NULL),
("449","<h4>RÉSEAUX</h4><br><br><br>Quel type de serveur associe un nom de domaine avec une adresse IP ?<br>","un serveur Web","un serveur HTTP","un serveur DNS","un serveur FTP","C","6","24",NULL,"1",NULL,"2020-03-19 13:31:15",NULL),
("450","<h4>RÉSEAUX</h4><br><br><br>Si la machine que je veux contacter n’est pas sur le même réseau que le mien, il faut que mon réseau dispose d’un :","Hub (concentrateur)","Switch (commutateur)","Routeur","Carte d’interface","C","6","24",NULL,"1",NULL,"2020-03-19 13:33:30",NULL),
("451","RÉSEAUX</h4><br><br><br>L’identification « classless » (sans classe) d’une machine est la suivante : 192.168.1.144/24. Cela veut dire que :","L’adresse IP est 192.168.1.144 et qu’il y a un maximum de 24 machines sur mon réseau","L’adresse IP est 192.168.1.6 (144/24)","L’adresse IP est 192.168.1.144 et le masque est 24","L’adresse IP est 192.168.1.144 et le masque est 255.255.255.0","D","6","24",NULL,"1",NULL,"2020-03-19 13:38:00",NULL),
("452","<h4>RÉSEAUX</h4><br><br><br>Laquelle de ces adresses IP est valide pour identifier une machine précise sur un réseau ?<br>","172.90.90.0/16","172.90.90.1/16","172.90.90.255/16","255.255.255.0/16","B","6","24",NULL,"1",NULL,"2020-03-19 13:39:56",NULL),
("453","<h4>RÉSEAUX</h4><br><br><br>Que se passe-t-il si le TTL d’un paquet devient égal à 0 ?<br>","Le paquet est arrivé à destination correctement.","Le paquet est arrivé à destination, mais il y a une erreur.","Le paquet est détruit.","Le paquet est renvoyé à l’expéditeur (source).","C","6","24",NULL,"1",NULL,"2020-03-19 13:41:59",NULL),
("454","<h4>IHM Web</h4><br><br><br>Dans un souci de débogage, on peut, en JavaScript, afficher la valeur d’une variable ‘test’ dans la console du navigateur. Pour cela, on utilise la commande :","console.log(‘test’);","console.log(test);","alert(test);","alert(‘test’)","B","5","19",NULL,"1",NULL,"2020-03-19 14:08:44",NULL),
("455","<h4>IHM Web</h4><br><br><br>Quelle est la méthode JavaScript qui permet de récupérer le contenu d’un élément « input » de type « text » dont l’identifiant est « essai »?","document.getElementById('essai').innerHTML ","document.getElementsByName('essai')","document.getElementById('essai').value ","document.getElementById('input').value ","C","5","19",NULL,"1",NULL,"2020-03-19 14:11:01",NULL),
("456","Pour inclure du code Javascript dans une page web, on peut utiliser une syntaxe du type :","<code>&lt;link src='...'/&gt;</code>","<code>&lt;link href='...'&gt; &lt;/link &gt;</code>","<code>&lt;script src='...'&gt; &lt;/script &gt;</code>","<code>&lt;script href='...'/&gt;</code>","C","5","19",NULL,"1",NULL,"2020-03-20 09:43:26",NULL),
("457","Le DOM est ...","... une représentation d'une page web qui est spécifique à chaque navigateur.","... une représentation de la structure d'une page web sous la forme d'une hiérarchie de nœuds.","... une modélisation d'une page web sous la forme d'un tableau javascript.","... une interface web pour gérer des objets connectés en domotique.","B","5","19",NULL,"1",NULL,"2020-03-20 09:48:54",NULL),
("458","On considère la partie de code d'une page web ci-dessous :<br><code><pre><br>&lt;body&gt;<br>  &lt;h1 id='titre'&gt;Interagir avec une page web en Javascript&lt;/h1&gt;<br>  &lt;p class='para' id='p1'&gt;1er paragraphe avec un &lt;a href='https://fr.wikipedia.org/wiki/JavaScript'&gt;lien&lt;/a&gt;...&lt;/p&gt;<br>  &lt;p class='para' id='p2'&gt;2nd paragraphe...&lt;/p&gt;<br>&lt;/body&gt;<br></pre><br></code><br>Parmi les instructions ci-dessous, laquelle ne permet pas d'accéder à l'élément identifié par 'p1' ?","<code>let p1 = document.getElementsByTagName('p')[0];</code>","<code>let p1 = document.querySelector('#p1');</code>","<code>let p1 = document.getElementById('p1');</code>","<code>let p1 = document.getElementsByClassName('para')[1];</code>","D","5","19",NULL,"1",NULL,"2020-03-20 10:13:44",NULL),
("459","On considère la portion de code html ci dessous :<br><code><br><pre><br>&lt;ul id='pokemons'&gt;<br>  &lt;li&gt;Pikatchu&lt;/li&gt;<br>  &lt;li&gt;Krabby&lt;/li&gt;<br>  &lt;li&gt;Tortank&lt;/li&gt;<br>&lt;/ul&gt;<br></pre><br></code><br>Que permet de faire la portion de code Javascript ci-dessous qui lui est associée ?<br><code><br><pre><br>let pokemons = document.getElementById('pokemons');<br>pokemons.innerHTML += '&lt;li&gt;Salamèche&lt;/li&gt;';<br></pre><br></code>","Elle permet de remplacer la liste à puces par la liste : <br><ul><li>Salamèche</li></ul>","Elle permet de rajouter, au début de la liste à puces, la puce :<br><ul><li>Salamèche</li></ul>","Elle permet de rajouter, à la fin de la liste à puces, la puce :<br><ul><li>Salamèche</li></ul>","Elle permet de rajouter, après la liste à puces, le texte : &lt;li&gt;Salamèche&lt;/li&gt;","C","5","19",NULL,"1",NULL,"2020-03-20 10:59:13",NULL),
("460","<p>Combien d'entiers positifs ou nuls (entiers non signés) peut-on représenter en machine sur 32 bits ?</p><br>","<span class='math inline'>2<sup>32</sup> − 1</span>","<span class='math inline'>2<sup>32</sup></span>","<span class='math inline'>2 × 32</span>","<span class='math inline'>2 × 32</span>","B","2","2",NULL,"1",NULL,"2020-03-23 22:41:08",NULL),
("461","<p>Quel est un avantage du codage UTF8 par rapport au codage ASCII ?</p>","il permet de coder un caractère sur un octet au lieu de deux","il permet de coder les majuscules","il permet de coder tous les caractères","il permet de coder différentes polices de caractères","C","2","6",NULL,"1",NULL,"2020-03-23 22:42:57",NULL),
("462","<p>Quelle est la représentation en binaire signé en complément à 2 de l’entier <span class='math inline'> − 1</span> sur un octet ?</p>","1000 0000","1000 0001","1111 1110","1111 1111","D","2","3",NULL,"1",NULL,"2020-03-24 08:56:39",NULL),
("463","<p>Quelle est la plage des valeurs entières (positifs ou négatifs) que l'on peut coder sur un octet (8 bits) en complément à 2 ?</p>","-127 à 128","-128 à 127","-255 à 128","-256 à 127","B","2","3",NULL,"1",NULL,"2020-03-24 08:57:54",NULL),
("464","<p>Dans quel système de numération 3F5 représente-t-il un nombre entier ?</p>","binaire (base 2)","octal (base 8)","décimal (base 10)","hexadécimal (base 16)","D","2","2",NULL,"1",NULL,"2020-03-24 08:59:00",NULL),
("465","<p>Parmi les quatre propositions, quelle est celle qui correspond au résultat de l'addition en écriture binaire 1101 1001 + 11 0110 ?</p>","1000 1111","10 0000 1111","1 0000 1111","1 1000 0111","C","2","2",NULL,"1",NULL,"2020-03-24 09:00:31",NULL),
("466","<p>On définit : L = [1,2,3,4,5,6]</p><br><p>Quelle est la valeur de L[3] ?</p>","[1,2,3]","3","4","[4,5,6]","C","3","10",NULL,"1",NULL,"2020-03-24 09:01:50",NULL),
("467","<p>On définit : T = [[1,2,3], [4,5,6], [7,8,9]]</p><br><p>Laquelle des expressions suivantes a pour valeur 7 ?</p>","T[3,1]","T[3][1]","T[2,0]","T[2][0]","D","3","12",NULL,"1",NULL,"2020-03-24 09:03:04",NULL),
("468","<p>Quelle est la valeur de l'expression [(i,i+1) for i in range(2)] ?</p>","[0,1,1,2]","[(1,2),(2,3)]","[(0,1),(1,2)]","[[0,1],[1,2]]","C","3","11",NULL,"1",NULL,"2020-03-24 09:04:19",NULL),
("469","<p>Quelle est l'expression qui a pour valeur la liste [1,4,9,16,25,36] ?</p>","{ n*n for n in range(1,7) }","{ n*n for n in range(6) }","[ n*n for n in range(1,7) ]","[ n*n for n in range(6) ]","C","3","11",NULL,"1",NULL,"2020-03-24 09:09:27",NULL),
("470","<p>Si la variable note est définie par  note = ['do','ré','mi','fa','sol','la','si'] alors :</p>","l'index de 'sol' est 5","l'index de note est 0","l'index de 'si' est 7","l'index de 'mi' est 2","D","3","10",NULL,"1",NULL,"2020-03-24 09:11:07",NULL),
("471","<p>On définit une grille G remplie de 0, sous la forme d'une liste de listes, où toutes les sous-listes ont le même nombre d'éléments.</p><br><p>G = [ [0, 0, 0, …, 0],</p><br><p>[0, 0, 0, …, 0],</p><br><p>[0, 0, 0, …, 0],</p><br><p>……</p><br><p>[0, 0, 0, …, 0] ]</p><br><p>On appelle <em>hauteur</em> de la grille le nombre de sous-listes contenues dans G et <em>largeur</em> de la grille le nombre d'éléments dans chacune de ces sous-listes. Comment peut-on les obtenir ?</p>","<p>hauteur = len(G[0])</p><br><p>largeur = len(G)</p>","<p>hauteur = len(G)</p><br><p>largeur = len(G[0])</p>","<p>hauteur = len(G[0])</p><br><p>largeur = len(G[1])</p>","<p>hauteur = len(G[1])</p><br><p>largeur = len(G[0])</p>","B","3","12",NULL,"1",NULL,"2020-03-24 09:12:24",NULL),
("472","<p>On définit :</p><br><p>T = [ {'fruit': 'banane', 'nombre': 25}, {'fruit': 'orange', 'nombre': 124},</p><br><p>{'fruit': 'pomme', 'nombre': 75}, {'fruit': 'kiwi', 'nombre': 51} ]</p><br><p>Quelle expression a-t-elle pour valeur le nombre de pommes ?</p>","T[2]['nombre']","T[2,'nombre']","T[3]['nombre']","T[3,'nombre']","A","4","14",NULL,"1",NULL,"2020-03-24 09:18:02",NULL),
("473","<p>Que réalise l'instruction suivante :</p><br><p>mon_fichier = open('exemple.txt', 'r')</p>","Elle permet d’ouvrir le fichier 'exemple.txt' en mode lecture si le fichier est dans le même dossier que le fichier du programme Python comportant cette instruction.","Elle permet d’ouvrir le fichier 'exemple.txt' en mode lecture même si le fichier n’est pas dans le même dossier que le fichier du programme Python comportant cette instruction.","Elle permet d’ouvrir le fichier 'exemple.txt' en mode écriture si le fichier est dans le même dossier que le fichier du programme Python comportant cette instruction.","Elle permet d’ouvrir le fichier 'exemple.txt' en mode écriture même si le fichier n’est pas dans le même dossier que le fichier du programme Python comportant cette instruction.","A","4","18",NULL,"1",NULL,"2020-03-24 09:19:19",NULL),
("474","<p>Laquelle de ces affirmations est vraie ?</p>","on ne peut accéder au contenu d'un fichier CSV que par l'intermédiaire d'un programme Python","CSV est un format de chiffrement des données","le format CSV a été conçu pour asssurer la confidentialité d'une partie du code d'un programme","les fichiers CSV sont composés de données séparées par des caractères comme des virgules","D","4","18",NULL,"1",NULL,"2020-03-24 09:20:28",NULL),
("475","<p>On considère la table suivant :</p><br><code>t = [ {'type': 'marteau', 'prix': 17, 'quantité': 32}, {'type': 'scie', 'prix': 24, 'quantité': 3}, {'type': 'tournevis', 'prix': 8, 'quantité': 45} ]</code><br><p>Quelle expression permet d'obtenir la quantité de scies ?</p>","t[2]['quantité']","t[1]['quantité']","t['quantité'][1]","t['scies']['quantité']","B","4","15",NULL,"1",NULL,"2020-03-24 09:22:42",NULL),
("476","<p>On considère la liste de p-uplets suivante :</p><br><code>table = [ ('Grace', 'Hopper', 'F', 1906),<br /><br>('Tim', 'Berners-Lee', 'H', 1955),<br /><br>('Ada', 'Lovelace', 'F', 1815),<br /><br>('Alan', 'Turing', 'H', 1912) ]</code><br><p>où chaque p-uplet représente un informaticien ou une informaticienne célèbre ; le premier élément est son prénom, le deuxième élément son nom, le troisième élément son sexe (‘H’ pour un homme, ‘F’ pour une femme) et le quatrième élément son année de naissance (un nombre entier entre 1000 et 2000).</p><br><p>On définit une fonction :</p><br><pre><br>def fonctionMystere(table):<br>    mystere = []<br>    for ligne in table:<br>        if ligne[2] == 'F':<br>            mystere.append(ligne[1])<br>    return mystere<br></pre><br><p>Que vaut <code>fonctionMystere(table)</code>?</p>","[‘Grace’, ‘Ada’]","[('Grace', 'Hopper', 'F', 1906), ('Ada', 'Lovelace', 'F', 1815)]","['Hopper', 'Lovelace']","[]","C","4","15",NULL,"1",NULL,"2020-03-24 09:30:29",NULL),
("477","<p>Quelle est la valeur de la variable table à la fin de l'exécution du script suivant :</p><br><p>table = [[1, 2, 3], [1, 2, 3], [1, 2, 3], [1, 2, 3]]</p><br><p>table [1][2] = 5</p>","[[1, 5, 3], [1, 2, 3], [1, 2, 3], [1, 2, 3]]","[[1, 2, 3], [5, 2, 3], [1, 2, 3], [1, 2, 3]]","[[1, 2, 3], [1, 2, 5], [1, 2, 3], [1, 2, 3]]","[[1, 2, 3], [1, 2, 3], [1, 2, 3], [1, 5, 3]]","C","4","14",NULL,"1",NULL,"2020-03-24 09:31:44",NULL),
("478"," <p>Quel est le code HTML correct pour créer un hyperlien vers le site Eduscol ?</p>","<pre><code class='html'>&lt;a url='https://www.eduscol.education.fr/'&gt; site Eduscol &lt;/a&gt;</code></pre>","<pre><code class='html'>&lt;a name='https://www.eduscol.education.fr/'&gt; site Eduscol &lt;/a&gt;</code></pre>","<pre><code class='html'>&lt;a href='https://www.eduscol.education.fr/'&gt; site Eduscol &lt;/a&gt;</code></pre>","<pre><code class='html'>&lt;a&gt; https://www.eduscol.education.fr/ &lt;/a&gt; site Eduscol</code></pre>","C","5","19",NULL,"1",NULL,"2020-05-12 14:21:18",NULL),
("479","<p>Lors de la consultation d’une page HTML, contenant un bouton auquel est associée la fonction javascript suivante, que se passe-t-il quand on clique sur ce bouton ?</p><br><pre><br>function action(event) {<br>    this.style.color = 'red'<br>}<br></pre>","le pointeur de souris devient rouge lorsqu'il arrive sur le bouton","le texte du bouton devient rouge","le texte du bouton est remplacé par le mot 'red'","le texte de la page passe en rouge","B","5","19",NULL,"1",NULL,"2020-03-24 09:35:03",NULL),
("480","<p>Quelle balise HTML permet de créer des liens entre pages ?</p>","&lt;r&gt;","&lt;l&gt;","&lt;link&gt;","&lt;a&gt;","D","5","19",NULL,"1",NULL,"2020-03-24 09:36:00",NULL),
("481","<p>Quelle est la fonction principale d’un fichier CSS ?</p><br>","Définir le style d’une page web","Assurer l’interaction entre une page web et l’utilisateur","Créer une page web dynamique","Créer un bouton dans une page web","A","5","19",NULL,"1",NULL,"2020-03-24 09:37:00",NULL),
("482","<p>Quelle URL parmi les suivantes témoigne que l'échange entre le navigateur et le serveur est chiffré ?</p><br>","http://www.mabanque.com/","http://www.mabanque.fr/","https://www.mabanque.fr/","http://secure.mabanque.fr/","C","5","20",NULL,"1",NULL,"2020-03-24 09:37:56",NULL),
("483","<p>Quel langage est interprété ou exécuté côté serveur ?</p>","JavaScript","PHP","HTML","CSS","B","5","20",NULL,"1",NULL,"2020-03-24 09:38:55",NULL),
("484","<p>Dans la console Linux, quelle commande faut-il exécuter pour obtenir la documentation sur la commande pwd ?</p><br>","man pwd","cd pwd","mkdir pwd","ls pwd","A","6","25",NULL,"1",NULL,"2020-03-24 09:39:55",NULL),
("485","<p>Dans un établissement scolaire, tous les ordinateurs sont reliés au réseau local par l'intermédiaire de câbles Ethernet. Il n'existe pas de liaisons sans fil. Dans chaque salle d'ordinateurs, les machines sont reliées à un commutateur. Chaque commutateur est relié par un câble jusqu'à la salle où se situe le serveur contrôleur de domaine et la passerelle internet (routeur).</p><br><p>Vous êtes connectés sur un ordinateur d'une de ces salles d'ordinateurs avec votre classe. Tout à coup, plus personne n'a accès à Internet, mais toutes les ressources locales de l'établissement sont toujours accessibles.</p><br><p>Parmi ces quatre propositions, laquelle est la plus vraisemblable ?</p>","Un de vos camarades a débranché accidentellement le câble Ethernet de votre machine.","Le routeur de l'étage est indisponible (il a été débranché ou est en panne).","Le commutateur de la salle est indisponible (il a été débranché ou est en panne).","La passerelle internet de l'établissement est indisponible (elle a été débranchée ou est en panne).","D","6","24",NULL,"1",NULL,"2020-03-24 09:43:22",NULL),
("486","<p>Lequel de ces périphériques n'est pas un périphérique d'entrée ?</p>","le moniteur","le clavier","la souris","le scanner","A","6","26",NULL,"1",NULL,"2020-03-24 09:44:43",NULL),
("487","<p>Dans la console Linux, étant positionné dans le répertoire /home/marcelH/travail, quelle commande faut-il exécuter pour remonter dans l'arborescence vers le répertoire /home/marcelH ?</p>","cd .","cd ..","cd ...","cd /../.","B","6","25",NULL,"1",NULL,"2020-03-24 09:45:29",NULL),
("488","<p>Dans un système Linux, on dispose d'un répertoire racine contenant deux répertoires <code>documents</code> et <code>sauvegardes</code>. On se trouve dans le répertoire documents où figure un fichier NSI.txt.</p><br><p>Quelle commande permet de créer une copie nommée NSI2.txt de ce fichier dans le répertoire sauvegardes ?</p>","cp NSI.txt NSI2.txt","cp NSI.txt sauvegardes/NSI2.txt","cp NSI.txt ../NSI2.txt","cp NSI.txt ../sauvegardes/NSI2.txt","D","6","25",NULL,"1",NULL,"2020-03-24 09:47:10",NULL),
("489","<p>La commande suivante vient d'être exécutée en ligne de commande sous Linux :</p><br><pre>cp /users/luc/interro.txt ./</pre><br><p>Que réalise cette commande ?</p>","copie du fichier users vers le répertoire luc","copie du fichier interro.txt vers le répertoire luc","copie du fichier interro.txt vers le répertoire courant","copie du fichier interro.txt vers le répertoire users","C","6","25",NULL,"1",NULL,"2020-03-24 09:48:15",NULL),
("490","<p>La documentation de la bibliothèque random de Python précise :</p><br><p><strong>random.randint(a, b)<br /><br>Renvoie un entier aléatoire N tel que a <= N <= b.</strong></p><br><p>Quelle est l’expression Python permettant de simuler le tirage d’un dé à 6 faces après avoir exécuté <code>import random</code> ?</p>","random.randint(6)","random.randint(1,6)","random.randint(1,7)","random.randint(0,6)","B","7","32",NULL,"1",NULL,"2020-03-24 09:50:05",NULL),
("491","<p>On a écrit une fonction qui prend en paramètre une liste non vide et qui renvoie son plus grand élément. Combien de tests faudrait-il écrire pour garantir que la fonction donne un résultat correct pour toute liste ?</p>","deux tests : pour une liste à un élément et pour une liste à deux éléments ou plus","deux tests : pour le cas où le plus grand élément est en début de liste, et pour le cas où le plus grand élément n’est pas en début de liste","trois tests : pour une liste vide, pour une liste à un élément, et pour une liste à deux éléments ou plus","il faudrait écrire une infinité de tests : on ne peut pas prouver que cette fonction est correcte, simplement en la testant","D","7","31",NULL,"1",NULL,"2020-03-24 09:51:14",NULL),
("492","<p>La fonction Python suivante ne calcule pas toujours correctement le résultat de <span class='math inline'><em>x</em><sup><em>y</em></sup></span> pour des arguments entiers. Parmi les tests suivants, lequel va permettre de détecter l’erreur ?</p>
<pre>
def puissance (x,y):
    p = x
    for i in range (y - 1):
        p = p * x
    return p
</pre>","puissance(2,0)","puissance(2,1)","puissance(2,2)","puissance(2,10)","A","7","31",NULL,"1",NULL,"2020-04-28 07:27:40",NULL),
("493","<p>En Python, quelle est la méthode pour charger la fonction sqrt du module math ?</p>","using math.sqrt","#include math.sqrt","from math include sqrt","from math import sqrt","D","7","32",NULL,"1",NULL,"2020-03-24 09:54:19",NULL),
("494","<p>On définit deux fonctions :</p><br><pre><br>def f(x):<br>    y = 2*x + 1<br>    return y<br></pre><br><pre><br>def calcul(x):<br>    y = x - 1<br>    return f(y)<br></pre><br><p>Quelle est la valeur renvoyée par l'appel <code>calcul(5)</code> ?</p>","4","9","11","19","B","7","28",NULL,"1",NULL,"2020-03-24 09:56:42",NULL),
("495","<p>Avec la définition de fonction capital_double suivante, que peut-on toujours affirmer à propos du résultat n retourné par la fonction ?</p><br><pre><br>def capital_double (capital, interet):<br>    montant = capital<br>    n = 0<br>    while montant <= 2 * capital:<br>        montant = montant + interet<br>        n = n + 1<br>    return n<br></pre>","<code>n == capital / interet</code>","<code>capital * n * interet > 2 * capital</code>","<code>capital + n * interet > 2 * capital</code>","<code>n == 2 * capital / interet</code>","C","7","30",NULL,"1",NULL,"2020-03-24 10:00:29",NULL),
("496","<p>L'algorithme suivant permet de calculer la somme des N premiers entiers, où N est un nombre entier donné :</p><br><pre>i =0<br>somme =0<br>while i < N :<br>    i = i +1<br>    somme = somme + i<br></pre><br><p>Un invariant de boucle de cet algorithme est le suivant :</p>","somme = 0 + 1 + 2 + ... + i    et i < N","somme = 0 + 1 + 2 + ... + N    et i < N","somme = 0 + 1 + 2 + ... + i    et i < N+1","somme = 0 + 1 + 2 + ... + N    et i < N+1","C","8","39",NULL,"1",NULL,"2020-03-24 14:29:24",NULL),
("497","<p>Quelle est la valeur de c à la fin de l'exécution du code suivant :</p><br><pre><br>L = [1,2,3,4,1,2,3,4,0,2]<br>c = 0<br>for k in L:<br>    if k == L[1]:<br>        c = c+1<br></pre>","0","2","3","10","C","8","34",NULL,"1",NULL,"2020-03-24 10:12:28",NULL),
("498","<p>On considère la fonction Python suivante, qui prend en argument une liste L et renvoie le maximum des éléments de la liste :</p><br><pre>def rechercheMaximum(L):<br>    max = L[0]<br>    for i in range(len(L)):<br>       if L[i] > max:<br>           max = L[i]<br>    return max<br></pre><br><p>On note <span class='math inline'><em>n</em></span> la taille de la liste.</p><br><p>Quelle est la complexité en nombre d’opérations de l’algorithme ?</p>","constante, c’est-à-dire ne dépend pas de <span class='math inline'><em>n</em></span>","linéaire, c’est-à-dire de l’ordre de <span class='math inline'><em>n</em></span>","quadratique, c’est-à-dire de l’ordre de <span class='math inline'><em>n</em>²</span>","cubique, c’est-à-dire de l’ordre de <span class='math inline'><em>n</em><sup>3</sup></span>","B","8","34",NULL,"1",NULL,"2020-03-24 10:15:16",NULL),
("499","<p>Quelle est la valeur du couple (s,i) à la fin de l'exécution du script suivant ?</p><br><pre><br>s = 0<br>i = 1<br>while i < 5:<br>    s = s + i<br>    i = i + 1<br></pre>","(4, 5)","(10, 4)","(10, 5)","(15, 5)","C","8","39",NULL,"1",NULL,"2020-03-24 10:17:06",NULL),
("500","<p>Pour trier par sélection une liste de 2500 entiers, le nombre de comparaisons nécessaires à l’algorithme est de l’ordre de :</p>","<span class='math inline'><math><br><br> <msqrt><br>    <mi>2500</mi><br>  </msqrt> <br><br></math></span>","<span class='math inline'>2500</span>","<span class='math inline'>2500<sup>2</sup></span>","<span class='math inline'>2<sup>2500</sup></span>","C","8","39",NULL,"1",NULL,"2020-03-24 10:20:54",NULL),
("501","<p>En utilisant une recherche dichotomique, combien faut-il de comparaisons pour trouver une valeur dans un tableau trié de 1000 nombres ?</p>","3","10","1000","1024","B","8","39",NULL,"1",NULL,"2020-03-24 10:22:11",NULL),
("502","<p>Pour envoyer un message reçu uniquement au bon ordinateur, un switch (ou commutateur) lit dans le message (et donc désencapsule la couche correspondante) : </p><br><br>","La totalité du message.","Les adresses Ethernet (ou adresses MAC).","Il ne lit pas le message, il envoie le message sur tous ses ports.","Les adresses IP.","B","6","24",NULL,"1",NULL,"2020-03-30 08:18:42",NULL),
("503","<p>Pour envoyer un message reçu au bon réseau, un routeur lit dans le message (et donc désencapsule la couche correspondante) :</p><br>","La totalité du message.","Les adresses Ethernet (ou adresses MAC).","Il ne lit pas le message, il l'envoie sur tous ces ports.","Les adresses IP.","D","6","24",NULL,"1",NULL,"2020-03-30 08:19:07",NULL),
("504","<p>En observant un échange de message avec Wireshark, le destinataire d'une requête de page web est adressé par : 128.93.164.1:80</p><br><p> Dire à quoi sert le port 80 :</p><br><br>","Il permet d'accéder à la machine hébergeant le serveur web.","Il permet d'accéder à une page particulière de site web.","Il indique qu'une erreur est survenue.","Il permet de définir le serveur demandé au sein de la machine.","D","6","24",NULL,"1",NULL,"2020-03-30 08:19:33",NULL),
("577","<p>Comment s'écrit en base 16 (en hexadécimal) le nombre dont l'écriture binaire est 0010 1100 ?</p><br>","<p>1D</p><br>","<p>2C</p><br>","<p>3C</p><br>","<p>3E</p><br>","B","2","7",NULL,"1",NULL,"2020-04-13 15:37:35",NULL),
("578","<br><br><p>Parmi les noms suivants, lequel <strong>n'est pas</strong> celui d'une méthode d'encodage des caractères ?</p><br>","<br><br><p>UTF-16</p><br>","<br><br><p>ASCII</p><br>","<br><br><p>Arial</p><br>","<br><br><p>Unicode</p><br>","C","2","7",NULL,"1",NULL,"2020-03-25 13:22:02",NULL),
("579","<br><br><p>Dans quel système de numération 3F5 représente-t-il un nombre entier&nbsp;?</p><br>","<br><br><p>binaire (base 2)</p><br>","<br><br><p>octal (base 8)</p><br>","<br><br><p>décimal (base 10)</p><br>","<br><br><p>hexadécimal (base 16)</p><br>","D","2","7",NULL,"1",NULL,"2020-03-25 13:22:10",NULL),
("580","<br><br><p>Quelle est la représentation binaire de l'entier positif 51 sur 8 bits ?</p><br>","<br><br><p>0010 0001</p><br>","<br><br><p>0010 1001</p><br>","<br><br><p>0011 0001</p><br>","<br><br><p>0011 0011</p><br>","D","2","7",NULL,"1",NULL,"2020-03-25 13:22:18",NULL),
("581","<br><br><p>On considère les nombres dont l'écriture en base 16 (en hexadécimal) sont de la forme suivante : un 1 suivi de 0 en nombre quelconque, comme 1, 10, 100, 1000 etc.</p><br><p>Tous ces nombres sont exactement :</p><br>","<br><br><p>les puissances de 2</p><br>","<br><br><p>les puissances de 8</p><br>","<br><br><p>les puissances de 10</p><br>","<br><br><p>les puissances de 16</p><br>","D","2","7",NULL,"1",NULL,"2020-03-25 13:22:27",NULL),
("582","<br><br><p>Quel est le résultat de l'addition binaire 0010&nbsp;0110 + 1000&nbsp;1110 ?</p><br>","<br><br><p>1010 1110</p><br>","<br><br><p>0000 0110</p><br>","<br><br><p>1011 0100</p><br>","<br><br><p>0101 0001</p><br>","C","2","7",NULL,"1",NULL,"2020-03-25 13:22:35",NULL),
("583","<p>On exécute le script suivant :</p><br><pre><code class='python'>L = [12,0,8,7,3,1,5,3,8]<br>a = [elt for elt in L if elt<4]</code></pre><br><p>Quelle est la valeur de a à la fin de son exécution ?</p><br>","<pre><code class='python'>[12,0,8]</code></pre><br>","<pre><code class='python'>[12,0,8,7]</code></pre><br>","<pre><code class='python'>[0,3,1,3]</code></pre><br>","<pre><code class='python'>[0,3,1]</code></pre><br>","C","3","13",NULL,"1",NULL,"2020-04-13 15:38:34",NULL),
("584","<br><p>Quelle est la valeur de l'expression suivante ?</p><br><pre><code class='python'>[ 2*k + 1 for k in range(4) ]</code></pre><br>","<br><pre><code class='python'>[1,3,5,7]</code></pre><br>","<br><pre><code class='python'>[0,1,2,3]</code></pre><br>","<br><pre><code class='python'>[3,5,7,9]</code></pre><br>","<br><pre><code class='python'>[1,2,3,4]</code></pre><br>","A","3","13",NULL,"1",NULL,"2020-04-13 15:39:50",NULL),
("585","<br><p>On définit ainsi une liste <code>M</code> :</p><br><pre><code class='python'>M = [['A','B','C','D'], ['E','F','G','H'], ['I','J','K','L']]</code></pre><br><p>Que vaut l'expression suivante ?</p><br><pre><code class='python'>M[2][1]</code></pre><br>","<br><pre><code class='python'>'G'</code></pre><br>","<br><pre><code class='python'>'J'</code></pre><br>","<br><pre><code class='python'>'E'</code></pre><br>","<br><pre><code class='python'>'B'</code></pre><br>","B","3","13",NULL,"1",NULL,"2020-04-13 15:41:35",NULL),
("586","
<p>Quel est le résultat de l'évaluation de l'expression Python suivante ?</p>
<pre><code>[ n * n for n in range(10) ]</code></pre>
","
<p>[0, 1, 4, 9, 16, 25, 36, 49, 64, 81]</p>
","
<p>[0, 1, 4, 9, 16, 25, 36, 49, 64, 81, 100]</p>
","
<p>[0, 2, 4, 8, 16, 32, 64, 128, 256, 512]</p>
","
<p>[0, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024]</p>
","A","3","13",NULL,"1",NULL,"2020-05-08 22:07:27",NULL),
("587","
<p>On définit <code>L = [[1,2,3,4,5],[6,7,8,9,10],[11,12,13,14,15]]</code>.<br />
Quelle est la valeur de L[0][2] ?
</p>","
<p>2</p>
","
<p>3</p>
","
<p>11</p>
","
<p>12</p>
","B","3","13",NULL,"1",NULL,"2020-05-08 22:09:21",NULL),
("588","
<p>On définit le tableau <code>t = [[1,5,7], [8,4,2], [3,9,6]]</code></p>
<p>Quel jeu d'indices permet d'obtenir l'élément '9' de ce tableau ?</p>
","
<p>t[3][2]</p>
","
<p>t[2][3]</p>
","
<p>t[1][2]</p>
","
<p>t[2][1]</p>
","D","3","13",NULL,"1",NULL,"2020-05-08 22:12:22",NULL),
("589","
<p>Soit le tableau défini de la manière suivante : <code>tableau = [[1,3,4],[2,7,8],[9,10,6],[12,11,5]]</code></p>
<p>On souhaite accéder à la valeur 12, on écrit pour cela :</p>
","
<p>tableau[4][1]</p>
","
<p>tableau[1][4]</p>
","
<p>tableau[3][0]</p>
","
<p>tableau[0][3]</p>
","C","4","18",NULL,"1",NULL,"2020-05-08 22:12:59",NULL),
("590","
<p>On définit la variable suivante : <code>lettres = {'a': 1, 'b': 2, 'c': 3}</code>.</p>
<p>Quelle est la valeur de l'expression <code>list(lettres.keys())</code> ?</p>
","
<p>[a,b,c]</p>
","
<p>[1,2,3]</p>
","
<p>['a','b','c']</p>
","
<p>{'a': 1, 'b': 2, 'c': 3}</p>
","C","4","18",NULL,"1",NULL,"2020-05-08 22:13:51",NULL),
("591","<br><br><p>Qu'est-ce qu'un fichier CSV ?</p><br>","<br><br><p>une librairie Python permettant l'affichage des images</p><br>","<br><br><p>un utilitaire de traitement d'image</p><br>","<br><br><p>un format d'image</p><br>","<br><br><p>un format de données</p><br>","D","4","18",NULL,"1",NULL,"2020-03-25 13:23:48",NULL),
("592","<p>On exécute le script suivant :</p><br><pre><code class='python'>a = [1, 2, 3]<br>b = [4, 5, 6]<br>c = a + b</code></pre><br><p>Que contient la variable c à la fin de cette exécution ?</p><br>","<p>[5,7,9]</p><br>","<p>[1,4,2,5,3,6]</p><br>","<p>[1,2,3,4,5,6]</p><br>","<p>[1,2,3,5,7,9]</p><br>","C","4","18",NULL,"1",NULL,"2020-04-09 21:41:39",NULL),
("593","<br><br><p>On souhaite construire une table de 4 lignes de 3 éléments que l’on va remplir de 0. Quelle syntaxe Python utilisera-t-on&nbsp;?</p><br>","<br><br><p>[ [ 0 ] * 3 for i in range (4) ]</p><br>","<br><br><p>for i in range (4) [ 0 ] * 3</p><br>","<br><br><p>[ 0 ] * 3 for i in range (4)</p><br>","<br><br><p>[ for i in range (4) [ 0 ] * 3 ]</p><br>","A","4","18",NULL,"1",NULL,"2020-03-25 13:24:05",NULL),
("594","<p>Soit la table de données suivante :</p><br><pre><code class='python'>  nom   |  prenom   | date_naissance<br>Dupont  |  Pierre   | 17/05/1987<br>Dupond  | Catherine | 18/07/1981<br>Haddock | Archibald | 23/04/1998</code></pre><br><p>Quels sont les descripteurs de ce tableau ?</p><br>","<p>nom, prenom et date_naissance</p><br>","<p>Dupont, Pierre et 17/05/1987</p><br>","<p>Dupont, Dupond et Haddock</p><br>","<p>il n'y en a pas</p><br>","A","4","18",NULL,"1",NULL,"2020-04-09 21:41:10",NULL),
("595","<br><br><p>En HTML, un formulaire commence par quelle balise ?</p><br>","<br><br><p>&lt;form&gt;</p><br>","<br><br><p>&lt;/form&gt;</p><br>","<br><br><p>&lt;input type='form'&gt;</p><br>","<br><br><p>&lt;!--form--&gt;</p><br>","A","5","22",NULL,"1",NULL,"2020-03-25 13:24:22",NULL),
("596","<br><p>Lors de la consultation d’une page HTML, contenant un bouton auquel est associée la fonction javascript suivante, que se passe-t-il quand on clique sur ce bouton ?</p><br><pre><br>function action(event) {<br>    this.style.color = 'red'<br>}</pre><br>","<br><p>le pointeur de souris devient rouge lorsqu'il arrive sur le bouton</p><br>","<br><p>le texte du bouton devient rouge</p><br>","<br><p>le texte du bouton est remplacé par le mot 'red'</p><br>","<br><p>le texte de la page passe en rouge</p><br>","B","5","22",NULL,"1",NULL,"2020-03-27 14:55:46",NULL),
("597","<p>Voici un formulaire contenu dans une page HTML :</p>
<pre><code class='html'>&lt;form action='traitement.php' method='get'&gt;
  &lt;div&gt;
    &lt;label for='nom'&gt;Classe:
    &lt;input type='text' id='nom' name='leNom' /&gt;
  &lt;/div&gt;
  &lt;div&gt;
    &lt;label for='effectif'&gt;Effectif:
    &lt;input type='number' id='effectif' name='n' /&gt;
  &lt;/div&gt;
  &lt;div&gt;
    &lt;button type='submit'&gt;Go!
  &lt;/div&gt;
&lt;/form&gt;</code></pre>

<p>Un utilisateur remplit le formulaire comme indiqué ci-dessous.</p>
 
<p>Quelle est l'adresse de la page obtenue lorsque l'utilisateur clique sur le bouton Go ?</p>","<p>traitement.php?leNom=Seconde B&amp;n=35</p>
","<p>traitement.php?leNom=Seconde+B&amp;n=35</p>
","<p>traitement.php?nom=Seconde+B&amp;effectif=35</p>
","<p>traitement.php</p>
","B","5","22",NULL,"1","597_233hCS.png","2020-05-08 22:14:58",NULL),
("598","<br><br><p>En HTML, qu'est-ce que la balise &lt;a&gt; ?</p><br>","<br><br><p>Une balise de formulaire</p><br>","<br><br><p>Une balise d'en-tête</p><br>","<br><br><p>Une balise de lien ou d'ancre</p><br>","<br><br><p>Une balise de tableau</p><br>","C","5","22",NULL,"1",NULL,"2020-03-25 13:24:53",NULL),
("599","
<p>Quelle est la machine qui exécute un programme JavaScript inclus dans une page HTML ?</p>
","
<p>le serveur WEB qui contient la page HTML</p>
","
<p>la machine de l'utilisateur qui consulte la page HTML</p>
","
<p>un serveur du réseau</p>
","
<p>un routeur du réseau</p>
","B","5","22",NULL,"1",NULL,"2020-05-08 22:16:08",NULL),
("600","<br><br><p>Un élément form (un formulaire) d'une page HTML contient un élément button de type submit. Un clic sur ce bouton :</p><br>","<br><br><p>envoie les données du formulaire vers la page définie par l'attribut action de l'élément form</p><br>","<br><br><p>efface les données entrées par l'utilisateur dans le formulaire</p><br>","<br><br><p>envoie les données du formulaire vers la page définie par l'attribut method de l'élément form</p><br>","<br><br><p>ne fait rien du tout si un script javascript n'est pas associé au bouton</p><br>","A","5","22",NULL,"1",NULL,"2020-03-25 13:25:11",NULL),
("601","<br><br><p>Quelle commande sous Linux permet de donner à tout le monde les droits en écriture sur un fichier nommé monFichier ?</p><br>","<br><br><p>chmod o+x monFichier</p><br>","<br><br><p>chmod a+x monFichier</p><br>","<br><br><p>chmod o+w monFichier</p><br>","<br><br><p>chmod a+w monFichier</p><br>","D","6","27",NULL,"1",NULL,"2020-03-25 13:25:19",NULL),
("602","<br><br><p>Parmi ces propositions, laquelle désigne un système d'exploitation libre ?</p><br>","<br><br><p>LibreOffice</p><br>","<br><br><p>Windows</p><br>","<br><br><p>MacOS</p><br>","<br><br><p>GNU-Linux</p><br>","D","6","27",NULL,"1",NULL,"2020-03-25 13:25:28",NULL),
("603","<br><br><p>Laquelle de ces adresses IP v4 n'est pas valide ?</p><br>","<br><br><p>201.201.123.147</p><br>","<br><br><p>168.124.211.12</p><br>","<br><br><p>10.19.9.1</p><br>","<br><br><p>192.168.123.267</p><br>","D","6","27",NULL,"1",NULL,"2020-03-25 13:25:38",NULL),
("604","<br><br><p>Dans une mémoire RAM, que peut-on faire ?</p><br>","<br><br><p>uniquement lire des données</p><br>","<br><br><p>uniquement écrire des données</p><br>","<br><br><p>lire et écrire des données</p><br>","<br><br><p>lire des données même en cas de coupure de courant</p><br>","C","6","27",NULL,"1",NULL,"2020-03-25 13:25:46",NULL),
("605","<br><br><p>Quel est le principal rôle d’une carte mère dans un ordinateur ?</p><br>","<br><br><p>stocker les informations en mémoire vive</p><br>","<br><br><p>exécuter les instructions en langage machine</p><br>","<br><br><p>reproduire le processeur en plusieurs exemplaires</p><br>","<br><br><p>connecter les différents composants de l'ordinateur</p><br>","D","6","27",NULL,"1",NULL,"2020-03-25 13:25:54",NULL),
("606","<br><br><p>Dans un shell sous Linux, Alice utilise la commande pwd.</p><br><p>Cette commande :</p><br>","<br><br><p>liste les fichiers du répertoire courant</p><br>","<br><br><p>liste les répertoires du répertoire courant</p><br>","<br><br><p>affiche le chemin du répertoire courant</p><br>","<br><br><p>affiche les permissions relatives au répertoire courant</p><br>","C","6","27",NULL,"1",NULL,"2020-03-25 13:26:03",NULL),
("607","<br><p>Dans le programme JavaScript suivant, quelle est la notation qui délimite le bloc d’instructions exécuté à chaque passage dans la boucle while ?</p><br><pre><br>i = 0<br>while (i < 10) {<br>    alert(i)<br>    i = i + 1<br>}<br>alert('Fin')<br></pre><br>","<br><p>le fait que les instructions soient encadrées entre { et }</p><br>","<br><p>le fait que les instructions soient indentées de 4 caractères comme en Python</p><br>","<br><p>le fait que les instructions suivent le mot clé while</p><br>","<br><p>le fait que les instructions suivent la parenthèse )</p><br>","A","7","33",NULL,"1",NULL,"2020-03-29 20:40:11",NULL),
("608","<p>Soit \(n\) un entier naturel. Sa factorielle est le produit des nombres entiers strictement positifs qui sont plus petits ou égaux à \(n\). Par exemple la factorielle de \(4 \) vaut \(1 \times 2 \times 3 \times 4 = 24\).</p><br><p>Quelle est la fonction correcte parmi les suivantes ?</p><br>","<pre><br>def factorielle(n):<br>    i = 0<br>    fact = 1<br>    while i <= n:<br>        fact = fact * i<br>        i = i + 1<br>    return fact<br></pre><br>","<pre><br>def factorielle(n):<br>    i = 1<br>    fact = 1<br>    while i < n:<br>         fact = fact * i<br>         i = i + 1<br>    return fact<br></pre><br>","<pre><br>def factorielle(n):<br>    i = 0<br>    fact = 1<br>    while i < n:<br>         i = i + 1<br>         fact = fact * i<br>    return fact<br></pre><br>","<pre><br>def factorielle(n):<br>    i = 0<br>    fact = 1<br>    while i <= n:<br>         i = i + 1<br>         fact = fact * i<br>    return fact<br></pre>","C","7","33",NULL,"1",NULL,"2020-03-29 16:43:50",NULL),
("609","<br><p>On exécute le script suivant.</p><br><pre><br>n = 6<br>s = 0<br>while n >= 0:<br>    s = s + n<br>    n = n -1<br></pre><br><p>Que contient la variable s à la fin de cette exécution ?</p><br>","<br><p>0</p><br>","<br><p>6</p><br>","<br><p>15</p><br>","<br><p>21</p><br>","D","7","33",NULL,"1",NULL,"2020-03-29 20:42:35",NULL),
("610","<br><p>On exécute le script suivant :</p><br><pre><br>tableau1 = [1, 2, 3]<br>tableau2 = [4, 5, 6]<br>long = len(tableau1 + tableau2)<br></pre><br><p>Quelle est la valeur de la variable long à la fin de cette exécution ?</p><br>","<br><p>1</p><br>","<br><p>3</p><br>","<br><p>6</p><br>","<br><p>rien, car le code engendre une erreur</p><br>","C","7","33",NULL,"1",NULL,"2020-03-29 20:43:36",NULL),
("611","<br><p>Soit T un tableau de flottants, a et b deux entiers. On considère une fonction nommée somme renvoyant la somme des éléments du tableau d'indice compris entre a et b définie par :</p><br><pre><br>def somme(T, a, b):<br>    S = 0<br>    for i in range(a, b+1) :<br>        S = S + T[i]<br>    return S<br></pre><br><p>Quel ensemble de préconditions doit-on prévoir pour cette fonction ?</p><br>","<br><p>a < b</p><br>","<br><p>a < longueur(T) et b < longueur(T)</p><br>","<br><p>a <= b < longueur(T)</p><br>","<br><p>a <= b < longueur(T) et T est un tableau trié</p><br>","C","7","33",NULL,"1",NULL,"2020-03-29 20:44:54",NULL),
("612","<br><p>Quelle est la valeur de la variable x à la fin de l'exécution du script suivant :</p><br><pre><br>def f(x):<br>    x = x + 1<br>    return x + 1<br><br>x = 0<br>f(x+1)<br></pre><br>","<br><p>0</p><br>","<br><p>1</p><br>","<br><p>2</p><br>","<br><p>3</p><br>","A","7","33",NULL,"1",NULL,"2020-03-29 16:38:42",NULL),
("613","<br><p>On considère la fonction suivante :</p><br><pre><br>def f(T,i):<br>    indice = i<br>    m = T[i]<br>    for k in range(i+1, len(T)):<br>        if T[k] < m:<br>            indice = k<br>            m = T[k]<br>    return indice<br></pre><br><p>Quelle est la valeur de f([ 7, 3, 1, 8, 19, 9, 3, 5 ], 0) ?</p><br>","<br><p>1</p><br>","<br><p>2</p><br>","<br><p>3</p><br>","<br><p>4</p><br>","B","8","39",NULL,"1",NULL,"2020-03-29 16:37:34",NULL),
("614","<p>On considère la fonction suivante :</p><br><pre><br>def comptage(phrase,lettre):<br>          i = 0<br>          for j in phrase:<br>              if j == lettre:<br>                  i = i+1<br>          return i<br></pre><br><p>Que renvoie l'appel comptage('Vive l’informatique','e') ?</p><br>","<p>0</p><br>","<p>2</p><br>","<p>19</p><br>","<p>'e'</p><br>","B","8","39",NULL,"1",NULL,"2020-03-25 19:45:31",NULL),
("615","<br><p>Quelle est la valeur de element à la fin de l'exécution du code suivant :</p><br><pre><br>L = [1,2,3,4,1,2,3,4,0,2]<br>element = L[0]<br>for k in L:<br>    if k > element:<br>        element = k<br></pre><br>","<br><p>0</p><br>","<br><p>1</p><br>","<br><p>4</p><br>","<br><p>10</p><br>","C","8","39",NULL,"1",NULL,"2020-03-27 14:57:23",NULL),
("616","<br><br><p>À quelle catégorie appartient l’algorithme classique de rendu de monnaie&nbsp;?</p><br>","<br><br><p>les algorithmes de classification et d'apprentissage</p><br>","<br><br><p>les algorithmes de tri</p><br>","<br><br><p>les algorithmes gloutons</p><br>","<br><br><p>les algorithmes de mariages stables</p><br>","C","8","39",NULL,"1",NULL,"2020-03-25 13:27:36",NULL),
("617","<br><p>L'algorithme suivant permet de calculer la somme des N premiers entiers, où N est un nombre entier donné :</p><br><pre><br>i =0<br>somme =0<br>while i < N :<br>    i = i + 1<br>    somme = somme + i<br></pre><br><p>Un invariant de boucle de cet algorithme est le suivant :</p><br>","<br><p>somme = 0 + 1 + 2 + ... + i    et i < N</p><br>","<br><p>somme = 0 + 1 + 2 + ... + N    et i < N</p><br>","<br><p>somme = 0 + 1 + 2 + ... + i    et i < N+1</p><br>","<br><p>somme = 0 + 1 + 2 + ... + N    et i < N+1</p><br>","C","8","39",NULL,"1",NULL,"2020-03-27 14:59:45",NULL),
("618","<p>Quelle est la valeur du couple (s,i) à la fin de l'exécution du script suivant ?</p>
<pre><code class='python'>s = 0
i = 1
while i &lt; 5:
    s = s + i
    i = i + 1
</code></pre>
","(4, 5)
","(10, 4)
","(10, 5)
","(15, 5)
","C","8","39",NULL,"1",NULL,"2020-05-08 22:17:26",NULL),
("619","<p>On exécute le code suivant</p>
<pre><code class='python'>a = 2
b = 3
c = a ** b
d = c % b
</code></pre>
<p>Quelle est la valeur de d à la fin de l'exécution ?</p>
","1
","<span class='math inline'>\(2\)</span>
","<span class='math inline'>\(3\)</span>
","4
","B","2","7",NULL,"1",NULL,"2020-04-26 12:07:24",NULL),
("620","<br><br><p>Quelle est la représentation binaire de l'entier 152<strong>&nbsp;</strong>?</p><br>","<br><br><p>0001 1001</p><br>","<br><br><p>0011 1010</p><br>","<br><br><p>0100 0100</p><br>","<br><br><p>1001 1000</p><br>","D","2","7",NULL,"1",NULL,"2020-03-25 14:01:43",NULL),
("621","<br><br><p>Quel est un avantage du codage UTF8 par rapport au codage ASCII ?</p><br>","<br><br><p>il permet de coder un caractère sur un octet au lieu de deux</p><br>","<br><br><p>il permet de coder les majuscules</p><br>","<br><br><p>il permet de coder tous les caractères</p><br>","<br><br><p>il permet de coder différentes polices de caractères</p><br>","C","2","7",NULL,"1",NULL,"2020-03-25 14:01:52",NULL),
("622","<br><br><p>Quelle est l'écriture décimale de l'entier positif dont l'écriture hexadécimale (en base 16) est 3F ?</p><br>","<br><br><p>18</p><br>","<br><br><p>45</p><br>","<br><br><p>63</p><br>","<br><br><p>315</p><br>","C","2","7",NULL,"1",NULL,"2020-03-25 14:02:00",NULL),
("623","<br><br><p>Parmi les quatre nombres suivants lequel est le seul à pouvoir être représenté de façon exacte en machine ?</p><br>","<br><br><p>3.1</p><br>","<br><br><p>4.2</p><br>","<br><br><p>5.24</p><br>","<br><br><p>7.25</p><br>","D","2","7",NULL,"1",NULL,"2020-03-25 14:02:08",NULL),
("624","<br><br><p>Le codage d’une couleur se fait à l'aide de trois nombres compris chacun, en écriture décimale, entre 0 et 255 (code RVB).</p><br><p>La couleur «&nbsp;vert impérial&nbsp;»&nbsp;est codée, en écriture décimale, par (0, 86, 27).</p><br><p>Le codage hexadécimal correspondant est&nbsp;:</p><br>","<br><br><p>(0, 134, 39)</p><br>","<br><br><p>(0, 134, 1B)</p><br>","<br><br><p>(0, 56, 1B)</p><br>","<br><br><p>(0, 56, 39)</p><br>","C","2","7",NULL,"1",NULL,"2020-03-25 14:02:17",NULL),
("625","<br><br><p>On crée la liste suivante :</p><br><p>t = [ [1,2,3,4], [5,6,7,8], [9,10,11,12] ]</p><br><p>Que vaut t[1][2] :</p><br>","<br><br><p>2</p><br>","<br><br><p>7</p><br>","<br><br><p>10</p><br>","<br><br><p>on obtient un message d'erreur 'indexError : list index out of range'</p><br>","B","3","13",NULL,"1",NULL,"2020-03-25 14:02:25",NULL),
("626","<br><br><p>On définit :</p><br><p>dico = {'Herve': 15, 'Kevin':17, 'Fatima':16}</p><br><p>qui associe nom et âge de trois élèves.</p><br><p>Comment accéder à l'âge de Kevin ?</p><br>","<br><br><p>dico[1]</p><br>","<br><br><p>dico[Kevin]</p><br>","<br><br><p>dico['Kevin']</p><br>","<br><br><p>dico('Kevin')</p><br>","C","3","13",NULL,"1",NULL,"2020-03-25 14:02:33",NULL),
("627","<p>L est une liste d'entiers.</p>
<p>On définit la fonction suivante :</p>
<pre><code class='python'>def f(L):
    m = L[0]
    for x in L:
        if x &gt; m:
            m = x
    return m
</code></pre>
<p>Que calcule cette fonction ?</p>
","<p>le maximum de la liste L passée en argument</p>
","<p>le minimum de la liste L passée en argument</p>
","<p>le premier terme de la liste L passée en argument</p>
","<p>le dernier terme de la liste L passée en argument</p>
","A","3","13",NULL,"1",NULL,"2020-05-08 22:18:31",NULL),
("628","<br><p>Après l'affectation suivante :</p><br><pre><code class='python'>alphabet = [ 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',<br>             'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' ]</code></pre><br><p>quelle est l'expression qui permet d'accéder à la lettre E ?</p><br>","<br><p>alphabet.E</p><br>","<br><p>alphabet['E']</p><br>","<br><p>alphabet[4]</p><br>","<br><p>alphabet[5]</p><br>","C","3","13",NULL,"1",NULL,"2020-04-09 21:46:28",NULL),
("629","
<p>Parmi les propositions suivantes, laquelle permet de créer en Python la liste des nombres impairs de 1 à 399 (inclus) ?</p>
","
<code>impairs = [1 + nb*2 for nb in range(200)]</code>
","
<pre><code class='python'>for nb in range(400) :
    impairs = 1 + 2 * nb</code></pre>
","
<code>impairs = [i + 2 for i in range(1,200)]</code>
","
<code>impairs = [1, 3, 5, 7, 9] * 40</code>
","A","3","13",NULL,"1",NULL,"2020-05-08 22:21:35",NULL),
("630","<br><br><p>On définit la liste L ainsi :</p><br><p>L = [ [1], [1,2], [1,2,3] ]</p><br><p>Des égalités suivantes, une seule est fausse. Laquelle&nbsp;?</p><br>","<br><br><p>len(L[0]) == 1</p><br>","<br><br><p>len(L) == 6</p><br>","<br><br><p>len(L[2]) == 3</p><br>","<br><br><p>L[2][2] == 3</p><br>","B","3","13",NULL,"1",NULL,"2020-03-25 14:03:07",NULL),
("631","<br><br><p>Dans la plupart des fichiers CSV, que contient la première ligne ?</p><br>","<br><br><p>des notes concernant la table de données</p><br>","<br><br><p>les sources des données</p><br>","<br><br><p>les descripteurs des champs de la table de données</p><br>","<br><br><p>l'auteur de la table de données</p><br>","C","4","18",NULL,"1",NULL,"2020-03-25 14:03:16",NULL),
("632","<br><p>On exécute le code suivant :</p><br><pre><code class='python'>table = [ ['lovelace', 'ada', 1815, 1852],<br>          ['von neumann','john', 1903, 1957],<br>          ['turing', 'alan', 1912, 1954],<br>          ['mccarthy', 'john', 1927, 2011],<br>          ['floyd', 'robert', 1936, 2001] ]<br><br>def age(personnage):<br>     return personnage[3] - personnage[2]<br><br>table.sort(key=age, reverse=True)</code></pre><br><p>Quelle est la première ligne de la table table à la suite de cette exécution ?</p><br>","<br><p>['lovelace', 'ada', 1815, 1852]</p><br>","<br><p>['mccarthy', 'john', 1927, 2011]</p><br>","<br><p>['turing', 'alan', 1912, 1954]</p><br>","<br><p>['mccarthy', 'floyd', 'von neumann', 'turing', 'lovelace']</p><br>","B","4","18",NULL,"1",NULL,"2020-04-09 21:48:39",NULL),
("633","<br><p>Quelle est la valeur de la variable table à la fin de l'exécution du script suivant :</p><br><pre><code class='python'>table = [[1, 2, 3], [1, 2, 3], [1, 2, 3], [1, 2, 3]]<br>table [1][2] = 5</code></pre><br>","<br><p>[[1, 5, 3], [1, 2, 3], [1, 2, 3], [1, 2, 3]]</p><br>","<br><p>[[1, 2, 3], [5, 2, 3], [1, 2, 3], [1, 2, 3]]</p><br>","<br><p>[[1, 2, 3], [1, 2, 5], [1, 2, 3], [1, 2, 3]]</p><br>","<br><p>[[1, 2, 3], [1, 2, 3], [1, 2, 3], [1, 5, 3]]</p><br>","C","4","18",NULL,"1",NULL,"2020-04-09 21:50:48",NULL),
("634","
<p>On définit les notes des élèves dans le dictionnaire suivant :</p>
<pre><code class='python'>notes = { 'Pierre' : 9, 'Paule' : 16, 'Claire' : 12,
'Sophie' : 11, 'Yasmine' : 8, 'David' : 17, 'Julie' : 3 }</code></pre>
<p>Quelle expression permet d'accéder à la note de l’élève Claire ?</p>
","
<p>notes[12]</p>
","
<p>notes[2]</p>
","
<p>notes['Claire']</p>
","
<p>notes.values('Claire')</p>
","C","4","18",NULL,"1",NULL,"2020-05-08 22:23:11",NULL),
("635","<br><p>On a défini :</p><br><pre><code class='python'>mendeleiev = [ ['H','.', '.','.','.','.','.','He'],<br>               ['Li','Be','B','C','N','O','Fl','Ne'],<br>               ['Na','Mg','Al','Si','P','S','Cl','Ar'],<br>                 ...... ]</code></pre><br><p>Une erreur s'est glissée dans le tableau, car le symbole du Fluor est F et non Fl. Quelle instruction permet de rectifier ce tableau ?</p><br>","<br><p>mendeleiev.append('F')</p><br>","<br><p>mendeleiev[1][6] = 'F'</p><br>","<br><p>mendeleiev[6][1] = 'F'</p><br>","<br><p>mendeleiev[-1][-1] = 'F'</p><br>","B","4","18",NULL,"1",NULL,"2020-04-09 21:52:49",NULL),
("636","<br><p>On définit ainsi une liste t :</p><br><pre><code class='python'> t = [ {'id':1, 'age':23, 'sejour':'PEKIN'},<br>       {'id':2, 'age':27, 'sejour':'ISTANBUL'},<br>       {'id':3, 'age':53, 'sejour':'LONDRES'},<br>       {'id':4, 'age':41, 'sejour':'ISTANBUL'},<br>       {'id':5, 'age':62, 'sejour':'RIO'},<br>       {'id':6, 'age':28, 'sejour':'ALGER'}]</code></pre><br><p>Quelle affirmation est correcte ?</p><br>","<br><p>t est une liste de listes</p><br>","<br><p>t est une liste de dictionnaires</p><br>","<br><p>t est un dictionnaire de listes</p><br>","<br><p>t est une liste de tuples</p><br>","B","4","18",NULL,"1",NULL,"2020-04-09 21:54:40",NULL),
("637","<br><br><p>Quelle est la fonction principale d’un fichier CSS&nbsp;?</p><br>","<br><br><p>Définir le style d’une page web</p><br>","<br><br><p>Assurer l’interaction entre une page web et l’utilisateur</p><br>","<br><br><p>Créer une page web dynamique</p><br>","<br><br><p>Créer un bouton dans une page web</p><br>","A","5","22",NULL,"1",NULL,"2020-03-25 14:04:14",NULL),
("638","<p>Voici un extrait d'une page HTML :</p>
<pre><code class='html'>
&lt;script&gt;
function sommeNombres(formulaire) {
    var somme = formulaire.n1.value + formulaire.n2.value;
    console.log(somme);
    }
&lt;/script&gt;
&lt;form&gt;
  Nombre 1 : &lt;input name='n1' value='30'&gt; &lt;br/&gt;
  Nombre 2 : &lt;input name='n2' value='10'&gt; &lt;br/&gt;
  &lt;input type='button' value='Somme' onclick='sommeNombres(this.form)'&gt;
&lt;/form&gt;</code></pre>
<p>Quand l'utilisateur clique sur le bouton Somme, le calcul de la fonction sommeNombre() se fait :</p>
","<p>uniquement dans le navigateur</p>
","<p>uniquement sur le serveur qui héberge la page</p>
","<p>à la fois dans le navigateur et sur le serveur</p>
","<p>si le calcul est complexe, le navigateur demande au serveur de faire le calcul</p>
","A","5","22",NULL,"1",NULL,"2020-04-26 11:10:44",NULL),
("639","
<p>Quelle URL parmi les suivantes témoigne que l'échange entre le navigateur et le serveur est chiffré ?</p>
","
http://www.mabanque.com/
","
http://www.mabanque.fr/
","
https://www.mabanque.fr/
","
http://secure.mabanque.fr/
","C","5","22",NULL,"1",NULL,"2020-04-26 12:04:57",NULL),
("640","<br><br><p>Parmi les balises HTML ci-dessous quelle est celle qui permet à l’utilisateur de saisir son nom dans un formulaire en respectant la norme HTML ?</p><br>","<br><br><p>&lt;select /&gt;</p><br>","<br><br><p>&lt;form /&gt;</p><br>","<br><br><p>&lt;input type='text' /&gt;</p><br>","<br><br><p>&lt;input type='name' /&gt;</p><br>","C","5","22",NULL,"1",NULL,"2020-03-25 14:04:44",NULL),
("641","<br><br><p>onmouseover est une méthode qui permet de traiter un événement de quel type ?</p><br>","<br><br><p>l'appui d'une touche du clavier</p><br>","<br><br><p>un clic sur un bouton de souris</p><br>","<br><br><p>un mouvement de la souris</p><br>","<br><br><p>le survol par la souris d'un élément de la page</p><br>","D","5","22",NULL,"1",NULL,"2020-03-25 14:04:53",NULL),
("642","<br><br><p>Dans le contexte du Web, qu’est-ce qu’une transmission chiffrée ?</p><br>","<br><br><p>une transmission optimisée pour les grands nombres</p><br>","<br><br><p>une transmission sous forme binaire</p><br>","<br><br><p>une transmission d’informations cryptées</p><br>","<br><br><p>une transmission facturée proportionnellement à la taille du message</p><br>","C","5","22",NULL,"1",NULL,"2020-03-25 14:05:04",NULL),
("643","<br><br><p>Quelle commande sous Linux permet de donner à tout le monde les droits en écriture sur un fichier nommé monFichier ?</p><br>","<br><br><p>chmod o+x monFichier</p><br>","<br><br><p>chmod a+x monFichier</p><br>","<br><br><p>chmod o+w monFichier</p><br>","<br><br><p>chmod a+w monFichier</p><br>","D","6","27",NULL,"1",NULL,"2020-03-25 14:05:13",NULL),
("644","<br><br><p>Sous Unix, quelle commande permet de créer un nouveau répertoire&nbsp;?</p><br>","<br><br><p>mkdir</p><br>","<br><br><p>echo</p><br>","<br><br><p>ls</p><br>","<br><br><p>rm</p><br>","A","6","27",NULL,"1",NULL,"2020-03-25 14:05:20",NULL),
("645","
<p>Sur la configuration IP d’une machine nommée MACH01 on peut lire :<br />
adresse Ipv4 : 172.16.100.201<br />
Masque de sous-réseau : 255.255.0.0<br />
Passerelle : 172.16.0.254</p>
<p>Sur la configuration IP d’une machine nommée MACH02 on peut lire :<br />
adresse Ipv4 : 172.16.100.202<br />
Masque de sous-réseau : 255.255.0.0<br />
Passerelle : 172.16.0.254</p>
<p>Depuis la machine MACH02, à l'aide de quelle commande peut-on tester le dialogue entre ces deux machines ?</p>
","
<p>ping 172.16.100.201</p>
","
<p>ping 172.16.100.202</p>
","
<p>ping 172.16.100.254</p>
","
<p>ping 255.255.0.0</p>
","A","6","27",NULL,"1",NULL,"2020-05-08 22:25:09",NULL),
("646","<br><br><p>Parmi les dispositifs d’entrée et de sortie suivants, lequel est uniquement un capteur ?</p><br>","<br><br><p>la diode</p><br>","<br><br><p>l'écran tactile</p><br>","<br><br><p>le thermomètre</p><br>","<br><br><p>le moteur pas à pas</p><br>","C","6","27",NULL,"1",NULL,"2020-03-25 14:05:37",NULL),
("647","<br><br><p>Sachant que le répertoire courant contient les fichiers fich.txt, mafich.txt et programme.py, quel est le résultat de la commande ls fich* dans un shell Linux ?</p><br>","<br><br><p>fich.txt mafich.txt</p><br>","<br><br><p>mafich.txt</p><br>","<br><br><p>fich.txt</p><br>","<br><br><p>programme.py</p><br>","C","6","27",NULL,"1",NULL,"2020-03-25 14:05:46",NULL),
("648","<br><br><p>Lorsque, en ligne de commande, on saisit la commande</p><br><p>chmod u+rw a.txt</p><br><p>ceci a pour effet :</p><br>","<br><br><p>de permettre au propriétaire du fichier de modifier le contenu de ce fichier</p><br>","<br><br><p>d'interdire au propriétaire de modifier le contenu de ce fichier</p><br>","<br><br><p>d'interdire à tous les autres utilisateurs de lire le fichier</p><br>","<br><br><p>d'effacer le fichier</p><br>","A","6","27",NULL,"1",NULL,"2020-03-25 14:05:56",NULL),
("649","<br><br><p>Parmi ces langages, lequel n'est pas un langage de programmation ?</p><br>","<br><br><p>HTML</p><br>","<br><br><p>JavaScript</p><br>","<br><br><p>PHP</p><br>","<br><br><p>Python</p><br>","A","7","33",NULL,"1",NULL,"2020-03-25 14:06:04",NULL),
("650","<br><p>Quelle est la valeur de la variable b à la fin de l'exécution du script suivant ?</p><br><pre><code class='python'>a = 2<br>b = 5<br>if a > 8:<br>    b = 10<br>elif a > 6:<br>    b = 3</code></pre><br>","<br><p>3</p><br>","<br><p>5</p><br>","<br><p>6</p><br>","<br><p>10</p><br>","B","7","33",NULL,"1",NULL,"2020-04-09 22:02:41",NULL),
("651","<br><p>La fonction ajoute(n,p) codée ci-dessous en Python doit calculer la somme de tous les entiers compris entre n et p (n et p compris).</p><br><p>Par exemple, ajoute(2,4) doit renvoyer 2+3+4 = 9.</p><br><pre><code class='python'>def ajoute(n,p):<br>    somme = 0<br>    for i in range(.........): # ligne à modifier<br>        somme = somme + i<br>    return somme</code></pre><br><p>Quelle est la bonne écriture de la ligne marquée à modifier ?</p><br>","<br><p>for i in range(n,1,p):</p><br>","<br><p>for i in range(n,p):</p><br>","<br><p>for i in range(n,p+1):</p><br>","<br><p>for i in range(n-1,p):</p><br>","C","7","33",NULL,"1",NULL,"2020-04-09 22:04:19",NULL),
("652","<br><p>La fonction maximum codée ci-dessous en Python doit renvoyer la plus grande valeur contenue dans le tableau d'entiers passé en argument.</p><br><pre><code class='python'>def maximum(tableau):<br>    tmp = tableau[0]<br>        for i in range(......): # à compléter<br>            if tableau[i] > tmp:<br>                tmp = tableau[i]<br>    return tmp</code></pre><br><p>Quelle expression faut-il écrire à la place des pointillés ?</p><br>","<br><p>len(tableau) - 1</p><br>","<br><p>1,len(tableau) - 1</p><br>","<br><p>1,len(tableau)</p><br>","<br><p>1,len(tableau) + 1</p><br>","C","7","33",NULL,"1",NULL,"2020-04-09 22:05:51",NULL),
("653","<br><p>La documentation de la bibliothèque random de Python précise que<br><br>random.randint(a,b) renvoie un entier aléatoire <span class='math inline'>\(N\)</span> tel que a <span class='math inline'>\(\leq \)</span>N <span class='math inline'>\(\leq\)</span> b.</p><br><p>Afin d’obtenir un entier choisi aléatoirement dans l’ensemble {-4 ; -2 ; 0 ; 2 ; 4}, après avoir importé la librairie random de Python, on peut utiliser l’instruction :</p><br>","<br><p>random.randint(0,8)/2</p><br>","<br><p>random.randint(0,8)/2 - 4</p><br>","<br><p>random.randint(0,4)*2 - 2</p><br>","<br><p>(random.randint(0,4) - 2) * 2</p><br>","D","7","33",NULL,"1",NULL,"2020-03-29 15:43:16",NULL),
("654","<br><p>On considère la fonction ci-dessous :</p><br><pre><code class='python'>def maFonction(c):<br>    if c <= 10:<br>        p = 12<br>    if c <= 18:<br>        p = 15<br>    if c <= 40:<br>        p = 19<br>    else:<br>        p = 20<br>    return p</code></pre><br><p>Que renvoie maFonction(18) ?</p><br>","<br><p>12</p><br>","<br><p>15</p><br>","<br><p>19</p><br>","<br><p>20</p><br>","C","7","33",NULL,"1",NULL,"2020-04-09 22:08:16",NULL),
("655","<br><br><p>Quelle est la complexité du tri par sélection ?</p><br>","<br><br><p>inconnue</p><br>","<br><br><p>linéaire</p><br>","<br><br><p>quadratique</p><br>","<br><br><p>exponentielle</p><br>","C","8","39",NULL,"1",NULL,"2020-03-25 14:06:58",NULL),
("656","<br><p>Soit L une liste de <span class='math inline'>\(n\)</span> nombres réels (<span class='math inline'>\(n\)</span> entier naturel non nul). On considère l'algorithme suivant, en langage Python, calculant la moyenne des éléments de L.</p><br><pre><code class='python'>M = 0<br>for k in range(n):<br>    M = M + L[k]<br>M = M/n</code></pre><br><p>Si le nombre <span class='math inline'>\(n\)</span> de données double alors le temps d'exécution de ce script :</p><br>","<br><p>reste le même</p><br>","<br><p>double aussi</p><br>","<br><p>est multiplié par <span class='math inline'>\(n\)</span></p><br>","<br><p>est multiplié par 4</p><br>","B","8","39",NULL,"1",NULL,"2020-04-09 22:09:43",NULL),
("657","<br><p>La fonction ci-dessous compte le nombre d'occurrences d'un élément x dans une liste L :</p><br><pre><code class='python'>def compteur(L,x):<br>    n = 0<br>    for item in L:<br>        if item == x:<br>            n = n + 1<br>    return n</code></pre><br><p>Comment évolue le temps d'exécution d'un appel de cette fonction si on prend comme argument une liste deux fois plus grande ?</p><br>","<br><p>c'est le même temps d'exécution</p><br>","<br><p>le temps d'exécution est à peu près doublé</p><br>","<br><p>le temps d'exécution est à peu près quadruplé</p><br>","<br><p>impossible de le prévoir, cela dépend aussi de l'argument x</p><br>","B","8","39",NULL,"1",NULL,"2020-04-09 22:11:19",NULL),
("658","<br><p>La fonction mystere suivante prend en argument un tableau d'entiers.</p><br><pre><code class='python'>def mystere(t):<br>    for i in range(len(t) - 1):<br>        if t[i+1] != t[i] + 1:<br>            return False<br>    return True</code></pre><br><p>À quelle condition la valeur renvoyée par la fonction est-elle True ?</p><br>","<br><p>si le tableau passé en argument est une suite d'entiers consécutifs</p><br>","<br><p>si le tableau passé en argument est trié en ordre croissant</p><br>","<br><p>si le tableau passé en argument est trié en ordre décroissant</p><br>","<br><p>si le tableau passé en argument contient des entiers tous identiques</p><br>","A","8","39",NULL,"1",NULL,"2020-04-09 22:14:11",NULL),
("659","<br><br><p>Un algorithme de recherche dichotomique dans une liste triée de taille <span class='math inline'>\(n\)</span> nécessite, dans le pire des cas, exactement <span class='math inline'>\(k\)</span> comparaisons.</p><br><p>Combien cet algorithme va-t-il utiliser, dans le pire des cas, de comparaisons sur une liste de taille <span class='math inline'>\(2n\)</span> ?</p><br>","<br><br><p><span class='math inline'>\(k\)</span></p><br>","<br><br><p><span class='math inline'>\(k + 1\)</span></p><br>","<br><br><p><span class='math inline'>\(2k\)</span></p><br>","<br><br><p><span class='math inline'>\(2k + 1\)</span></p><br>","B","8","39",NULL,"1",NULL,"2020-03-25 14:07:41",NULL),
("660","<br><p>La fonction suivante doit déterminer la valeur maximale d'un tableau de nombres passé en argument. Avec quelles expressions faut-il remplacer les pointillés du script suivant pour que la fonction soit correcte ?</p><br><pre><code class='python'>def maximum(T):<br>    maxi = T[0]<br>    n = len(T)<br>    for i in range(i, .….):<br>        if T[i] > maxi:<br>            maxi = ......<br>    return maxi</code></pre><br>","<br><p>n puis T[i]</p><br>","<br><p>n puis T[i-1]</p><br>","<br><p>n-1 puis T[i]</p><br>","<br><p>n-1 puis T[i-1]</p><br>","A","8","39",NULL,"1",NULL,"2020-04-09 22:15:47",NULL),
("661","<br><br><p>On considère les nombres dont l'écriture en base 16 (en hexadécimal) sont de la forme suivante : un 1 suivi de 0 en nombre quelconque, comme 1, 10, 100, 1000 etc.</p><br><p>Tous ces nombres sont exactement :</p><br>","<br><br><p>les puissances de 2</p><br>","<br><br><p>les puissances de 8</p><br>","<br><br><p>les puissances de 10</p><br>","<br><br><p>les puissances de 16</p><br>","D","2","7",NULL,"1",NULL,"2020-03-25 14:08:41",NULL),
("662","<br><br><p>Combien d'entiers positifs ou nuls (entiers non signés) peut-on représenter en machine sur 32&nbsp;bits&nbsp;?</p><br>","<br><br><p><span class='math inline'>\(2^{32} - 1\)</span></p><br>","<br><br><p><span class='math inline'>\(2^{32}\)</span></p><br>","<br><br><p><span class='math inline'>\(2 \times 32\)</span></p><br>","<br><br><p><span class='math inline'>\(32^{2}\)</span></p><br>","B","2","7",NULL,"1",NULL,"2020-03-25 14:08:51",NULL),
("663","<br><br><p>Quel est le résultat de l'addition binaire 0100 1110 + 0110 1101 ?</p><br>","<br><br><p>0101 1011</p><br>","<br><br><p>1010 1101</p><br>","<br><br><p>1011 0110</p><br>","<br><br><p>1011 1011</p><br>","D","2","7",NULL,"1",NULL,"2020-03-25 14:08:59",NULL),
("664","<br><br><p>Le code ASCII permet de représenter en binaire les caractères alphanumériques. Quel est son principal inconvénient ?</p><br>","<br><br><p>Il utilise beaucoup de bits.</p><br>","<br><br><p>Il ne différencie pas les majuscules des minuscules.</p><br>","<br><br><p>Il ne représente pas les caractères accentués.</p><br>","<br><br><p>Il n'est pas compatible avec la plupart des systèmes informatiques.</p><br>","C","2","7",NULL,"1",NULL,"2020-03-25 14:09:07",NULL),
("665","<br><br><p>L'entier positif 255 se représente en hexadécimal (base 16) par :</p><br>","<br><br><p>99</p><br>","<br><br><p>AA</p><br>","<br><br><p>CC</p><br>","<br><br><p>FF</p><br>","D","2","7",NULL,"1",NULL,"2020-03-25 14:09:15",NULL),
("666","<br><br><p>Deux entiers positifs ont pour écriture en base 16 : A7 et 84.</p><br><p>Quelle est l'écriture en base 16 de leur somme ?</p><br>","<br><br><p>1811</p><br>","<br><br><p>12B</p><br>","<br><br><p>13A</p><br>","<br><br><p>A784</p><br>","B","2","7",NULL,"1",NULL,"2020-03-25 14:09:23",NULL),
("667","<br><p>Quelle est la valeur de la variable S à la fin de l'exécution du script suivant ?</p><br><pre><code class='python'>res = [ [1,2,3], [4,5,6], [7,8,9] ]<br>S = 0<br>for i in range(3):<br>    S = S + res[i][2]</code></pre><br>","<br><p>12</p><br>","<br><p>15</p><br>","<br><p>18</p><br>","<br><p>24</p><br>","C","3","13",NULL,"1",NULL,"2020-04-09 22:17:04",NULL),
("668","<br><br><p>On définit la variable suivante : citation = 'Les nombres gouvernent le monde'.</p><br><p>Quelle est la valeur de l'expression citation[5:10] ?</p><br>","<br><br><p>'ombre'</p><br>","<br><br><p>'ombres'</p><br>","<br><br><p>'nombre'</p><br>","<br><br><p>'nombres'</p><br>","A","3","13",NULL,"1",NULL,"2020-03-25 14:09:39",NULL),
("669","<br><br><p>On définit&nbsp;: t = [2, 8, 9, 2]</p><br><p>Quelle est la valeur de l'expression [ x*x for x in t&nbsp;]&nbsp;?</p><br>","<br><br><p>une erreur</p><br>","<br><br><p>[[2, 8, 9, 2], [2, 8, 9, 2]]</p><br>","<br><br><p>[2, 8, 8, 9, 9, 9, 2, 2, 2, 2]</p><br>","<br><br><p>[4, 64, 81, 4]</p><br>","D","3","13",NULL,"1",NULL,"2020-03-25 14:09:47",NULL),
("670","<br><p>On considère le code suivant :</p><br><pre><code class='python'>def s(tuple1, tuple2):<br>    (x1,y1) = tuple1<br>    (x2,y2) = tuple2<br>    return (x1+x2, y1+y2)</code></pre><br><p>Que renvoie l'appel s((1,3), (2,4)) ?</p><br>","<br><p>le tuple (3,7)</p><br>","<br><p>le tuple (4,6)</p><br>","<br><p>un entier</p><br>","<br><p>une erreur</p><br>","A","3","13",NULL,"1",NULL,"2020-04-09 22:18:05",NULL),
("671","<br><p>Soient n et p deux entiers au moins égaux à 2. On définit une liste de listes t par le code suivant :</p><br><pre><code class='python'>#n et p sont initialisés dans les lignes précédentes<br>t = [ [ 0 for j in range(p) ] for i in range(n) ]<br>for k in range(n*p):<br>    t[k%n][k%p] = k</code></pre><br>Une et une seule des affirmations suivantes est <strong>fausse</strong>. Laquelle ?<br>","<br><p>La liste t contient des entiers <span class='math inline'>\(k\)</span> tels que <span class='math inline'>\(0 \leq k < n \times p\)</span>.</p><br>","<br><p>Pour tout <span class='math inline'>\(j\)</span> tel que <span class='math inline'>\(0 \leq j < n - 1\)</span>, t[j][0] est un multiple de <span class='math inline'>\(p\)</span>.</p><br>","<br><p>La liste t[0] contient des entiers qui sont tous multiples de <span class='math inline'>\(n\)</span>.</p><br>","<br><p>Pour tout <span class='math inline'>\(j\)</span> tel que <span class='math inline'>\(0 \leq j < n - 1\)</span>, t[0][j] est un multiple de <span class='math inline'>\(p\)</span>.</p><br>","D","3","13",NULL,"1",NULL,"2020-04-09 22:19:48",NULL),
("672","<br><br><p>On définit : L = [10,9,8,7,6,5,4,3,2,1].</p><br><p>Quelle est la valeur de L[L[3]] ?</p><br>","<br><br><p>3</p><br>","<br><br><p>4</p><br>","<br><br><p>7</p><br>","<br><br><p>8</p><br>","A","3","13",NULL,"1",NULL,"2020-03-25 14:10:19",NULL),
("673","<br><p>On exécute le script suivant :</p><br><pre><code class='python'>a = [1, 2, 3]<br>b = [4, 5, 6]<br>c = a + b</code></pre><br><p>Que contient la variable c à la fin de cette exécution ?</p><br>","<br><p>[5,7,9]</p><br>","<br><p>[1,4,2,5,3,6]</p><br>","<br><p>[1,2,3,4,5,6]</p><br>","<br><p>[1,2,3,5,7,9]</p><br>","C","4","18",NULL,"1",NULL,"2020-04-09 22:23:32",NULL),
("674","<br><br><p>Laquelle de ces affirmations est vraie ?</p><br>","<br><br><p>on ne peut accéder au contenu d'un fichier CSV que par l'intermédiaire d'un programme Python</p><br>","<br><br><p>CSV est un format de chiffrement des données</p><br>","<br><br><p>le format CSV a été conçu pour asssurer la confidentialité d'une partie du code d'un programme</p><br>","<br><br><p>les fichiers CSV sont composés de données séparées par des caractères comme des virgules</p><br>","D","4","18",NULL,"1",NULL,"2020-03-25 14:10:35",NULL),
("675","
<p>On a défini deux tables de données :</p>
<pre><code class='python'>data1 = [(‘Bruce’, ‘Wayne’), (‘Chuck’, ‘Norris’), (‘Bruce’, ‘Lee’), (‘Clark’, ‘Kent’)]
data2 = [(‘Diana’, ‘Prince’), (‘Chuck’, ‘Norris’), (‘Peter’, ‘Parker’)]</code></pre>
<p>Quelle instruction permet de construire une table data regroupant l'ensemble des informations de data1 et data2 ?</p>
","
<p>data = data1 + data2</p>
","
<p>data == data1 + data2</p>
","
<p>data = [element for element in data1 or data2]</p>
","
<p>data = [data1] + [data2]</p>
","A","4","18",NULL,"1",NULL,"2020-05-08 22:32:07",NULL),
("676","<br><p>Quelle est la valeur de la variable table après exécution du programme Python suivant ?</p><br><pre><code class='python'>table = [12, 43, 6, 22, 37]<br>for i in range(len(table) - 1):<br>    if table[i] > table[i+1]:<br>        table[i],table[i+1] = table[i+1], table[i]</code></pre><br>","<br><p>[6, 12, 22, 37, 43]</p><br>","<br><p>[12, 6, 22, 37, 43]</p><br>","<br><p>[43, 12, 22, 37, 6]</p><br>","<br><p>[43, 37, 22, 12, 6]</p><br>","B","4","18",NULL,"1",NULL,"2020-04-09 22:24:47",NULL),
("677","
<p>On considère l’extraction suivante d'une base de données des départements français. Cette extraction a ensuite été sauvegardée dans un fichier texte.</p>
<pre><code>'1','01','Ain','AIN','ain','A500'
'2','02','Aisne','AISNE','aisne','A250'
'3','03','Allier','ALLIER','allier','A460'
'4','04','Alpes-de-Haute-Provence','ALPES-DE-HAUTE-PROVENCE','alpes-de-haute-provence','A412316152'
'5','05','Hautes-Alpes','HAUTES-ALPES','hautes-alpes','H32412'</code></pre>
<p>Quel est le format de ce fichier ?</p>
","
<p>YML</p>
","
<p>XML</p>
","
<p>CSV</p>
","
<p>JSON</p>
","C","4","18",NULL,"1",NULL,"2020-05-08 22:33:34",NULL),
("678","<br><br><p>On souhaite construire une table de 4 lignes de 3 éléments que l’on va remplir de 0. Quelle syntaxe Python utilisera-t-on&nbsp;?</p><br>","<br><br><p>[ [ 0 ] * 3 for i in range (4) ]</p><br>","<br><br><p>for i in range (4) [ 0 ] * 3</p><br>","<br><br><p>[ 0 ] * 3 for i in range (4)</p><br>","<br><br><p>[ for i in range (4) [ 0 ] * 3 ]</p><br>","A","4","18",NULL,"1",NULL,"2020-03-25 14:11:10",NULL),
("679","<br><br><p>Lorsque la méthode POST est associée à un formulaire au sein d’une page HTML, comment les réponses du formulaire sont-elles envoyées au serveur ?</p><br>","<br><br><p>Elles sont visibles dans l’URL</p><br>","<br><br><p>Elles sont cachées de l’URL</p><br>","<br><br><p>Elles sont transmises via un service postal spécifique</p><br>","<br><br><p>Elles sont découpées en plusieurs petites URL limitées à 4 mots</p><br>","B","5","22",NULL,"1",NULL,"2020-03-25 14:11:22",NULL),
("680","<br><br><p>Les pages HTML sont affichées par …</p><br>","<br><br><p>le compilateur</p><br>","<br><br><p>le serveur</p><br>","<br><br><p>l'interpréteur</p><br>","<br><br><p>le navigateur Web</p><br>","A","5","22",NULL,"1",NULL,"2020-03-25 14:11:31",NULL),
("681","<br><br><p>Parmi les balises HTML ci-dessous quelle est celle qui permet à l’utilisateur de saisir son nom dans un formulaire en respectant la norme HTML ?</p><br>","<br><br><p>&lt;select /&gt;</p><br>","<br><br><p>&lt;form /&gt;</p><br>","<br><br><p>&lt;input type='text' /&gt;</p><br>","<br><br><p>&lt;input type='name' /&gt;</p><br>","C","5","22",NULL,"1",NULL,"2020-03-25 14:11:39",NULL),
("682","
<p>On considère cet extrait de fichier HTML représentant les onglets d'une barre de navigation :</p>
<pre><code class='html'>function BoutonGris() {
    var btn = document.createElement('BUTTON');
    btn.innerHTML = 'Annulation';
    document.getElementById('DIV').appendChild(btn);
}</code></pre>
","<br><p>elle remplace un élément DIV par un bouton</p><br>","<br><p>elle annule l'élément BUTTON</p><br>","<br><p>elle crée un bouton comportant le texte 'Annulation'</p><br>","<br><p>elle recherche le bouton 'BUTTON' et crée une copie appelée 'btn'</p><br>","C","5","22",NULL,"1",NULL,"2020-04-26 11:01:53",NULL),
("683","<br><br><p>Quelle est le code HTML permettant de créer un lien ?</p><br>","<br><br><p>&lt;a&gt;http://tip-top.fr &lt;/a&gt;</p><br>","<br><br><p>&lt;a href='http://tip-top.fr'&gt;Site du TIP-TOP&lt;/a&gt;</p><br>","<br><br><p>&lt;a name='http://tip.top.fr&lt;/a&gt;</p><br>","<br><br><p>&lt;a url=' http://tip-top.fr'&gt;Site du TIP-TOP&lt;/a&gt;</p><br>","B","5","22",NULL,"1",NULL,"2020-03-25 14:11:56",NULL),
("684","<br><br><p>Un fichier HTML contient la ligne suivante.</p><br><p>&lt;p&gt;Coucou ! Ca va?&lt;/p&gt;</p><br><p>Quelle commande CSS écrire pour que le texte apparaisse en rose sur fond jaune ?</p><br>","<br><br><p>p { couleur: rose ; fond: jaune;}</p><br>","<br><br><p>&lt;p&gt; { color = pink background-color = yellow}</p><br>","<br><br><p>&lt;p&gt; { color = pink ; background-color: yellow} &lt;/p&gt;</p><br>","<br><br><p>p { color: pink ; background-color: yellow ;}</p><br>","D","5","22",NULL,"1",NULL,"2020-03-25 14:12:05",NULL),
("685","<br><br><p>Dans la console Linux, quelle commande faut-il exécuter pour obtenir la liste des répertoires et dossiers contenus dans le répertoire courant ?</p><br>","<br><br><p>man pwd</p><br>","<br><br><p>cd pwd</p><br>","<br><br><p>ls -l</p><br>","<br><br><p>man ls -l</p><br>","C","6","27",NULL,"1",NULL,"2020-03-25 14:12:12",NULL),
("686","<br><br><p>Lequel de ces périphériques n'est pas un périphérique d'entrée ?</p><br>","<br><br><p>le moniteur</p><br>","<br><br><p>le clavier</p><br>","<br><br><p>la souris</p><br>","<br><br><p>le scanner</p><br>","A","6","27",NULL,"1",NULL,"2020-03-25 14:12:23",NULL),
("687","
<p>Lorsque, en ligne de commande, on saisit la commande <code>rm</code></p>
<p>ceci a pour effet :</p>
","
<p>d’activer une télécommande</p>
","
<p>d’accéder au répertoire parent du répertoire courant</p>
","
<p>d’effacer tous les fichiers du répertoire courant et ses sous-répertoires</p>
","
<p>d’effacer tous les fichiers du répertoire courant</p>
","D","6","27",NULL,"1",NULL,"2020-05-08 22:35:16",NULL),
("688","<br><br><p>Sachant que &nbsp;hibou est un fichier présent dans le répertoire courant, quel est l’effet de la commande suivante&nbsp;: mv hibou chouette</p><br>","<br><br><p>déplacer le fichier hibou dans le répertoire chouette</p><br>","<br><br><p>ajouter le contenu du fichier hibou à la fin du fichier chouette</p><br>","<br><br><p>renommer le fichier hibou en chouette</p><br>","<br><br><p>créer le fichier chouette, copie du fichier hibou</p><br>","C","6","27",NULL,"1",NULL,"2020-03-25 14:12:43",NULL),
("689","
<p>Sur la configuration IP d’une machine nommée MACH01 on peut lire :<br />
adresse Ipv4 : 172.16.100.201<br />
Masque de sous-réseau : 255.255.0.0<br />
Passerelle : 172.16.0.254</p>
<p>Sur la configuration IP d’une machine nommée MACH02 on peut lire :<br />
adresse Ipv4 : 172.16.100.202<br />
Masque de sous-réseau : 255.255.0.0<br />
Passerelle : 172.16.0.254</p>
<p>Depuis la machine MACH02, à l'aide de quelle commande peut-on tester le dialogue entre ces deux machines ?</p>
","
<p>ping 172.16.100.201</p>
","
<p>ping 172.16.100.202</p>
","
<p>ping 172.16.100.254</p>
","
<p>ping 255.255.0.0</p>
","A","6","27",NULL,"1",NULL,"2020-05-08 22:37:13",NULL),
("690","<p>Pour analyser les réponses saisies par l'utilisateur dans un formulaire d’une page Web personnelle, hébergée chez unfournisseur d'accès à internet, on dispose du code suivant :</p>
<pre><code class='html'>&lt;?php 
if ($_POST['choix']=='choix4'){
    echo 'Bravo !';
}
else{
    echo 'Non, vous vous trompez !';
}
?></code></pre>","<p>dans le premier routeur permettant d'accéder au serveur</p><br>","<p>dans le dernier routeur permettant d'accéder au serveur</p><br>","<p>dans le serveur qui héberge la page personnelle</p><br>","<p>dans la machine de l'utilisateur qui consulte la page personnelle</p><br>","C","6","27",NULL,"1",NULL,"2020-04-26 11:02:35",NULL),
("691","<p>On définit :</p><br><pre><code class='python'>def f(a,m):<br>    i = 1<br>    n = 0<br>    while n <= m:<br>        i = i * a<br>        n = n + 1<br>    return i<br></code></pre><br><p>Quelle est la valeur renvoyée par l'appel f(2,4) ?</p><br>","<p>8</p><br>","<p>16</p><br>","<p>32</p><br>","<p>64</p><br>","C","7","33",NULL,"1",NULL,"2020-04-09 22:36:54",NULL),
("692","<br><p>La fonction ajoute(n,p) codée ci-dessous en Python doit calculer la somme de tous les entiers compris entre n et p (n et p compris).</p><br><p>Par exemple, ajoute(2,4) doit renvoyer 2+3+4 = 9.</p><br><pre><code class='python'>def ajoute(n,p):<br>    somme = 0<br>    for i in range(.........): # ligne à modifier<br>        somme = somme + i<br>    return somme</code></pre><br><p>Quelle est la bonne écriture de la ligne marquée à modifier ?</p><br>","<br><p>for i in range(n,1,p):</p><br>","<br><p>for i in range(n,p):</p><br>","<br><p>for i in range(n,p+1):</p><br>","<br><p>for i in range(n-1,p):</p><br>","C","7","33",NULL,"1",NULL,"2020-04-09 22:38:15",NULL),
("693","<br><p>En voulant programmer une fonction qui calcule la valeur minimale d'une liste d'entiers, on a écrit :</p><br><pre><code class='python'>def minimum(L):<br>    mini = 0<br>    for e in L:<br>        if e < mini:<br>            mini = e<br>    return mini</code></pre><br><p>Cette fonction a été mal programmée. Pour quelle liste ne donnera-t-elle pas le résultat attendu, c'est-à-dire son minimum ?</p><br>","<br><p>[-1,-8,12,2,23]</p><br>","<br><p>[0,18,12,2,3]</p><br>","<br><p>[-1,-1,12,12,23]</p><br>","<br><p>[1,8,12,2,23]</p><br>","D","7","33",NULL,"1",NULL,"2020-04-09 22:39:26",NULL),
("694","<br><p>On exécute le code suivant :</p><br><pre><code class='python'>def essai():<br>    a = 2<br>    b = 3<br>    c = 4<br>    return a<br>    return b<br>    return c<br><br>t = essai()</code></pre><br><p>Quelle est la valeur de t après l'exécution de ce code ?</p><br>","<br><p>2</p><br>","<br><p>3</p><br>","<br><p>4</p><br>","<br><p>(2,3,4)</p><br>","A","7","33",NULL,"1",NULL,"2020-04-09 22:41:00",NULL),
("695","<br><p>On définit la fonction suivante :</p><br><pre><code class='python'>def f(x):<br>    for d in range(2,x):<br>        if x%d == 0:<br>            return d</code></pre><br><p>Quelle est la valeur renvoyée par l'appel f(15) ?</p><br>","<br><p>3</p><br>","<br><p>5</p><br>","<br><p>3,5</p><br>","<br><p>3,5,15</p><br>","A","7","33",NULL,"1",NULL,"2020-04-09 22:42:34",NULL),
("696","<p>On exécute le code suivant :</p>
<pre><code class='python'>def f(t):
    n = len(t)
    for k in range(1,n):
        t[k] = t[k] + t[k-1]

L = [1, 3, 4, 5, 2]
f(L)</code></pre>
<p>Quelle est la valeur de L après l'exécution de ce code ?</p>
","<pre><code class='python'>[1, 3, 4, 5, 2]</code></pre>
","<pre><code class='python'>[1, 4, 7, 9, 7]</code></pre>
","<pre><code class='python'>[1, 4, 8, 13, 15]</code></pre>
","<pre><code class='python'>[3, 6, 10, 15, 17]</code></pre>
","C","7","33",NULL,"1",NULL,"2020-05-12 14:26:05",NULL),
("697","<br><p><span class='math inline'>\(a\)</span> et <span class='math inline'>\(m\)</span> étant deux entiers supérieurs à 1, la fonction suivante renvoie <span class='math inline'>\(a^{m}\)</span>.</p><br><pre><code class='python'>def puissance(a,m):<br>    p = 1<br>    n = 0<br>    while n < m:<br>        #<br>        p = p * a<br>        n = n + 1<br>    return p</code></pre><br><p>Quelle est l'égalité qui est vérifiée à chaque passage par la ligne marquée # ?</p><br>","<br><p><span class='math inline'>\(p = a^{n - 1}\)</span></p><br>","<br><p><span class='math inline'>\(p = a^{n}\)</span></p><br>","<br><p><span class='math inline'>\(p = a^{n + 1}\)</span></p><br>","<br><p><span class='math inline'>\(p = a^{m}\)</span></p><br>","B","8","39",NULL,"1",NULL,"2020-04-09 22:45:39",NULL),
("698","<br><p>Quelle est la valeur de element à la fin de l'exécution du code suivant :</p><br><pre><code class='python'>L = [1, 2, 3, 4, 1, 2, 3, 4, 0, 2]<br>element = L[0]<br>for k in L:<br>    if k > element:<br>        element = k</code></pre><br>","<br><p>0</p><br>","<br><p>1</p><br>","<br><p>4</p><br>","<br><p>10</p><br>","C","8","39",NULL,"1",NULL,"2020-04-09 22:47:01",NULL),
("699","<br><br><p>On dispose en quantité illimité de pièces de 1 euro, 2 euros et 5 euros. On veut totaliser une somme de 18&nbsp;euros. Quelle est la solution donnée par l’algorithme glouton ?</p><br>","<br><br><p>[5, 5, 5, 2, 1]</p><br>","<br><br><p>[5, 5, 5, 2, 2, 1]</p><br>","<br><br><p>[5, 5, 2, 2, 2, 1, 1]</p><br>","<br><br><p>[5, 2, 2, 2, 2, 1, 1, 1, 1, 1]</p><br>","A","8","39",NULL,"1",NULL,"2020-03-25 14:14:25",NULL),
("700","<br><p>On considère la fonction suivante :</p><br><pre><code class='python'>def f(x,L):<br>    g = 0<br>    d = len(L)-1<br>    while g < d:<br>        m = (g+d)//2<br>        if x <= L[m]:<br>            d = m<br>        else:<br>            g = m + 1<br>    return g</code></pre><br><p>Cette fonction implémente :</p><br>","<br><p>le tri par insertion</p><br>","<br><p>le tri par sélection</p><br>","<br><p>la recherche dichotomique</p><br>","<br><p>la recherche du plus proche voisin</p><br>","C","8","39",NULL,"1",NULL,"2020-04-09 22:54:51",NULL),
("701","<br><p>On considère la fonction suivante :</p><br><pre><code class='python'><br>def comptage(phrase,lettre):<br>    i = 0<br>    for j in phrase:<br>        if j == lettre:<br>            i = i+1<br>    return i<br></code></pre><br><p>Que renvoie l'appel comptage('Vive l’informatique','e') ?</p><br>","<br>0<br>","<br>2<br>","<br>19<br>","<br>'e'<br>","B","8","39",NULL,"1",NULL,"2020-04-15 10:29:10",NULL),
("702","<p>Quelle est la valeur du couple (s,i) à la fin de l'exécution du script suivant ?</p>
<pre><code class='python'>s = 0
i = 1
while i &lt; 5:
    s = s + i
    i = i + 1</code></pre>
","(4, 5)
","(10, 4)
","(10, 5)
","(15, 5)
","C","8","39",NULL,"1",NULL,"2020-05-08 22:38:08",NULL),
("703","<br><br><p>Quelle est l'écriture décimale de l'entier qui s'écrit 1010 en binaire ?</p><br>","<br><br><p>5</p><br>","<br><br><p>10</p><br>","<br><br><p>20</p><br>","<br><br><p>22</p><br>","B","2","7",NULL,"1",NULL,"2020-03-25 14:15:41",NULL),
("704","<br><p>Quelle est la valeur de x à la fin de l'exécution du script Python suivant ?</p><br><pre><code class='python'>x = 1<br>for i in range(10):<br>    x = x * 2<br></code></pre><br>","<br><p>2</p><br>","<br><p>1024</p><br>","<br><p>2048</p><br>","<br><p>20000000000</p><br>","B","2","7",NULL,"1",NULL,"2020-04-22 22:37:16",NULL),
("705","<br><br><p>Parmi les quatre propositions, quelle est celle qui correspond au résultat de l'addition en écriture binaire 1101&nbsp;1001&nbsp;+&nbsp;11&nbsp;0110 ?</p><br>","<br><br><p>1000&nbsp;1111</p><br>","<br><br><p>10 0000 1111</p><br>","<br><br><p>1&nbsp;0000&nbsp;1111</p><br>","<br><br><p>1&nbsp;1000&nbsp;0111</p><br>","C","2","7",NULL,"1",NULL,"2020-03-25 14:15:58",NULL),
("706","<br><br><p>Dans quel système de numération 3F5 représente-t-il un nombre entier&nbsp;?</p><br>","<br><br><p>binaire (base 2)</p><br>","<br><br><p>octal (base 8)</p><br>","<br><br><p>décimal (base 10)</p><br>","<br><br><p>hexadécimal (base 16)</p><br>","D","2","7",NULL,"1",NULL,"2020-03-25 14:16:06",NULL),
("707","<p>Soient <span class='math inline'>\(P\)</span> et <span class='math inline'>\(Q\)</span> deux formules logiques telles que <span class='math inline'>\(P\)</span> est vraie et <span class='math inline'>\(Q\)</span> est fausse.</p><br><p>Quelle est la valeur de l'expression <span class='math inline'>\((P \text{ ET } Q) \text{ OU } (\text{ NON }(P) \text{ OU } Q)\)</span>?</p><br>","<p>vraie</p><br>","<p>fausse</p><br>","<p>ni vraie, ni fausse</p><br>","<p>vraie et fausse en même temps</p><br>","B","2","7",NULL,"1",NULL,"2020-04-22 22:35:20",NULL),
("708","<br><br><p>Parmi les quatre propositions, quelle est celle qui correspond au résultat de la soustraction en écriture hexadécimale CD8FA&nbsp;-&nbsp;9FF81?</p><br>","<br><br><p>2E979</p><br>","<br><br><p>3D989</p><br>","<br><br><p>2D979</p><br>","<br><br><p>2DA979</p><br>","C","2","7",NULL,"1",NULL,"2020-03-25 14:16:24",NULL),
("709","<br><br><p>On définit la liste L ainsi :</p><br><p>L = [ [1], [1,2], [1,2,3] ]</p><br><p>Des égalités suivantes, une seule est fausse. Laquelle&nbsp;?</p><br>","<br><br><p>len(L[0]) == 1</p><br>","<br><br><p>len(L) == 6</p><br>","<br><br><p>len(L[2]) == 3</p><br>","<br><br><p>L[2][2] == 3</p><br>","B","3","13",NULL,"1",NULL,"2020-03-25 14:16:32",NULL),
("710","<br><br><p>Laquelle des expressions suivantes a-t-elle pour valeur la liste des carrés des premiers entiers qui ne sont <strong>pas</strong> multiples de&nbsp;5&nbsp;?</p><br>","<br><br><p>[x*x for x in range (11) if x//5 != 0]</p><br>","<br><br><p>[x*x if x%5 != 0 for x in range (11)]</p><br>","<br><br><p>[x*x if x//5 != 0 for x in range (11)]</p><br>","<br><br><p>[x*x for x in range (11) if x%5 != 0]</p><br>","D","3","13",NULL,"1",NULL,"2020-03-25 14:16:41",NULL),
("711","<br><p>L est une liste d'entiers.</p><br><p>On définit la fonction suivante :</p><br><pre><code class='python'>def f(L):<br>    m = L[0]<br>    for x in L:<br>        if x > m:<br>            m = x<br>    return m<br></code></pre><br><p>Que calcule cette fonction ?</p><br>","<br><p>le maximum de la liste L passée en argument</p><br>","<br><p>le minimum de la liste L passée en argument</p><br>","<br><p>le premier terme de la liste L passée en argument</p><br>","<br><p>le dernier terme de la liste L passée en argument</p><br>","A","3","13",NULL,"1",NULL,"2020-04-22 22:39:23",NULL),
("712","<br><p>On exécute le code suivant :</p><br><pre><code class='python'>placard = { 'chemise': 3, 'pantalon': 6, 'tee shirt': 7 }<br>placard['chaussette'] = 4<br>placard['chemise'] = 5<br>L = list(placard.values())<br></code></pre><br><p>Quelle est la valeur de la variable L à l'issue de cette exécution ?</p><br>","<br><p>[ 3, 6, 7 ]</p><br>","<br><p>[ 3, 6, 7, 4 ]</p><br>","<br><p>[ 5, 6, 7 ]</p><br>","<br><p>[ 5, 6, 7, 4 ]</p><br>","D","3","13",NULL,"1",NULL,"2020-04-22 22:40:56",NULL),
("713","<br><br><p>Quelle est la valeur de l'expression [[i,2*i] for i in range(3)]&nbsp;?</p><br>","<br><br><p>[0,0,1,2,2,4]</p><br>","<br><br><p>[[0,0],[1,2],[2,4]]</p><br>","<br><br><p>[1,2,2,4,3,6]</p><br>","<br><br><p>[[1,2],[2,4],[3,6]]</p><br>","B","3","13",NULL,"1",NULL,"2020-03-25 14:17:07",NULL),
("714","<br><p>On définit le dictionnaire dico par les instructions suivantes&nbsp;:</p><br><pre><code class='python'>def f(x):<br>    return x*x<br><br>def g(x):<br>    return x + x<br><br>def h(x):<br>    return 3*x<br><br>dico = { 'F': f, 'G': g(6), 'H': h }<br></code></pre><br><p>Une seule des affirmations suivantes est incorrecte. Laquelle ?</p><br>","<br><p>dico['F'] est une fonction</p><br>","<br><p>dico['F'](5) est un entier</p><br>","<br><p>dico['G'] est une fonction</p><br>","<br><p>dico['G'] est un entier</p><br>","C","3","13",NULL,"1",NULL,"2020-04-22 22:42:23",NULL),
("715","<br><p>On dispose du fichier «&nbsp;info.csv&nbsp;» donné ci-dessous&nbsp;:</p><br><p>nom, prenom, naissance, deces<br><br>lovelace, ada, 1815, 1852<br><br>von neumann, john, 1903, 1957<br><br>turing, alan, 1912, 1954<br><br>mccarthy, john, 1927, 2011<br><br>floyd, robert, 1936, 2001</p><br><p>Le programme ci-dessous nous permet de créer un tableau à partir de ce fichier.</p><br><pre><code class='python'>file = open('info.csv', 'r')<br>firstLine = file.readline() # chargement de la ligne d’entête<br>tableau = [line.split(',') for line in file] # chargement des données<br></code></pre><br><p>Les index des lignes de ce tableau vont…</p><br>","<br><p>de 0 à 3</p><br>","<br><p>de 1 à 4</p><br>","<br><p>de 0 à 4</p><br>","<br><p>de 0 à 5</p><br>","C","4","18",NULL,"1",NULL,"2020-04-22 22:45:07",NULL),
("716","<br><br><p>On utilise habituellement un fichier d'extension csv pour quel type de données ?</p><br>","<br><br><p>des données structurées graphiquement</p><br>","<br><br><p>des données sonores</p><br>","<br><br><p>des données compressées</p><br>","<br><br><p>des données structurées en tableau</p><br>","D","4","18",NULL,"1",NULL,"2020-03-25 14:17:36",NULL),
("717","<br><p>On considère la table suivante&nbsp;:</p><br><pre><code class='python'>t = [ {'type': 'marteau', 'prix': 17, 'quantité': 32},<br>{'type': 'scie', 'prix': 24, 'quantité': 3},<br>{'type': 'tournevis', 'prix': 8, 'quantité': 45} ]<br></code></pre><br><p>Quelle expression permet d'obtenir la quantité de scies ?</p><br>","<br><p>t[2]['quantité']</p><br>","<br><p>t[1]['quantité']</p><br>","<br><p>t['quantité'][1]</p><br>","<br><p>t['scie']['quantité']</p><br>","B","4","18",NULL,"1",NULL,"2020-04-22 22:47:13",NULL),
("718","<br><p>On définit les notes des élèves dans le dictionnaire suivant :</p><br><pre><code class='python'>notes = { 'Pierre' : 9, 'Paule' : 16, 'Claire' : 12,<br>'Sophie' : 11, 'Yasmine' : 8,<br>'David' : 17, 'Julie' : 3 }<br></code></pre><br><p>Quelle expression permet d'accéder à la note de l’élève Claire ?</p><br>","<br><p>notes[12]</p><br>","<br><p>notes[2]</p><br>","<br><p>notes['Claire']</p><br>","<br><p>notes.values('Claire')</p><br>","C","4","18",NULL,"1",NULL,"2020-04-22 22:50:51",NULL),
("719","<br><p>Soit la table de données suivante :</p><br><p>nom,prenom,date_naissance<br><br>Dupont,Pierre,17/05/1987<br><br>Dupond,Catherine,18/07/1981<br><br>Haddock,Archibald,23/04/1998</p><br><p>Quels sont les descripteurs de ce tableau&nbsp;?</p><br>","<br><p>nom, prenom et date_naissance</p><br>","<br><p>Dupont, Pierre et 17/05/1987</p><br>","<br><p>Dupont, Dupond et Haddock</p><br>","<br><p>il n'y en a pas</p><br>","A","4","18",NULL,"1",NULL,"2020-04-22 22:49:16",NULL),
("720","<p>On définit :</p><br><pre><code class='python'>stock = [ {'nom': 'flageolets', 'quantité': 50, 'prix': 5.68},<br>          {'nom': 'caviar', 'quantité': 0, 'prix': 99.99},<br>          .........<br>          .........<br>          {'nom': 'biscuits', 'quantité': 100, 'prix': 7.71} ]<br></code></pre><br><p>Quelle expression permet d'obtenir la liste des noms des produits effectivement présents dans le stock (c'est-à-dire ceux dont la quantité n'est pas nulle) ?</p><br>","<code class='python'>['nom' for p in stock if 'quantité' != 0]</code><br>","<code class='python'>[p for p in stock if p['quantité'] != 0]</code><br>","<code class='python'>[p['nom'] for p in stock if 'quantité' != 0]</code><br>","<code class='python'>[p['nom'] for p in stock if p['quantité'] != 0]</code><br>","D","4","18",NULL,"1",NULL,"2020-04-22 22:32:42",NULL),
("721","<br><br><p>Par quoi commence l’URL d’une page Web sécurisée ?</p><br>","<br><br><p>http</p><br>","<br><br><p>https</p><br>","<br><br><p>ftp</p><br>","<br><br><p>smtp</p><br>","B","5","22",NULL,"1",NULL,"2020-03-25 14:18:20",NULL),
("722","<br><p>Quelle est la machine qui exécute un programme JavaScript inclus dans une page HTML&nbsp;?</p><br>","<br><p>le serveur WEB qui contient la page HTML</p><br>","<br><p>la machine de l'utilisateur qui consulte la page HTML</p><br>","<br><p>un serveur du réseau</p><br>","<br><p>un routeur du réseau</p><br>","B","5","22",NULL,"1",NULL,"2020-04-22 22:54:53",NULL),
("723","<br><p>On considère cet extrait de fichier HTML représentant les onglets d'une barre de navigation :</p><br><pre><code class='html'>function BoutonGris() {<br>    var btn = document.createElement('BUTTON');<br>    btn.innerHTML = 'Annulation';<br>    document.getElementById('DIV').appendChild(btn);<br>}</code></pre><br><p>Que fait la fonction BoutonGris ?</p>","<br><p>elle remplace un élément DIV par un bouton</p><br>","<br><p>elle annule l'élément BUTTON</p><br>","<br><p>elle crée un bouton comportant le texte 'Annulation'</p><br>","<br><p>elle recherche le bouton 'BUTTON' et crée une copie appelée 'btn'</p><br>","C","5","22",NULL,"1",NULL,"2020-04-22 22:54:15",NULL),
("724","<br><br><p>Quelle URL parmi les suivantes témoigne que l'échange entre le navigateur et le serveur est chiffré ?</p><br>","<br><br><p>http://www.mabanque.com/</p><br>","<br><br><p>http://www.mabanque.fr/</p><br>","<br><br><p>https://www.mabanque.fr/</p><br>","<br><br><p>http://secure.mabanque.fr/</p><br>","C","5","22",NULL,"1",NULL,"2020-03-25 14:18:50",NULL),
("725","<br><br><p>Parmi les couples de balises suivants, lequel permet de créer un formulaire ?</p><br>","<br><br><p>&lt;body&gt; &lt;/body&gt;</p><br>","<br><br><p>&lt;html&gt; &lt;/html&gt;</p><br>","<br><br><p>&lt;div&gt; &lt;/div&gt;</p><br>","<br><br><p>&lt;form&gt; &lt;/form&gt;</p><br>","D","5","22",NULL,"1",NULL,"2020-03-25 14:18:58",NULL),
("726","<br><br><p>Quelle méthode est utilisée via une requête HTTP pour envoyer une image via un formulaire HTML ?</p><br>","<br><br><p>HEAD</p><br>","<br><br><p>PUT</p><br>","<br><br><p>POST</p><br>","<br><br><p>GET</p><br>","C","5","22",NULL,"1",NULL,"2020-03-25 14:19:06",NULL),
("727","<br><br><p>Sous Linux, la console indique que l'utilisateur se trouve dans le dossier /var/lib. Quelle commande doit-il exécuter pour revenir dans son dossier personnel /home/martin ?</p><br>","<br><br><p>cd ~</p><br>","<br><br><p>cd /home</p><br>","<br><br><p>dir</p><br>","<br><br><p>dir /home/martin</p><br>","A","6","27",NULL,"1",NULL,"2020-03-25 14:19:14",NULL),
("728","<br><br><p>Le répertoire personnel de l'utilisateur contient deux répertoires tempo et sauve.</p><br><p>On souhaite déplacer le fichier bac.txt du repertoire tempo vers le répertoire sauve.</p><br><p>Quelle commande permet de réaliser ce déplacement ?</p><br>","<br><br><p>mkdir ~/tempo/bac.txt ~/sauve</p><br>","<br><br><p>mkdir ~/sauve ~/tempo/bac.txt</p><br>","<br><br><p>mv ~/tempo/bac.txt ~/sauve</p><br>","<br><br><p>mv ~/sauve ~/tempo/bac.txt</p><br>","C","6","27",NULL,"1",NULL,"2020-03-25 14:19:23",NULL),
("729","<br><p>Vivien télécharge un logiciel à partir d’un site commercial. Le transfert par Internet du logiciel a débuté entre le serveur (machine S) et son domicile (machine V). On a représenté des routeurs A, B, C, D et E et les liens existants. Les paquets IP suivent le chemin passant par les routeurs A, B, C et E.</p><br><p>Durant un orage, la foudre frappe et détruit le serveur C par lequel transitent les paquets correspondant au fichier que télécharge Vivien. Que se passe-t-il ?</p><br>","<br><p>la liaison étant coupée, le serveur ne sera plus accessible</p><br>","<br><p>le téléchargement n’est pas interrompu&nbsp;car les paquets peuvent transiter par le routeur D</p><br>","<br><p>le téléchargement est interrompu, Vivien doit redémarrer une nouvelle connexion à partir de</p><br><p>zéro</p><br>","<br><p>le téléchargement se poursuit mais des données seront perdues</p><br>","B","6","27",NULL,"1","729_reseau.jpg","2020-04-22 23:19:30",NULL),
("730","<br><br><p>La mémoire RAM :</p><br>","<br><br><p>ne fonctionne qu'en mode lecture</p><br>","<br><br><p>ne fonctionne qu'en mode écriture</p><br>","<br><br><p>conserve les données en cas de coupure de l'alimentation</p><br>","<br><br><p>perd les données en cas de coupure de l'alimentation</p><br>","D","6","27",NULL,"1",NULL,"2020-03-25 14:19:44",NULL),
("731","<br><br><p>Dans un ordinateur, que permet de faire la mémoire vive&nbsp;?</p><br>","<br><br><p>Stocker les données de façon permanente</p><br>","<br><br><p>Afficher les informations sur l’écran</p><br>","<br><br><p>Réaliser les calculs</p><br>","<br><br><p>Stocker les données de façon temporaire</p><br>","D","6","27",NULL,"1",NULL,"2020-03-25 14:19:53",NULL),
("732","<br><br><p>Parmi ces propositions, laquelle désigne un système d'exploitation libre ?</p><br>","<br><br><p>LibreOffice</p><br>","<br><br><p>Windows</p><br>","<br><br><p>MacOS</p><br>","<br><br><p>GNU-Linux</p><br>","D","6","27",NULL,"1",NULL,"2020-03-25 14:20:02",NULL),
("733","<br><p>On exécute le script suivant :</p><br><pre><code class='python'>a = 4<br>b = 4<br>c = 4<br>while a < 5:<br>    a = a - 1<br>    b = b + 1<br>    c = c * b<br></code></pre><br><p>Que peut-on dire ?</p><br>","<br><p>ce programme ne termine pas</p><br>","<br><p>à la fin de l'exécution, la variable a vaut 5</p><br>","<br><p>à la fin de l'exécution, la variable b vaut 34</p><br>","<br><p>à la fin de l'exécution, la variable c vaut 42</p><br>","A","7","33",NULL,"1",NULL,"2020-04-22 23:05:40",NULL),
("734","<br><p>On a défini une liste L de nombres entiers.</p><br><p>Quelle est la valeur de la variable m à la fin de l'exécution du script suivant ?</p><br><pre><code class='python'>m = L[0]<br>for j in range(len(L)):<br>    if m < L[j]:<br>        m = L[j]<br></code></pre><br>","<br><p>la moyenne de la liste L</p><br>","<br><p>le minimum de la liste L</p><br>","<br><p>le maximum de la liste L</p><br>","<br><p>la longueur de la liste L</p><br>","C","7","33",NULL,"1",NULL,"2020-04-22 23:03:56",NULL),
("735","<br><p>Dans le programme JavaScript suivant, quelle est la notation qui délimite le bloc d’instructions exécuté à chaque passage dans la boucle while ?</p><br><pre><code class='html'>i = 0<br>while (i &lt; 10) {<br>    alert(i)<br>    i = i + 1<br>}<br>alert('Fin')<br></code></pre><br>","<br><p>le fait que les instructions soient encadrées entre { et }</p><br>","<br><p>le fait que les instructions soient indentées de 4 caractères comme en Python</p><br>","<br><p>le fait que les instructions suivent le mot clé while</p><br>","<br><p>le fait que les instructions suivent la parenthèse )</p><br>","A","7","33",NULL,"1",NULL,"2020-04-22 23:01:05",NULL),
("736","<br><p>On souhaite écrire une fonction qui renvoie le maximum d'une liste d'entiers :</p><br><pre><code class='python'>def maximum(L):<br>    m = L[0]<br>    for i in range(1,len(L)):<br>        if .........:<br>            m = L[i]<br>    return m<br></code></pre><br><p>Par quoi faut-il remplacer les pointillés pour que cette fonction produise bien le résultat attendu ?</p><br>","<br><p>i &gt; m</p><br>","<br><p>L[i] &gt; m</p><br>","<br><p>L[i] &gt; L[i-1]</p><br>","<br><p>L[i] &gt; L[i+1]</p><br>","B","7","33",NULL,"1",NULL,"2020-04-22 22:56:37",NULL),
("737","<br><p>On définit la fonction :</p><br><pre><code class='python'>def f(a,b):<br>    assert b!=0,'le deuxième argument est nul'<br>    result = a/b<br>    return result<br></code></pre><br><p>Qu'obtient-on en exécutant la commande r = f(4,0) ?</p><br>","<br><p>une erreur 'ZeroDivisionError: division by zero' et l'arrêt de l'exécution</p><br>","<br><p>une erreur 'NameError: name 'b' is not defined' et l'arrêt de l'exécution</p><br>","<br><p>une erreur 'AssertionError: le deuxième argument est nul' et la variable r prend la valeur 0</p><br>","<br><p>une erreur 'AssertionError: le deuxième argument est nul' et l'arrêt de l'exécution</p><br>","D","7","33",NULL,"1",NULL,"2020-04-22 22:58:21",NULL),
("738","<br><p>Quelles sont les valeurs des variables x et y à la fin de l'exécution du script suivant :</p><br><pre><code class='python'>x = 4<br>while x > 0:<br>    y = 1<br>while y < x:<br>    y = y + 1<br>    x = x - 1<br></code></pre>","<br><p>l'exécution ne termine pas !</p><br>","<br><p>la valeur de x est 0, celle de y est 0</p><br>","<br><p>la valeur de x est 0, celle de y est 1</p><br>","<br><p>la valeur de x est -1, celle de y est 0</p><br>","A","7","33",NULL,"1",NULL,"2020-04-22 23:07:14",NULL),
("739","<br><p>À la fin de l'exécution du code suivant, quelle sera la valeur de la variable cpt ?</p><br><pre><code class='python'>a = 1<br>cpt = 20<br>while cpt > 8:<br>    a = 2*a<br>    cpt = cpt - 1<br></code></pre>","<br><p>0</p><br>","<br><p>7</p><br>","<br><p>8</p><br>","<br><p>9</p><br>","C","8","39",NULL,"1",NULL,"2020-04-22 23:08:18",NULL),
("740","<br><br><p>En utilisant une recherche dichotomique, combien faut-il de comparaisons pour trouver une valeur dans un tableau trié de 1000 nombres ?</p><br>","<br><br><p>3</p><br>","<br><br><p>10</p><br>","<br><br><p>1000</p><br>","<br><br><p>1024</p><br>","B","8","39",NULL,"1",NULL,"2020-03-25 14:21:15",NULL),
("741","<br><br><p>Un algorithme de recherche dichotomique dans une liste triée de taille <span class='math inline'>\(n\)</span> nécessite, dans le pire des cas, exactement <span class='math inline'>\(k\)</span> comparaisons.</p><br><p>Combien cet algorithme va-t-il utiliser, dans le pire des cas, de comparaisons sur une liste de taille <span class='math inline'>\(2n\)</span> ?</p><br>","<br><br><p><span class='math inline'>\(k\)</span></p><br>","<br><br><p><span class='math inline'>\(k + 1\)</span></p><br>","<br><br><p><span class='math inline'>\(2k\)</span></p><br>","<br><br><p><span class='math inline'>\(2k + 1\)</span></p><br>","B","8","39",NULL,"1",NULL,"2020-03-25 14:21:24",NULL),
("742","<br><p>On définit la fonction suivante&nbsp;:</p><br><pre><code class='python'>def traitement(liste) :<br>    m = liste[0]<br>    for i in range (len(liste)) :<br>        if liste[i] > m:<br>            m = liste[i]<br>    return m<br></code></pre><br><p>Que vaut traitement([-2,5,6,-10,35])&nbsp;?</p><br>","<br><p>None</p><br>","<br><p>-10</p><br>","<br><p>-6</p><br>","<br><p>35</p><br>","D","8","39",NULL,"1",NULL,"2020-04-22 23:09:51",NULL),
("743","<br><p><span class='math inline'>\(a\)</span> et <span class='math inline'>\(m\)</span> étant deux entiers supérieurs à 1, la fonction suivante renvoie <span class='math inline'>\(a^{m}\)</span>.</p><br><pre><code class='python'>def puissance(a,m):<br>    p = 1<br>    n = 0<br>    while n < m:<br>        p = p * a<br>        #<br>        n = n + 1<br>    return p<br></code></pre><br><p>Quelle est l'égalité qui est vérifiée à chaque passage par la ligne marquée # ?</p><br>","<br><p><span class='math inline'>\(p = a^{n - 1}\)</span></p><br>","<br><p><span class='math inline'>\(p = a^{n}\)</span></p><br>","<br><p><span class='math inline'>\(p = a^{n + 1}\)</span></p><br>","<br><p><span class='math inline'>\(p = a^{m}\)</span></p><br>","C","8","39",NULL,"1",NULL,"2020-04-22 23:11:08",NULL),
("744","<br><br><p>Quelle valeur permet de compléter l’affirmation suivante&nbsp;: «&nbsp;Le nombre d’opérations nécessaires pour rechercher un élément séquentiellement dans un tableau de longueur <span class='math inline'>\(n\)</span> est de l’ordre de …&nbsp;»&nbsp;?</p><br>","<br><br><p>1</p><br>","<br><br><p><span class='math inline'>\(n\)</span></p><br>","<br><br><p><span class='math inline'>\(n^{2}\)</span></p><br>","<br><br><p><span class='math inline'>\(n^{3}\)</span></p><br>","B","8","39",NULL,"1",NULL,"2020-03-25 14:21:55",NULL),
("745","<br><br><p>Parmi les quatre propositions, quelle est celle qui correspond au résultat de la soustraction en écriture binaire 1010&nbsp;1101&nbsp;-&nbsp;101&nbsp;1000 ?</p><br>","<br><br><p>101&nbsp;0101</p><br>","<br><br><p>110 0001</p><br>","<br><br><p>100&nbsp;1111</p><br>","<br><br><p>1&nbsp;1000&nbsp;0111</p><br>","A","2","7",NULL,"1",NULL,"2020-03-25 14:22:49",NULL),
("746","<br><br><p>Quelle est la représentation binaire en complément à deux sur huit bits du nombre –3 ?</p><br>","<br><br><p>1000 0011</p><br>","<br><br><p>1111 1100</p><br>","<br><br><p>1111 1101</p><br>","<br><br><p>1 0000 0011</p><br>","C","2","7",NULL,"1",NULL,"2020-03-25 14:22:58",NULL),
("747","<br><br><p>Quel est le plus grand entier positif (non signé) représentable en binaire sur 2 octets (c'est-à-dire 16 bits) ?</p><br>","<br><br><p><span class='math inline'>\(2^{15} - 1\)</span></p><br>","<br><br><p><span class='math inline'>\(2^{15}\)</span></p><br>","<br><br><p><span class='math inline'>\(2^{16} - 1\)</span></p><br>","<br><br><p><span class='math inline'>\(2^{16}\)</span></p><br>","C","2","7",NULL,"1",NULL,"2020-03-25 14:23:07",NULL),
("748","<br><br><p>Combien d'entiers positifs ou nuls (entiers non signés) peut-on représenter en machine sur 32&nbsp;bits&nbsp;?</p><br>","<br><br><p><span class='math inline'>\(2^{32} - 1\)</span></p><br>","<br><br><p><span class='math inline'>\(2^{32}\)</span></p><br>","<br><br><p><span class='math inline'>\(2 \times 32\)</span></p><br>","<br><br><p><span class='math inline'>\(32^{2}\)</span></p><br>","B","2","7",NULL,"1",NULL,"2020-03-25 14:23:16",NULL),
("749","<br><br><p>Quelle est l'écriture décimale de l'entier qui s'écrit 1010 en binaire ?</p><br>","<br><br><p>5</p><br>","<br><br><p>10</p><br>","<br><br><p>20</p><br>","<br><br><p>22</p><br>","B","2","7",NULL,"1",NULL,"2020-03-25 14:23:25",NULL),
("750","<p>À quelle affectation sont équivalentes les instructions suivantes, où a, b sont des variables entières et c une variable booléenne ?</p>
<pre><code class='python'>c = False
if a==b:
    c = True
if a &gt; b+10:
    c = True</code></pre>","<p>c = (a==b) or (a &gt; b+10)</p>
","<p>c = (a==b) and (a &gt; b+10)</p>
","<p>c = not(a==b)</p>
","<p>c = not(a &gt; b+10)</p>
","A","2","7",NULL,"1",NULL,"2020-05-08 21:02:52",NULL),
("751","
<p>Après l'affectation suivante :</p>
<pre><code class='python'>alphabet = [ 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' ]</code></pre>
<p>quelle est l'expression qui permet d'accéder à la lettre E ?</p>
","
<p>alphabet.E</p>
","
<p>alphabet['E']</p>
","
<p>alphabet[4]</p>
","
<p>alphabet[5]</p>
","C","3","13",NULL,"1",NULL,"2020-05-08 16:35:45",NULL),
("752","<br><br><p>On définit une liste<strong>&nbsp;:</strong> L = [1, 1, 2, 9, 3, 4, 5, 6, 7].</p><br><p>Quelle expression a-t-elle pour valeur la liste [4, 16, 36] ?</p><br>","<br><br><p>[(x * x) % 2 == 0 for x in liste]</p><br>","<br><br><p>[x for x in liste if x % 2 == 0]</p><br>","<br><br><p>[x * x for x in liste]</p><br>","<br><br><p>[x * x for x in liste if x % 2 == 0]</p><br>","D","3","13",NULL,"1",NULL,"2020-03-25 14:23:53",NULL),
("753","<br><br><p>Que vaut l'expression [ 2*k for k in range(5) ] ?</p><br>","<br><br><p>[0,2,4,6,8]</p><br>","<br><br><p>[2,4,6,8,10]</p><br>","<br><br><p>[1,2,4,8,16]</p><br>","<br><br><p>[2,4,8,16,32]</p><br>","A","3","13",NULL,"1",NULL,"2020-03-25 14:24:01",NULL),
("787","<br><br><p>Quelle est la représentation binaire, en complément à 2 sur 8 bits, de l'entier négatif –25 ?</p><br>","<br><br><p>0001 1001</p><br>","<br><br><p>1001 1001</p><br>","<br><br><p>1110 0110</p><br>","<br><br><p>1110 0111</p><br>","D","2","7",NULL,"1",NULL,"2020-03-25 14:29:59",NULL),
("788","
<p>On considère les nombres dont l'écriture en base 16 (en hexadécimal) est de la forme suivante : un 1 suivi d'un nombre quelconque de 0, comme 1, 10, 100, 1000 etc.</p>
<p>Tous ces nombres sont exactement :</p>
","
<p>les puissances de 2</p>
","
<p>les puissances de 8</p>
","
<p>les puissances de 10</p>
","
<p>les puissances de 16</p>
","D","2","7",NULL,"1",NULL,"2020-05-08 11:07:06",NULL),
("789","<br><br><p>En ajoutant trois chiffres 0 à droite de l'écriture binaire d'un entier <span class='math inline'>\(N\)</span> strictement positif, on obtient l'écriture binaire de :</p><br>","<br><br><p><span class='math inline'>\(6 \times N\)</span></p><br>","<br><br><p><span class='math inline'>\(8 \times N\)</span></p><br>","<br><br><p><span class='math inline'>\(1000 \times N\)</span></p><br>","<br><br><p>aucune des réponses précédentes</p><br>","B","2","7",NULL,"1",NULL,"2020-03-25 14:30:17",NULL),
("790","<br><br><p>Quel est le nombre minimal de bits nécessaire pour représenter l'entier positif 79 en binaire&nbsp;?</p><br>","<br><br><p>2</p><br>","<br><br><p>6</p><br>","<br><br><p>7</p><br>","<br><br><p>8</p><br>","C","2","7",NULL,"1",NULL,"2020-03-25 14:30:26",NULL),
("791","<br><br><p>Voici les écritures binaires de quatre nombres entiers positifs.</p><br><p>Lequel est pair ?</p><br>","<br><br><p>10 0001</p><br>","<br><br><p>10 0010</p><br>","<br><br><p>11 0001</p><br>","<br><br><p>11 1111</p><br>","B","2","7",NULL,"1",NULL,"2020-03-25 14:30:34",NULL),
("792","<br><br><p>Le résultat de la multiplication en binaire 1011 * 101 est égal au nombre binaire&nbsp;:</p><br>","<br><br><p>102111</p><br>","<br><br><p>101110</p><br>","<br><br><p>110111</p><br>","<br><br><p>110011</p><br>","C","2","7",NULL,"1",NULL,"2020-03-25 14:30:42",NULL),
("793","
<p>On considère le code suivant :</p>
<pre><code class='python'>def s(tuple1, tuple2):
    (x1,y1) = tuple1
    (x2,y2) = tuple2
    return (x1+x2, y1+y2)</code></pre>
<p>Que renvoie l'appel s((1,3), (2,4)) ?</p>
","
<p>le tuple (3,7)</p>
","
<p>le tuple (4,6)</p>
","
<p>un entier</p>
","
<p>une erreur</p>
","A","3","13",NULL,"1",NULL,"2020-05-08 11:11:05",NULL),
("794","<br><br><p>Quelle est la valeur de l'expression [[0] * 3 for i in range(2)] ?</p><br>","<br><br><p>[[0,0], [0,0], [0,0]]</p><br>","<br><br><p>[[0,0,0], [0,0,0]]</p><br>","<br><br><p>[[0.000], [0.000]]</p><br>","<br><br><p>[[0.00], [0.00], [0.00]]</p><br>","B","3","13",NULL,"1",NULL,"2020-03-25 14:30:59",NULL),
("795","<br><br><p>On s'intéresse à la valeur 14 présente dans la liste suivante:</p><br><p>L = [[1,2,3,4,5], [6,7,8,9,10], [11,12,13,14,15], [16,17,18,19,20]].</p><br><p>Quelle expression vaut 14 parmi les suivantes ?</p><br>","<br><br><p>T[2][3]</p><br>","<br><br><p>T[3][4]</p><br>","<br><br><p>T[3][2]&nbsp;</p><br>","<br><br><p>T[4][3]</p><br>","A","3","13",NULL,"1",NULL,"2020-03-25 14:31:07",NULL),
("796","
<p>Quelle est la valeur affichée à l'exécution du programme Python suivant ?</p>
<pre><code class='python'>ports = { 'http': 80, 'imap': 142, 'smtp': 25 }
ports['ftp'] = 21
print(ports['ftp'])</code></pre>
","
<p>3</p>
","
<p>21</p>
","
<p>{ 'ftp': 21 }</p>
","
<p>Key not found</p>
","B","3","13",NULL,"1",NULL,"2020-05-08 11:13:42",NULL),
("797","<br><br><p>On définit : L = [1,2,3,4,5,6]</p><br><p>Quelle est la valeur de L[3] ?</p><br>","<br><br><p>[1,2,3]</p><br>","<br><br><p>3</p><br>","<br><br><p>4</p><br>","<br><br><p>[4,5,6]</p><br>","C","3","13",NULL,"1",NULL,"2020-03-25 14:31:23",NULL),
("798","
<p>Que vaut l'expression <code>[ 2*k for k in range(5) ]</code> ?</p>
","
<p>[0,2,4,6,8]</p>
","
<p>[2,4,6,8,10]</p>
","
<p>[1,2,4,8,16]</p>
","
<p>[2,4,8,16,32]</p>
","A","3","13",NULL,"1",NULL,"2020-05-08 21:53:30",NULL),
("799","
<p>L'entier positif dont l'écriture binaire est 0011 1011 se représente en hexadécimal (base 16) par :</p>
","
<p>32</p>
","
<p>33</p>
","
<p>3B</p>
","
<p>B3</p>
","C","2","2",NULL,"1",NULL,"2020-05-08 12:01:04",NULL),
("800","<p>On définit une table d'élèves et une liste finale de la façon suivante :</p>
<pre><code class='python'>table_eleves = [ {'prenom': 'Ada', 'nom': 'Lovelace', 'age': 7},
{'prenom': 'Charles', 'nom': 'Babbage', 'age': 18},
......
{'prenom': 'John', 'nom': 'Von Neumann', 'age': 16} ]

liste_finale = [ eleve for eleve in table_eleves if eleve['age'] &gt;= 18 ]</code></pre>
<p>Que contient cette liste finale ?</p>
","<p>La liste des prénoms des élèves majeurs de la table.</p>
","<p>La liste des âges des élèves majeurs de la table.</p>
","<p>La liste des élèves majeurs de la table, chaque élément de la liste étant représenté par un dictionnaire.</p>
","<p>La liste des élèves majeurs de la table, chaque élément de la liste étant représenté par une liste.</p>
","C","4","18",NULL,"1",NULL,"2020-05-08 11:19:14",NULL),
("801","
<p>On exécute le code suivant :</p>
<pre><code class='python'>a = [5, 4, 3, 4, 7]
a.append(4)</code></pre>
<p>Quelle est la valeur de la variable a à la fin de cette exécution ?</p>
","
<p>2</p>
","
<p>[4, 4]</p>
","
<p>[5, 4, 3, 4, 7, 4]</p>
","
<p>True</p>
","C","4","18",NULL,"1",NULL,"2020-05-08 11:20:31",NULL),
("802","
<p>On exécute le code suivant :</p>
<pre><code class='python'>def maxi(t):
    m = t[0]
    for x in t:
        if x[1] &gt;= m[1]:
            m = x
    return m

L = [ ('Alice', 17), ('Barnabé', 17),
('Casimir', 17), ('Doriane', 17),
('Emilien', 14), ('Fabienne', 16) ]</code></pre>
<p>Quelle est alors la valeur de maxi(L) ?</p>
","
<p>('Alice',17)</p>
","
<p>('Doriane',17)</p>
","
<p>('Fabienne',17)</p>
","
<p>('Emilien',14)</p>
","B","4","18",NULL,"1",NULL,"2020-05-08 11:23:08",NULL),
("803","
<p>Quelle est la valeur de la variable table à la fin de l'exécution du script suivant :</p>
<pre><code class='python'>table = [[1, 2, 3], [1, 2, 3], [1, 2, 3], [1, 2, 3]]
table [1][2] = 5</code></pre>
","
<p>[[1, 5, 3], [1, 2, 3], [1, 2, 3], [1, 2, 3]]</p>
","
<p>[[1, 2, 3], [5, 2, 3], [1, 2, 3], [1, 2, 3]]</p>
","
<p>[[1, 2, 3], [1, 2, 5], [1, 2, 3], [1, 2, 3]]</p>
","
<p>[[1, 2, 3], [1, 2, 3], [1, 2, 3], [1, 5, 3]]</p>
","C","4","18",NULL,"1",NULL,"2020-05-08 11:24:57",NULL),
("804","<br><br><p>Laquelle de ces affirmations est vraie ?</p><br>","<br><br><p>on peut ouvrir un fichier CSV à l'aide d'un tableur</p><br>","<br><br><p>un fichier CSV permet de gérer l'apparence du code dans l'éditeur</p><br>","<br><br><p>un fichier CSV permet de gérer l'apparence d'une page HTML</p><br>","<br><br><p>un fichier CSV contient un programme à compiler</p><br>","A","4","18",NULL,"1",NULL,"2020-03-25 14:32:29",NULL),
("805","<br><br><p>Quelle balise HTML permet de créer des liens entre pages ?</p><br>","<br><br><p>&lt;r&gt;</p><br>","<br><br><p>&lt;l&gt;</p><br>","<br><br><p>&lt;link&gt;</p><br>","<br><br><p>&lt;a&gt;</p><br>","D","5","22",NULL,"1",NULL,"2020-03-25 14:32:37",NULL),
("806","<br><br><p>Quel est le code HTML correct pour créer un hyperlien vers le site Eduscol ?</p><br>","<br><br><p>&lt;a url='https://www.eduscol.education.fr/'&gt; site Eduscol &lt;/a&gt;</p><br>","<br><br><p>&lt;a name='https://www.eduscol.education.fr/'&gt; site Eduscol &lt;/a&gt;</p><br>","<br><br><p>&lt;a href='https://www.eduscol.education.fr/'&gt; site Eduscol &lt;/a&gt;</p><br>","<br><br><p>&lt;a&gt; https://www.eduscol.education.fr/ &lt;/a&gt; site Eduscol</p><br>","C","5","22",NULL,"1",NULL,"2020-03-25 14:32:48",NULL),
("807","
<p>Un internaute clique sur un lien qui envoie la requête HTTP suivante à un serveur :<br /><code>http://jaimelaneige.com/ma_planche/traitement.php?nom=Snow&amp;prenom=Jon</code></p>
<p>Que demande cette requête au serveur ?</p>
","
<p>de renvoyer le fichier traitement.php en identifiant nom et prénom à Snow et Jon</p>
","
<p>d'exécuter le fichier traitement.php en identifiant nom et prénom à Snow et Jon</p>
","
<p>d'indiquer si Jon Snow a bien pris son traitement</p>
","
<p>de renvoyer le fichier traitement.php en affichant prénom et nom : Jon Snow</p>
","B","5","22",NULL,"1",NULL,"2020-05-08 23:20:09",NULL),
("808","
<p>Voici un extrait d'un document HTML.</p>
<pre><code class='html'>&lt;body&gt;
  .........
  Clic !
  &lt;/button&gt;
  &lt;h1&gt;&lt;span id='valeur'&gt;2000&lt;/span&gt;&lt;/h1&gt;
&lt;/body&gt;</code></pre>
<p>Quelle doit être la ligne qui remplace les pointillés pour obtenir un bouton dont l'appui déclenche la fonction javascript actionBouton() ?</p>
","
<p>&lt;button click = 'actionBouton();'&gt;</p>
","
<p>&lt;button onclick = 'actionBouton();'&gt;</p>
","
<p>&lt;button onclick =&gt; 'actionBouton();'</p>
","
<p>&lt;button&gt; onclick = 'actionBouton();'</p>
","B","5","22",NULL,"1",NULL,"2020-05-08 11:29:37",NULL),
("809","<br><br><p>Quelle est la machine qui va exécuter un programme JavaScript inclus dans une page HTML ?</p><br>","<br><br><p>la machine de l’utilisateur sur laquelle s’exécute le navigateur Web</p><br>","<br><br><p>le serveur Web sur lequel est stockée la page HTML</p><br>","<br><br><p>la machine de l’utilisateur ou du serveur, selon celle qui est la plus disponible</p><br>","<br><br><p>la machine de l’utilisateur ou du serveur, suivant la confidentialité des données manipulées</p><br>","A","5","22",NULL,"1",NULL,"2020-03-25 14:33:17",NULL),
("810","<br><br><p>Un site internet utilise une requête HTTP avec la méthode POST pour transmettre les données d'un formulaire. Laquelle des affirmations suivantes est <strong>incorrecte</strong>&nbsp;?</p><br>","<br><br><p>les données envoyées ne sont pas visibles</p><br>","<br><br><p>il est possible de transmettre des données de type binaire</p><br>","<br><br><p>les données transmises sont cryptées</p><br>","<br><br><p>il n'y a pas de restriction de longueur pour les données transmises</p><br>","C","5","22",NULL,"1",NULL,"2020-03-25 14:33:26",NULL),
("811","
<p>Quel est l’effet de la commande shell suivante ?</p>
<pre><code>cp NSI_ex1_Franck.txt NSI_ex1_Marie.txt</code></pre>
","
<p>Le fichier NSI_ex1_Franck.txt est copié sous le nom NSI_ex1_Marie.txt</p>
","
<p>Le fichier NSI_ex1_Franck.txt est renommé sous le nom NSI_ex1_Marie.txt</p>
","
<p>Le fichier NSI_ex1_Marie.txt est copié sous le nom NSI_ex1_Franck.txt</p>
","
<p>Le fichier NSI_ex1_Marie.txt est renommé sous le nom NSI_ex1_Franck.txt</p>
","A","6","27",NULL,"1",NULL,"2020-05-08 11:32:02",NULL),
("812","<br><br><p>Lequel de ces systèmes d'exploitation est libre ?</p><br>","<br><br><p>Linux</p><br>","<br><br><p>Windows</p><br>","<br><br><p>MacOS</p><br>","<br><br><p>iOS</p><br>","A","6","27",NULL,"1",NULL,"2020-03-25 14:33:43",NULL),
("813","<p>Vivien télécharge un logiciel à partir d’un site commercial. Le transfert par Internet du logiciel a débuté entre le serveur (machine S) et son domicile (machine V). On a représenté des routeurs A, B, C, D et E et les liens existants. Les paquets IP suivent le chemin passant par les routeurs A, B, C et E.</p>
<p>Durant un orage, la foudre frappe et détruit le serveur C par lequel transitent les paquets correspondant au fichier que télécharge Vivien. Que se passe-t-il ?</p>
","<p>la liaison étant coupée, le serveur ne sera plus accessible</p>
","<p>le téléchargement n’est pas interrompu car les paquets peuvent transiter par le routeur D</p>
","<p>le téléchargement est interrompu, Vivien doit redémarrer une nouvelle connexion à partir de zéro</p>
","<p>le téléchargement se poursuit mais des données seront perdues</p>
","B","6","27",NULL,"1","813_graphe.png","2020-05-08 11:34:15",NULL),
("814","<br><br><p>Sous UNIX, que va réaliser la ligne de commande&nbsp;cat file.txt<strong>&nbsp;</strong>?</p><br>","<br><br><p>rien du tout</p><br>","<br><br><p>l'affichage du contenu du fichier file.txt dans la console</p><br>","<br><br><p>la création d'un fichier file.txt</p><br>","<br><br><p>la suppression du fichier file.txt</p><br>","B","6","27",NULL,"1",NULL,"2020-03-25 14:34:03",NULL),
("815","<br><br><p>Un protocole est un ensemble de …</p><br>","<br><br><p>matériels connectés entre eux</p><br>","<br><br><p>serveurs et de clients connectés entre eux</p><br>","<br><br><p>règles qui régissent les échanges entre équipements informatiques</p><br>","<br><br><p>règles qui régissent les échanges entre un système d’exploitation et les applications</p><br>","C","6","27",NULL,"1",NULL,"2020-03-25 14:34:23",NULL),
("816","<br><br><p>Parmi les adresses suivantes, laquelle est une adresse IP non valide ?</p><br>","<br><br><p>1.2.3.4</p><br>","<br><br><p>192.168.23.242</p><br>","<br><br><p>127.3.87.256</p><br>","<br><br><p>10.1.64.42</p><br>","C","6","27",NULL,"1",NULL,"2020-03-25 14:34:30",NULL),
("817","<br><br><p>Parmi ces langages, lequel n'est pas un langage de programmation ?</p><br>","<br><br><p>HTML</p><br>","<br><br><p>JavaScript</p><br>","<br><br><p>PHP</p><br>","<br><br><p>Python</p><br>","A","7","33",NULL,"1",NULL,"2020-03-25 14:34:39",NULL),
("818","
<p>On exécute le script Python suivant :</p>
<pre><code class='python'>def cube(L):
    for i in range(len(L)):
        L[i] = L[i] * L[i] * L[i]
    return L

L = [2, 5]
b = cube(L)</code></pre>
<p>Que vaut le couple (L,b) à la fin de l'exécution ?</p>
","
<p>([2,5], [8,125])</p>
","
<p>([8,125], [8,125])</p>
","
<p>([8,125], [2,5])</p>
","
<p>([2,5], [2,5])</p>
","B","7","33",NULL,"1",NULL,"2020-05-08 11:37:35",NULL),
("819","<br><p>La documentation de la bibliothèque random de Python précise que<br><br>random.randint(a,b) renvoie un entier aléatoire <span class='math inline'>\(N\)</span> tel que a <span class='math inline'>\( \leq \)</span>N <span class='math inline'>\(\leq\)</span> b.</p><br><p>Afin d’obtenir un entier choisi aléatoirement dans l’ensemble {-4 ; -2 ; 0 ; 2 ; 4}, après avoir importé la librairie random de Python, on peut utiliser l’instruction :</p><br>","<br><p>random.randint(0,8)/2</p><br>","<br><p>random.randint(0,8)/2 - 4</p><br>","<br><p>random.randint(0,4)*2 - 2</p><br>","<br><p>(random.randint(0,4) - 2) * 2</p><br>","D","7","33",NULL,"1",NULL,"2020-03-29 15:42:41",NULL),
("820","<br><br><p>En Python, quelle est la méthode pour charger la fonction sqrt du module math ?</p><br>","<br><br><p>using math.sqrt</p><br>","<br><br><p>#include math.sqrt</p><br>","<br><br><p>from math include sqrt</p><br>","<br><br><p>from math import sqrt</p><br>","D","7","33",NULL,"1",NULL,"2020-03-25 14:35:07",NULL),
("821","<p>La documentation de la fonction floor de la bibliothèque math est :</p>
<pre><code>floor(x)
  Return the floor of x as an Integral. This is the largest integer &lt;= x.</code></pre>
<p>Que vaut <code>floor(-2.2)</code> ?</p>
","<p>– 2</p>
","<p>– 3</p>
","<p>on obtient une erreur, car –2.2 n'est pas un entier</p>
","<p>2.2</p>
","B","7","33",NULL,"1",NULL,"2020-05-08 23:21:46",NULL),
("822","
<p>Soit <span class='math inline'>\(n\)</span> un entier naturel. Sa factorielle est le produit des nombres entiers strictement positifs qui sont plus petits ou égaux à <span class='math inline'>\(n\)</span>. Par exemple la factorielle de 4 vaut <span class='math inline'>\(1 \times 2 \times 3 \times 4 = 24\)</span>.</p>
<p>Quelle est la fonction correcte parmi les suivantes ?</p>
","
<pre><code class='python'>def factorielle(n):
    i = 0
    fact = 1
    while i &lt;= n:
        fact = fact * i
        i = i + 1
    return fact</code></pre>
","
<pre><code class='python'>def factorielle(n):
    i = 1
    fact = 1
    while i &lt; n:
        fact = fact * i
        i = i + 1
    return fact</code></pre>
","
<pre><code class='python'>def factorielle(n):
    i = 0
    fact = 1
    while i &lt; n:
        i = i + 1
        fact = fact * i
    return fact</code></pre>
","
<pre><code class='python'>def factorielle(n):
    i = 0
    fact = 1
    while i &lt;= n:
        i = i + 1
        fact = fact * i
    return fact</code></pre>
","C","7","33",NULL,"1",NULL,"2020-05-08 11:54:44",NULL),
("823","
<p>Que fait la fonction suivante :</p>
<pre><code class='python'>def trouver(L):
    i = 0
    for j in range(1, len(L))
        if L[j] &gt;= L[i]:
            i = j
    return i</code></pre>
","
<p>elle renvoie le maximum de la liste</p>
","
<p>elle renvoie le minimum de la liste</p>
","
<p>elle renvoie l’indice de la première occurrence du maximum de la liste</p>
","
<p>elle renvoie l’indice de la dernière occurrence du maximum de la liste</p>
","D","8","39",NULL,"1",NULL,"2020-05-08 11:56:40",NULL),
("824","<p>Qu'effectue-t-on en lançant la commande suivante dans un terminal Linux :<br /><code>mv /etc/professeur/fichier.conf /home/nsi/fichier.conf</code></p>
","<p>un déplacement de fichier</p>
","<p>une copie de fichier</p>
","<p>un renommage de fichier</p>
","<p>un changement de répertoire</p>
","A","6","25",NULL,"1",NULL,"2020-05-08 23:22:57",NULL),
("825","<br><br><p>Quel est le coût d'un algorithme de tri par insertion ?</p><br>","<br><br><p>constant</p><br>","<br><br><p>logarithmique</p><br>","<br><br><p>linéaire</p><br>","<br><br><p>quadratique</p><br>","D","8","39",NULL,"1",NULL,"2020-03-25 14:35:57",NULL),
("826","
<p>Combien d’échanges effectue la fonction Python suivante pour trier un tableau de 10 éléments au pire des cas ?</p>
<pre><code class='python'>def tri(tab):
    for i in range(1, len(tab)):
        for j in range(len(tab) - i):
            if tab[j]&gt;tab[j+1]:
                tab[j],tab[j+1] = tab[j+1], tab[j]</code></pre>
","
<p>10</p>
","
<p>45</p>
","
<p>55</p>
","
<p>100</p>
","B","8","39",NULL,"1",NULL,"2020-05-08 11:48:54",NULL),
("827","
<p>On exécute le code suivant :</p>
<pre><code class='python'>tab = [1, 4, 3, 8, 2]
S = 0
for i in range(len(tab)):
    S = S + tab[i]</code></pre>
<p>Que vaut la variable S à la fin de l'exécution ?</p>
","
<p>1</p>
","
<p>8</p>
","
<p>18</p>
","
<p>3.6</p>
","C","8","39",NULL,"1",NULL,"2020-05-08 12:06:57",NULL),
("828","<br><br><p>On dispose de sacs de jetons portant les nombres 10, 5, 3 et 1.</p><br><p>On veut obtenir un total de 21 en utilisant ces jetons.</p><br><p>Si on utilise le principe de l'algorithme glouton, quelle addition va-t-on réaliser pour obtenir ce total de 21 ?</p><br>","<br><br><p>5 + 5 + 5 + 5 + 1</p><br>","<br><br><p>10 + 5 + 3 + 3</p><br>","<br><br><p>10 + 5 + 5 + 1</p><br>","<br><br><p>10 + 10 + 1</p><br>","D","8","39",NULL,"1",NULL,"2020-03-25 14:36:22",NULL),
("829","<br><br><p>Quel est le plus grand entier positif que l'on peut coder sur un mot de 16 bits ?</p><br>","<br><br><p><span class='math inline'>\(2^{15} - 1 = 32767\)</span></p><br>","<br><br><p><span class='math inline'>\(2^{15} = 32768\)</span></p><br>","<br><br><p><span class='math inline'>\(2^{16} - 1 = 65535\)</span></p><br>","<br><br><p><span class='math inline'>\(2^{16} = 65536\)</span></p><br>","C","2","7",NULL,"1",NULL,"2020-03-25 14:37:16",NULL),
("830","<p>À quelle affectation sont équivalentes les instructions suivantes, où a, b sont des variables entières et c une variable booléenne ?</p>
<pre><code class='python'>if a==b:
    c = True
elif a &gt; b+10:
    c = True
else:
    c = False</code></pre>","<pre><code class='python'>c = (a==b) or (a &gt; b+10)</code></pre>
","<pre><code class='python'>c = (a==b) and (a &gt; b+10)</code></pre>
","<pre><code class='python'>c = not(a==b)</code></pre>
","<pre><code class='python'>c = not(a &gt; b+10)</code></pre>
","A","2","7",NULL,"1",NULL,"2020-05-12 08:41:20",NULL),
("831","<br><section id='a-2' class='level3'><br><br><p>A et B sont deux propositions vraies. Laquelle de ces propositions est également vraie ?</p><br></section><br>","<br><p>((non A) et B) ou (A et (non B))</p><br>","<br><p>((non A) ou B) et ((non A) ou (non B))</p><br>","<br><p>((non A) ou B) et (A ou (non B))</p><br>","<br><p>((non A) et B) et (A et (non B))</p><br>","C","2","7",NULL,"1",NULL,"2020-03-28 17:32:44",NULL),
("832","<br><p>Le résultat de la soustraction en binaire 101001 - 101 est égal au nombre binaire :</p><br>","<br><p>100900</p><br>","<br><p>101110</p><br>","<br><p>100100</p><br>","<br><p>100110</p><br>","C","2","7",NULL,"1",NULL,"2020-03-28 17:21:18",NULL),
("833","<br><p>Quelle est la représentation en binaire de l'entier 64 sur un octet ?</p><br>","<br><p>0101 0000</p><br>","<br><p>1100 0100</p><br>","<br><p>0100 0000</p><br>","<br><p>0000 1100</p><br>","C","2","7",NULL,"1",NULL,"2020-03-28 17:22:02",NULL),
("834","<br><p>Quel est l'entier codé sur 4 bits en complément à 2 par 1101 ?</p><br>","<br><p>-6</p><br>","<br><p>-3</p><br>","<br><p>13</p><br>","<br><p>14</p><br>","B","2","7",NULL,"1",NULL,"2020-03-28 17:24:04",NULL),
("835","<p>On considère la liste de listes suivante :</p><br><pre><br>tictactoe = [['X', 'O', 'O'],<br>             ['O', 'O', 'O'], <br>             ['O', 'O', 'X']]<br></pre><br><p>Quelle instruction permet d'obtenir une diagonale de 'X' ?</p><br>","<p>tictactoe[3] = 'X'</p><br>","<p>tictactoe[4] = 'X'</p><br>","<p>tictactoe[1][1] = 'X'</p><br>","<p>tictactoe[2][2] = 'X'</p><br>","C","3","13",NULL,"1",NULL,"2020-03-28 17:26:36",NULL),
("836","<br><p>Considérons le tableau suivant :</p><br><pre>tableau = [[1,2],[3,4],[5,6]]</pre><br><p>Quelle est la valeur de l'expression tableau[2][1] ?</p><br>","<br><p>3</p><br>","<br><p>6</p><br>","<br><p>[3,4],[1,2]</p><br>","<br><p>[5,6],[2,4]</p><br>","B","3","13",NULL,"1",NULL,"2020-03-28 17:27:19",NULL),
("837","<p>Si a vaut False et b vaut True, que vaut l’expression booléenne NOT(a AND b) ?</p><br>","<p>0</p><br>","<p>False</p><br>","<p>True</p><br>","<p>None</p><br>","C","3","13",NULL,"1",NULL,"2020-04-20 15:14:48",NULL),
("838","<br><p>On définit le dictionnaire suivant d = {'A': 3, 'B': 7, 'C': 2}. Quelle expression permet de récupérer la valeur de la clé 'B' ?</p><br>","<br><p>d['B']</p><br>","<br><p>d[1]</p><br>","<br><p>d[7]</p><br>","<br><p>d[B]</p><br>","A","3","13",NULL,"1",NULL,"2020-03-28 17:33:41",NULL),
("839","<br><p>On définit : T = [[1,2,3], [4,5,6], [7,8,9]]</p><br><p>Laquelle des expressions suivantes a pour valeur 7 ?</p><br>","<br><p>T[3,1]</p><br>","<br><p>T[3][1]</p><br>","<br><p>T[2,0]</p><br>","<br><p>T[2][0]</p><br>","D","3","13",NULL,"1",NULL,"2020-03-28 17:34:22",NULL),
("840","<br><p>On considère le code suivant :</p><br><pre>def feed(t):<br>    for i in range(len(t)):<br>        t[i] = 0<br>    return t<br></pre><br><p>Que renvoie feed([12, 24, 32]) ?</p><br>","<br><p>[120, 240, 320]</p><br>","<br><p>[0, 0, 0]</p><br>","<br><p>[ ]</p><br>","<br><p>[0]</p><br>","B","3","13",NULL,"1",NULL,"2020-03-28 17:35:35",NULL),
("841","<br><p>Qu'est-ce qu'un fichier CSV ?</p><br>","<br><p>une librairie Python permettant l'affichage des images</p><br>","<br><p>un utilitaire de traitement d'image</p><br>","<br><p>un format d'image</p><br>","<br><p>un format de données</p><br>","D","4","18",NULL,"1",NULL,"2020-03-28 17:36:04",NULL),
("842","<br><p>Laquelle de ces listes de chaînes de caractères est triée en ordre croissant ?</p><br>","<br><p>['112', '19', '27', '45', '8']</p><br>","<br><p>['8', '19', '27', '45', '112']</p><br>","<br><p>['8', '112', '19', '27', '45']</p><br>","<br><p>['19', '112', '27', '45', '8']</p><br>","A","4","18",NULL,"1",NULL,"2020-03-28 17:36:31",NULL),
("843","<br><p>Soit le tableau défini de la manière suivante : tableau = [[1,3,4],[2,7,8],[9,10,6],[12,11,5]]</p><br><p>On souhaite accéder à la valeur 12, on écrit pour cela :</p><br>","<br><p>tableau[4][1]</p><br>","<br><p>tableau[1][4]</p><br>","<br><p>tableau[3][0]</p><br>","<br><p>tableau[0][3]</p><br>","C","4","18",NULL,"1",NULL,"2020-03-28 17:37:10",NULL),
("844","<p>On considère l’extraction suivante d'une base de données des départements français. Cette extraction a ensuite été sauvegardée dans un fichier texte.</p>
<pre><code>'1','01','Ain','AIN','ain','A500'
'2','02','Aisne','AISNE','aisne','A250'
'3','03','Allier','ALLIER','allier','A460'
'4','04','Alpes-de-Haute-Provence','ALPES-DE-HAUTE-PROVENCE','alpes-de-haute-provence','A412316152'
'5','05','Hautes-Alpes','HAUTES-ALPES','hautes-alpes','H32412'</code></pre>
<p>On considère le code suivant :</p>
<pre>
import csv
with open('departements.csv', newline='') as monFichier:
lesLignes = csv.reader(monFichier)
for uneLigne in lesLignes:
    print(uneLigne[3])
</pre>
<p>Que va produire l'exécution de ce code ?</p>
","<p>L'affichage de la troisième colonne à savoir le nom du département avec une majuscule initiale</p>
","<p>L'affichage de tout le contenu du fichier</p>
","<p>L'affichage du nombre total de départements figurant dans le fichier</p>
","<p>L'affichage de la quatrième colonne, à savoir le nom du département tout en majuscules</p>
","D","4","18",NULL,"1",NULL,"2020-05-08 12:25:11",NULL),
("845","<br><p>Dans la plupart des fichiers CSV, que contient la première ligne ?</p><br>","<br><p>des notes concernant la table de données</p><br>","<br><p>les sources des données</p><br>","<br><p>les descripteurs des champs de la table de données</p><br>","<br><p>l'auteur de la table de données</p><br>","C","4","18",NULL,"1",NULL,"2020-03-28 17:39:08",NULL),
("846","<p>On considère le code suivant :</p>
<pre><code class='python'>def clearfield(f):
    for i in range(len(f)):
        fiche[i]['code'] = None
    return f

fiche = [{'nom': 'pierre', 'note': 5.99, 'code': 125},
         {'nom': 'pol', 'note': 2.99, 'code': 82},
         {'nom': 'jack', 'note': 7.99, 'code': 135}]</code></pre>
<p>Que renvoie clearfield(fiche) ?</p>
","<pre><code class='python'>[{'nom': 'pierre', 'note': 5.99, 'code': 125},
{'nom': 'pol', 'note': 2.99, 'code': 82},
{'nom': 'jack', 'note': 7.99, 'code': 135}]</code></pre>
","<pre><code class='python'>[{'nom': 'pierre', 'note': None, 'code': 125},
{'nom': 'pol', 'note': None, 'code': 82},
{'nom': 'jack', 'note': None, 'code': 135}]</code></pre>
","<pre><code class='python'>[{'nom': 'pierre', 'note': 5.99, 'None': 125},
{'nom': 'pol', 'note': 2.99, 'None': 82},
{'nom': 'jack', 'note': 7.99, 'None': 135}]</code></pre>
","<pre><code class='python'>[{'nom': 'pierre', 'note': 5.99, 'code': None},
{'nom': 'pol', 'note': 2.99, 'code': None},
{'nom': 'jack', 'note': 7.99, 'code': None}]</code></pre>
","D","4","18",NULL,"1",NULL,"2020-05-08 12:35:44",NULL),
("847","<br><p>Par quoi commence l’URL d’une page Web sécurisée ?</p><br>","<br><p>http</p><br>","<br><p>https</p><br>","<br><p>ftp</p><br>","<br><p>smtp</p><br>","B","5","22",NULL,"1",NULL,"2020-03-28 17:50:09",NULL),
("848","<p>Mehdi a écrit une page HTML contenant des éléments input de formulaire.</p><br><p>Il place ces éléments de formulaire :</p><br>","<p>entre la balise &lt;form&gt; et la balise &lt;/form&gt;</p><br>","<p>entre la balise &lt;formulary&gt; et la balise &lt;/formulary&gt;</p><br>","<p>entre la balise &lt;code&gt; et la balise &lt;/code&gt;</p><br>","<p>entre la balise &lt;script&gt; et la balise &lt;/script&gt;</p><br>","A","5","22",NULL,"1",NULL,"2020-03-28 17:53:53",NULL),
("849","<p>Dans un fichier HTML nommé reservation.html, on a défini au sein d’une balise &lt;script&gt; la fonction confirmer. Ce fichier contient aussi la ligne suivante :</p>
<pre><code>&lt;button onclick='confirmer();'&gt;Annuler la réservation&lt;/button&gt;</code></pre>
<p>On affiche cette page dans un navigateur Web (pour lequel JavaScript est activé).</p>
<p>En cliquant sur le bouton « Annuler la réservation » :</p>
","<p>Le navigateur va nécessairement générer une requête HTTP à destination du serveur pour confirmer cette action.</p>
","<p>Le navigateur ne peut pas générer une requête HTTP à destination du serveur pour confirmer cette action.</p>
","<p>Le navigateur va nécessairement déclencher un appel à la fonction confirmer.</p>
","<p>Le navigateur ne peut pas déclencher un appel à la fonction confirmer.</p>
","C","5","22",NULL,"1",NULL,"2020-05-08 23:25:33",NULL),
("850","<br><p>Dans quels langages les balises &lt;img&gt; et &lt;form&gt; sont-elles utilisées ?</p><br>","<br><p>Python</p><br>","<br><p>HTML</p><br>","<br><p>Javascript</p><br>","<br><p>PHP</p><br>","B","5","22",NULL,"1",NULL,"2020-03-28 17:57:40",NULL),
("851","<p>Saisir l'URL <code>http://monsite.com/monprogramme.py?id=25</code> dans la barre d'adresse d'un navigateur ne peut jamais permettre :</p>
","<p>d'obtenir une image</p>
","<p>d'envoyer une requête GET</p>
","<p>d'envoyer une requête POST</p>
","<p>d'exécuter un programme Python sur le serveur</p>
","C","5","22",NULL,"1",NULL,"2020-05-08 12:38:09",NULL),
("852","<br><p>Parmi les balises HTML ci-dessous quelle est celle qui permet à l’utilisateur de saisir son nom dans un formulaire en respectant la norme HTML ?</p><br>","<br><p>&lt;select /&gt;</p><br>","<br><p>&lt;form /&gt;</p><br>","<br><p>&lt;input type='text' /&gt;</p><br>","<br><p>&lt;input type='name' /&gt;</p><br>","C","5","22",NULL,"1",NULL,"2020-03-28 17:59:18",NULL),
("853","<br><p>Quel matériel permet d'interconnecter des <strong>réseaux</strong> entre eux :</p><br>","<br><p>un routeur</p><br>","<br><p>un commutateur (ou <em>switch</em>)</p><br>","<br><p>un interconnecteur</p><br>","<br><p>un serveur</p><br>","A","6","27",NULL,"1",NULL,"2020-03-28 17:59:47",NULL),
("854","<p>Le shell Linux renvoie ce résultat à la commande ls -al :</p>
<pre><code>lrwxr--r-- 2 toto toto 807 juin 26 14:06 eclipse
drwxr-xr-x 2 toto toto 4096 juin 26 15:00 Doc_1
-rw-r-xr-x 2 toto toto 4096 juin 26 14:06 QCM
-rwxr-xr-x 2 toto toto 4096 juin 26 14:06 Doc_Travail</code></pre>
<p>Quel est le nom du fichier du répertoire courant, de taille 4096 octets, exécutable par son propriétaire ?</p>
","<p>eclipse</p>
","<p>Doc_1</p>
","<p>QCM</p>
","<p>Doc_Travail</p>
","D","6","27",NULL,"1",NULL,"2020-05-08 12:40:56",NULL),
("855","<br><p>Identifier parmi les éléments suivants celui qui est uniquement un périphérique de sortie.</p><br>","<br><p>clavier</p><br>","<br><p>souris</p><br>","<br><p>écran</p><br>","<br><p>microphone</p><br>","C","6","27",NULL,"1",NULL,"2020-03-28 18:00:46",NULL),
("856","<br><p>Sachant que  hibou est un fichier présent dans le répertoire courant, quel est l’effet de la commande suivante : mv hibou chouette</p><br>","<br><p>déplacer le fichier hibou dans le répertoire chouette</p><br>","<br><p>ajouter le contenu du fichier hibou à la fin du fichier chouette</p><br>","<br><p>renommer le fichier hibou en chouette</p><br>","<br><p>créer le fichier chouette, copie du fichier hibou</p><br>","C","6","27",NULL,"1",NULL,"2020-03-28 18:01:14",NULL),
("857","<br><p>Quel est le principe de l'encapsulation des données dans un réseau informatique ?</p><br>","<br><p>Cacher les données afin que l'on ne puisse pas les lire</p><br>","<br><p>Mettre les données les unes à la suite des autres</p><br>","<br><p>Chiffrer les données afin que l'on ne puisse pas les lire</p><br>","<br><p>Inclure les données d'un protocole dans un autre protocole</p><br>","D","6","27",NULL,"1",NULL,"2020-03-28 18:01:41",NULL),
("858","<br><p>Dans la console Linux, quelle commande faut-il exécuter pour obtenir la documentation sur la commande pwd ?</p><br>","<br><p>man pwd</p><br>","<br><p>cd pwd</p><br>","<br><p>mkdir pwd</p><br>","<br><p>ls pwd</p><br>","A","6","27",NULL,"1",NULL,"2020-03-28 18:02:14",NULL),
("859","<br><p>On définit la fonction suivante :</p><br><pre>def f(x):<br>    for d in range(2,x):<br>        if x%d == 0: <br>            return d<br></pre><br><p>Quelle est la valeur renvoyée par l'appel f(15) ?</p><br>","<br><p>3</p><br>","<br><p>5</p><br>","<br><p>3,5</p><br>","<br><p>3,5,15</p><br>","A","7","33",NULL,"1",NULL,"2020-03-28 18:03:18",NULL),
("860","<p>On considère le code incomplet suivant, où la fonction maximum renvoie le plus grand élément d’une liste de nombres :</p>
<pre>def maximum(L):
    m = L[0]
    for i in range(1,len(L)): 
        ..............
        ..............
    return m
</pre>
<p>Que faut-il écrire à la place des lignes pointillées ?</p>
","<pre>
if m &lt; L[i]:
    L[i] = m
</pre>
","<pre>
if L[i-1] &lt; L[i]:
    m = L[i]
</pre>
","<pre>if L[i] &lt; L[0]:
    L[i],L[0] = L[0],L[i]
</pre>
","<pre>
if L[i] &gt; m:
    m = L[i]
</pre>
","D","7","33",NULL,"1",NULL,"2020-05-08 12:44:25",NULL),
("861","<p>La documentation de la bibliothèque random de Python précise :</p>
<pre><code>random.randint(a, b)
  Renvoie un entier aléatoire N tel que a &lt;= N &lt;= b.</code></pre>
<p>Quelle est l’expression Python permettant de simuler le tirage d’un dé à 6 faces après avoir exécuté import random ?</p>
","<p>random.randint(6)</p>
","<p>random.randint(1,6)</p>
","<p>random.randint(1,7)</p>
","<p>random.randint(0,6)</p>
","B","7","33",NULL,"1",NULL,"2020-05-08 13:18:29",NULL),
("862","<br><p>Lequel des langages suivants n'est pas un langage de programmation :</p><br>","<br><p>PHP</p><br>","<br><p>Javascript</p><br>","<br><p>HTML</p><br>","<br><p>Python</p><br>","C","7","33",NULL,"1",NULL,"2020-03-28 18:06:54",NULL),
("863","<p>Quelle est la valeur de la variable x à la fin de l'exécution du script suivant :</p><br><pre><br>def f(x):<br>    x = x + 1<br>    return x + 1<br><br>x = 0<br>f(x+1)<br></pre><br>","<p>0</p><br>","<p>1</p><br>","<p>2</p><br>","<p>3</p><br>","A","7","33",NULL,"1",NULL,"2020-03-28 18:08:38",NULL),
("864","<br><p>On définit une fonction f de la façon suivante :</p><br><pre><br>def f(L,m):<br>    R = []<br>    for i in range(len(L)):<br>        if L[i] > m:<br>            R.append(L[i])<br>    return R<br></pre><br><p>On définit L = [1, 7, 3, 4, 8, 2, 0, 3, 5].</p><br><p>Que vaut f(L,4) ?</p><br>","<br><p>[0, 7, 0, 0, 8, 0, 0, 0, 5]</p><br>","<br><p>[0, 0, 0, 5]</p><br>","<br><p>[7, 8, 5]</p><br>","<br><p>[]</p><br>","C","7","33",NULL,"1",NULL,"2020-03-28 18:10:00",NULL),
("865","<br><p>La fonction mystere suivante prend en argument un tableau d'entiers.</p><br><pre><br>def mystere(t):<br>    for i in range(len(t) - 1):<br>        if t[i] + 1 != t[i+1]:<br>            return False<br>    return True<br></pre><br><p>À quelle condition la valeur renvoyée par la fonction est-elle True ?</p><br>","<br><p>si le tableau passé en argument est une suite d'entiers consécutifs</p><br>","<br><p>si le tableau passé en argument est trié en ordre croissant</p><br>","<br><p>si le tableau passé en argument est trié en ordre décroissant</p><br>","<br><p>si le tableau passé en argument contient des entiers tous identiques</p><br>","A","8","39",NULL,"1",NULL,"2020-03-28 18:11:12",NULL),
("866","<br><p>En utilisant une recherche dichotomique, combien faut-il de comparaisons pour trouver une valeur dans un tableau trié de 1000 nombres ?</p><br>","<br><p>3</p><br>","<br><p>10</p><br>","<br><p>1000</p><br>","<br><p>1024</p><br>","B","8","39",NULL,"1",NULL,"2020-03-28 18:11:57",NULL),
("867","<br><p>Quelle est la valeur de c à la fin de l'exécution du code suivant :</p><br><pre><br>L = [1,2,3,4,1,2,3,4,0,2]<br>c = 0<br>for k in L:<br>    if k == L[1]:<br>        c = c+1<br></pre><br>","<br><p>0</p><br>","<br><p>2</p><br>","<br><p>3</p><br>","<br><p>10</p><br>","C","8","39",NULL,"1",NULL,"2020-03-28 18:13:00",NULL),
("868","<p>La fonction suivante doit calculer le produit de tous les éléments de la liste passée en paramètre. Avec quelles expressions doit-on la compléter pour que cette fonction soit correcte ?</p>
<pre><code class='python'>def produit (L):
    p = ...
    for elt in L:
        ........
    return p</code></pre>
","<p>1 puis p = p * elt</p>
","<p>0 puis p = p * elt</p>
","<p>1 puis p = elt</p>
","<p>0 puis p = elt</p>
","A","8","39",NULL,"1",NULL,"2020-05-08 13:22:36",NULL),
("869","<br><p>On définit une fonction de calcul de la moyenne d'une liste de nombres :</p><br><pre><br>def moyenne(L):<br>    s = 0<br>    n = len(L)<br>    for x in L:<br>        s = s + x<br>    return s/n<br></pre><br><p>Combien cette fonction utilise-t-elle d'additions et de divisions pour calculer la moyenne d'une liste de 7 nombres ?</p><br>","<br><p>7</p><br>","<br><p>8</p><br>","<br><p>9</p><br>","<br><p>10</p><br>","B","8","39",NULL,"1",NULL,"2020-03-28 18:15:34",NULL),
("870","<br><p>Quelle est la valeur du couple (s,i) à la fin de l'exécution du script suivant ?</p><br><pre><br>s = 0<br>i = 1<br>while i < 5:<br>    s = s + i<br>    i = i + 1<br></pre><br>","<br><p>(4, 5)</p><br>","<br><p>(10, 4)</p><br>","<br><p>(10, 5)</p><br>","<br><p>(15, 5)</p><br>","C","8","39",NULL,"1",NULL,"2020-03-28 18:16:41",NULL),
("871","<br><p>À quelle affectation sont équivalentes les instructions suivantes, où a, b sont des variables entières et c une variable booléenne ?</p><br><pre><br>if a==b:<br>    c = True<br>elif a > b+10:<br>    c = True<br>else:<br>    c = False<br></pre><br>","<br><p>c = (a==b) or (a > b+10)</p><br>","<br><p>c = (a==b) and (a > b+10)</p><br>","<br><p>c = not(a==b)</p><br>","<br><p>c = not(a > b+10)</p><br>","A","2","7",NULL,"1",NULL,"2020-03-28 18:17:58",NULL),
("872","<br><br><p>Parmi les noms suivants, lequel <strong>n'est pas</strong> celui d'une méthode d'encodage des caractères ?</p><br>","<br><br><p>UTF-16</p><br>","<br><br><p>ASCII</p><br>","<br><br><p>Arial</p><br>","<br><br><p>Unicode</p><br>","C","2","7",NULL,"1",NULL,"2020-03-25 14:54:20",NULL),
("873","<br><br><p>Quel est le plus grand entier positif (non signé) représentable en binaire sur 2 octets (c'est-à-dire 16 bits) ?</p><br>","<br><br><p><span class='math inline'>\(2^{15} - 1\)</span></p><br>","<br><br><p><span class='math inline'>\(2^{15}\)</span></p><br>","<br><br><p><span class='math inline'>\(2^{16} - 1\)</span></p><br>","<br><br><p><span class='math inline'>\(2^{16}\)</span></p><br>","C","2","7",NULL,"1",NULL,"2020-03-25 14:54:29",NULL),
("874","<br><br><p>Quel est le nombre minimum de bits qui permet de représenter les 7 couleurs de l'arc-en-ciel ?</p><br>","<br><br><p>2</p><br>","<br><br><p>3</p><br>","<br><br><p>4</p><br>","<br><br><p>5</p><br>","B","2","7",NULL,"1",NULL,"2020-03-25 14:54:37",NULL),
("875","<br><br><p>Dans quel système de numération 3F5 représente-t-il un nombre entier&nbsp;?</p><br>","<br><br><p>binaire (base 2)</p><br>","<br><br><p>octal (base 8)</p><br>","<br><br><p>décimal (base 10)</p><br>","<br><br><p>hexadécimal (base 16)</p><br>","D","2","7",NULL,"1",NULL,"2020-03-25 14:55:17",NULL),
("876","<br><br><p>Quelle est l'écriture en hexadécimal (base 16) du nombre entier positif qui s'écrit 1110 1101 en base 2 ?</p><br>","<br><br><p>DE</p><br>","<br><br><p>ED</p><br>","<br><br><p>EDF</p><br>","<br><br><p>FEFD</p><br>","B","2","7",NULL,"1",NULL,"2020-03-25 14:55:24",NULL),
("877","<br><br><p>Quelle est la valeur de l'expression [ 2*k + 1 for k in range(4) ] ?</p><br>","<br><br><p>[1,3,5,7]</p><br>","<br><br><p>[0,1,2,3]</p><br>","<br><br><p>[3,5,7,9]</p><br>","<br><br><p>[1,2,3,4]</p><br>","A","3","13",NULL,"1",NULL,"2020-03-25 14:55:32",NULL),
("878","<br><br><p>De quelle expression la liste suivante est-elle la valeur ?</p><br><p>[[0,0,0,0], [1,1,1,1], [2,2,2,2]]</p><br>","<br><br><p>[[i] * 4 for i in range(4)]</p><br>","<br><br><p>[[i] * 3 for i in range(4)]</p><br>","<br><br><p>[[i] * 4 for i in range(3)]</p><br>","<br><br><p>[[i] * 3 for i in range(3)]</p><br>","C","3","13",NULL,"1",NULL,"2020-03-25 14:55:39",NULL),
("879","<br><p>On exécute le script suivant :</p><br><pre><br>inventaire = {'pommes': 430, 'bananes': 312, 'oranges' : 274, 'poires' : 137}<br>stock = 0<br>for fruit in inventaire.keys():<br>    if fruit != 'bananes':<br>        stock = stock + inventaire[fruit]<br></pre><br><p>Que contient la variable stock à la fin de cette exécution ?</p><br>","<br><p>{430, 274, 137}</p><br>","<br><p>312</p><br>","<br><p>841</p><br>","<br><p>{ 'pommes', 'oranges', 'poires' }</p><br>","C","3","13",NULL,"1",NULL,"2020-03-29 16:26:17",NULL),
("880","<p>On considère le code suivant :</p>
<pre><code class='python'>t = [0, 3, 5, 7, 9]
t[9] = 3 + t[5]</code></pre>
<p>Que vaut t à la fin de son exécution ?</p>
","<pre><code class='python'>[0, 3, 5, 7, 9]</code></pre>
","<pre><code class='python'>[0, 3, 5, 7, 9, 3]</code></pre>
","<pre><code class='python'>[0, 3, 5, 7, 9, 8]</code></pre>
","l'exécution déclenche une erreur
","D","3","13",NULL,"1",NULL,"2020-05-12 08:46:59",NULL),
("881","<br><br><p>Quelle est la valeur de l'expression [(i,i+1) for i in range(2)] ?</p><br>","<br><br><p>[0,1,1,2]</p><br>","<br><br><p>[(1,2),(2,3)]</p><br>","<br><br><p>[(0,1),(1,2)]</p><br>","<br><br><p>[[0,1],[1,2]]</p><br>","C","3","13",NULL,"1",NULL,"2020-03-25 14:56:04",NULL),
("882","<br><br><p>On définit la variable suivante : citation = 'Les nombres gouvernent le monde'.</p><br><p>Quelle est la valeur de l'expression citation[5:10] ?</p><br>","<br><br><p>'ombre'</p><br>","<br><br><p>'ombres'</p><br>","<br><br><p>'nombre'</p><br>","<br><br><p>'nombres'</p><br>","A","3","13",NULL,"1",NULL,"2020-03-25 14:56:13",NULL),
("883","<br><p>Qu'affiche le code Python ci-dessous ?</p><br><pre><br>notes = {'Paul': 12, 'Jean': 16, 'Clara': 14, 'Aïssa': 18}<br>for i in notes:<br>    print(notes[i])<br></pre>","<br><p>Les prénoms des élèves</p><br>","<br><p>Les notes des élèves</p><br>","<br><p>Le prénom puis la note de chaque élève</p><br>","<br><p>Une erreur</p><br>","B","4","18",NULL,"1",NULL,"2020-04-08 00:32:40",NULL),
("884","<br><br><p>Laquelle de ces affirmations est vraie ?</p><br>","<br><br><p>on peut ouvrir un fichier CSV à l'aide d'un tableur</p><br>","<br><br><p>un fichier CSV permet de gérer l'apparence du code dans l'éditeur</p><br>","<br><br><p>un fichier CSV permet de gérer l'apparence d'une page HTML</p><br>","<br><br><p>un fichier CSV contient un programme à compiler</p><br>","A","4","18",NULL,"1",NULL,"2020-03-25 14:56:31",NULL),
("885","
<p>On exécute le script suivant :</p>
<pre><code class='python'>notes = {'Paul': 12, 'Jean': 16, 'Clara': 14, 'Aïssa': 18}
t = list(notes.keys())</code></pre>
<p>Quelle est la valeur de t à la fin de cette exécution ?</p>
","
<p>Paul</p>
","
<p>['Paul', ''Jean', 'Clara', ''Aïssa']</p>
","
<p>[12, 16, 14, 18]</p>
","
<p>[ 'Paul': 12, 'Jean': 16, 'Clara': 14, 'Aïssa': 18 ]</p>
","B","4","18",NULL,"1",NULL,"2020-05-08 23:31:39",NULL),
("886","<p>On a défini deux tables de données :</p>
<pre><code class='python'>data1 = [('Bruce', 'Wayne'), ('Chuck', 'Norris'), ('Bruce', 'Lee'), ('Clark', 'Kent')]
data2 = [('Diana', 'Prince'), ('Chuck', 'Norris'), ('Peter', 'Parker')]</code></pre>
<p>Quelle instruction permet de construire une table data regroupant l'ensemble des informations de data1 et data2 ?</p>
","<p>data = data1 + data2</p>
","<p>data == data1 + data2</p>
","<p>data = [element for element in data1 or data2]</p>
","<p>data = [data1] + [data2]</p>
","A","4","18",NULL,"1",NULL,"2020-05-08 23:32:49",NULL),
("887","<br><br><p>Qu'est-ce que le format de fichier CSV ?</p><br>","<br><br><p>un format de fichier mis au point par Microsoft pour Excel</p><br>","<br><br><p>un format de fichier pour décrire une base de données</p><br>","<br><br><p>un format de fichier où les données sont séparées par un caractère tel qu'une virgule</p><br>","<br><br><p>un format de fichier décrivant une page Web</p><br>","C","4","18",NULL,"1",NULL,"2020-03-25 14:57:00",NULL),
("888","
<p>On a extrait les deux premières lignes de différents fichiers.</p>
<p>Déterminer celui qui est un authentique fichier CSV :</p>
","
<p>Nom,Pays,Temps<br />
Camille Muffat,France,241.45</p>
","
<p>Nom Pays Temps<br />
Camille Muffat France 241.45</p>
","
<p>[<br />
{ 'Nom': 'Camille Muffat', 'Pays': 'France', 'Temps': 241.45},</p>
","
<p>[<br />
{ Nom: 'Camille Muffat', Pays: 'France', Temps: 241.45},</p>
","A","4","18",NULL,"1",NULL,"2020-05-08 23:34:05",NULL),
("889","<br><br><p>Parmi les propriétés suivantes d’une balise &lt;button /&gt; dans une page HTML, laquelle doit être rédigée en langage JavaScript ?</p><br>","<br><br><p>la propriété name</p><br>","<br><br><p>la propriété type</p><br>","<br><br><p>la propriété onclick</p><br>","<br><br><p>la propriété id</p><br>","C","5","22",NULL,"1",NULL,"2020-03-25 14:57:19",NULL),
("890","<br><br><p>Par quoi commence l’URL d’une page Web sécurisée ?</p><br>","<br><br><p>http</p><br>","<br><br><p>https</p><br>","<br><br><p>ftp</p><br>","<br><br><p>smtp</p><br>","B","5","22",NULL,"1",NULL,"2020-03-25 14:57:26",NULL),
("891","<br><p>Dans le code html ci-dessous :<br><br><code>&lt;p&gt;&lt;img src='../html.gif' alt='Html'&gt; &lt;a href='corrections/index.php'&gt;Exercices corrigés&lt;/p&gt;&lt;/a&gt;</code><br></p><br>","<br><p>L'ordre des balises n'est pas correct</p><br>","<br><p>Le nom d'une des balises n'est pas correct</p><br>","<br><p>Un des attributs d'une des balises n'est pas correct</p><br>","<br><p>Un des chemins de fichiers n'est pas correct</p><br>","A","5","22",NULL,"1",NULL,"2020-04-08 00:05:06",NULL),
("892","<br><br><p>Quel est le code HTML correct pour créer un hyperlien vers le site Eduscol ?</p><br>","<br><br><p>&lt;a url='https://www.eduscol.education.fr/'&gt; site Eduscol &lt;/a&gt;</p><br>","<br><br><p>&lt;a name='https://www.eduscol.education.fr/'&gt; site Eduscol &lt;/a&gt;</p><br>","<br><br><p>&lt;a href='https://www.eduscol.education.fr/'&gt; site Eduscol &lt;/a&gt;</p><br>","<br><br><p>&lt;a&gt; https://www.eduscol.education.fr/ &lt;/a&gt; site Eduscol</p><br>","C","5","22",NULL,"1",NULL,"2020-03-25 14:57:45",NULL),
("893","<p>On considère l’extrait d’un fichier « html » ci-dessous qui génère le champ de saisie d’une application web destiné à recueillir la réponse à une question.</p><br><pre><br>&lt;form action='gestion_reponse.php' id='form_reponse' method='get'> <br>    &lt;input type='text' name='saisie' /> <br>    &lt;input type='submit' value = 'Soumettre' /> <br>&lt;/form><br></pre><br><p>Lorsque l’utilisateur clique sur le bouton de ce formulaire, que se passe-t-il ?</p><br>","<p>Le contenu du champ de saisie est transmis au serveur web en étant visible dans l’URL de la page cible.</p><br>","<p>Le contenu du champ de saisie est transmis au serveur web sans être visible dans l’URL de la page cible.</p><br>","<p>Sans communiquer avec le serveur web, le contenu du champ de saisie est passé en argument à une fonction qui s’exécute sur le client puis le contenu du champ de saisie est rendu visible dans l’URL.</p><br>","<p>Sans communiquer avec le serveur web, le contenu du champ de saisie est passé en argument à une fonction qui s’exécute sur le client sans que le contenu du champ de saisie ne soit visible dans l’URL.</p><br>","A","5","22",NULL,"1",NULL,"2020-04-03 19:45:04",NULL),
("894","<br><br><p>Dans une page web, on souhaite créer un bouton permettant l’appel de la fonction javascript traitement().</p><br><p>Quelle ligne d’instructions permettra de le faire&nbsp;?</p><br>","<br><br><p>&lt;button onclick = 'traitement()'&gt;Cliquez ici&lt;/button&gt;</p><br>","<br><br><p>&lt;a href = traitement()&gt;Cliquez ici&lt;/a&gt;</p><br>","<br><br><p>&lt;button&gt;Cliquez ici&lt;/button = traitement()&gt;</p><br>","<br><br><p>&lt;button&gt;Cliquez ici = traitement()&lt;/button&gt;</p><br>","A","5","22",NULL,"1",NULL,"2020-03-25 14:58:08",NULL),
("895","<br><p>Sous Unix, que fait la commande suivante :</p><br><p>ls –a /home/pi >> toto.txt</p><br>","<br><p>elle liste uniquement les répertoires cachés du répertoire /home/pi</p><br>","<br><p>elle liste tous les fichiers du répertoire /home/pi et enregistre le résultat dans un fichier toto.txt</p><br>","<br><p>elle liste tous les fichiers des répertoires de /home/pi et de toto.txt</p><br>","<br><p>elle liste tous les fichiers du répertoire courant et enregistre le résultat dans un fichier /home/pi/toto.txt</p><br>","B","6","27",NULL,"1",NULL,"2020-04-07 23:21:38",NULL),
("896","<br><br><p>L'architecture client-serveur :</p><br>","<br><br><p>est un mode de communication entre programmes</p><br>","<br><br><p>est une architecture matérielle de coopération entre machines</p><br>","<br><br><p>est un mode de communication entre routeurs</p><br>","<br><br><p>est un mode de communication entre commutateurs</p><br>","A","6","27",NULL,"1",NULL,"2020-03-25 14:58:26",NULL),
("897","<br><br><p>Depuis le répertoire /home/ubuntu/ on exécute la commande</p><br><p>mkdir ./Documents/Holidays</p><br><p>Quel est son effet ?</p><br>","<br><br><p>supprimer le dossier Holidays situé dans Documents</p><br>","<br><br><p>changer de répertoire pour se retrouver dans le répertoire /home/Documents/Holidays</p><br>","<br><br><p>créer un dossier Holidays dans le répertoire /home/ubuntu/Documents</p><br>","<br><br><p>lister le contenu du répertoire Holidays de Documents</p><br>","C","6","27",NULL,"1",NULL,"2020-03-25 14:58:35",NULL),
("898","<br><br><p>Parmi ces composants électroniques, lequel est d'échelle microscopique dans un ordinateur ?</p><br>","<br><br><p>le bus</p><br>","<br><br><p>le radiateur</p><br>","<br><br><p>le transistor</p><br>","<br><br><p>le disque dur</p><br>","C","6","27",NULL,"1",NULL,"2020-03-25 14:58:43",NULL),
("899","<p>Sur la configuration IP d’une machine nommée MACH01 on peut lire :<br />
Adresse Ipv4 : 172.16.100.201<br />
Masque de sous-réseau : 255.255.0.0<br />
Passerelle : 172.16.0.254</p>
<p>Sur la configuration IP d’une machine nommée MACH02 on peut lire :<br />
Adresse Ipv4 : 172.16.100.202<br />
Masque de sous-réseau : 255.255.0.0<br />
Passerelle : 172.16.0.254</p>
<p>Depuis la machine MACH02, à l'aide de quelle commande peut-on tester le dialogue entre ces deux machines ?</p>
","<p>ping 172.16.100.201</p>
","<p>ping 172.16.100.202</p>
","<p>ping 172.16.100.254</p>
","<p>ping 255.255.0.0</p>
","A","6","27",NULL,"1",NULL,"2020-05-08 23:35:37",NULL),
("900","<br><br><p>Quelle commande permet de changer les droits d'accès d'un fichier ou d'un répertoire ?</p><br>","<br><br><p>lsmod</p><br>","<br><br><p>chmod</p><br>","<br><br><p>chown</p><br>","<br><br><p>pwd</p><br>","B","6","27",NULL,"1",NULL,"2020-03-25 14:59:01",NULL),
("901","<p><span class='math inline'>\(n\)</span> étant un entier strictement positif, la fonction suivante calcule sa factorielle, c'est-à-dire le produit <span class='math inline'>\(1 \times 2 \times 3 \times \cdots \times (n - 1) \times n\)</span>. Comment faut-il écrire la ligne en pointillée ci-dessous pour ce faire ?</p><br><pre><br>def factorielle(n):<br>    f = 1<br>    .........<br>        f = f * i<br>    return f<br></pre><br>","<p>for i in range(1,n):</p><br>","<p>for i in range(n+1):</p><br>","<p>for i in range(0,n):</p><br>","<p>for i in range(1,n+1):</p><br>","D","7","33",NULL,"1",NULL,"2020-03-29 16:31:55",NULL),
("902","<br><p>On exécute le script suivant :</p><br><pre><br>a = 4<br>b = 4<br>c = 4<br>while a < 5:<br>    a = a - 1<br>    b = b + 1<br>    c = c * b<br></pre><br><p>Que peut-on dire ?</p><br>","<br><p>ce programme ne termine pas</p><br>","<br><p>à la fin de l'exécution, la variable a vaut 5</p><br>","<br><p>à la fin de l'exécution, la variable b vaut 34</p><br>","<br><p>à la fin de l'exécution, la variable c vaut 42</p><br>","A","7","33",NULL,"1",NULL,"2020-04-03 18:08:32",NULL),
("903","<br><p>On souhaite écrire une fonction qui renvoie le maximum d'une liste d'entiers :</p><br><pre><br>def maximum(L): <br>    m = L[0]<br>    for i in range(1,len(L)):<br>        if .........:<br>            m = L[i]<br>    return m<br></pre><br><p>Par quoi faut-il remplacer les pointillés pour que cette fonction produise bien le résultat attendu ?</p><br>","<br><p>i > m</p><br>","<br><p>L[i] > m</p><br>","<br><p>L[i] > L[i-1]</p><br>","<br><p>L[i] > L[i+1]</p><br>","B","7","33",NULL,"1",NULL,"2020-04-03 18:10:08",NULL),
("904","<br><br><p>Quel est le seul langage de programmation parmi les propositions suivantes ?</p><br>","<br><br><p>HTML</p><br>","<br><br><p>CSS</p><br>","<br><br><p>C++</p><br>","<br><br><p>WEB</p><br>","C","7","33",NULL,"1",NULL,"2020-03-25 14:59:41",NULL),
("905","<p>La fonction suivante calcule la racine carrée du double d’un nombre flottant.</p>
<pre><code class='python'>from math import sqrt
def racine_du_double(x): 
    return sqrt(2*x)</code></pre>
<p>Quelle est la précondition sur l'argument de cette fonction ?</p>
","<p>x &lt; 0</p>
","<p>x &gt;= 0</p>
","<p>2 * x &gt; 0</p>
","<p>sqrt(x) &gt;= 0</p>
","B","7","33",NULL,"1",NULL,"2020-05-08 23:37:27",NULL),
("906","<br><p>La fonction maxi ci-dessous a pour but de renvoyer la valeur maximale présente dans la liste qui lui est passée en argument.</p><br><pre><br>def maxi(L):<br>    dernier_indice = len(L) - 1<br>    valeur_max = L[0]<br>    for i in range(1,dernier_indice):<br>        if L[i] > valeur_max:<br>            valeur_max = L[i]<br>    return valeur_max <br></pre><br><p>Cette fonction a été mal programmée. On souhaite réaliser un test pour le démontrer.</p><br><p>Parmi les propositions suivantes, laquelle mettra la fonction maxi en défaut ?</p><br>","<br><p>maxi([1, 2, 3, 4])</p><br>","<br><p>maxi([4, 3, 2, 1])</p><br>","<br><p>maxi([1, 3, 3, 2])</p><br>","<br><p>maxi([1, 1, 1, 1])</p><br>","A","7","33",NULL,"1",NULL,"2020-04-03 18:05:17",NULL),
("907","<br><p>Quelle est la valeur de c à la fin de l'exécution du code suivant :</p><br><pre><br>L = [1,2,3,4,1,2,3,4,0,2] <br>c = 0 <br>for k in L: <br>    if k == L[1]: <br>        c = c+1 <br></pre><br>","<br><p>0</p><br>","<br><p>2</p><br>","<br><p>3</p><br>","<br><p>10</p><br>","C","8","39",NULL,"1",NULL,"2020-04-03 18:18:30",NULL),
("908","<br><p>Que renvoie la fonction suivante quand on l'appelle avec un nombre entier et une liste d'entiers ?</p><br><pre><br>def mystere(n,L):<br>    for x in L:<br>        if n == x:<br>            return True<br>    return False<br></pre>","<br><p>une valeur booléenne indiquant si le nombre n est présent au moins une fois dans la liste L</p><br>","<br><p>une valeur booléenne indiquant si le nombre n est présent plusieurs fois dans la liste L</p><br>","<br><p>une valeur booléenne indiquant si le nombre n est le plus grand de la liste L</p><br>","<br><p>une valeur booléenne indiquant si le nombre n est le plus petit de la liste L</p><br>","A","8","39",NULL,"1",NULL,"2020-03-30 23:57:36",NULL),
("909","<br><p>La fonction mystere suivante prend en argument un tableau d'entiers.</p><br><pre><br>def mystere(t): <br>    for i in range(len(t) - 1): <br>        if t[i] + 1 != t[i+1]: <br>            return False <br>    return True <br></pre><br><p>À quelle condition la valeur renvoyée par la fonction est-elle True ?</p><br>","<br><p>si le tableau passé en argument est une suite d'entiers consécutifs</p><br>","<br><p>si le tableau passé en argument est trié en ordre croissant</p><br>","<br><p>si le tableau passé en argument est trié en ordre décroissant</p><br>","<br><p>si le tableau passé en argument contient des entiers tous identiques</p><br>","A","8","39",NULL,"1",NULL,"2020-04-03 18:11:43",NULL),
("910","<p>On exécute le script suivant :</p><br> <br><pre><br>def recherche(liste): <br>    valeur_1 = liste[0]<br>    valeur_2 = liste[0] <br>    for item in liste: <br>        if item < valeur_1: <br>            valeur_1 = item <br>        elif item > valeur_2: <br>            valeur_2 = item <br>        else: <br>            pass <br>    return (valeur_1, valeur_2)<br></pre><br><br><pre><br>liste = [48, 17, 25 , 9, 34, 12, -5, 89, 54, 12, 78, 8, 155, -85]<br></pre><br><p>Que va renvoyer l'appel recherche(liste) ?</p><br>","<p>(-85, 155)</p><br>","<p>[-85, 155]</p><br>","<p>(155, -85)</p><br>","<p>(-85; 155)</p><br>","A","8","39",NULL,"1",NULL,"2020-04-03 18:16:50",NULL),
("911","<br><br><p>Un algorithme de recherche dichotomique dans une liste triée de taille <span class='math inline'>\(n\)</span> nécessite, dans le pire des cas, exactement <span class='math inline'>\(k\)</span> comparaisons.</p><br><p>Combien cet algorithme va-t-il utiliser, dans le pire des cas, de comparaisons sur une liste de taille <span class='math inline'>\(2n\)</span> ?</p><br>","<br><br><p><span class='math inline'>\(k\)</span></p><br>","<br><br><p><span class='math inline'>\(k + 1\)</span></p><br>","<br><br><p><span class='math inline'>\(2k\)</span></p><br>","<br><br><p><span class='math inline'>\(2k + 1\)</span></p><br>","B","8","39",NULL,"1",NULL,"2020-03-25 15:01:24",NULL),
("912","<br><p>On considère la fonction suivante :</p><br><pre><br>def f(x,L):<br>    i = 0<br>    j = len(L)-1<br>    while i < j:<br>        k = (i+j)//2<br>        if x <= L[k]:<br>            j = k<br>        else:<br>            i = k + 1<br>    return i<br></pre><br><p>Cette fonction implémente  :</p><br>","<p>le tri par insertion</p><br>","<p>le tri par sélection</p><br>","<p>la recherche dichotomique</p><br>","<p>la recherche du plus proche voisin</p><br>","C","8","39",NULL,"1",NULL,"2020-03-30 23:55:51",NULL),
("913","<br><p>Quelle est, en écriture décimale, la somme d'entiers dont l'écriture en base 16 (hexadécimale) est 2A + 2 ?</p><br>","<br><p>22</p><br>","<br><p>31</p><br>","<br><p>49</p><br>","<br><p>44</p><br>","D","2","7",NULL,"1",NULL,"2020-03-29 19:10:59",NULL),
("914","
<p>On considère l'extrait de code suivant :</p>
<pre><code class='python'>while (a &lt; 20) or (b &gt; 50):
    ......
    ......</code></pre>
<p>Quelles conditions permettent de mettre fin à cette boucle ?</p>
","
<p>la boucle prend fin lorsque a &lt; 20 ou b &gt; 50</p>
","
<p>la boucle prend fin lorsque a &lt; 20 et b &gt; 50</p>
","
<p>la boucle prend fin lorsque a &gt;= 20 ou b &lt;= 50</p>
","
<p>la boucle prend fin lorsque a &gt;= 20 et b &lt;= 50</p>
","D","2","7",NULL,"1",NULL,"2020-05-08 23:39:20",NULL),
("915","<br><br><p>Quelle est la séquence de bit qui représente –25 en complément à 2 sur 8 bits&nbsp;?</p><br>","<br><br><p>0001 1001</p><br>","<br><br><p>0001 1010</p><br>","<br><br><p>1110 0110</p><br>","<br><br><p>1110 0111</p><br>","D","2","7",NULL,"1",NULL,"2020-03-25 15:02:53",NULL),
("916","<p>Si A et B sont des variables booléennes, laquelle de ces expressions booléennes est équivalente à (not A) or B ?</p>
","<p>(A and B) or (not A and B)</p>
","<p>(A and B) or (not A and B) or (not A and not B)</p>
","<p>(not A and B) or (not A and not B)</p>
","<p>(A and B) or (not A and not B)</p>
","B","2","7",NULL,"1",NULL,"2020-05-08 23:40:07",NULL),
("917","<br><br><p>Parmi les quatre expressions suivantes, laquelle s'évalue en True ?</p><br>","<br><br><p>False and (True and False)</p><br>","<br><br><p>False or (True and False)</p><br>","<br><br><p>True and (True and False)</p><br>","<br><br><p>True or (True and False)</p><br>","D","2","7",NULL,"1",NULL,"2020-03-25 15:03:11",NULL),
("918","<br><br><p>Quelle est l'écriture en hexadécimal (base 16) du nombre entier positif qui s'écrit 1110 1101 en base 2 ?</p><br>","<br><br><p>DE</p><br>","<br><br><p>ED</p><br>","<br><br><p>EDF</p><br>","<br><br><p>FEFD</p><br>","B","2","7",NULL,"1",NULL,"2020-03-25 15:03:18",NULL),
("919","
<p>On exécute le script suivant :</p>
<pre><code class='python'>L = [12,0,8,7,3,1,5,3,8]
a = [elt for elt in L if elt&lt;4]</code></pre>
<p>Quelle est la valeur de a à la fin de son exécution ?</p>
","
<p>[12,0,8]</p>
","
<p>[12,0,8,7]</p>
","
<p>[0,3,1,3]</p>
","
<p>[0,3,1]</p>
","C","3","13",NULL,"1",NULL,"2020-05-08 23:41:07",NULL),
("920","
<p>On définit une grille G remplie de 0, sous la forme d'une liste de listes, où toutes les sous-listes ont le même nombre d'éléments.</p>
<pre><code class='python'>G = [ [0, 0, 0, …, 0],
      [0, 0, 0, …, 0],
      [0, 0, 0, …, 0],
       ……
      [0, 0, 0, …, 0] ]</code></pre>
<p>On appelle <em>hauteur</em> de la grille le nombre de sous-listes contenues dans G et <em>largeur</em> de la grille le nombre d'éléments dans chacune de ces sous-listes. Comment peut-on les obtenir ?</p>
","
<p>hauteur = len(G[0])</p>
<p>largeur = len(G)</p>
","
<p>hauteur = len(G)</p>
<p>largeur = len(G[0])</p>
","
<p>hauteur = len(G[0])</p>
<p>largeur = len(G[1])</p>
","
<p>hauteur = len(G[1])</p>
<p>largeur = len(G[0])</p>
","B","3","13",NULL,"1",NULL,"2020-05-08 23:43:07",NULL),
("921","<br><br><p>On définit : L = [10,9,8,7,6,5,4,3,2,1].</p><br><p>Quelle est la valeur de L[L[3]] ?</p><br>","<br><br><p>3</p><br>","<br><br><p>4</p><br>","<br><br><p>7</p><br>","<br><br><p>8</p><br>","A","3","13",NULL,"1",NULL,"2020-03-25 15:03:48",NULL),
("922","<br><br><p>On dispose d’une table patients de personnes décrits par 4 colonnes «&nbsp;Nom&nbsp;», «&nbsp;Prénom&nbsp;», «&nbsp;Age&nbsp;», «&nbsp;Numéro de sécurité sociale&nbsp;» et d’une table affections contenant «&nbsp;Nom&nbsp;», «&nbsp;Prénom&nbsp;», «&nbsp;Numéro de sécurité sociale&nbsp;», «&nbsp;Maladie&nbsp;», «&nbsp;Date d’entrée à l’hôpital&nbsp;».</p><br><p>On souhaite fusionner ces deux tables pour faciliter la gestion des patients et leur distribution entre les services pédiatriques, gérontologiques et autres. Quelle donnée doit-on utiliser pour unifier ces tables&nbsp;:</p><br>","<br><br><p>Le nom du patient</p><br>","<br><br><p>Le prénom du patient</p><br>","<br><br><p>Le numéro de sécurité sociale du patient</p><br>","<br><br><p>La maladie du patient</p><br>","C","3","13",NULL,"1",NULL,"2020-03-25 15:03:59",NULL),
("923","<br><br><p>Quelle est la valeur de l'expression [[0] * 3 for i in range(2)] ?</p><br>","<br><br><p>[[0,0], [0,0], [0,0]]</p><br>","<br><br><p>[[0,0,0], [0,0,0]]</p><br>","<br><br><p>[[0.000], [0.000]]</p><br>","<br><br><p>[[0.00], [0.00], [0.00]]</p><br>","B","3","13",NULL,"1",NULL,"2020-03-25 15:04:07",NULL),
("924","
<p>On dispose du dictionnaire regions ci-dessous :</p>
<pre><code class='python'>regions = { 'Mayotte': 376, 'Pays de la Loire': 32082,
            'La Réunion': 2504, 'Grand Est': 57441,
            'Martinique': 1128, 'Corse': 8680,
            'Bretagne': 27208, 'Nouvelle-Aquitaine': 84036 }</code></pre>
<p>Parmi les instructions suivantes, laquelle permet d'ajouter une nouvelle région ?</p>
","
<p>INSERT ''Hauts de France':31806' INTO regions</p>
","
<p>regions = dict(['Hauts de France'] = 31806)</p>
","
<p>regions('Hauts de France') = 31806</p>
","
<p>regions['Hauts de France'] = 31806</p>
","D","3","13",NULL,"1",NULL,"2020-05-08 23:44:51",NULL),
("925","<br><br><p>Un fichier CSV …</p><br>","<br><br><p>ne peut être lu que par un tableur</p><br>","<br><br><p>est l'unique format utilisé pour construire une base de données</p><br>","<br><br><p>est un fichier texte</p><br>","<br><br><p>est un format propriétaire</p><br>","C","4","18",NULL,"1",NULL,"2020-03-25 15:04:28",NULL),
("926","
<p>On exécute le script suivant :</p>
<pre><code class='python'>a = [1, 2, 3]
b = [4, 5, 6]
c = a + b</code></pre>
<p>Que contient la variable c à la fin de cette exécution ?</p>
","
<p>[5,7,9]</p>
","
<p>[1,4,2,5,3,6]</p>
","
<p>[1,2,3,4,5,6]</p>
","
<p>[1,2,3,5,7,9]</p>
","C","4","18",NULL,"1",NULL,"2020-05-08 21:11:29",NULL),
("927","<br><br><p>Qu'est-ce que le format de fichier CSV ?</p><br>","<br><br><p>un format de fichier mis au point par Microsoft pour Excel</p><br>","<br><br><p>un format de fichier pour décrire une base de données</p><br>","<br><br><p>un format de fichier où les données sont séparées par un caractère tel qu'une virgule</p><br>","<br><br><p>un format de fichier décrivant une page Web</p><br>","C","4","18",NULL,"1",NULL,"2020-03-25 15:04:47",NULL),
("928","<br><p>Laquelle de ces listes de chaînes de caractères est triée en ordre croissant ? (l'ordre<br>choisi étant l'ordre alphabétique)</p><br>","<br><p>['Chat', 'Chien', 'Cheval', 'Cochon']</p><br>","<br><p>['Chat', 'Cheval', 'Chien', 'Cochon']</p><br>","<br><p>['Chien', 'Cheval', 'Cochon', 'Chat']</p><br>","<br><p>['Cochon', 'Chien', 'Cheval', 'Chat']</p><br>","B","4","18",NULL,"1",NULL,"2020-04-03 19:48:03",NULL),
("929","<br><p>Quelle est la valeur de la variable t1 à la fin de l'exécution du script suivant :</p><br><pre><br>t1 = [['Valenciennes', 24],['Lille', 23],['Laon', 31],['Arras', 18]]<br>t2 = [['Lille', 62],['Arras', 53],['Valenciennes', 67],['Laon', 48]]<br>for i in range(len(t1)):<br>    for v in t2:<br>        if v[0] == t1[i][0]:<br>           t1[i].append(v[1])<br></pre><br>","<br><p>[['Valenciennes', 67], ['Lille', 62], ['Laon', 48], ['Arras', 53]]</p><br>","<br><p>[['Valenciennes', 24, 67], ['Lille', 23, 62], ['Laon', 31, 48], ['Arras', 18, 53]]</p><br>","<br><p>[['Arras', 18, 53],['Laon', 31, 48], ['Lille', 23, 62], ['Valenciennes', 24, 67]]</p><br>","<br><p>[['Valenciennes', 67, 24], ['Lille', 62,23], ['Laon', 48, 31], ['Arras', 53, 18]]</p><br>","B","4","18",NULL,"1",NULL,"2020-03-31 00:04:36",NULL),
("930","<p>On exécute le script suivant :</p><br><pre><br>asso = []<br>L =  [ ['marc','marie'], ['marie','jean'], ['<span class='underline'>paul</span>','marie'], ['marie','marie'], ['marc','<span class='underline'>anne</span>'] ]<br>for c in L :<br>    if c[1]=='marie':<br>        asso.append(c[0])<br></pre><br><p>Que vaut asso à la fin de l'exécution ?</p><br>","<p>['marc', 'jean', 'paul']</p><br>","<p>[['marc','marie'], ['paul','marie'], ['marie','marie']]</p><br>","<p>['marc', 'paul', 'marie'] </p><br>","<p>['marie', 'anne']</p><br>","C","4","18",NULL,"1",NULL,"2020-04-22 02:44:31",NULL),
("931","<p>Dans une page HTML, lequel de ces codes permet la présence d'un bouton qui appelle la fonction javascript afficher_reponse() lorsque l'utilisateur clique dessus ?</p>","&lt;a href='afficher_reponse()'&gt;Cliquez ici&lt;/a&gt;<br>","&lt;button if_clicked='afficher_reponse()'>Cliquez ici&lt;/button><br>","&lt;button value='Cliquez ici'>&lt;a> afficher_reponse()&lt;/a>&lt;/button><br>","&lt;button onclick='afficher_reponse()'>Cliquez ici&lt;/button><br>","D","5","22",NULL,"1",NULL,"2020-04-15 12:45:10",NULL),
("932","<br><br><p>Pour créer un lien vers la page d'accueil de Wikipédia, que devra-t-on écrire dans une page Web ?</p><br>","<br><br><p>&lt;a target='http://fr.wikipedia.org'&gt;Wikipédia&lt;/a&gt;</p><br>","<br><br><p>&lt;a href='http://fr.wikipedia.org' /&gt;</p><br>","<br><br><p>&lt;a href='http://fr.wikipedia.org'&gt;Wikipédia&lt;/a&gt;</p><br>","<br><br><p>&lt;link src='http://fr.wikipedia.org'&gt;Wikipédia&lt;/link&gt;</p><br>","C","5","22",NULL,"1",NULL,"2020-03-25 15:05:38",NULL),
("933","<br><br><p>Parmi GET et POST, quelle méthode d'envoi de formulaire crypte les informations envoyées au serveur ?</p><br>","<br><br><p>les deux : GET et POST</p><br>","<br><br><p>GET seulement</p><br>","<br><br><p>POST seulement</p><br>","<br><br><p>aucune des deux</p><br>","D","5","22",NULL,"1",NULL,"2020-03-25 15:05:46",NULL),
("934","<br><br><p>Quelle méthode est utilisée via une requête HTTP pour envoyer une image via un formulaire HTML ?</p><br>","<br><br><p>HEAD</p><br>","<br><br><p>PUT</p><br>","<br><br><p>POST</p><br>","<br><br><p>GET</p><br>","C","5","22",NULL,"1",NULL,"2020-03-25 15:05:53",NULL),
("935","<p>Voici un extrait d'une page HTML :</p>
<pre><code class='html'>&lt;script&gt;
function sommeNombres(formulaire) {
    var somme = formulaire.n1.value + formulaire.n2.value;
    console.log(somme);
}
&lt;/script&gt;
&lt;form&gt;
Nombre 1 : &lt;input name='n1' value='30'&gt; &lt;br&gt;
Nombre 2 : &lt;input name='n2' value='10'&gt; &lt;br&gt;
&lt;input type='button' value='Somme' onclick='sommeNombres(this.form)'&gt;
&lt;/form&gt;</code></pre>
<p>Quand l'utilisateur clique sur le bouton Somme, le calcul de la fonction sommeNombre() se fait :</p>
","<p>uniquement dans le navigateur</p>
","<p>uniquement sur le serveur qui héberge la page</p>
","<p>à la fois dans le navigateur et sur le serveur</p>
","<p>si le calcul est complexe, le navigateur demande au serveur de faire le calcul</p>
","A","5","22",NULL,"1",NULL,"2020-05-08 23:45:49",NULL),
("936","<br><br><p>Parmi les couples de balises suivants, lequel permet de créer un formulaire ?</p><br>","<br><br><p>&lt;body&gt; &lt;/body&gt;</p><br>","<br><br><p>&lt;html&gt; &lt;/html&gt;</p><br>","<br><br><p>&lt;div&gt; &lt;/div&gt;</p><br>","<br><br><p>&lt;form&gt; &lt;/form&gt;</p><br>","D","5","22",NULL,"1",NULL,"2020-03-25 15:06:13",NULL),
("937","<br><br><p>Quelle est la seule affirmation exacte ?</p><br>","<br><br><p>la mémoire RAM ne fonctionne qu'en mode lecture</p><br>","<br><br><p>la mémoire RAM permet de stocker des données et des programmes</p><br>","<br><br><p>une mémoire morte ne peut pas être utilisée</p><br>","<br><br><p>la mémoire RAM permet de stocker définitivement des données</p><br>","B","6","27",NULL,"1",NULL,"2020-03-25 15:06:22",NULL),
("938","<br><p>Vivien télécharge un logiciel à partir d’un site commercial. Le transfert par Internet du logiciel a débuté entre le serveur (machine S) et son domicile (machine V). On a représenté des routeurs A, B, C, D et E et les liens existants. Les paquets IP suivent le chemin passant par les routeurs A, B, C et E.</p><br><p>Durant un orage, la foudre frappe et détruit le serveur C par lequel transitent les paquets correspondant au fichier que télécharge Vivien. Que se passe-t-il ?</p><br>","<br><p>la liaison étant coupée, le serveur ne sera plus accessible</p><br>","<br><p>le téléchargement n’est pas interrompu car les paquets peuvent transiter par le routeur D</p><br>","<br><p>le téléchargement est interrompu, Vivien doit redémarrer une nouvelle connexion à partir de</p><br><p>zéro</p><br>","<br><p>le téléchargement se poursuit mais des données seront perdues</p><br>","B","6","27",NULL,"1","938_genumsi.png","2020-03-29 19:21:36",NULL),
("939","<br><br><p>Sous Unix, que fait la commande suivante**&nbsp;:</p><br><p>ls –a /home/pi &gt;&gt; toto.txt</p><br>","<br><br><p>elle liste uniquement les répertoires cachés du répertoire /home/pi</p><br>","<br><br><p>elle liste tous les fichiers du répertoire /home/pi et enregistre le résultat dans un fichier toto.txt</p><br>","<br><br><p>elle liste tous les fichiers des répertoires de /home/pi et de toto.txt</p><br>","<br><br><p>elle liste tous les fichiers du répertoire courant et enregistre le résultat dans un fichier /home/pi/toto.txt</p><br>","B","6","27",NULL,"1",NULL,"2020-03-25 15:06:42",NULL),
("940","<br><br><p>Dans un réseau informatique, que peut-on dire de la transmission de données par paquets ?</p><br>","<br><br><p>cela empêche l’interception des données transmises</p><br>","<br><br><p>cela garantit que toutes les données empruntent le même chemin</p><br>","<br><br><p>cela assure une utilisation efficace des liens de connexion</p><br>","<br><br><p>cela nécessite la réservation d’un chemin entre l’émetteur et le récepteur</p><br>","C","6","27",NULL,"1",NULL,"2020-03-25 15:06:52",NULL),
("941","<br><br><p>Lequel de ces périphériques n'est pas un périphérique d'entrée ?</p><br>","<br><br><p>le moniteur</p><br>","<br><br><p>le clavier</p><br>","<br><br><p>la souris</p><br>","<br><br><p>le scanner</p><br>","A","6","27",NULL,"1",NULL,"2020-03-25 15:07:00",NULL),
("942","<br><br><p>À quoi sert la RAM dans le fonctionnement d'un ordinateur ?</p><br>","<br><br><p>à stocker des données lors de l'exécution de programmes</p><br>","<br><br><p>à stocker des fichiers</p><br>","<br><br><p>à relier les périphériques</p><br>","<br><br><p>à accélérer la connexion à Internet</p><br>","A","6","27",NULL,"1",NULL,"2020-03-25 15:07:08",NULL),
("943","<br><p>T est un tableau de nombres entiers non vide. Que représente la valeur de s renvoyée par cette fonction ?</p><br><pre>def mystere(T):<br>    s = 0<br>    for k in T:<br>        if k % 2 == 0:<br>            s = s+k<br>    return s<br></pre><br>","<br><p>la somme des valeurs du tableau T</p><br>","<br><p>la somme des valeurs positives du tableau T</p><br>","<br><p>la somme des valeurs impaires du tableau T</p><br>","<br><p>la somme des valeurs paires du tableau T</p><br>","D","7","33",NULL,"1",NULL,"2020-03-29 16:33:36",NULL),
("944","<br><p>Soit <span class='math inline'>\(n\)</span> un entier naturel. Sa factorielle est le produit des nombres entiers strictement positifs qui sont plus petits ou égaux à <span class='math inline'>\(n\)</span>. Par exemple la factorielle de 4 vaut <span class='math inline'>\(1 \times 2 \times 3 \times 4 = 24\)</span>.</p><br><p>Quelle est la fonction correcte parmi les suivantes ?</p><br>","<p><br><pre><br>def factorielle(n):<br>    i = 0<br>    fact = 1<br>    while i <= n:<br>        fact = fact * i<br>        i = i + 1<br>    return fact<br></pre><br></p><br>","<p><br><pre><br>def factorielle(n):<br>    i = 1<br>    fact = 1<br>    while i < n:<br>        fact = fact * i<br>        i = i + 1<br>    return fact<br></pre><br></p><br>","<p><br><pre><br>def factorielle(n):<br>    i = 0<br>    fact = 1<br>    while i < n:<br>        i = i + 1<br>        fact = fact * i<br>    return fact<br></pre><br></p><br>","<p><br><pre><br>def factorielle(n):<br>    i = 0<br>    fact = 1<br>    while i <= n:<br>        i = i + 1<br>        fact = fact * i<br>    return fact<br></pre><br></p><br>","C","7","33",NULL,"1",NULL,"2020-03-29 19:28:15",NULL),
("945","<br><p>On définit une fonction f de la façon suivante :</p><br><p><br><pre><br>def f(L,m):<br>    R = []<br>    for i in range(len(L)):<br>        if L[i] > m:<br>            R.append(L[i])<br>    return R<br></pre><br></p><br><p>On définit L = [1, 7, 3, 4, 8, 2, 0, 3, 5].</p><br><p>Que vaut f(L,4) ?</p><br>","<br><p>[0, 7, 0, 0, 8, 0, 0, 0, 5]</p><br>","<br><p>[0, 0, 0, 5]</p><br>","<br><p>[7, 8, 5]</p><br>","<br><p>[]</p><br>","C","7","33",NULL,"1",NULL,"2020-03-29 19:31:35",NULL),
("946","<p>On exécute le script suivant :</p>
<pre><code class='python'>a = 4
b = 4
c = 4
while a &lt; 5:
    a = a - 1
    b = b + 1
    c = c * b
</code></pre>

<p>Que peut-on dire ?</p>
","<p>ce programme ne termine pas</p>
","<p>à la fin de l'exécution, la variable a vaut 5</p>
","<p>à la fin de l'exécution, la variable b vaut 34</p>
","<p>à la fin de l'exécution, la variable c vaut 42</p>
","A","7","33",NULL,"1",NULL,"2020-05-12 14:28:56",NULL),
("947","<p>En voulant programmer une fonction qui calcule la valeur minimale d'une liste d'entiers, on a écrit :</p><br><p><br><pre><br>def minimum(L):<br>    mini = 0<br>    for e in L:<br>        if e < mini:<br>           mini = e<br>    return mini<br></pre><br></p><br><p>Cette fonction a été mal programmée. Pour quelle liste ne donnera-t-elle pas le résultat attendu, c'est-à-dire son minimum ?</p><br>","<p>[-1,-8,12,2,23]</p><br>","<p>[0,18,12,2,3]</p><br>","<p>[-1,-1,12,12,23]</p><br>","<p>[1,8,12,2,23]</p><br>","D","7","33",NULL,"1",NULL,"2020-04-20 15:15:49",NULL),
("948","<br><p>Quelle est la valeur de la variable n à la fin de l'exécution du script ci-dessous ?</p><br><p><br><pre><br>n = 1<br>while n != 20:<br>    n = n + 2<br></pre><br></p><br>","<br><p>1</p><br>","<br><p>20</p><br>","<br><p>22</p><br>","<br><p>le programme ne termine pas, la boucle tourne indéfiniment</p><br>","D","7","33",NULL,"1",NULL,"2020-03-29 19:35:44",NULL),
("949","<br><p>On considère le code incomplet suivant qui recherche le maximum dans une liste.</p><br><p><br><pre><br>liste = [5,12,15,3,15,17,29,1]<br>iMax = 0<br>for i in range(1,len(liste)):<br>    ............<br>    iMax = i<br>print (liste[iMax])<br></pre><br></p><br><p>Par quoi faut-il remplacer la ligne pointillée ?</p><br>","<br><p>if i > iMax:</p><br>","<br><p>if liste[i] > liste[iMax]:</p><br>","<br><p>if liste[i] > iMax:</p><br>","<br><p>if i > liste[iMax]:</p><br>","B","8","39",NULL,"1",NULL,"2020-03-29 19:40:01",NULL),
("950","<br><br><p>On conçoit un algorithme permettant de déterminer la valeur maximale parmi une liste quelconque de valeurs comparables.</p><br><p>Pour une liste de 100 valeurs, le nombre minimal de comparaisons que doit effectuer cet algorithme est&nbsp;:</p><br>","<br><br><p>7</p><br>","<br><br><p>99</p><br>","<br><br><p>200</p><br>","<br><br><p>10000</p><br>","B","8","39",NULL,"1",NULL,"2020-03-25 15:08:28",NULL),
("951","<br><p>Quelle est la valeur de element à la fin de l'exécution du code suivant :</p><br><p><br><pre><br>L = [1,2,3,4,1,2,3,4,0,2]<br>element = L[0]<br>for k in L:<br>    if k > element:<br>        element = k<br></pre><br></p><br>","<br><p>0</p><br>","<br><p>1</p><br>","<br><p>4</p><br>","<br><p>10</p><br>","C","8","39",NULL,"1",NULL,"2020-03-29 19:41:07",NULL),
("952","<br><br><p>Un algorithme de recherche dichotomique dans une liste triée de taille <span class='math inline'>\(n\)</span> nécessite, dans le pire des cas, exactement <span class='math inline'>\(k\)</span> comparaisons.</p><br><p>Combien cet algorithme va-t-il utiliser, dans le pire des cas, de comparaisons sur une liste de taille <span class='math inline'>\(2n\)</span> ?</p><br>","<br><br><p><span class='math inline'>\(k\)</span></p><br>","<br><br><p><span class='math inline'>\(k + 1\)</span></p><br>","<br><br><p><span class='math inline'>\(2k\)</span></p><br>","<br><br><p><span class='math inline'>\(2k + 1\)</span></p><br>","B","8","39",NULL,"1",NULL,"2020-03-25 15:08:45",NULL),
("953","<br><p>La fonction suivante doit calculer la moyenne d’un tableau de nombres, passé en paramètre. Avec quelles expressions faut-il remplacer les points de suspension pour que la fonction soit correcte ?</p><br><p><br><pre><br>def moyenne(tableau):<br>    total = ...<br>    for valeur in tableau:<br>        total = total + valeur<br>    return total / ...<br></pre><br></p><br>","<br><p>1 et (len(tableau) + 1)</p><br>","<br><p>1 et len(tableau)</p><br>","<br><p>0 et (len(tableau) + 1)</p><br>","<br><p>0 et len(tableau)</p><br>","D","8","39",NULL,"1",NULL,"2020-03-29 19:43:29",NULL),
("954","<br><p>On considère le code suivant, où n désigne un entier au moins égal à 2.</p><br><p><br><pre><br>p = 1<br>while p < n:<br>    p = 2*p<br></pre><br></p><br><p>Quel argument permet d'affirmer que son exécution termine à coup sûr ?</p><br>","<br><p>p est une puissance de 2</p><br>","<br><p>toute boucle while termine</p><br>","<br><p>les valeurs successives de p constituent une suite d'entiers positifs strictement croissante</p><br>","<br><p>les valeurs successives de n – p constituent une suite d'entiers positifs strictement décroissante</p><br>","D","8","39",NULL,"1",NULL,"2020-03-29 19:44:34",NULL),
("958","<br><br><p>Laquelle de ces affirmations concernant le codage UTF-8 des caractères est vraie ?</p><br>","<br><br><p>le codage UTF-8 est sur 7 bits</p><br>","<br><br><p>le codage UTF-8 est sur 8 bits</p><br>","<br><br><p>le codage UTF-8 est sur 1 à 4 octets</p><br>","<br><br><p>le codage UTF-8 est sur 8 octets</p><br>","C","2","7",NULL,"1",NULL,"2020-03-25 15:13:48",NULL),
("959","<br><br><p>Quelle est l'écriture décimale du nombre qui s'écrit 11,0101 en binaire ?</p><br>","<br><br><p>3</p><br>","<br><br><p>3,0101</p><br>","<br><br><p>3,05</p><br>","<br><br><p>3,3125</p><br>","D","2","7",NULL,"1",NULL,"2020-03-25 15:13:58",NULL),
("960","
<p>On exécute le code suivant</p>
<pre><code class='python'>a = 2
b = 3
c = a ** b
d = c % b</code></pre>
<p>Quelle est la valeur de d à la fin de l'exécution ?</p>
","
\(1\)
","
\(2\)
","
\(3\)
","
\(4\)
","B","2","7",NULL,"1",NULL,"2020-04-29 08:53:29",NULL),
("961","<br><br><p>Comment s'écrit le nombre <span class='math inline'>\(- 42\)</span> en binaire, sur 8 bits, en complément à 2 ?</p><br>","<br><br><p>-0010 1010</p><br>","<br><br><p>1010 1011</p><br>","<br><br><p>1101 0101</p><br>","<br><br><p>1101 0110</p><br>","D","2","7",NULL,"1",NULL,"2020-03-25 15:14:16",NULL),
("962","<br><br><p>Quelle est l'écriture décimale de l'entier qui s'écrit 1010 en binaire ?</p><br>","<br><br><p>5</p><br>","<br><br><p>10</p><br>","<br><br><p>20</p><br>","<br><br><p>22</p><br>","B","2","7",NULL,"1",NULL,"2020-03-25 15:14:24",NULL),
("963","<br><br><p>Combien d'entiers positifs ou nuls (entiers non signés) peut-on représenter en machine sur 32&nbsp;bits&nbsp;?</p><br>","<br><br><p><span class='math inline'>\(2^{32} - 1\)</span></p><br>","<br><br><p><span class='math inline'>\(2^{32}\)</span></p><br>","<br><br><p><span class='math inline'>\(2 \times 32\)</span></p><br>","<br><br><p><span class='math inline'>\(32^{2}\)</span></p><br>","B","2","7",NULL,"1",NULL,"2020-03-25 15:14:36",NULL),
("964","<p>On exécute le script suivant :</p>
<pre><code class='python'>inventaire = {'pommes':430, 'bananes':312, 'oranges':274, 'poires':137}
stock = 0
for fruit in inventaire.keys():
    if fruit != 'bananes':
        stock = stock + inventaire[fruit]</code></pre>
<p>Que contient la variable stock à la fin de cette exécution ?</p>
","<pre><code class='python'>{430, 274, 137}</code></pre>
","<pre><code class='python'>312</code></pre>
","<pre><code class='python'>841</code></pre>
","<pre><code class='python'>{'pommes', 'oranges', 'poires'}</code></pre>
","C","3","13",NULL,"1",NULL,"2020-05-07 21:38:36",NULL),
("965","
<p>On définit :</p>
<pre><code class='python'>L = [[1,2,3,4,5],[6,7,8,9,10],[11,12,13,14,15]]</code></pre>
<p>Quelle est la valeur de L[0][2] ?</p>
","
<p>2</p>
","
<p>3</p>
","
<p>11</p>
","
<p>12</p>
","B","3","13",NULL,"1",NULL,"2020-04-29 08:57:31",NULL),
("966","
<p>Considérons le tableau suivant :</p>
<pre><code class='html'>tableau = [[1,2],[3,4],[5,6]]</code></pre>
<p>Quelle est la valeur de l'expression tableau[2][1] ?</p>
","
<p>3</p>
","
<p>6</p>
","
<p>[3,4],[1,2]</p>
","
<p>[5,6],[2,4]</p>
","B","3","13",NULL,"1",NULL,"2020-04-29 08:58:19",NULL),
("967","<p>On a défini</p>
<pre><code class='python'>repertoire = [ {'nom': 'Francette', 'poste': 412},
{'nom': 'Jeanne', 'poste': 222},
{'nom': 'Éric', 'poste': 231} ]</code></pre>
<p>Quelle expression permet d'accéder au poste d'Éric ?</p>
","<p>repertoire[2]['poste']</p>
","<p>repertoire['poste'][2]</p>
","<p>repertoire['Éric']['poste']</p>
","<p>repertoire['Éric']</p>
","A","3","13",NULL,"1",NULL,"2020-05-07 21:43:45",NULL),
("968","<br><br><p>On définit : T = [[1,2,3], [4,5,6], [7,8,9]]</p><br><p>Laquelle des expressions suivantes a pour valeur 7 ?</p><br>","<br><br><p>T[3,1]</p><br>","<br><br><p>T[3][1]</p><br>","<br><br><p>T[2,0]</p><br>","<br><br><p>T[2][0]</p><br>","D","3","13",NULL,"1",NULL,"2020-03-25 15:15:19",NULL),
("969","
<p>On exécute le code suivant :</p>
<pre><code class='python'>t = [1,2,3,4,5,6,7,8,9]
v = [c for c in t if c%3 == 0]</code></pre>
<p>Quelle est la valeur de la variable v à la fin de cette exécution ?</p>
","
<p>18</p>
","
<p>[1,4,7]</p>
","
<p>[3,6,9]</p>
","
<p>[1,2,3,4,5,6,7,8,9]</p>
","C","3","13",NULL,"1",NULL,"2020-05-07 21:46:09",NULL),
("970","<p>On considère la table suivante :</p>
<pre><code class='python'>t = [ {'type': 'marteau', 'prix': 17, 'quantité': 32},
{'type': 'scie', 'prix': 24, 'quantité': 3},
{'type': 'tournevis', 'prix': 8, 'quantité': 45} ]</code></pre>
<p>Quelle expression permet d'obtenir la quantité de scies ?</p>
","<p>t[2]['quantité']</p>
","<p>t[1]['quantité']</p>
","<p>t['quantité'][1]</p>
","<p>t['scies']['quantité']</p>
","B","4","18",NULL,"1",NULL,"2020-05-07 21:48:18",NULL),
("971","<br><br><p>Qu'est-ce que le format de fichier CSV ?</p><br>","<br><br><p>un format de fichier mis au point par Microsoft pour Excel</p><br>","<br><br><p>un format de fichier pour décrire une base de données</p><br>","<br><br><p>un format de fichier où les données sont séparées par un caractère tel qu'une virgule</p><br>","<br><br><p>un format de fichier décrivant une page Web</p><br>","C","4","18",NULL,"1",NULL,"2020-03-25 15:15:49",NULL),
("972","<p>On définit :</p>
<pre><code class='python'>contacts = { 'Toto': 'toto@nsi.fr', 'Chloé': 'chloe@nsi.com',
             'Paul': 'paul@nsi.net', 'Clémence': 'clemence@nsi.org' }
</code></pre>
<p>Parmi les propositions suivantes, laquelle est exacte ?</p>
","'Chloé' est une <strong>valeur</strong> de la variable contacts
","'Chloé' est une <strong>clé</strong> de la variable contacts
","'Chloé' est un <strong>attribut</strong> de la variable contacts
","'Chloé' est un <strong>champ</strong> de la variable contacts
","B","4","18",NULL,"1",NULL,"2020-04-29 09:00:50",NULL),
("973","
<p>Quelle est la valeur de x après exécution du programme ci-dessous ?</p>
<pre><code class='python'>t = [[3,4,5,1],[33,6,1,2]]
x = t[0][0]
for i in range(len(t)):
    for j in range(len(t[i])):
        if x &lt; t[i][j]:
            x = t[i][j]</code></pre>
","
<p>3</p>
","
<p>5</p>
","
<p>6</p>
","
<p>33</p>
","D","4","18",NULL,"1",NULL,"2020-04-29 09:02:30",NULL),
("974","<br><br><p>On utilise habituellement un fichier d'extension csv pour quel type de données ?</p><br>","<br><br><p>des données structurées graphiquement</p><br>","<br><br><p>des données sonores</p><br>","<br><br><p>des données compressées</p><br>","<br><br><p>des données structurées en tableau</p><br>","D","4","18",NULL,"1",NULL,"2020-03-25 15:16:20",NULL),
("975","<br><br><p>On souhaite construire une table de 4 lignes de 3 éléments que l’on va remplir de 0. Quelle syntaxe Python utilisera-t-on&nbsp;?</p><br>","<br><br><p>[ [ 0 ] * 3 for i in range (4) ]</p><br>","<br><br><p>for i in range (4) [ 0 ] * 3</p><br>","<br><br><p>[ 0 ] * 3 for i in range (4)</p><br>","<br><br><p>[ for i in range (4) [ 0 ] * 3 ]</p><br>","A","4","18",NULL,"1",NULL,"2020-03-25 15:16:30",NULL),
("976","<br><br><p>Après avoir tenté d’accéder à un site, le navigateur affiche&nbsp;: &nbsp;403 Forbidden.</p><br><p>Cela signifie que&nbsp;:</p><br>","<br><br><p>la connexion à Internet est défaillante</p><br>","<br><br><p>le navigateur a refusé d'envoyer la requête</p><br>","<br><br><p>le serveur a répondu par un code d'erreur</p><br>","<br><br><p>le serveur n'a jamais répondu</p><br>","C","5","22",NULL,"1",NULL,"2020-03-25 15:16:38",NULL),
("977","<br><br><p>Un site internet utilise une requête HTTP avec la méthode POST pour transmettre les données d'un formulaire. Laquelle des affirmations suivantes est <strong>incorrecte</strong>&nbsp;?</p><br>","<br><br><p>les données envoyées ne sont pas visibles</p><br>","<br><br><p>il est possible de transmettre des données de type binaire</p><br>","<br><br><p>les données transmises sont cryptées</p><br>","<br><br><p>il n'y a pas de restriction de longueur pour les données transmises</p><br>","C","5","22",NULL,"1",NULL,"2020-03-25 15:16:48",NULL),
("978","<br><br><p>Parmi GET et POST, quelle méthode d'envoi de formulaire crypte les informations envoyées au serveur ?</p><br>","<br><br><p>les deux : GET et POST</p><br>","<br><br><p>GET seulement</p><br>","<br><br><p>POST seulement</p><br>","<br><br><p>aucune des deux</p><br>","D","5","22",NULL,"1",NULL,"2020-03-25 15:16:58",NULL),
("979","<p>Dans un formulaire sur une page web, pour transmettre des données sécurisées comme un mot de passe ou un numéro de carte bancaire, il vaut mieux utiliser la méthode :</p>
","
<p>HEAD</p>
","
<p>GET</p>
","
<p>HTTPS</p>
","
<p>POST</p>
","D","5","22",NULL,"1",NULL,"2020-05-07 21:54:31",NULL),
("980","<br><br><p>Comment s'appelle la méthode permettant de transmettre les variables en les faisant apparaître dans la barre d’adresse du navigateur ?</p><br>","<br><br><p>URL</p><br>","<br><br><p>HEAD</p><br>","<br><br><p>POST</p><br>","<br><br><p>GET</p><br>","D","5","22",NULL,"1",NULL,"2020-03-25 15:17:15",NULL),
("981","
<p>Dans une page HTML se trouve le formulaire suivant :</p>
<pre><code class='html'>&lt;form method='.........' action='traitement.html'&gt;
  &lt;p&gt;Nom : &lt;input type='text' name='nom'&gt;&lt;/p&gt;
  &lt;p&gt;Mot de passe : &lt;input type='password' name='mdp'&gt;&lt;/p&gt;
  &lt;p&gt;&lt;input type='submit' name='envoi' value='Envoyer'&gt;&lt;/p&gt;
&lt;/form&gt;</code></pre>
<p>Par quoi faut-il remplacer les pointillés pour que les données du formulaire n'apparaissent pas dans l'URL au moment où l'utilisateur soumet le formulaire au serveur ?</p>
","
<p>GET</p>
","
<p>POST</p>
","
<p>SECRET</p>
","
<p>HIDDEN</p>
","B","5","22",NULL,"1",NULL,"2020-05-07 21:57:25",NULL),
("982","<br><br><p>Quelle commande du shell Linux permet de renommer un fichier ?</p><br>","<br><br><p>cp</p><br>","<br><br><p>rm</p><br>","<br><br><p>mv</p><br>","<br><br><p>touch</p><br>","C","6","27",NULL,"1",NULL,"2020-03-25 15:17:33",NULL),
("983","<br><br><p>On cherche à connaitre l’itinéraire vers une destination sur un réseau. On utilisera la commande :</p><br>","<br><br><p>ping</p><br>","<br><br><p>traceroute</p><br>","<br><br><p>ipconfig</p><br>","<br><br><p>arp</p><br>","B","6","27",NULL,"1",NULL,"2020-03-25 15:17:41",NULL),
("984","<br><br><p>Quelle commande permet de changer les droits d'accès d'un fichier ou d'un répertoire ?</p><br>","<br><br><p>lsmod</p><br>","<br><br><p>chmod</p><br>","<br><br><p>chown</p><br>","<br><br><p>pwd</p><br>","B","6","27",NULL,"1",NULL,"2020-03-25 15:44:26",NULL),
("985","<br><br><p>Dans une mémoire RAM, que peut-on faire ?</p><br>","<br><br><p>uniquement lire des données</p><br>","<br><br><p>uniquement écrire des données</p><br>","<br><br><p>lire et écrire des données</p><br>","<br><br><p>lire des données même en cas de coupure de courant</p><br>","C","6","27",NULL,"1",NULL,"2020-03-25 15:44:34",NULL),
("986","<br><br><p>Sous Unix, quelle commande permet de créer un nouveau répertoire&nbsp;?</p><br>","<br><br><p>mkdir</p><br>","<br><br><p>echo</p><br>","<br><br><p>ls</p><br>","<br><br><p>rm</p><br>","A","6","27",NULL,"1",NULL,"2020-03-25 15:44:42",NULL),
("987","
<p>T est un tableau de nombres entiers non vide. Que représente la valeur de s renvoyée par cette fonction ?</p>
<pre><code class='python'>def mystere(T):
    s = 0
    for k in T:
        if k % 2 == 0:
            s = s+k
    return s</code></pre>
","
<p>la somme des valeurs du tableau T</p>
","
<p>la somme des valeurs positives du tableau T</p>
","
<p>la somme des valeurs impaires du tableau T</p>
","
<p>la somme des valeurs paires du tableau T</p>
","D","7","33",NULL,"1",NULL,"2020-04-29 09:03:46",NULL),
("988","
<p>On définit la fonction :</p>
<pre><code class='python'>def fib(n):
    t = [0] * n
    t[1] = 1
    for in in range(2,n):
        t[i] = t[i-1] + t[i-2]
    return t</code></pre>
<p>Quelle est la valeur renvoyée par l'appel fib(6) ?</p>
","
<p>[0, 1, 1, 2, 3]</p>
","
<p>[0, 1, 1, 2, 3, 5]</p>
","
<p>[0, 1, 1, 2, 3, 5, 8]</p>
","
<p>[0, 1, 2, 3, 5, 8]</p>
","B","7","33",NULL,"1",NULL,"2020-04-29 09:05:04",NULL),
("989","
<p>On considère le code suivant :</p>
<pre><code class='python'>if x &lt; 4:
    x = x + 3
else:
    x = x - 3</code></pre>
<p>Quelle construction élémentaire peut-on identifier ?</p>
","
<p>une boucle non bornée</p>
","
<p>une structure conditionnelle</p>
","
<p>une boucle bornée</p>
","
<p>un appel de fonction</p>
","B","7","33",NULL,"1",NULL,"2020-04-29 09:05:40",NULL),
("990","<p>La fonction <code>ajoute(n,p)</code> codée ci-dessous en Python doit calculer la somme de tous les entiers compris entre n et p (n et p compris).</p>
<p>Par exemple, <code>ajoute(2,4)</code> doit renvoyer 2+3+4 = 9.</p>
<pre><code class='python'>def ajoute(n,p):
    somme = 0
    for i in range(.........): # ligne à modifier
        somme = somme + i
    return somme</code></pre>
<p>Quelle est la bonne écriture de la ligne marquée à modifier ?</p>
","<p>for i in range(n,1,p):</p>
","<p>for i in range(n,p):</p>
","<p>for i in range(n,p+1):</p>
","<p>for i in range(n-1,p):</p>
","C","7","33",NULL,"1",NULL,"2020-05-07 22:04:51",NULL),
("991","<p>On exécute le code suivant :</p>
<pre><code class='python'>def f(t):
    n = len(t)
    for k in range(1,n):
        t[k] = t[k] + t[k-1]

L = [1, 3, 4, 5, 2]
f(L)</code></pre>
<p>Quelle est la valeur de L après l'exécution de ce code ?</p>
","<p>[1, 3, 4, 5, 2]</p>
","<p>[1, 4, 7, 9, 7]</p>
","<p>[1, 4, 8, 13, 15]</p>
","<p>[3, 6, 10, 15, 17]</p>
","C","7","33",NULL,"1",NULL,"2020-05-07 22:06:04",NULL),
("992","
<p>On considère l'instruction suivante :</p>
<pre><code class='python'>resultat = [0] * 7</code></pre>
<p>Que contient la variable resultat après son exécution ?</p>
","
<p>0</p>
","
<p>[0]</p>
","
<p>[[0], [0], [0], [0], [0], [0], [0]]</p>
","
<p>[0, 0, 0, 0, 0, 0, 0]</p>
","D","7","33",NULL,"1",NULL,"2020-04-29 09:08:06",NULL),
("993","<p>L'algorithme suivant permet de calculer la somme des N premiers entiers, où N est un nombre entier donné :</p>
<pre><code class='python'>i =0
somme = 0
while i &lt; N :
    i = i +1
    somme = somme + i</code></pre>
<p>Un invariant de boucle de cet algorithme est le suivant :</p>
","<p>somme = 0 + 1 + 2 + ... + i    et    i &lt; N</p>
","<p>somme = 0 + 1 + 2 + ... + N    et    i &lt; N</p>
","<p>somme = 0 + 1 + 2 + ... + i    et    i &lt; N+1</p>
","<p>somme = 0 + 1 + 2 + ... + N    et    i &lt; N+1</p>
","C","8","39",NULL,"1",NULL,"2020-05-07 22:08:33",NULL),
("994","
<p>On exécute le script suivant :</p>
<pre><code class='python'>for i in range(n):
    for j in range(i):
        print('NSI')</code></pre>
<p>Combien de fois le mot NSI est-il affiché ?</p>
","
\(n^{2}\)
","\({(n + 1)}^{2}\)","
\(1 + 2 + \cdots + (n - 1)\)
","
\(1 + 2 + \cdots + (n - 1) + n\)
","C","8","39",NULL,"1",NULL,"2020-04-29 09:09:50",NULL),
("995","<p>Quel code parmi les quatre proposés ci-dessous s'exécute-t-il en un temps linéaire en \(n\) (c'est-à-dire avec un temps d'exécution majoré par \(A \times n  + B\) où \(A\) et \(B\) sont deux constantes) ?</p>
","<pre><code class='python'>for i in range(n//2):
    for j in range(i+1,n):
        print('hello')</code></pre>
","<pre><code class='python'>for i in range(n):
    print('hello')</code></pre>","<pre><code class='python'>L = [ i+j for i in range(n) for j in range(n) ]
for x in L:
    print('hello')</code></pre>","<pre><code class='python'>for i in range(n//2):
    for j in range(n//2):
        print('hello')</code></pre>
","B","8","39",NULL,"1",NULL,"2020-04-29 09:12:09",NULL),
("996","
<p>On considère le code suivant, où n désigne un entier au moins égal à 2.</p>
<pre><code class='python'>p = 1
while p &lt; n:
    p = 2*p</code></pre>
<p>Quel argument permet d'affirmer que son exécution termine à coup sûr ?</p>
","
<p>p est une puissance de 2</p>
","
<p>toute boucle while termine</p>
","
<p>les valeurs successives de p constituent une suite d'entiers positifs strictement croissante</p>
","
<p>les valeurs successives de n – p constituent une suite d'entiers positifs strictement décroissante</p>
","D","8","39",NULL,"1",NULL,"2020-04-29 09:13:02",NULL),
("997","<br><br><p>Soit <span class='math inline'>\(T\)</span> le temps nécessaire pour trier, à l'aide de l'algorithme du tri par insertion, une liste de 1000 nombres entiers. Quel est l'ordre de grandeur du temps nécessaire, avec le même algorithme, pour trier une liste de 10&nbsp;000 entiers, c'est-à-dire une liste dix fois plus grande ?</p><br>","<br><br><p>à peu près le même temps <span class='math inline'>\(T\)</span></p><br>","<br><br><p>environ <span class='math inline'>\(10 \times T\)</span></p><br>","<br><br><p>environ <span class='math inline'>\(100 \times T\)</span></p><br>","<br><br><p>environ <span class='math inline'>\(T^{2}\)</span></p><br>","C","8","39",NULL,"1",NULL,"2020-03-25 15:46:25",NULL),
("998","<br><br><p>À quelle catégorie appartient l’algorithme des k plus proches voisins ?</p><br>","<br><br><p>algorithmes de tri</p><br>","<br><br><p>algorithmes gloutons</p><br>","<br><br><p>algorithmes de recherche de chemins</p><br>","<br><br><p>algorithmes de classification et d’apprentissage</p><br>","D","8","39",NULL,"1",NULL,"2020-03-25 15:46:34",NULL),
("999","<p>Choisir une expression booléenne pour la variable S qui satisfait la table de vérité suivante.</p>
<table class='table_verite' border='1' width='100'><thead><tr><th>A</th>
<th>B</th>
<th>S</th>
</tr></thead><tbody><tr><td>0</td>
<td>0</td>
<td>1</td>
</tr><tr><td>0</td>
<td>1</td>
<td>0</td>
</tr><tr><td>1</td>
<td>0</td>
<td>1</td>
</tr><tr><td>1</td>
<td>1</td>
<td>1</td>
</tr></tbody></table><br />","<p>A ou (non B)</p>","<p>(non A) ou B</p>
","<p>(non A) ou (non B)</p>
","<p>non (A ou B)</p>
","A","2","7",NULL,"1",NULL,"2020-05-09 00:11:20",NULL),
("1000","<p>Parmi les propositions suivantes, laquelle est la représentation binaire de 753 ?</p>
","<p>11 1100 1101</p>
","<p>11 1110 0101</p>
","<p>10 0111 1001</p>
","<p>10 1111 0001</p>
","D","2","7",NULL,"1",NULL,"2020-06-03 14:28:04",NULL),
("1001","
<p>A et B sont deux propositions vraies. Laquelle des ces propositions est-elle également vraie ?</p>
","
<p>((non A) et B) ou (A et (non B))</p>
","
<p>((non A) ou B) et ((non A) ou (non B))</p>
","
<p>((non A) ou B) et (A ou (non B))</p>
","
<p>((non A) et B) et (A et (non B))</p>
","C","2","7",NULL,"1",NULL,"2020-05-09 00:13:36",NULL),
("1002","<p>Que peut-on dire du programme Python suivant de calcul sur les nombres flottants ?</p>
<pre><code class='python'>x = 1.0
while x != 0.0:
    x = x - 0.1</code></pre>
","<p>l'exécution peut ne pas s'arrêter, si la variable x n'est jamais exactement égale à 0.0</p>
","<p>à la fin de l'exécution, x vaut – 0.00001</p>
","<p>à la fin de l'exécution, x vaut 0.00001</p>
","<p>l'exécution s'arrête sur une erreur FloatingPointError</p>
","A","2","7",NULL,"1",NULL,"2020-05-09 00:16:11",NULL),
("1003","
<p>Combien de bits sont nécessaires pour représenter 15 en binaire ?</p>
","
<p>2</p>
","
<p>3</p>
","
<p>4</p>
","
<p>5</p>
","C","2","7",NULL,"1",NULL,"2020-05-09 00:16:47",NULL),
("1004","
<p>Sachant que l'expression not(a or b) a la valeur True, quelles peuvent être les valeurs des variables booléennes a et b ?</p>
","
<p>True et True</p>
","
<p>False et True</p>
","
<p>True et False</p>
","
<p>False et False</p>
","D","2","7",NULL,"1",NULL,"2020-05-09 00:17:44",NULL),
("1005","
<p>On définit en Python la fonction suivante :</p>
<pre><code class='python'>def f(L):
    S = []
    for i in range(len(L)-1):
        S.append(L[i] + L[i+1])
    return S</code></pre>
<p>Quelle est la liste renvoyée par f([1, 2, 3, 4, 5, 6]) ?</p>
","
<p>[3, 5, 7, 9, 11, 13]</p>
","
<p>[1, 3, 5, 7, 9, 11]</p>
","
<p>[3, 5, 7, 9, 11]</p>
","
<p>cet appel de fonction déclenche un message d'erreur</p>
","C","3","13",NULL,"1",NULL,"2020-05-09 00:19:45",NULL),
("1006","
<p>Quelle est la valeur de l'expression <code>[(a,b) for a in range(3) for b in range(3) if a &gt; b]</code> ?</p>
","
<p>[(a,b),(a,b),(a,b),(a,b),(a,b),(a,b),(a,b),(a,b),(a,b)]</p>
","
<p>[(0,0),(0,1),(0,2),(1,0),(1,1),(1,2),(2,0),(2,1),(2,2)]</p>
","
<p>[(1,0),(2,0),(2,1)]</p>
","
<p>[(0,0),(0,1),(0,2),(1,0),(1,1),(1,2),(1,0),(1,1),(1,2)]</p>
","C","3","13",NULL,"1",NULL,"2020-05-09 00:21:09",NULL),
("1007","
<p>On définit : <code>matrice = [[1,2,3], [4,5,6], [7,8,9], [10,11,12]]</code>.</p>
<p>Quelle est la valeur de matrice[1][2] ?</p>
","
<p>2</p>
","
<p>4</p>
","
<p>6</p>
","
<p>8</p>
","C","3","13",NULL,"1",NULL,"2020-05-09 00:22:22",NULL),
("1008","
<p>On construit une matrice par compréhension :</p>
<pre><code class='python'>M = [ [i*j for j in range(4)] for i in range(4) ]</code></pre>
<p>Laquelle des conditions suivantes est-elle vérifiée ?</p>
","
<p>M[4][4] == 16</p>
","
<p>M[0][1] == 1</p>
","
<p>M[2][3] == 6</p>
","
<p>M[1][2] == 3</p>
","C","3","13",NULL,"1",NULL,"2020-05-09 00:23:38",NULL),
("1009","
<p>On définit L = [2,3,5,7,-4].</p>
<p>En demandant la valeur de L[5], qu'obtient-on ?</p>
","
<p>-4</p>
","
<p>2</p>
","
<p>3</p>
","
<p>une erreur</p>
","D","3","13",NULL,"1",NULL,"2020-05-09 00:24:25",NULL),
("1010","
<p>On définit L = [4,25,10,9,7,13]. Quelle est la valeur de L[2] ?</p>
","
<p>4</p>
","
<p>25</p>
","
<p>10</p>
","
<p>9</p>
","C","3","13",NULL,"1",NULL,"2020-05-09 00:24:53",NULL),
("1011","
<p>On définit ainsi une liste t puis une liste r :</p>
<pre><code class='python'>t = [ {'id':1, 'age':23, 'sejour':'PEKIN'},
      {'id':2, 'age':27, 'sejour':'ISTANBUL'},
      {'id':3, 'age':53, 'sejour':'LONDRES'},
      {'id':4, 'age':41, 'sejour':'ISTANBUL'},
      {'id':5, 'age':62, 'sejour':'RIO'},
      {'id':6, 'age':28, 'sejour':'ALGER'} ]

r = [ c for c in t if c['age']&gt;30 and c['sejour']=='ISTANBUL' ]</code></pre>
<p>Combien la liste r contient-elle d'éléments ?</p>
","
<p>0</p>
","
<p>1</p>
","
<p>2</p>
","
<p>3</p>
","B","4","18",NULL,"1",NULL,"2020-05-09 00:27:13",NULL),
("1012","
<p>On exécute le code suivant :</p>
<pre><code class='python'>table = [ ['lovelace', 'ada', 1815, 1852],
          ['von neumann','john', 1903, 1957],
          ['turing', 'alan', 1912, 1954],
          ['mccarthy', 'john', 1927, 2011],
          ['floyd', 'robert', 1936, 2001] ]

def age(personnage):
    return personnage[3] - personnage[2]

table.sort(key=age, reverse=True)</code></pre>
<p>Quelle est la première ligne de la table table à la suite de cette exécution ?</p>
","
<p>['lovelace', 'ada', 1815, 1852]</p>
","
<p>['mccarthy', 'john', 1927, 2011]</p>
","
<p>['turing', 'alan', 1912, 1954]</p>
","
<p>['mccarthy', 'floyd', 'von neumann', 'turing', 'lovelace']</p>
","B","4","18",NULL,"1",NULL,"2020-05-09 00:29:54",NULL),
("1013","<p>On exécute le code suivant :</p>
<pre><code class='python'>collection = [ ('Renault', '4L', 1974, 30),
               ('Peugeot', '504', 1970, 82),
               ('Citroën', 'Traction', 1950, 77) ]</code></pre>
<p>Que vaut collection[1][2] ?</p>
","<p>1970</p>
","<p>'4L'</p>
","<p>('Peugeot', '504', 1970, 82)</p>
","<p>('Renault', '4L', 1974, 30)</p>
","A","4","18",NULL,"1",NULL,"2020-05-09 00:31:57",NULL),
("1014","
<p>Laquelle de ces listes de chaînes de caractères est triée en ordre croissant ?</p>
","
<p>['112', '19', '27', '45', '8']</p>
","
<p>['8', '19', '27', '45', '112']</p>
","
<p>['8', '112', '19', '27', '45']</p>
","
<p>['19', '112', '27', '45', '8']</p>
","A","4","18",NULL,"1",NULL,"2020-05-09 00:32:46",NULL),
("1015","<p>On définit ainsi une liste t :</p>
<pre><code class='python'>t = [ {'id':1, 'age':23, 'sejour':'PEKIN'},
      {'id':2, 'age':27, 'sejour':'ISTANBUL'},
      {'id':3, 'age':53, 'sejour':'LONDRES'},
      {'id':4, 'age':41, 'sejour':'ISTANBUL'},
      {'id':5, 'age':62, 'sejour':'RIO'},
      {'id':6, 'age':28, 'sejour':'ALGER'} ]</code></pre>
<p>Quelle affirmation est correcte ?</p>
","t est une liste de listes
","t est une liste de dictionnaires
","t est un dictionnaire de listes
","t est une liste de tuples
","B","4","18",NULL,"1",NULL,"2020-05-09 00:33:47",NULL),
("1016","
<p>On définit :</p>
<pre><code class='python'>contacts = { 'Toto': 'toto@nsi.fr', 'Chloé': 'chloe@nsi.com',
             'Paul': 'paul@nsi.net', 'Clémence': 'clemence@nsi.org' }</code></pre>
<p>Parmi les propositions suivantes, laquelle est exacte ?</p>
","
<p>'Chloé' est une <strong>valeur</strong> de la variable contacts</p>
","
<p>'Chloé' est une <strong>clé</strong> de la variable contacts</p>
","
<p>'Chloé' est un <strong>attribut</strong> de la variable contacts</p>
","
<p>'Chloé' est un <strong>champ</strong> de la variable contacts</p>
","B","4","18",NULL,"1",NULL,"2020-05-09 00:35:42",NULL),
("1017","
<p>Dans une page HTML, lequel de ces codes permet la présence d'un bouton qui appelle la fonction javascript afficher_reponse() lorsque l'utilisateur clique dessus ?</p>
","
<p>&lt;a href='afficher_reponse()'&gt;Cliquez ici&lt;/a&gt;</p>
","
<p>&lt;button if_clicked='afficher_reponse()'&gt;Cliquez ici&lt;/button&gt;</p>
","
<p>&lt;button value='Cliquez ici'&gt;&lt;a&gt; afficher_reponse()&lt;/a&gt;&lt;/button&gt;</p>
","
<p>&lt;button onclick='afficher_reponse()'&gt;Cliquez ici&lt;/button&gt;</p>
","D","5","22",NULL,"1",NULL,"2020-05-09 00:37:25",NULL),
("1018","
<p>Parmi les quatre propositions suivantes, laquelle est la seule à correspondre à un entête correct de formulaire d'une page HTML ?</p>
","
<p>&lt;form method='formulaire.php' action='submit'&gt;</p>
","
<p>&lt;form method='post' action=onclick()&gt;</p>
","
<p>&lt;form method='get' action='arret.php'&gt;</p>
","
<p>&lt;form method='post' action=arret.php&gt;</p>
","C","5","22",NULL,"1",NULL,"2020-05-09 00:39:10",NULL),
("1019","
<p>On considère l’extrait suivant d'une page web d'un site de vente en ligne.</p>
<pre><code class='html'>&lt;form id='fmCreerCommande' name='fmCreerCommande' action='gestion.php' method='post'&gt;
  &lt;input type='HIDDEN' name='reference' value='F1245'&gt;
  &lt;label for='quantite' class='dropdown'&gt;Quantité :&lt;/label&gt;
  &lt;select name='quantite' autocomplete='off' id='quantite' class='dropdown'&gt;
    &lt;option value='1' selected&gt;1&lt;/option&gt;
    &lt;option value='2'&gt;2&lt;/option&gt;
    &lt;option value='3'&gt;3&lt;/option&gt;
  &lt;/select&gt;
  &lt;div id='btnMulti' class='clValidBtn'&gt;
    &lt;input type='button' value='Ajouter' class='btn' onclick='ajouterProduit()'&gt;
  &lt;/div&gt;
&lt;/form&gt;</code></pre>
<p>Quelle est la méthode utilisée pour transmettre les informations saisies dans ce formulaire ?</p>
","
<p>La méthode HIDDEN</p>
","
<p>La méthode GET</p>
","
<p>La méthode POST</p>
","
<p>La méthode SELECT</p>
","C","5","22",NULL,"1",NULL,"2020-05-09 00:44:00",NULL),
("1020","
<p>Dans quels langages les balises &lt;img&gt; et &lt;form&gt; sont-elles utilisées ?</p>
","
<p>Python</p>
","
<p>HTML</p>
","
<p>Javascript</p>
","
<p>PHP</p>
","B","5","22",NULL,"1",NULL,"2020-05-09 00:44:37",NULL),
("1021","
<p>Quelle est la machine qui va exécuter un programme JavaScript inclus dans une page HTML ?</p>
","
<p>la machine de l’utilisateur sur laquelle s’exécute le navigateur Web</p>
","
<p>le serveur Web sur lequel est stockée la page HTML</p>
","
<p>la machine de l’utilisateur ou du serveur, selon celle qui est la plus disponible</p>
","
<p>la machine de l’utilisateur ou du serveur, suivant la confidentialité des données manipulées</p>
","A","5","22",NULL,"1",NULL,"2020-05-09 00:45:31",NULL),
("1022","
<p>Quelle est la machine qui exécute un programme JavaScript inclus dans une page HTML ?</p>
","
<p>le serveur WEB qui contient la page HTML</p>
","
<p>la machine de l'utilisateur qui consulte la page HTML</p>
","
<p>un serveur du réseau</p>
","
<p>un routeur du réseau</p>
","B","5","22",NULL,"1",NULL,"2020-05-09 00:46:56",NULL),
("1023","
<p>Laquelle des propositions suivantes n'est pas une adresse IP valide ?</p>
","
<p>255.300.1.1</p>
","
<p>255.255.1.1</p>
","
<p>255.32.6.1</p>
","
<p>255.2.35.249</p>
","A","6","27",NULL,"1",NULL,"2020-05-09 00:47:37",NULL),
("1024","
<p>Sous Linux, dans quel but utilise-t-on la commande ls ?</p>
","
<p>pour afficher le chemin vers le répertoire courant</p>
","
<p>pour afficher les noms de fichiers et répertoires du répertoire courant</p>
","
<p>pour effacer un fichier</p>
","
<p>pour copier un fichier</p>
","B","6","27",NULL,"1",NULL,"2020-05-09 00:48:17",NULL),
("1025","
<p>Sur un ordinateur, où est stocké de manière permanente le système d'exploitation ?</p>
","
<p>dans la mémoire RAM</p>
","
<p>sur le bus de donnée</p>
","
<p>sur le disque dur ou le disque SSD</p>
","
<p>dans le Cloud</p>
","C","6","27",NULL,"1",NULL,"2020-05-09 00:48:56",NULL),
("1026","
<p>Quelles sont les quatre parties distinctes de l’architecture de Von Neumann ?</p>
","
<p>L’unité logique, l’unité de contrôle, la mémoire et les dispositifs d’entrée-sortie</p>
","
<p>L’écran, le clavier, le disque dur et le micro-processeur</p>
","
<p>Le disque dur, le micro-processeur, la carte-mère et la carte graphique</p>
","
<p>La mémoire des programmes, la mémoire des données, les entrées-sorties et l’unité logique</p>
","A","6","27",NULL,"1",NULL,"2020-05-09 00:49:45",NULL),
("1027","
<p>Sachant que  hibou est un fichier présent dans le répertoire courant, quel est l’effet de la commande suivante : <code>mv hibou chouette</code></p>
","
<p>déplacer le fichier hibou dans le répertoire chouette</p>
","
<p>ajouter le contenu du fichier hibou à la fin du fichier chouette</p>
","
<p>renommer le fichier hibou en chouette</p>
","
<p>créer le fichier chouette, copie du fichier hibou</p>
","C","6","27",NULL,"1",NULL,"2020-05-09 00:51:02",NULL),
("1028","<p>La commande suivante vient d'être exécutée en ligne de commande sous Linux :</p>
<pre> cp /users/luc/interro.txt ./ </pre>
<p>Que réalise cette commande ?</p>
","<p>copie du fichier users vers le répertoire luc</p>
","<p>copie du fichier interro.txt vers le répertoire luc</p>
","<p>copie du fichier interro.txt vers le répertoire courant</p>
","<p>copie du fichier interro.txt vers le répertoire users</p>
","C","6","27",NULL,"1",NULL,"2020-06-03 14:20:53",NULL),
("1029","
<p>La fonction maxi ci-dessous a pour but de renvoyer la valeur maximale présente dans la liste qui lui est passée en argument.</p>
<pre><code class='python'>def maxi(L):
    dernier_indice = len(L) - 1
    valeur_max = L[0]
    for i in range(1,dernier_indice):
        if L[i] &gt; valeur_max:
            valeur_max = liste[i]
    return valeur_max</code></pre>
<p>Cette fonction a été mal programmée. On souhaite réaliser un test pour le démontrer.</p>
<p>Parmi les propositions suivantes, laquelle mettra la fonction maxi en défaut ?</p>
","
<p>maxi([1, 2, 3, 4])</p>
","
<p>maxi([4, 3, 2, 1])</p>
","
<p>maxi([1, 3, 3, 2])</p>
","
<p>maxi([1, 1, 1, 1])</p>
","A","7","33",NULL,"1",NULL,"2020-05-09 00:56:32",NULL),
("1030","
<p>La fonction suivante ne calcule pas toujours correctement le maximum des deux nombres donnés en argument. On rappelle que abs(z) calcule la valeur absolue du nombre z.</p>
<pre><code class='python'>def maxi(x,y):
    m = (x-y+abs(x+y))/2
    return m</code></pre>
<p>Parmi les tests suivants, lequel va détecter l'erreur ?</p>
","
<p>maxi(3,-2)</p>
","
<p>maxi(2,2)</p>
","
<p>maxi(3,2)</p>
","
<p>maxi(2,3)</p>
","D","7","33",NULL,"1",NULL,"2020-05-09 00:58:51",NULL),
("1031","
<p>La fonction maximum codée ci-dessous en Python doit renvoyer la plus grande valeur contenue dans le tableau d'entiers passé en argument.</p>
<pre><code class='python'>def maximum(tableau):
    tmp = tableau[0]
    for i in range(......): # à compléter
        if tableau[i] &gt; tmp:
            tmp = tableau[i]
    return tmp</code></pre>
<p>Quelle expression faut-il écrire à la place des pointillés ?</p>
","
<p>len(tableau) - 1</p>
","
<p>1,len(tableau) - 1</p>
","
<p>1,len(tableau)</p>
","
<p>1,len(tableau) + 1</p>
","C","7","33",NULL,"1",NULL,"2020-05-09 01:00:30",NULL),
("1032","
<p>Lequel des langages suivants n'est pas un langage de programmation :</p>
","
<p>PHP</p>
","
<p>Javascript</p>
","
<p>HTML</p>
","
<p>Python</p>
","C","7","33",NULL,"1",NULL,"2020-05-09 01:01:17",NULL),
("1033","
<p>En Python, quelle est la méthode pour charger la fonction sqrt du module math ?</p>
","
<p>using math.sqrt</p>
","
<p>#include math.sqrt</p>
","
<p>from math include sqrt</p>
","
<p>from math import sqrt</p>
","D","7","33",NULL,"1",NULL,"2020-05-09 01:02:08",NULL),
("1034","
<p>Quelle est la valeur de la variable b à la fin de l'exécution du script suivant ?</p>
<pre><code class='python'>a = 2
b = 5
if a &gt; 8:
    b = 10
elif a &gt; 6:
    b = 3</code></pre>
","
<p>3</p>
","
<p>5</p>
","
<p>6</p>
","
<p>10</p>
","B","7","33",NULL,"1",NULL,"2020-05-09 01:03:44",NULL),
("1035","
<p>On considère le code suivant, où n désigne un entier au moins égal à 2.</p>
<pre><code class='python'>p = 1
while p &lt; n:
    p = 2*p</code></pre>
<p>Quel argument permet d'affirmer que son exécution termine à coup sûr ?</p>
","
<p>p est une puissance de 2</p>
","
<p>toute boucle while termine</p>
","
<p>les valeurs successives de p constituent une suite d'entiers positifs strictement croissante</p>
","
<p>les valeurs successives de n – p constituent une suite d'entiers positifs strictement décroissante</p>
","D","8","39",NULL,"1",NULL,"2020-05-09 01:06:05",NULL),
("1036","
<p>Soit L une liste de <span class='math inline'>\(n\)</span> nombres réels (<span class='math inline'>\(n\)</span> entier naturel non nul). On considère l'algorithme suivant, en langage Python, calculant la moyenne des éléments de L.</p>
<pre><code class='python'>M = 0
for k in range(n):
    M = M + L[k]
M = M/n</code></pre>
<p>Si le nombre <span class='math inline'>\(n\)</span> de données double alors le temps d'exécution de ce script :</p>
","
<p>reste le même</p>
","
<p>double aussi</p>
","
<p>est multiplié par <span class='math inline'>\(n\)</span></p>
","
<p>est multiplié par 4</p>
","B","8","39",NULL,"1",NULL,"2020-05-09 01:08:17",NULL),
("1037","
<p>On considère la fonction suivante :</p>
<pre><code class='python'>def f(T,i):
    indice = i
    m = T[i]
    for k in range(i+1, len(T)):
        if T[k] &lt; m:
            indice = k
            m = T[k]
    return indice</code></pre>
<p>Quelle est la valeur de f([ 7, 3, 1, 8, 19, 9, 3, 5 ], 0) ?</p>
","
<p>1</p>
","
<p>2</p>
","
<p>3</p>
","
<p>4</p>
","B","8","39",NULL,"1",NULL,"2020-05-09 01:10:38",NULL),
("1038","
<p>Quel est l’ordre de grandeur du coût du tri par insertion (dans le pire des cas) ?</p>
","
<p>l'ordre de grandeur du coût dépend de l'ordinateur utilisé</p>
","
<p>linéaire en la taille du tableau à trier</p>
","
<p>quadratique en la taille du tableau à trier</p>
","
<p>indépendant de la taille du tableau à trier</p>
","C","8","39",NULL,"1",NULL,"2020-05-09 01:11:32",NULL),
("1039","<br><p>À quelle catégorie appartient l’algorithme des k plus proches voisins ?</p><br>","<br>algorithmes de tri<br>","<br><p>algorithmes gloutons</p><br>","<br><p>algorithmes de recherche de chemins</p><br>","<br><p>algorithmes de classification et d’apprentissage</p><br>","D","8","39",NULL,"1",NULL,"2020-04-19 10:32:51",NULL),
("1040","
<p>À quelle catégorie appartient l’algorithme classique de rendu de monnaie ?</p>
","
<p>les algorithmes de classification et d'apprentissage</p>
","
<p>les algorithmes de tri</p>
","
<p>les algorithmes gloutons</p>
","
<p>les algorithmes de mariages stables</p>
","C","8","39",NULL,"1",NULL,"2020-05-09 01:13:25",NULL),
("1041","<br><p>Qu'est-ce qui permet de traduire un nom d'hôte en adresse IP ?</p><br>","<br><p>un serveur DNS</p><br>","<br><p>un serveur DHCP</p><br>","<br><p>un pare-feu</p><br>","<br><p>un hub</p><br>","A","2","7",NULL,"1",NULL,"2020-04-21 20:52:55",NULL),
("1042","
<p>Si A et B sont des variables booléennes, laquelle de ces expressions booléennes est équivalente à (not A) or B ?</p>
","
<p>(A and B) or (not A and B)</p>
","
<p>(A and B) or (not A and B) or (not A and not B)</p>
","
<p>(not A and B) or (not A and not B)</p>
","
<p>(A and B) or (not A and not B)</p>
","B","2","7",NULL,"1",NULL,"2020-04-27 22:40:15",NULL),
("1043","<br><br><p>Parmi les quatre propositions, quelle est celle qui correspond au résultat de la soustraction en écriture hexadécimale CD8FA&nbsp;-&nbsp;9FF81?</p><br>","<br><br><p>2E979</p><br>","<br><br><p>3D989</p><br>","<br><br><p>2D979</p><br>","<br><br><p>2DA979</p><br>","C","2","7",NULL,"1",NULL,"2020-03-25 16:04:38",NULL),
("1044","<br><br><p>Deux entiers positifs ont pour écriture en base 16 : A7 et 84.</p><br><p>Quelle est l'écriture en base 16 de leur somme ?</p><br>","<br><br><p>1811</p><br>","<br><br><p>12B</p><br>","<br><br><p>13A</p><br>","<br><br><p>A784</p><br>","B","2","7",NULL,"1",NULL,"2020-03-25 16:04:46",NULL),
("1045","<br><br><p>Quelle est l'écriture binaire du nombre entier 183 ?</p><br>","<br><br><p>0100 1000</p><br>","<br><br><p>1110 1101</p><br>","<br><br><p>1011 0111</p><br>","<br><br><p>1001 0101</p><br>","C","2","7",NULL,"1",NULL,"2020-03-25 16:04:53",NULL),
("1046","<br><br><p>Parmi les quatre expressions suivantes, laquelle s'évalue en True ?</p><br>","<br><br><p>False and (True and False)</p><br>","<br><br><p>False or (True and False)</p><br>","<br><br><p>True and (True and False)</p><br>","<br><br><p>True or (True and False)</p><br>","D","2","7",NULL,"1",NULL,"2020-03-25 16:05:01",NULL),
("1047","
<p>On a défini</p>
<pre><code class='python'>repertoire = [ {'nom': 'Francette', 'poste': 412},
{'nom': 'Jeanne', 'poste': 222},
{'nom': 'Éric', 'poste': 231} ]</code></pre>
<p>Quelle expression permet d'accéder au poste d'Éric ?</p>
","
<p>repertoire[2]['poste']</p>
","
<p>repertoire['poste'][2]</p>
","
<p>repertoire['Éric']['poste']</p>
","
<p>repertoire['Éric']</p>
","A","3","13",NULL,"1",NULL,"2020-04-27 23:41:47",NULL),
("1048","<br><br><p>Quelle est la valeur de l'expression suivante : [[i, i+1] for i in range(2)] ?</p><br>","<br><br><p>[[0,1],[1,2]]</p><br>","<br><br><p>[[1,2],[2,3]]</p><br>","<br><br><p>[0,1,1,2]</p><br>","<br><br><p>[1,2,2,3]</p><br>","A","3","13",NULL,"1",NULL,"2020-03-25 16:05:16",NULL),
("1049","<br><br><p>De quelle expression la liste suivante est-elle la valeur ?</p><br><p>[[0,0,0,0], [1,1,1,1], [2,2,2,2]]</p><br>","<br><br><p>[[i] * 4 for i in range(4)]</p><br>","<br><br><p>[[i] * 3 for i in range(4)]</p><br>","<br><br><p>[[i] * 4 for i in range(3)]</p><br>","<br><br><p>[[i] * 3 for i in range(3)]</p><br>","C","3","13",NULL,"1",NULL,"2020-03-25 16:05:24",NULL),
("1050","<p>On considère le code suivant :</p>
<pre><code class='python'>D= { 'a': '1', '2': 'a', 'b': 'a', 'c': '3'}</code></pre>
<p>Que vaut D['a'] à la fin de son exécution ?</p>
","<p>'1'</p>
","<p>2</p>
","<p>[ '2', 'b' ]</p>
","<p>[ '1', '3' ]</p>
","A","3","13",NULL,"1",NULL,"2020-05-09 01:20:14",NULL),
("1051","<br><p>On définit la variable suivante : citation = 'Les nombres gouvernent le monde'.</p><br><p>Quelle est la valeur de l'expression citation[5:10] ?</p><br>","<br><p>'ombre'</p><br>","<br><p>'ombres'</p><br>","<br><p>'nombre'</p><br>","<br><p>'nombres'</p><br>","A","3","13",NULL,"1",NULL,"2020-03-29 08:44:46",NULL),
("1052","<p>On exécute l'instruction suivante :</p>
<pre><code class='python'>T = [[12,13,14,15],
     [24,25,26,27],
     [35,36,49,33],
     [61,53,55,58]]</code></pre>
<p>Quelle expression parmi les quatre suivantes a pour valeur 26 ?</p>
","<pre><code class='python'>T[1][2]</code></pre>
","<pre><code class='python'>T[2][1]</code></pre>
","<pre><code class='python'>T[2][3]</code></pre>
","<pre><code class='python'>T[3][2]</code></pre>
","A","3","13",NULL,"1",NULL,"2020-05-12 08:48:12",NULL),
("1053","<br><p>On utilise habituellement un fichier d'extension csv pour quel type de données ?</p><br>","<br><p>des données structurées graphiquement</p><br>","<br><p>des données sonores</p><br>","<br><p>des données compressées</p><br>","<br><p>des données structurées en tableau</p><br>","D","4","18",NULL,"1",NULL,"2020-03-29 08:46:24",NULL),
("1054","<br><p>Parmi les extensions suivantes, laquelle caractérise un fichier contenant des données que l'on peut associer à un tableau de pixels ?</p><br>","<br><p>pdf</p><br>","<br><p>xls</p><br>","<br><p>png</p><br>","<br><p>exe</p><br>","C","4","18",NULL,"1",NULL,"2020-03-29 08:46:52",NULL),
("1055","<br><p>On définit ainsi une liste t puis une liste r :</p><br><pre><br>t = [{'id':1, 'age':23, 'sejour':'PEKIN'},<br>     {'id':2, 'age':27, 'sejour':'ISTANBUL'},<br>     {'id':3, 'age':53, 'sejour':'LONDRES'},<br>     {'id':4, 'age':41, 'sejour':'ISTANBUL'},<br>     {'id':5, 'age':62, 'sejour':'RIO'},<br>     {'id':6, 'age':28, 'sejour':'ALGER'}]<br>r = [ c for c in t if c['age']>30 and c['sejour']=='ISTANBUL' ]<br></pre><br><p>Combien la liste r contient-elle d'éléments ?</p><br>","<br><p>0</p><br>","<br><p>1</p><br>","<br><p>2</p><br>","<br><p>3</p><br>","B","4","18",NULL,"1",NULL,"2020-03-29 08:48:15",NULL),
("1056","<br><p>On a défini :</p><br><pre><br>mendeleiev = [['H','.', '.','.','.','.','.','He'],<br>              ['Li','Be','B','C','N','O','Fl','Ne'],<br>              ['Na','Mg','Al','Si','P','S','Cl','Ar'],<br>              ...... ]<br></pre><br><p>Comment construire la liste des gaz rares, c'est-à-dire la liste des éléments de la dernière colonne ?</p><br>","<br><p>gaz_rares = [ periode[7] for periode in mendeleiev]</p><br>","<br><p>gaz_rares = [ periode for periode in mendeleiev[7]]</p><br>","<br><p>gaz_rares = [ periode for periode[7] in mendeleiev]</p><br>","<br><p>gaz_rares = [ periode[8] for periode in mendeleiev]</p><br>","A","4","18",NULL,"1",NULL,"2020-03-29 08:49:28",NULL),
("1057","<br><p>On souhaite construire une table de 4 lignes de 3 éléments que l’on va remplir de 0. Quelle syntaxe Python utilisera-t-on ?</p><br>","<br><p>[ [ 0 ] * 3 for i in range (4) ]</p><br>","<br><p>for i in range (4) [ 0 ] * 3</p><br>","<br><p>[ 0 ] * 3 for i in range (4)</p><br>","<br><p>[ for i in range (4) [ 0 ] * 3 ]</p><br>","A","4","18",NULL,"1",NULL,"2020-03-29 08:49:58",NULL),
("1058","<br><p>Un fichier CSV …</p><br>","<br><p>ne peut être lu que par un tableur</p><br>","<br><p>est l'unique format utilisé pour construire une base de données</p><br>","<br><p>est un fichier texte</p><br>","<br><p>est un format propriétaire</p><br>","C","4","18",NULL,"1",NULL,"2020-03-29 08:50:28",NULL),
("1059","<br><p>Pour créer un lien vers la page d'accueil de Wikipédia, que devra-t-on écrire dans une page Web ?</p><br>","<br><p>&lt;a target='http://fr.wikipedia.org'&gt;Wikipédia&lt;/a&gt;</p><br>","<br><p>&lt;a href='http://fr.wikipedia.org' /&gt;</p><br>","<br><p>&lt;a href='http://fr.wikipedia.org'&gt;Wikipédia&lt;/a&gt;</p><br>","<br><p>&lt;link src='http://fr.wikipedia.org'&gt;Wikipédia&lt;/link&gt;</p><br>","C","5","22",NULL,"1",NULL,"2020-03-29 08:52:07",NULL),
("1060","<br><p>On souhaite qu’un menu apparaisse à chaque fois que l’utilisateur passe sa souris sur l’image de bannière du site. L’attribut de la balise img dans lequel on doit mettre un code Javascript à cet effet est :</p><br>","<br><p>onclick</p><br>","<br><p>src</p><br>","<br><p>alt</p><br>","<br><p>onmouseover</p><br>","D","5","22",NULL,"1",NULL,"2020-03-29 08:53:16",NULL),
("1061","<br><p>On considère le formulaire ci-dessous :</p><br><p>Quel est votre langage préféré ?<br><br>Python □ Java □ Php □</p><br><p>Quelle balise parmi les quatre suivantes a été utilisée pour les cases à cocher ?</p><br>","<br><p>&lt;input type='radio'&gt;</p><br>","<br><p>&lt;input type='circle'&gt;</p><br>","<br><p>&lt;input type='checkbox'&gt;</p><br>","<br><p>&lt;input type='square'&gt;</p><br>","C","5","22",NULL,"1",NULL,"2020-03-29 08:58:37",NULL),
("1062","<p>Une page Web contient un formulaire dont le code HTML est le suivant :</p>
<pre>
&lt;form action='/action_page.php' method='post'&gt;
    First name : &lt;input type='text' name = 'fname'&gt;
    Last name : &lt;input type='text' name = 'lname'&gt;
    &lt;input type='submit' value='Submit'&gt;
&lt;/form&gt;
</pre>
<p>Que peut-on dire des informations transmises dans ce formulaire ?</p>
","<p>elles seront enregistrées dans l’historique du navigateur</p>
","<p>elles seront enregistrées dans le cache du navigateur</p>
","<p>elles ne devront pas dépasser une limite en nombre de caractères transmis</p>
","<p>elles ne seront pas visibles dans la barre d'adresse du navigateur</p>
","D","5","22",NULL,"1",NULL,"2020-04-27 23:23:23",NULL),
("1063","<br><p>Parmi les quatre propositions suivantes, laquelle est la seule à correspondre à un entête correct de formulaire d'une page HTML ?</p><br>","<br><p>&lt;form method='formulaire.php' action='submit'&gt;</p><br>","<br><p>&lt;form method='post' action=onclick()&gt;</p><br>","<br><p>&lt;form method='get' action='arret.php'&gt;</p><br>","<br><p>&lt;form method='post' action=arret.php&gt;</p><br>","C","5","22",NULL,"1",NULL,"2020-03-29 09:01:33",NULL),
("1064","<br><p>Quelle méthode est utilisée via une requête HTTP pour envoyer une image via un formulaire HTML ?</p><br>","<br><p>HEAD</p><br>","<br><p>PUT</p><br>","<br><p>POST</p><br>","<br><p>GET</p><br>","C","5","22",NULL,"1",NULL,"2020-03-29 09:02:01",NULL),
("1065","<br><p>Un protocole est un ensemble de …</p><br>","<br><p>matériels connectés entre eux</p><br>","<br><p>serveurs et de clients connectés entre eux</p><br>","<br><p>règles qui régissent les échanges entre équipements informatiques</p><br>","<br><p>règles qui régissent les échanges entre un système d’exploitation et les applications</p><br>","C","6","27",NULL,"1",NULL,"2020-03-29 09:02:27",NULL),
("1066","<br><p>À partir du répertoire ~/Perso/Doc quelle commande permet de rejoindre le répertoire ~/Public ?</p><br>","<br><p>cd ./Public</p><br>","<br><p>cd ../Public</p><br>","<br><p>cd ././Public</p><br>","<br><p>cd ../../Public</p><br>","D","6","27",NULL,"1",NULL,"2020-03-29 09:02:56",NULL),
("1067","<br><p>On réalise une petite station météo.</p><br><p>Quel composant est un capteur ?</p><br>","<br><p>l'afficheur LCD</p><br>","<br><p>l'écran de l'ordinateur</p><br>","<br><p>la LED</p><br>","<br><p>le thermomètre</p><br>","D","6","27",NULL,"1",NULL,"2020-03-29 09:03:21",NULL),
("1068","<br><p>Dans la console Linux, étant positionné dans le répertoire /home/marcelH/travail, quelle commande faut-il exécuter pour remonter dans l'arborescence vers le répertoire /home/marcelH ?</p><br>","<br><p>cd .</p><br>","<br><p>cd ..</p><br>","<br><p>cd ...</p><br>","<br><p>cd /../.</p><br>","B","6","27",NULL,"1",NULL,"2020-03-29 09:03:50",NULL),
("1069","<p>Pour analyser les réponses saisies par l'utilisateur dans un formulaire d’une page Web personnelle, hébergée chez un fournisseur d'accès à internet, on dispose du code suivant :</p>
<pre>
&lt;?php if ($_POST['choix']=='choix4')
    {echo 'Bravo,';}
        else
    {echo 'Non, vous vous trompez !';}
?&gt;
</pre>
<p>Où s’exécutera ce code ?</p>
","<p>dans le premier routeur permettant d'accéder au serveur</p>
","<p>dans le dernier routeur permettant d'accéder au serveur</p>
","<p>dans le serveur qui héberge la page personnelle</p>
","<p>dans la machine de l'utilisateur qui consulte la page personnelle</p>
","C","6","27",NULL,"1",NULL,"2020-04-27 22:59:02",NULL),
("1070","<br><p>Dans un shell sous Linux, Alice utilise la commande pwd.</p><br><p>Cette commande :</p><br>","<br><p>liste les fichiers du répertoire courant</p><br>","<br><p>liste les répertoires du répertoire courant</p><br>","<br><p>affiche le chemin du répertoire courant</p><br>","<br><p>affiche les permissions relatives au répertoire courant</p><br>","C","6","27",NULL,"1",NULL,"2020-03-29 09:06:47",NULL),
("1071","<br><p>On souhaite écrire une fonction qui renvoie le maximum d'une liste d'entiers :</p><br><pre><br>def maximum(L):<br>    m = L[0]<br>    for i in range(1,len(L)):<br>        if .........:<br>            m = L[i]<br>    return m<br></pre><br><p>Par quoi faut-il remplacer les pointillés pour que cette fonction produise bien le résultat attendu ?</p><br>","<br><p>i > m</p><br>","<br><p>L[i] > m</p><br>","<br><p>L[i] > L[i-1]</p><br>","<br><p>L[i] > L[i+1]</p><br>","B","7","33",NULL,"1",NULL,"2020-03-29 09:08:09",NULL),
("1072","<br><p>On exécute le script suivant :</p><br><pre><br>def calcul(a,b):<br>    a = a + 2<br>    b = b + 5<br>    c = a + b<br>    return c<br><br>a,b = 3,5<br>calcul(a,b)<br></pre><br><p>À la fin de cette exécution :</p><br>","<br><p>a vaut 3, b vaut 5 et c vaut 15</p><br>","<br><p>a vaut 3, b vaut 5 et c n'est pas défini</p><br>","<br><p>a vaut 5, b vaut 10 et c vaut 15</p><br>","<br><p>a vaut 5, b vaut 10 et c n'est pas défini</p><br>","B","7","33",NULL,"1",NULL,"2020-03-29 09:09:23",NULL),
("1073","<br><p>On exécute le code suivant :</p><br><pre><br>def essai():<br>    a = 2<br>    b = 3<br>    c = 4<br>    return a<br>    return b<br>    return c<br><br>t = essai()<br></pre><br><p>Quelle est la valeur de t après l'exécution de ce code ?</p><br>","<br><p>2</p><br>","<br><p>3</p><br>","<br><p>4</p><br>","<br><p>(2,3,4)</p><br>","A","7","33",NULL,"1",NULL,"2020-03-29 09:10:50",NULL),
("1074","<br><p>On exécute le code suivant :</p><br><pre><br>def f(t):<br>    n = len(t)<br>    for i in range(n-1):<br>        if t[i] > t[i+1]:<br>            t[i],t[i+1] = t[i+1],t[i]<br><br>L = [4, 8, -7, 0, 1]<br>f(L)<br></pre><br><p>Quelle est la valeur de L après l'exécution de ce code ?</p><br>","<br><p>[4, -7, 8, 0, 1]</p><br>","<br><p>[-7, 0, 1, 4, 8]</p><br>","<br><p>[4, 8, -7, 0, 1]</p><br>","<br><p>[4, -7, 0, 1, 8]</p><br>","D","7","33",NULL,"1",NULL,"2020-03-29 09:11:57",NULL),
("1075","<br><p>La fonction suivante calcule la racine carrée du double d’un nombre flottant.</p><br><pre><br>from math import sqrt<br>def racine_du_double(x):<br>    return sqrt(2*x)<br></pre><br><p>Quelle est la précondition sur l'argument de cette fonction ?</p><br>","<br><p>x < 0</p><br>","<br><p>x >= 0</p><br>","<br><p>2 * x > 0</p><br>","<br><p>sqrt(x) >= 0</p><br>","B","7","33",NULL,"1",NULL,"2020-03-29 09:12:45",NULL),
("1076","<p>Soit T un tableau de flottants, a et b deux entiers. On considère une fonction nommée 'somme' renvoyant la somme des éléments du tableau d'indices compris entre a et b définie par :</p>
<pre><code class='python'>def somme(T, a, b):
    S = 0
    for i in range(a, b+1) :
        S = S + T[i]
    return S
</code></pre>
<p>Quel ensemble de préconditions doit-on prévoir pour cette fonction ?</p>
","<p>a &lt; b</p>
","<p>a &lt; longueur(T) et b &lt; longueur(T)</p>
","<p>a &lt;= b &lt; longueur(T)</p>
","<p>a &lt;= b &lt; longueur(T) et T est un tableau trié</p>
","C","7","33",NULL,"1",NULL,"2020-05-12 14:27:07",NULL),
("1077","<br><p>Un algorithme de tri d’une liste d’entiers est implémenté de la façon suivante :</p><br><pre><br>def trier(L) :<br>    for i in range(len(L)):<br>        indice_min = i<br>        for j in range(i+1, len(L)):<br>            if L[j] < L[indice_min] :<br>                indice_min = j<br>            L[i], L[indice_min] = L[indice_min], L[i]<br>    return L<br></pre><br><p>Quelle est l'affirmation exacte ?</p><br>","<br><p>cet algorithme est celui du tri par sélection et il a un coût linéaire en la taille de la liste à trier</p><br>","<br><p>cet algorithme est celui du tri par insertion et il a un coût linéaire en la taille de la liste à trier</p><br>","<br><p>cet algorithme est celui du tri par sélection et il a un coût quadratique en la taille de la liste à trier</p><br>","<br><p>cet algorithme est celui du tri par insertion et il a un coût quadratique en la taille de la liste à trier</p><br>","C","8","39",NULL,"1",NULL,"2020-03-29 09:16:20",NULL),
("1078","<br><p>On considère la fonction suivante :</p><br><pre><br>def comptage(phrase,lettre):<br>    i = 0<br>    for j in phrase:<br>        if j == lettre:<br>            i = i+1<br>    return i<br></pre><br><p>Que renvoie l'appel comptage('Vive l’informatique','e') ?</p><br>","<br><p>0</p><br>","<br><p>2</p><br>","<br><p>19</p><br>","<br><p>'e'</p><br>","B","8","39",NULL,"1",NULL,"2020-03-29 09:21:07",NULL),
("1079","<p>Quel code parmi les quatre proposés ci-dessous s'exécute-t-il en un temps linéaire en \(n\) (c'est-à-dire avec un temps d'exécution majoré par \(A \times n + B\) où \(A\) et \(B\) sont deux constantes) ?</p><br>","<pre><br>for i in range(n//2):<br>    for j in range(i+1,n):<br>        print('hello')<br></pre><br>","<pre><br>for i in range(n):<br>    print('hello')<br></pre><br>","<pre><br>L = [ i+j for i in range(n) for j in range(n) ]<br>for x in L:<br>    print('hello')<br></pre><br>","<pre><br>for i in range(n//2):<br>    for j in range(n//2):<br>        print('hello')<br></pre><br>","B","8","39",NULL,"1",NULL,"2020-03-29 15:46:23",NULL),
("1080","<br><p>Quelle précondition suppose l'algorithme de recherche dichotomique dans un tableau ?</p><br>","<br><p>que le tableau soit à éléments positifs</p><br>","<br><p>que le tableau soit trié</p><br>","<br><p>que l'élément cherché dans le tableau soit positif</p><br>","<br><p>que l'élément cherché figure effectivement dans le tableau</p><br>","B","8","39",NULL,"1",NULL,"2020-03-29 09:19:57",NULL),
("1081","<br><p>Soit <span class='math inline'>\(T\)</span> le temps nécessaire pour trier, à l'aide de l'algorithme du tri par insertion, une liste de 1000 nombres entiers. Quel est l'ordre de grandeur du temps nécessaire, avec le même algorithme, pour trier une liste de 10 000 entiers, c'est-à-dire une liste dix fois plus grande ?</p><br>","<br><p>à peu près le même temps <span class='math inline'>\(T\)</span></p><br>","<br><p>environ <span class='math inline'>\(10 \times T\)</span></p><br>","<br><p>environ <span class='math inline'>\(100 \times T\)</span></p><br>","<br><p>environ <span class='math inline'>\(T^{2}\)</span></p><br>","C","8","39",NULL,"1",NULL,"2020-03-29 09:21:57",NULL),
("1082","<br><p>On considère la fonction suivante :</p><br><pre><br>def trouverLettre(phrase,lettre):<br>    indexResultat = 0<br>    for i in range(len(phrase)):<br>        if phrase[i]== lettre:<br>            indexResultat=i<br>    return indexResultat<br></pre><br><p>Que renvoie l'appel trouverLettre('Vive l’informatique','e') ?</p><br>","<br><p>3</p><br>","<br><p>4</p><br>","<br><p>18</p><br>","<br><p>'e'</p><br>","C","8","39",NULL,"1",NULL,"2020-03-29 09:25:15",NULL),
("1083","<br><br><p>En ajoutant trois chiffres 0 à droite de l'écriture binaire d'un entier <span class='math inline'>\(N\)</span> strictement positif, on obtient l'écriture binaire de :</p><br>","<br><br><p><span class='math inline'>\(6 \times N\)</span></p><br>","<br><br><p><span class='math inline'>\(8 \times N\)</span></p><br>","<br><br><p><span class='math inline'>\(1000 \times N\)</span></p><br>","<br><br><p>aucune des réponses précédentes</p><br>","B","2","7",NULL,"1",NULL,"2020-03-25 16:10:53",NULL),
("1084","<br><br><p>À quoi sert le codage en complément à 2&nbsp;?</p><br>","<br><br><p>à inverser un nombre binaire</p><br>","<br><br><p>à coder des nombres entiers négatifs en binaire</p><br>","<br><br><p>à convertir un nombre en hexadécimal</p><br>","<br><br><p>à multiplier par 2 un nombre en binaire</p><br>","B","2","7",NULL,"1",NULL,"2020-03-25 16:11:01",NULL),
("1085","<br><br><p>L'écriture décimale du nombre 1001 1101 écrit sur 8 bits en complément à 2 est&nbsp;:</p><br>","<br><br><p>–4</p><br>","<br><br><p>–29</p><br>","<br><br><p>–99</p><br>","<br><br><p>157</p><br>","C","2","7",NULL,"1",NULL,"2020-03-25 16:11:09",NULL),
("1086","<br><br><p>Quel est le nombre entier positif dont la représentation binaire est 0010 0011 ?</p><br>","<br><br><p>19</p><br>","<br><br><p>33</p><br>","<br><br><p>35</p><br>","<br><br><p>64</p><br>","C","2","7",NULL,"1",NULL,"2020-03-25 16:11:17",NULL),
("1087","<br><br><p>Quelle est l'écriture décimale du nombre qui s'écrit 11,0101 en binaire ?</p><br>","<br><br><p>3</p><br>","<br><br><p>3,0101</p><br>","<br><br><p>3,05</p><br>","<br><br><p>3,3125</p><br>","D","2","7",NULL,"1",NULL,"2020-03-25 16:11:25",NULL),
("1088","<br><br><p>Quel est le résultat de l'addition binaire 0010&nbsp;0110 + 1000&nbsp;1110 ?</p><br>","<br><br><p>1010 1110</p><br>","<br><br><p>0000 0110</p><br>","<br><br><p>1011 0100</p><br>","<br><br><p>0101 0001</p><br>","C","2","7",NULL,"1",NULL,"2020-03-25 16:11:32",NULL),
("1089","
<p>On définit <code class='python'>L = [[1,2,3,4,5],[6,7,8,9,10],[11,12,13,14,15]]</code>. Quelle est la valeur de L[0][2] ?</p>
","
<p>2</p>
","
<p>3</p>
","
<p>11</p>
","
<p>12</p>
","B","3","13",NULL,"1",NULL,"2020-04-28 18:44:25",NULL),
("1090","
<p>Quel est le résultat de l'évaluation de l'expression Python suivante ?<br />
[ n * n for n in range(10) ]</p>
","
<p>[0, 1, 4, 9, 16, 25, 36, 49, 64, 81]</p>
","
<p>[0, 1, 4, 9, 16, 25, 36, 49, 64, 81, 100]</p>
","
<p>[0, 2, 4, 8, 16, 32, 64, 128, 256, 512]</p>
","
<p>[0, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024]</p>
","A","3","13",NULL,"1",NULL,"2020-04-28 18:40:19",NULL),
("1091","<p>On définit :</p>
<pre><code class='python'>tableau = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]</code></pre>
<p>Quelle est la valeur de <code class='python'>tableau[2][1]</code> ?</p>
","2
","4
","6
","8
","D","3","13",NULL,"1",NULL,"2020-05-12 08:52:36",NULL),
("1092","<br><br><p>Quelle est l'expression qui a pour valeur la liste [1,4,9,16,25,36] ?</p><br>","<br><br><p>{ n*n for n in range(1,7) }</p><br>","<br><br><p>{ n*n for n in range(6) }</p><br>","<br><br><p>[ n*n for n in range(1,7) ]</p><br>","<br><br><p>[ n*n for n in range(6) ]</p><br>","C","3","13",NULL,"1",NULL,"2020-03-25 16:12:04",NULL),
("1093","<p>On définit une grille G remplie de 0, sous la forme d'une liste de listes, où toutes les sous-listes ont le même nombre d'éléments.</p>
<pre><code class='python'>G = [ [0, 0, 0, …, 0],
      [0, 0, 0, …, 0],
      [0, 0, 0, …, 0],
      ……
      [0, 0, 0, …, 0] ]</code></pre>
<p>On appelle <em>hauteur</em> de la grille le nombre de sous-listes contenues dans G et <em>largeur</em> de la grille le nombre d'éléments dans chacune de ces sous-listes. Comment peut-on les obtenir ?</p>
","<p>hauteur = len(G[0])<br />
largeur = len(G)</p>
","<p>hauteur = len(G)<br />
largeur = len(G[0])</p>
","<p>hauteur = len(G[0])<br />
largeur = len(G[1])</p>
","<p>hauteur = len(G[1])<br />
largeur = len(G[0])</p>
","B","3","13",NULL,"1",NULL,"2020-05-09 01:26:31",NULL),
("1094","<p>On définit :
<code class='python'>L = [ ['lundi',10,0.87], ['mardi',11,0.82], ['mercredi',12,0.91] ]</code></p>
<p>Quel est le type de la variable <code class='python'>a</code> définie par <code class='python'>a = L[1][2]</code> ?</p>
","<p>nombre entier</p>
","<p>liste</p>
","<p>nombre flottant</p>
","<p>chaîne de caractères</p>
","C","3","13",NULL,"1",NULL,"2020-05-04 23:52:10",NULL),
("1095","<br><br><p>Laquelle de ces listes de chaînes de caractères est triée en ordre croissant ?</p><br>","<br><br><p>['112', '19', '27', '45', '8']</p><br>","<br><br><p>['8', '19', '27', '45', '112']</p><br>","<br><br><p>['8', '112', '19', '27', '45']</p><br>","<br><br><p>['19', '112', '27', '45', '8']</p><br>","A","4","18",NULL,"1",NULL,"2020-03-25 16:12:30",NULL),
("1096","<p>On exécute le code suivant :</p>
<pre><code class='python'>def maxi(t):
    m = t[0]
    for x in t:
        if x[1] &gt;= m[1]:
            m = x
    return m

L = [ ('Alice', 17), ('Barnabé', 17),('Casimir', 17), ('Doriane', 17), ('Emilien', 14), ('Fabienne', 16) ]</code></pre>
<p>Quelle est alors la valeur de maxi(L) ?</p>
","<p>('Alice',17)</p>
","<p>('Doriane',17)</p>
","<p>('Fabienne',17)</p>
","<p>('Emilien',14)</p>
","B","4","18",NULL,"1",NULL,"2020-04-28 18:52:25",NULL),
("1097","
<p>Soit la table de données suivante :</p>
<pre><code class='python'>nom,prenom,date_naissance
Dupont,Pierre,17/05/1987
Dupond,Catherine,18/07/1981
Haddock,Archibald,23/04/1998</code></pre>
<p>Quels sont les descripteurs de ce tableau ?</p>
","
<p>nom, prenom et date_naissance</p>
","
<p>Dupont, Pierre et 17/05/1987</p>
","
<p>Dupont, Dupond et Haddock</p>
","
<p>il n'y en a pas</p>
","A","4","18",NULL,"1",NULL,"2020-04-28 18:48:35",NULL),
("1098","<p>On exécute le code suivant :</p>
<pre><code class='python'>collection = [('Renault', '4L', 1974, 30),
('Peugeot', '504', 1970, 82),
('Citroën', 'Traction', 1950, 77)]</code></pre>
<p>Que vaut collection[1][2] ?</p>
","<p>1970</p>
","<p>'4L'</p>
","<p>('Peugeot', '504', 1970, 82)</p>
","<p>('Renault', '4L', 1974, 30)</p>
","A","4","18",NULL,"1",NULL,"2020-05-05 00:13:41",NULL),
("1099","<br><br><p>Laquelle de ces listes de chaînes de caractères est triée en ordre croissant ?</p><br>","<br><br><p>['8', '12', '142', '21']</p><br>","<br><br><p>['8', '12', '21', '142']</p><br>","<br><br><p>['12', '142', '21', '8']</p><br>","<br><br><p>['12', '21', '8', '142']</p><br>","C","4","18",NULL,"1",NULL,"2020-03-25 16:13:05",NULL),
("1100","<br><br><p>Dans la plupart des fichiers CSV, que contient la première ligne ?</p><br>","<br><br><p>des notes concernant la table de données</p><br>","<br><br><p>les sources des données</p><br>","<br><br><p>les descripteurs des champs de la table de données</p><br>","<br><br><p>l'auteur de la table de données</p><br>","C","4","18",NULL,"1",NULL,"2020-03-25 16:13:14",NULL),
("1101","
<p>Lors de la consultation d’une page HTML contenant un bouton auquel est associée la fonction suivante, que se passe-t-il quand on clique sur ce bouton ?</p>
<pre><code class='html'>function action(event) {
  this.style.color = 'blue'
}</code></pre>
","
<p>le texte de la page passe en bleu</p>
","
<p>le texte du bouton passe en bleu</p>
","
<p>le texte du bouton est changé et affiche maintenant le mot 'bleu'</p>
","
<p>le pointeur de la souris devient bleu quand il arrive sur le bouton</p>
","B","5","22",NULL,"1",NULL,"2020-05-04 23:47:21",NULL),
("1102","
<p>Le site internet d'un quotidien d'information permet aux visiteurs de laisser des commentaires textuels.<br />
Ces commentaires doivent être visibles par les autres visiteurs.</p>
<p>Laquelle des affirmations suivantes est correcte ?</p>
","
<p>Il suffit que la page HTML contienne des champs de la forme &lt;textarea &gt;</p>
","
<p>Il suffit que la page HTML contienne des champs de la forme &lt;textarea &gt; et d'utiliser JavaScript pour enregistrer les commentaires</p>
","
<p>Il faut un programme en PHP ou un script Python sur le serveur pour traiter les données</p>
","
<p>Non, ce n'est pas possible avec la technologie actuelle</p>
","C","5","22",NULL,"1",NULL,"2020-05-04 23:53:34",NULL),
("1103","
<p>Quel est le nom de l’événement généré lorsque l’utilisateur clique sur un bouton de type button dans une page HTML ?</p>
","
<p>action</p>
","
<p>mouse</p>
","
<p>submit</p>
","
<p>onclick</p>
","D","5","22",NULL,"1",NULL,"2020-04-28 18:59:24",NULL),
("1104","<br><br><p>Dans une page HTML, lequel de ces codes permet la présence d'un bouton qui appelle la fonction javascript afficher_reponse() lorsque l'utilisateur clique dessus ?</p><br>","<br><br><p>&lt;a href='afficher_reponse()'&gt;Cliquez ici&lt;/a&gt;</p><br>","<br><br><p>&lt;button if_clicked='afficher_reponse()'&gt;Cliquez ici&lt;/button&gt;</p><br>","<br><br><p>&lt;button value='Cliquez ici'&gt;&lt;a&gt; afficher_reponse()&lt;/a&gt;&lt;/button&gt;</p><br>","<br><br><p>&lt;button onclick='afficher_reponse()'&gt;Cliquez ici&lt;/button&gt;</p><br>","D","5","22",NULL,"1",NULL,"2020-03-25 16:13:50",NULL),
("1105","
<p>Lorsque la méthode POST est associée à un formulaire au sein d’une page HTML, comment les réponses du formulaire sont-elles envoyées au serveur ?</p>
","
<p>Elles sont visibles dans l’URL</p>
","
<p>Elles ne sont pas visibles dans l’URL</p>
","
<p>Elles sont transmises via un service postal spécifique</p>
","
<p>Elles sont découpées en plusieurs petites URL limitées à 4 mots</p>
","B","5","22",NULL,"1",NULL,"2020-04-28 18:54:32",NULL),
("1106","<br><br><p>Parmi les quatre propositions suivantes, laquelle est la seule à correspondre à un entête correct de formulaire d'une page HTML ?</p><br>","<br><br><p>&lt;form method='formulaire.php' action='submit'&gt;</p><br>","<br><br><p>&lt;form method='post' action=onclick()&gt;</p><br>","<br><br><p>&lt;form method='get' action='arret.php'&gt;</p><br>","<br><br><p>&lt;form method='post' action=arret.php&gt;</p><br>","C","5","22",NULL,"1",NULL,"2020-03-25 16:14:07",NULL),
("1107","<br><br><p>Quelle est la racine du système de fichier de Linux ?</p><br>","<br><br><p>/</p><br>","<br><br><p>root</p><br>","<br><br><p>sudo</p><br>","<br><br><p>home</p><br>","A","6","27",NULL,"1",NULL,"2020-03-25 16:14:15",NULL),
("1108","<br><br><p>Parmi les systèmes d’exploitation suivants, lequel est libre ?</p><br>","<br><br><p>Mac OS</p><br>","<br><br><p>iOS</p><br>","<br><br><p>Microsoft Windows</p><br>","<br><br><p>GNU/Linux</p><br>","D","6","27",NULL,"1",NULL,"2020-03-25 16:14:23",NULL),
("1109","<br><br><p>Quelle est l’utilité de la commande ping dans un réseau informatique ?</p><br>","<br><br><p>établir un réseau privé virtuel</p><br>","<br><br><p>tester si la connexion peut être établie avec une machine distante</p><br>","<br><br><p>obtenir la route suivie par un paquet dans le réseau</p><br>","<br><br><p>mesurer les performances d'une machine distante</p><br>","B","6","27",NULL,"1",NULL,"2020-03-25 16:14:32",NULL),
("1110","<br><br><p>Comment s'appelle l'ensemble des règles qui régissent les échanges sur Internet ?</p><br>","<br><br><p>les couches</p><br>","<br><br><p>le wifi</p><br>","<br><br><p>les protocoles</p><br>","<br><br><p>les commutateurs</p><br>","C","6","27",NULL,"1",NULL,"2020-03-25 16:14:40",NULL),
("1111","<br><br><p>Sous Linux, on se place dans un répertoire appelé documents. Dans quel répertoire se trouve-t-on après avoir exécuté la commande cd ../images ?</p><br>","<br><br><p>dans un répertoire images, qui est un sous-répertoire du répertoire documents</p><br>","<br><br><p>dans un répertoire images, qui est à la racine du système de fichiers</p><br>","<br><br><p>dans un répertoire images, qui est dans l'arborescence de fichiers à la même hauteur que le répertoire documents</p><br>","<br><br><p>dans un répertoire images, qui est sur le CD-ROM du lecteur de l'ordinateur</p><br>","C","6","27",NULL,"1",NULL,"2020-03-25 16:14:50",NULL),
("1112","
<p>Quel est l’effet de la commande shell suivante : <code>cp NSI_ex1_Franck.txt NSI_ex1_Marie.txt</code></p>
","
<p>Le fichier NSI_ex1_Franck.txt est copié sous le nom NSI_ex1_Marie.txt</p>
","
<p>Le fichier NSI_ex1_Franck.txt est renommé sous le nom NSI_ex1_Marie.txt</p>
","
<p>Le fichier NSI_ex1_Marie.txt est copié sous le nom NSI_ex1_Franck.txt</p>
","
<p>Le fichier NSI_ex1_Marie.txt est renommé sous le nom NSI_ex1_Franck.txt</p>
","A","6","27",NULL,"1",NULL,"2020-05-04 23:56:30",NULL),
("1113","
<p>On exécute le code suivant :</p>
<pre><code class='python'>def essai():
    a = 2
    b = 3
    c = 4
    return a
    return b
    return c

t = essai()</code></pre>
<p>Quelle est la valeur de t après l'exécution de ce code ?</p>
","
<p>2</p>
","
<p>3</p>
","
<p>4</p>
","
<p>(2,3,4)</p>
","A","7","33",NULL,"1",NULL,"2020-04-28 19:19:26",NULL),
("1114","
<p>On considère la fonction suivante :</p>
<pre><code class='python'>def comparaison(a,b):
if a &lt; b:
    return a
else:
    return b</code></pre><p>Quel est le type de la valeur renvoyée par l'appel comparaison(6,5) ?</p>
","
<p>un booléen (vrai/faux)</p>
","
<p>un nombre entier</p>
","
<p>un nombre flottant</p>
","
<p>une chaîne de caractères</p>
","B","7","33",NULL,"1",NULL,"2020-04-28 19:22:59",NULL),
("1115","
<p>Quelle est la valeur de la variable n à la fin de l'exécution du script ci-dessous ?</p>
<pre><code class='python'>n = 1
for i in range(4):
    n = n + 2</code></pre>
","
<p>1</p>
","
<p>8</p>
","
<p>9</p>
","
<p>18</p>
","C","7","33",NULL,"1",NULL,"2020-04-28 19:21:12",NULL),
("1116","<p>On construit une liste L de la façon suivante :</p>
<pre><code class='python'>L = []
for i in range(1,11,2):
    L.append(5*i)</code></pre>
<p>et on obtient ainsi la liste [5, 15, 25, 35, 45].</p>
<p>On pourrait aussi procéder de la façon suivante :</p>
<pre><code class='python'>L = []
# ligne 1 .......
while i &lt; 11:
    L.append(5*i)
    # ligne 2 .......</code></pre>
<p>Que faudrait-il écrire en ligne 1 et en ligne 2 pour obtenir le même résultat ?</p>
","
<p>i = 0 en ligne 1, et i = i + 1 en ligne 2</p>
","
<p>i = 0 en ligne 1, et i = i + 2 en ligne 2</p>
","
<p>i = 1 en ligne 1, et i = i + 1 en ligne 2</p>
","
<p>i = 1 en ligne 1, et i = i + 2 en ligne 2</p>
","D","7","33",NULL,"1",NULL,"2020-04-28 19:08:35",NULL),
("1117","
<p>La fonction suivante calcule la racine carrée du double d’un nombre flottant.</p>
<pre><code class='python'>from math import sqrt
def racine_du_double(x):
    return sqrt(2*x)</code></pre>
<p>Quelle est la précondition sur l'argument de cette fonction ?</p>
","
<p>x &lt; 0</p>
","
<p>x &gt;= 0</p>
","
<p>2 * x &gt; 0</p>
","
<p>sqrt(x) &gt;= 0</p>
","B","7","33",NULL,"1",NULL,"2020-04-28 19:17:04",NULL),
("1118","
<p>On définit une fonction f de la façon suivante :</p>
<pre><code class='python'>def f(L,m):
    R = []
    for i in range(len(L)):
        if L[i] &gt; m:
            R.append(L[i])
    return R</code></pre>
<p>On définit <code class='python'>L = [1, 7, 3, 4, 8, 2, 0, 3, 5]</code>.</p>
<p>Que vaut <code class='python'>f(L,4)</code> ?</p>","
<p>[0, 7, 0, 0, 8, 0, 0, 0, 5]</p>
","
<p>[0, 0, 0, 5]</p>
","
<p>[7, 8, 5]</p>
","
<p>[]</p>
","C","7","33",NULL,"1",NULL,"2020-04-28 19:15:22",NULL),
("1119","
<p>La fonction ci-dessous permet d’effectuer une recherche par dichotomie de l’index m de l’élément x dans un tableau L de valeurs distinctes et triées.</p>
<pre><code class='python'>def dicho(x,L):
    g = 0
    d = len(L)-1
    while g &lt;= d:
        m = (g+d)//2
        if L[m] == x:
            return m
        elif L[m] &lt; x:
            g = m+1
        else:
            d = m-1
    return None</code></pre>
<p>Combien de fois la cinquième ligne du code de la fonction (<code class='python'>m = (g+d)//2</code>) sera-t-elle exécutée dans l'appel <code class='python'>dicho(32, [4, 5, 7, 25, 32, 50, 51, 60])</code> ?</p>
","
<p>1 fois</p>
","
<p>2 fois</p>
","
<p>3 fois</p>
","
<p>4 fois</p>
","C","8","39",NULL,"1",NULL,"2020-04-28 19:43:06",NULL),
("1120","
<p>On définit la fonction f comme suit :</p>
<pre><code class='python'>def f(L):
    a = L[0]
    for x in L:
        if x &lt; a:
            a = x
    return a</code></pre>
<p>Quelle est la valeur renvoyée par l'appel <code class='python'>f([7, 10.3, -4, 12 ,7 ,2, 0.7, -5, 14, 1.4])</code> ?</p>
","
<p>-5</p>
","
<p>1.4</p>
","
<p>7</p>
","
<p>14</p>
","A","8","39",NULL,"1",NULL,"2020-04-28 19:46:12",NULL),
("1121","<br><br><p>À quelle catégorie appartient l’algorithme des k plus proches voisins ?</p><br>","<br><br><p>algorithmes de tri</p><br>","<br><br><p>algorithmes gloutons</p><br>","<br><br><p>algorithmes de recherche de chemins</p><br>","<br><br><p>algorithmes de classification et d’apprentissage</p><br>","D","8","39",NULL,"1",NULL,"2020-03-25 16:16:18",NULL),
("1122","<br><br><p>Soit <span class='math inline'>\(T\)</span> le temps nécessaire pour trier, à l'aide de l'algorithme du tri par insertion, une liste de 1000 nombres entiers. Quel est l'ordre de grandeur du temps nécessaire, avec le même algorithme, pour trier une liste de 10&nbsp;000 entiers, c'est-à-dire une liste dix fois plus grande ?</p><br>","<br><br><p>à peu près le même temps <span class='math inline'>\(T\)</span></p><br>","<br><br><p>environ <span class='math inline'>\(10 \times T\)</span></p><br>","<br><br><p>environ <span class='math inline'>\(100 \times T\)</span></p><br>","<br><br><p>environ <span class='math inline'>\(T^{2}\)</span></p><br>","C","8","39",NULL,"1",NULL,"2020-03-25 16:16:28",NULL),
("1123","
<p>On exécute le code suivant :</p>
<pre><code class='python'>tab = [1, 4, 3, 8, 2]
S = 0
for i in range(len(tab)):
    S = S + tab[i]</code></pre>
<p>Que vaut la variable S à la fin de l'exécution ?</p>
","
<p>1</p>
","
<p>8</p>
","
<p>18</p>
","
<p>3.6</p>
","C","8","39",NULL,"1",NULL,"2020-04-28 19:24:48",NULL),
("1124","
<p>Un algorithme de calcul de moyenne est implémenté de la façon suivante :</p>
<pre><code class='python'>def moyenne(liste) :
    t = 0
    for e in liste :
        t = t + e
        # assertion vraie à cet endroit
    return t/len(liste)</code></pre>
<p>Parmi les propositions suivantes, laquelle reste vraie à la fin de chaque itération de la boucle ?</p>
","
<p>e vaut le nombre de passages dans la boucle</p>
","
<p>t vaut la somme des éléments visités de la liste</p>
","
<p>t vaut la moyenne des éléments visités de la liste</p>
","
<p>après k passages dans la boucle la liste contient k termes</p>
","B","8","39",NULL,"1",NULL,"2020-04-28 19:34:13",NULL),
("1125","<p>Si a vaut False et b vaut True, que vaut l’expression booléenne NOT(a AND b) ?</p>
","
<p>0</p>
","
<p>False</p>
","
<p>True</p>
","
<p>None</p>
","C","2","7",NULL,"1",NULL,"2020-05-07 22:20:16",NULL),
("1126","<br><br><p>Quelle est l'écriture hexadécimale de l'entier <span class='math inline'>\(n\)</span> dont l'écriture binaire est 101010 ?</p><br>","<br><br><p>2A</p><br>","<br><br><p>A2</p><br>","<br><br><p>42</p><br>","<br><br><p>24</p><br>","C","2","7",NULL,"1",NULL,"2020-03-25 16:17:43",NULL),
("1127","<br><br><p>Le code ASCII permet de représenter en binaire les caractères alphanumériques. Quel est son principal inconvénient ?</p><br>","<br><br><p>Il utilise beaucoup de bits.</p><br>","<br><br><p>Il ne différencie pas les majuscules des minuscules.</p><br>","<br><br><p>Il ne représente pas les caractères accentués.</p><br>","<br><br><p>Il n'est pas compatible avec la plupart des systèmes informatiques.</p><br>","C","2","7",NULL,"1",NULL,"2020-03-25 16:17:52",NULL),
("1128","<br><br><p>Quel est le nombre minimal de bits nécessaire pour représenter l'entier positif 79 en binaire&nbsp;?</p><br>","<br><br><p>2</p><br>","<br><br><p>6</p><br>","<br><br><p>7</p><br>","<br><br><p>8</p><br>","C","2","7",NULL,"1",NULL,"2020-03-25 16:17:59",NULL),
("1129","
<p>Si A et B sont des variables booléennes, laquelle de ces expressions booléennes est équivalente à : (not A) or B ?</p>
","
<p>(A and B) or (not A and B)</p>
","
<p>(A and B) or (not A and B) or (not A and not B)</p>
","
<p>(not A and B) or (not A and not B)</p>
","
<p>(A and B) or (not A and not B)</p>
","B","2","7",NULL,"1",NULL,"2020-05-07 22:25:09",NULL),
("1130","<br><br><p>Quelle est l'écriture hexadécimale (en base 16) du nombre entier 157 ?</p><br>","<br><br><p>8F</p><br>","<br><br><p>9C</p><br>","<br><br><p>9D</p><br>","<br><br><p>AD</p><br>","C","2","7",NULL,"1",NULL,"2020-03-25 16:18:16",NULL),
("1131","<br><br><p>Quelle expression Python a pour valeur la liste [1,3,5,7,9,11] ?</p><br>","<br><br><p>[2*i - 1 for i in range(6)]</p><br>","<br><br><p>[2*i + 1 for i in range(6)]</p><br>","<br><br><p>[2*i + 1 for i in range(5)]</p><br>","<br><br><p>[2*i - 1 for i in range(7)]</p><br>","B","3","13",NULL,"1",NULL,"2020-03-25 16:18:24",NULL),
("1132","
<p>Quelle expression permet d'accéder à la valeur 'hello' après qu'on a défini :</p>
<pre><code class='python'>L = [['a','b','c'],['bonjour','hello']]</code></pre>
","
<p>L[5]</p>
","
<p>L[1][1]</p>
","
<p>L[2][2]</p>
","
<p>L['hello']</p>
","B","3","13",NULL,"1",NULL,"2020-05-09 01:35:13",NULL),
("1133","<p>On dispose du dictionnaire regions ci-dessous :</p>
<pre><code class='python'>regions = { 'Mayotte': 376,
    'Pays de la Loire': 32082,
    'La Réunion': 2504,
    'Grand Est': 57441,
    'Martinique': 1128,
    'Corse': 8680,
    'Bretagne': 27208,
    'Nouvelle-Aquitaine': 84036 }</code></pre>
<p>Parmi les instructions suivantes, laquelle permet d'ajouter une nouvelle région ?</p>
","<pre><code>INSERT ''Hauts de France':31806' INTO regions</code></pre>
","<pre><code class='python'>regions = dict(['Hauts de France'] = 31806)</code></pre>
","<pre><code class='python'>regions('Hauts de France') = 31806</code></pre>
","<pre><code class='python'>regions['Hauts de France'] = 31806</code></pre>
","D","3","13",NULL,"1",NULL,"2020-05-12 14:16:35",NULL),
("1134","<br><br><p>Quelle est la valeur de l'expression [ 2*k + 1 for k in range(4) ] ?</p><br>","<br><br><p>[1,3,5,7]</p><br>","<br><br><p>[0,1,2,3]</p><br>","<br><br><p>[3,5,7,9]</p><br>","<br><br><p>[1,2,3,4]</p><br>","A","3","13",NULL,"1",NULL,"2020-03-25 16:18:50",NULL),
("1135","
<p>Quelle est la valeur de :</p>
<code class='python'>[ x - y for x in range(4) for y in range(3) if x &gt; y ]</code>
","
<p>[1, 2, 1, 3, 2, 1]</p>
","
<p>[1, 2, 3, 1, 2, 1]</p>
","
<p>[1, 2, 3, 3, 2, 1]</p>
","
<p>[1, 2, 1, 2, 3, 1]</p>
","A","3","13",NULL,"1",NULL,"2020-05-07 22:31:12",NULL),
("1136","
<p>Quelle est la valeur de l'expression <code class='python'>[2**i for i in range(5)]</code> ?</p>
","
<p>[0,1,4,9,16]</p>
","
<p>[1,4,9,16,25]</p>
","
<p>[0,2,4,6,8]</p>
","
<p>[1,2,4,8,16]</p>
","D","3","13",NULL,"1",NULL,"2020-05-07 22:32:35",NULL),
("1137","<br><br><p>Qu'est-ce que le CSV ?</p><br>","<br><br><p>Un langage de programmation</p><br>","<br><br><p>Un format de fichier permettant de stocker de l’information</p><br>","<br><br><p>Un algorithme permettant de rechercher une information dans un fichier</p><br>","<br><br><p>Un format de fichier permettant de définir le style d’une page web</p><br>","B","4","18",NULL,"1",NULL,"2020-03-25 16:19:15",NULL),
("1138","<br><br><p>On utilise habituellement un fichier d'extension csv pour quel type de données ?</p><br>","<br><br><p>des données structurées graphiquement</p><br>","<br><br><p>des données sonores</p><br>","<br><br><p>des données compressées</p><br>","<br><br><p>des données structurées en tableau</p><br>","D","4","18",NULL,"1",NULL,"2020-03-25 16:19:23",NULL),
("1139","
<p>Par quelle expression remplacer les pointillés dans le programme Python suivant, pour que son exécution affiche le numéro de Dupond ?</p>
<pre><code class='python'>repertoire = [ {'nom':'Dupont', 'tel':'5234'},
{'nom':'Tournesol', 'tel':'5248'},
{'nom':'Dupond', 'tel':'3452'}]
for i in range(len(repertoire)):
    if ...... :
        print(repertoire[i]['tel'])</code></pre>
","
<p>nom == 'Dupond'</p>
","
<p>repertoire['nom'] == 'Dupond'</p>
","
<p>repertoire[i] == 'Dupond'</p>
","
<p>repertoire[i]['nom'] == 'Dupond'</p>
","D","4","18",NULL,"1",NULL,"2020-05-07 22:36:08",NULL),
("1140","
<p>On définit une table d'élèves et une liste finale de la façon suivante :</p>
<pre><code class='python'>table_eleves = [ {'prenom': 'Ada', 'nom': 'Lovelace', 'age': 17},
{'prenom': 'Charles', 'nom': 'Babbage', 'age': 18},
......
{'prenom': 'John', 'nom': 'Von Neumann', 'age': 16} ]

liste_finale = [ eleve for eleve in table_eleves if eleve['age'] &gt;= 18 ]</code></pre>
<p>Que contient cette liste finale ?</p>
","
<p>La liste des prénoms des élèves majeurs de la table.</p>
","
<p>La liste des âges des élèves majeurs de la table.</p>
","
<p>La liste des élèves majeurs de la table, chaque élément de la liste étant représenté par un dictionnaire.</p>
","
<p>La liste des élèves majeurs de la table, chaque élément de la liste étant représenté par une liste.</p>
","C","4","18",NULL,"1",NULL,"2020-05-07 22:40:08",NULL),
("1141","
<p>Soit le tableau défini de la manière suivante :<br /><code>tableau = [[1,3,4],[2,7,8],[9,10,6],[12,11,5]]</code></p>
<p>On souhaite accéder à la valeur 12, on écrit pour cela :</p>
","
<p>tableau[4][1]</p>
","
<p>tableau[1][4]</p>
","
<p>tableau[3][0]</p>
","
<p>tableau[0][3]</p>
","C","4","18",NULL,"1",NULL,"2020-05-07 22:42:17",NULL),
("1142","<br><br><p>Parmi les extensions suivantes, laquelle caractérise un fichier contenant des données que l'on peut associer à un tableau de pixels ?</p><br>","<br><br><p>pdf</p><br>","<br><br><p>xls</p><br>","<br><br><p>png</p><br>","<br><br><p>exe</p><br>","C","4","18",NULL,"1",NULL,"2020-03-25 16:20:00",NULL),
("1143","
<p>Un fichier HTML contient la ligne suivante.<br /><code>&lt;p&gt;Coucou ! Ca va?&lt;/p&gt;</code></p>
<p>Quelle commande CSS écrire pour que le texte apparaisse en rose sur fond jaune ?</p>
","
<p>p { couleur: rose ; fond: jaune;}</p>
","
<p>&lt;p&gt; { color = pink background-color = yellow}</p>
","
<p>&lt;p&gt; { color = pink ; background-color: yellow} &lt;/p&gt;</p>
","
<p>p { color: pink ; background-color: yellow ;}</p>
","D","5","22",NULL,"1",NULL,"2020-05-07 22:44:24",NULL),
("1144","<br><br><p>Mehdi a écrit une page HTML contenant des éléments input de formulaire.</p><br><p>Il place ces éléments de formulaire :</p><br>","<br><br><p>entre la balise &lt;form&gt; et la balise &lt;/form&gt;</p><br>","<br><br><p>entre la balise &lt;formulary&gt; et la balise &lt;/formulary&gt;</p><br>","<br><br><p>entre la balise &lt;code&gt; et la balise &lt;/code&gt;</p><br>","<br><br><p>entre la balise &lt;script&gt; et la balise &lt;/script&gt;</p><br>","A","5","22",NULL,"1",NULL,"2020-03-25 16:20:18",NULL),
("1145","<br><br><p>Quelle est la balise HTML utilisée pour indiquer un titre de niveau d'importance maximal ?</p><br>","<br><br><p>la balise &lt;h0&gt;</p><br>","<br><br><p>la balise &lt;h1&gt;</p><br>","<br><br><p>la balise &lt;head&gt;</p><br>","<br><br><p>la balise &lt;header&gt;</p><br>","B","5","22",NULL,"1",NULL,"2020-03-25 16:20:27",NULL),
("1146","<br><br><p>Quel code d'erreur renvoie un serveur Web, lorsque la ressource demandée par une requête n'existe pas ?</p><br>","<br><br><p>100</p><br>","<br><br><p>200</p><br>","<br><br><p>404</p><br>","<br><br><p>504</p><br>","C","5","22",NULL,"1",NULL,"2020-03-25 16:20:34",NULL),
("1147","<br><br><p>Quel est le nom d'un protocole qui permet à un client de faire une requête de page Web auprès d'un serveur ?</p><br>","<br><br><p>WWW</p><br>","<br><br><p>FTP</p><br>","<br><br><p>HTTP</p><br>","<br><br><p>DNS</p><br>","C","5","22",NULL,"1",NULL,"2020-03-25 16:20:42",NULL),
("1148","<br><br><p>Parmi les balises HTML ci-dessous quelle est celle qui permet à l’utilisateur de saisir son nom dans un formulaire en respectant la norme HTML ?</p><br>","<br><br><p>&lt;select /&gt;</p><br>","<br><br><p>&lt;form /&gt;</p><br>","<br><br><p>&lt;input type='text' /&gt;</p><br>","<br><br><p>&lt;input type='name' /&gt;</p><br>","C","5","22",NULL,"1",NULL,"2020-03-25 16:20:50",NULL),
("1149","<br><br><p>Laquelle des mémoires suivantes est volatile ?</p><br>","<br><br><p>RAM</p><br>","<br><br><p>disque dur</p><br>","<br><br><p>ROM</p><br>","<br><br><p>clef USB</p><br>","A","6","27",NULL,"1",NULL,"2020-03-25 16:20:58",NULL),
("1150","<br><br><p>Une et une seule de ces affirmations est <strong>fausse</strong>. Laquelle ?</p><br>","<br><br><p>Un système d'exploitation libre est la plupart du temps gratuit</p><br>","<br><br><p>Je peux contribuer à un système d'exploitation libre</p><br>","<br><br><p>Il est interdit d'étudier un système d'exploitation propriétaire</p><br>","<br><br><p>Un système d'exploitation propriétaire est plus sécurisé</p><br>","D","6","27",NULL,"1",NULL,"2020-03-25 16:21:07",NULL),
("1151","<br><br><p>L'adresse IP du site www.education.gouv.fr est 185.75.143.24.</p><br><p>Quel dispositif permet d'associer l'adresse IP et l'URL www.education.gouv.fr ?</p><br>","<br><br><p>un routeur</p><br>","<br><br><p>un serveur DNS</p><br>","<br><br><p>un serveur de temps</p><br>","<br><br><p>un serveur Web</p><br>","B","6","27",NULL,"1",NULL,"2020-03-25 16:21:15",NULL),
("1152","<br><br><p>Parmi les commandes suivantes, laquelle permet à n’importe quel utilisateur d’exécuter le fichier appelé jeu ?</p><br>","<br><br><p>chmod u+x jeu</p><br>","<br><br><p>chmod u+rwx jeu</p><br>","<br><br><p>chmod a+x jeu</p><br>","<br><br><p>chmod a-x jeu</p><br>","C","6","27",NULL,"1",NULL,"2020-03-25 16:21:23",NULL),
("1153","
<p>Depuis le répertoire /home/ubuntu/ on exécute la commande<br /><code>mkdir ./Documents/Holidays</code></p>
<p>Quel est son effet ?</p>
","
<p>supprimer le dossier Holidays situé dans Documents</p>
","
<p>changer de répertoire pour se retrouver dans le répertoire /home/Documents/Holidays</p>
","
<p>créer un dossier Holidays dans le répertoire /home/ubuntu/Documents</p>
","
<p>lister le contenu du répertoire Holidays de Documents</p>
","C","6","27",NULL,"1",NULL,"2020-05-09 01:52:04",NULL),
("1154","
<p>Par quelle ligne de commande peut-on créer, sous le système d’exploitation Linux, trois répertoires nommés : JAVA, PYTHON et PHP ?</p>
","
<p>mkdir JAVA, PYTHON, PHP</p>
","
<p>mk -dir JAVA PYTHON PHP</p>
","
<p>mkdir JAVA PYTHON PHP</p>
","
<p>mk dir JAVA PYTHON PHP</p>
","C","6","27",NULL,"1",NULL,"2020-05-07 22:55:44",NULL),
("1155","
<p>On définit la fonction :</p>
<pre><code class='python'>def fib(n):
    t = [0] * n
    t[1] = 1
    for in in range(2,n):
        t[i] = t[i-1] + t[i-2]
    return t</code></pre>
<p>Quelle est la valeur renvoyée par l'appel fib(6) ?</p>
","
<p>[0, 1, 1, 2, 3]</p>
","
<p>[0, 1, 1, 2, 3, 5]</p>
","
<p>[0, 1, 1, 2, 3, 5, 8]</p>
","
<p>[0, 1, 2, 3, 5, 8]</p>
","B","7","33",NULL,"1",NULL,"2020-05-07 22:57:59",NULL),
("1156","
<p>Quelle est la valeur de la variable n à la fin de l'exécution du script ci-dessous ?</p>
<pre><code class='python'>n = 1
for i in range(4):
    n = n + 2</code></pre>
","
<p>1</p>
","
<p>8</p>
","
<p>9</p>
","
<p>18</p>
","C","7","33",NULL,"1",NULL,"2020-05-07 22:59:29",NULL),
("1157","
<p>On définit deux fonctions :</p>
<pre><code class='python'>def f(x):
    y = 2*x + 1
    return y

def calcul(x):
    y = x - 1
    return f(y)</code></pre>
<p>Quelle est la valeur renvoyée par l'appel <code>calcul(5)</code> ?</p>
","
<p>4</p>
","
<p>9</p>
","
<p>11</p>
","
<p>19</p>
","B","7","33",NULL,"1",NULL,"2020-05-07 23:02:00",NULL),
("1158","
<p>On exécute le script Python suivant :</p>
<pre><code class='python'>def cube(a):
    a = a*a*a
    return a

a = 2
b = cube(a)</code></pre>
<p>Que vaut le couple (a,b) à la fin de l'exécution ?</p>
","
<p>(8, 8)</p>
","
<p>(8, 2)</p>
","
<p>(2, 2)</p>
","
<p>(2, 8)</p>
","D","7","33",NULL,"1",NULL,"2020-05-07 23:04:40",NULL),
("1159","
<p>On définit la fonction f suivante qui prend en argument une liste t d'entiers :</p>
<pre><code class='python'>def f(t):
    n = len(t)
    for i in range(n-1):
        for j in range(i+1,n):
            if t[i] == t[j]:
                return True
    return False</code></pre>
<p>Pour quelle valeur de t, f(t) vaut-elle True ?</p>
","
<p>[[2,3], [3,4], [4,5], [2,3]]</p>
","
<p>[[2,2], [3,4], [4,5], [2,3]]</p>
","
<p>[[2,3], [3,2], [4,5], [5,4]]</p>
","
<p>[[3,3], [4,4], [5,5], [6,6]]</p>
","A","7","33",NULL,"1",NULL,"2020-05-07 23:07:03",NULL),
("1160","<br><br><p>Lequel des langages suivants n'est pas un langage de programmation :</p><br>","<br><br><p>PHP</p><br>","<br><br><p>Javascript</p><br>","<br><br><p>HTML</p><br>","<br><br><p>Python</p><br>","C","7","33",NULL,"1",NULL,"2020-03-25 16:22:32",NULL),
("1161","<p>Lors de l'exécution du code suivant, combien de fois l'opération a = 2*a sera-t-elle effectuée ?</p>
<pre><code class='python'>a = 1
cpt = 1
while cpt &lt; 8:
    a = 2*a
    cpt = cpt+1</code></pre>
","0
","1
","7
","8
","C","8","39",NULL,"1",NULL,"2020-05-07 23:08:41",NULL),
("1162","<p>Qu'affiche le programme suivant :</p>
<pre><code class='python'>a = 3
b = 4
if a &gt; b and a == 3:
    print('vert')
if a &gt; b and b == 4:
    print('rouge')
if a == 4 or b &gt; a:
    print('bleu')
if a == 3 or a &lt; b:
    print('jaune')</code></pre>
","<p>vert<br />
rouge</p>
","<p>bleu<br />
jaune</p>
","<p>bleu</p>
","<p>vert<br />
jaune</p>
","B","8","39",NULL,"1",NULL,"2020-05-09 01:53:14",NULL),
("1163","
<p><span class='math inline'>\(a\)</span> et <span class='math inline'>\(m\)</span> étant deux entiers supérieurs à 1, la fonction suivante renvoie <span class='math inline'>\(a^{m}\)</span>.</p>
<pre><code class='python'>def puissance(a,m):
    p = 1
    n = 0
    while n &lt; m:
        #
        p = p * a
        n = n + 1
    return p</code></pre>
<p>Quelle est l'égalité qui est vérifiée à chaque passage par la ligne marquée # ?</p>
","
<p><span class='math inline'>\(p = a^{n - 1}\)</span></p>
","
<p><span class='math inline'>\(p = a^{n}\)</span></p>
","
<p><span class='math inline'>\(p = a^{n + 1}\)</span></p>
","
<p><span class='math inline'>\(p = a^{m}\)</span></p>
","B","8","39",NULL,"1",NULL,"2020-05-07 23:13:39",NULL),
("1164","
<p>On définit :</p>
<pre><code class='python'>def traite(chaine,a):
    nouvelle_chaine = ''
    for k in range(len(chaine)):
        if chaine[k] != a:
            nouvelle_chaine = nouvelle_chaine + chaine[k]
    return nouvelle_chaine</code></pre>
<p>Quelle est la valeur renvoyée par l'appel <code>traite('histoire','i')</code> ?</p>
","
<p>'hstore'</p>
","
<p>'ii'</p>
","
<p>'histoire'</p>
","
<p>''</p>
","A","8","39",NULL,"1",NULL,"2020-05-07 23:16:22",NULL),
("1165","
<p>Quelle est la valeur du couple (s,i) à la fin de l'exécution du script suivant ?</p>
<pre><code class='python'>s = 0
i = 1
while i &lt; 5:
    s = s + i
    i = i + 1</code></pre>
","
<p>(4, 5)</p>
","
<p>(10, 4)</p>
","
<p>(10, 5)</p>
","
<p>(15, 5)</p>
","C","8","39",NULL,"1",NULL,"2020-05-07 23:17:59",NULL),
("1166","<br><br><p>Quelle valeur permet de compléter l’affirmation suivante&nbsp;: «&nbsp;Le nombre d’opérations nécessaires pour rechercher un élément séquentiellement dans un tableau de longueur <span class='math inline'>\(n\)</span> est de l’ordre de …&nbsp;»&nbsp;?</p><br>","<br><br><p>1</p><br>","<br><br><p><span class='math inline'>\(n\)</span></p><br>","<br><br><p><span class='math inline'>\(n^{2}\)</span></p><br>","<br><br><p><span class='math inline'>\(n^{3}\)</span></p><br>","B","8","39",NULL,"1",NULL,"2020-03-25 16:23:26",NULL),
("1167","<br><br><p>Soit <span class='math inline'>\(n\)</span> l'entier positif dont l'écriture binaire est 10001. Quelle est l'écriture binaire de l'entier <span class='math inline'>\(2n\)</span> ?</p><br>","<br><br><p>20002</p><br>","<br><br><p>100010</p><br>","<br><br><p>010001</p><br>","<br><br><p>1000110001</p><br>","B","2","7",NULL,"1",NULL,"2020-03-25 16:24:17",NULL),
("1168","<br><br><p>À quoi sert le codage en complément à 2&nbsp;?</p><br>","<br><br><p>à inverser un nombre binaire</p><br>","<br><br><p>à coder des nombres entiers négatifs en binaire</p><br>","<br><br><p>à convertir un nombre en hexadécimal</p><br>","<br><br><p>à multiplier par 2 un nombre en binaire</p><br>","B","2","7",NULL,"1",NULL,"2020-03-25 16:24:25",NULL),
("1169","<br><br><p>Combien de nombres entiers positifs peut-on coder en binaire sur 4 bits ?</p><br>","<br><br><p>4</p><br>","<br><br><p>16</p><br>","<br><br><p>64</p><br>","<br><br><p>256</p><br>","B","2","7",NULL,"1",NULL,"2020-03-25 16:24:32",NULL),
("1170","<br><br><p>Le résultat de la multiplication en binaire 1011 * 101 est égal au nombre binaire&nbsp;:</p><br>","<br><br><p>102111</p><br>","<br><br><p>101110</p><br>","<br><br><p>110111</p><br>","<br><br><p>110011</p><br>","C","2","7",NULL,"1",NULL,"2020-03-25 16:24:39",NULL),
("1171","<br><br><p>Combien de valeurs entières positives ou nulles un octet peut-il représenter ?</p><br>","<br><br><p>2</p><br>","<br><br><p>8</p><br>","<br><br><p>16</p><br>","<br><br><p>256</p><br>","D","2","7",NULL,"1",NULL,"2020-03-25 16:24:46",NULL),
("1172","<br><br><p>Quelle est l'écriture hexadécimale (en base 16) du nombre entier 157 ?</p><br>","<br><br><p>8F</p><br>","<br><br><p>9C</p><br>","<br><br><p>9D</p><br>","<br><br><p>AD</p><br>","C","2","7",NULL,"1",NULL,"2020-03-25 16:24:54",NULL),
("1173","
<p>On exécute le script suivant :</p>
<pre><code class='python'>inventaire = {'pommes': 430, 'bananes': 312, 'oranges' : 274, 'poires' : 137}
stock = 0
for fruit in inventaire.keys():
    if fruit != 'bananes':
        stock = stock + inventaire[fruit]</code></pre>
<p>Que contient la variable stock à la fin de cette exécution ?</p>
","
<p>{430, 274, 137}</p>
","
<p>312</p>
","
<p>841</p>
","
<p>{ 'pommes', 'oranges', 'poires' }</p>
","C","3","13",NULL,"1",NULL,"2020-05-09 01:56:57",NULL),
("1174","
<p>On considère la fonction suivante :</p>
<pre><code class='python'>def h(L,m,n):
    for i in range(m, (m+n)//2 + 1):
        L[i], L[m+n-i] = L[m+n-i],L[i]</code></pre>


<p>On exécute les instructions suivantes :</p>
<pre><code class='python'>L = [ 2, 3, 4, 5, 7, 8 ]
h(L,0,2)
h(L,3,5)
h(L,0,5)</code></pre>
<p>Quelle est la valeur de L à la fin de cette exécution ?</p>
","
<p>[8, 7, 5, 4, 3, 2]</p>
","
<p>[2, 3, 4, 5, 7, 8]</p>
","
<p>[4, 3, 2, 8, 7, 5]</p>
","
<p>[5, 7, 8, 2, 3, 4]</p>","D","3","13",NULL,"1",NULL,"2020-05-09 02:18:05",NULL),
("1175","
<p>Quel est le résultat de l'évaluation de l'expression Python suivante ?</p>
<p>[ n * n for n in range(10) ]</p>
","
<p>[0, 1, 4, 9, 16, 25, 36, 49, 64, 81]</p>
","
<p>[0, 1, 4, 9, 16, 25, 36, 49, 64, 81, 100]</p>
","
<p>[0, 2, 4, 8, 16, 32, 64, 128, 256, 512]</p>
","
<p>[0, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024]</p>","A","3","13",NULL,"1",NULL,"2020-05-09 02:19:14",NULL),
("1176","
<p>On a défini</p>
<pre><code class='python'>repertoire = [ {'nom': 'Francette', 'poste': 412},
               {'nom': 'Jeanne', 'poste': 222},
               {'nom': 'Éric', 'poste': 231} ]</code></pre>
<p>Quelle expression permet d'accéder au poste d'Éric ?</p>
","
<p>repertoire[2]['poste']</p>
","
<p>repertoire['poste'][2]</p>
","
<p>repertoire['Éric']['poste']</p>
","
<p>repertoire['Éric']</p>
","A","3","13",NULL,"1",NULL,"2020-05-09 02:21:08",NULL),
("1177","
<p>On définit ainsi une liste M :</p>
<p><code>M = [['A','B','C','D'], ['E','F','G','H'], ['I','J','K','L']]</code></p>
<p>Quelle expression vaut la chaîne de caractères 'H' ?</p>
","
<p>M[1][3]</p>
","
<p>M[3][1]</p>
","
<p>M(7)</p>
","
<p>M(8)</p>
","A","3","13",NULL,"1",NULL,"2020-05-09 02:22:18",NULL),
("1178","
<p>Si on tape dans la console d'éxécution la commande :</p>
<p>[1,4,3] + [2,4,5]</p>
<p>qu'obtient-on ?</p>
","
<p>[3, 8, 8]</p>
","
<p>[19]</p>
","
<p>[1, 4, 3, 2, 4, 5] </p>
","
<p>un message d'erreur car l'addition n'est pas compatible avec les listes</p>
","C","3","13",NULL,"1",NULL,"2020-05-09 02:23:01",NULL),
("1179","
<p>On a défini :</p>
<pre><code class='python'>mendeleiev = [ ['H','.', '.','.','.','.','.','He'],
               ['Li','Be','B','C','N','O','Fl','Ne'],
               ['Na','Mg','Al','Si','P','S','Cl','Ar'],
               ...... ]</code></pre>
<p>Comment construire la liste des gaz rares, c'est-à-dire la liste des éléments de la dernière colonne ?</p>
","
<p>gaz_rares = [ periode[7] for periode in mendeleiev]</p>
","
<p>gaz_rares = [ periode for periode in mendeleiev[7]]</p>
","
<p>gaz_rares = [ periode for periode[7] in mendeleiev]</p>
","
<p>gaz_rares = [ periode[8] for periode in mendeleiev]</p>
","A","4","18",NULL,"1",NULL,"2020-05-09 02:25:49",NULL),
("1180","
<p>Laquelle de ces listes de chaînes de caractères est triée en ordre croissant ?</p>
","
<p>['112', '19', '27', '45', '8']</p>
","
<p>['8', '19', '27', '45', '112']</p>
","
<p>['8', '112', '19', '27', '45']</p>
","
<p>['19', '112', '27', '45', '8']</p>
","A","4","18",NULL,"1",NULL,"2020-05-09 02:26:45",NULL),
("1181","
<p>Qu'est-ce que le CSV ?</p>
","
<p>Un langage de programmation</p>
","
<p>Un format de fichier permettant de stocker de l’information</p>
","
<p>Un algorithme permettant de rechercher une information dans un fichier</p>
","
<p>Un format de fichier permettant de définir le style d’une page web</p>
","B","4","18",NULL,"1",NULL,"2020-05-09 02:27:32",NULL),
("1182","
<p>On considère l’extraction suivante d'une base de données des départements français. Cette extraction a ensuite été sauvegardée dans un fichier texte.</p>
<pre><code>'1','01','Ain','AIN','ain','A500'
'2','02','Aisne','AISNE','aisne','A250'
'3','03','Allier','ALLIER','allier','A460'
'4','04','Alpes-de-Haute-Provence','ALPES-DE-HAUTE-PROVENCE','alpes-de-haute-provence','A412316152'
'5','05','Hautes-Alpes','HAUTES-ALPES','hautes-alpes','H32412'</code></pre>
<p>Quel est le format de ce fichier ?</p>
","
<p>YML</p>
","
<p>XML</p>
","
<p>CSV</p>
","
<p>JSON</p>
","C","4","18",NULL,"1",NULL,"2020-05-09 02:29:06",NULL),
("1183","
<p>On exécute le script suivant :</p>
<pre><code class='python'>asso = []
L =  [ ['marc','marie'], ['marie','jean'], ['<span class='underline'>paul</span>','marie'], 
       ['marie','marie'], ['marc','<span class='underline'>anne</span>'] ]
for c in L :
    if c[1]==‘marie’:
        asso.append(c[0])</code></pre>
<p>Que vaut asso à la fin de l'exécution ?</p>
","
<p>['marc', 'jean', 'paul']</p>
","
<p>[['marc','marie'], ['paul','marie'], ['marie','marie']]</p>
","
<p>['marc', 'paul', 'marie'] </p>
","
<p>['marie', 'anne']</p>
","C","4","18",NULL,"1",NULL,"2020-05-09 02:32:20",NULL),
("1184","<p>On exécute le code suivant :</p>
<pre><code class='python'>collection = [('Renault', '4L', 1974, 30),
              ('Peugeot', '504', 1970, 82),
              ('Citroën', 'Traction', 1950, 77)]</code></pre>
<p>Que vaut collection[1][2] ?</p>
","<p>1970</p>
","<p>'4L'</p>
","<p>('Peugeot', '504', 1970, 82)</p>
","<p>('Renault', '4L', 1974, 30)</p>
","A","4","18",NULL,"1",NULL,"2020-05-09 02:34:32",NULL),
("1185","
<p>Un internaute clique sur un lien qui envoie la requête HTTP suivante à un serveur :</p>
<p><code>http://jaimelaneige.com/ma_planche/traitement.php?nom=Snow&amp;prenom=Jon</code></p>
<p>Quelle est l'adresse du serveur ?</p>
","
<p>jaimelaneige</p>
","
<p>jaimelaneige.com</p>
","
<p>jaimelaneige.com/ma_planche</p>
","
<p>jaimelaneige.com/ma_planche/traitement.php</p>
","B","5","22",NULL,"1",NULL,"2020-05-09 02:35:48",NULL),
("1186","
<p>Les pages HTML sont affichées par …</p>
","
<p>le compilateur</p>
","
<p>le serveur</p>
","
<p>l'interpréteur</p>
","
<p>le navigateur Web</p>
","A","5","22",NULL,"1",NULL,"2020-05-09 02:36:30",NULL),
("1187","
<p>Mehdi a écrit une page HTML contenant des éléments input de formulaire.</p>
<p>Il place ces éléments de formulaire :</p>
","
<p>entre la balise &lt;form&gt; et la balise &lt;/form&gt;</p>
","
<p>entre la balise &lt;formulary&gt; et la balise &lt;/formulary&gt;</p>
","
<p>entre la balise &lt;code&gt; et la balise &lt;/code&gt;</p>
","
<p>entre la balise &lt;script&gt; et la balise &lt;/script&gt;</p>
","A","5","22",NULL,"1",NULL,"2020-05-09 02:37:40",NULL),
("1188","
<p>On considère cet extrait de fichier HTML représentant les onglets d'une barre de navigation :</p>
<pre><code class='html'>function BoutonGris() {
  var btn = document.createElement('BUTTON');
  btn.innerHTML = 'Annulation';
  document.getElementById('DIV').appendChild(btn);
}</code></pre>
","
<p>elle remplace un élément DIV par un bouton</p>
","
<p>elle annule l'élément BUTTON</p>
","
<p>elle crée un bouton comportant le texte 'Annulation'</p>
","
<p>elle recherche le bouton 'BUTTON' et crée une copie appelée 'btn'</p>
","C","5","22",NULL,"1",NULL,"2020-05-09 02:39:59",NULL),
("1189","
<p>Parmi les balises HTML ci-dessous quelle est celle qui permet à l’utilisateur de saisir son nom dans un formulaire en respectant la norme HTML ?</p>
","
<p>&lt;select /&gt;</p>
","
<p>&lt;form /&gt;</p>
","
<p>&lt;input type='text' /&gt;</p>
","
<p>&lt;input type='name' /&gt;</p>","C","5","22",NULL,"1",NULL,"2020-05-09 02:40:57",NULL),
("1190","
<p>Comment s'appelle la méthode permettant de transmettre les variables en les faisant apparaître dans la barre d’adresse du navigateur ?</p>
","
<p>URL</p>
","
<p>HEAD</p>
","
<p>POST</p>
","
<p>GET</p>
","D","5","22",NULL,"1",NULL,"2020-05-09 02:41:39",NULL),
("1191","
<p>Quel est l’effet de la commande shell suivante ?</p>
<p><code>cp NSI_ex1_Franck.txt NSI_ex1_Marie.txt</code></p>
","
<p>Le fichier NSI_ex1_Franck.txt est copié sous le nom NSI_ex1_Marie.txt</p>
","
<p>Le fichier NSI_ex1_Franck.txt est renommé sous le nom NSI_ex1_Marie.txt</p>
","
<p>Le fichier NSI_ex1_Marie.txt est copié sous le nom NSI_ex1_Franck.txt</p>
","
<p>Le fichier NSI_ex1_Marie.txt est renommé sous le nom NSI_ex1_Franck.txt</p>
","A","6","27",NULL,"1",NULL,"2020-05-09 02:43:11",NULL),
("1192","
<p>Lors d'un échange TCP/IP entre deux machines sur le réseau Internet, un paquet n'arrive pas à destination. Qui gère le renvoi du paquet ?</p>
","
<p>le protocole IP de la machine de départ</p>
","
<p>le protocole IP de la machine d'arrivée</p>
","
<p>le protocole TCP de la machine de départ</p>
","
<p>le protocole TCP de la machine d'arrivée</p>
","C","6","27",NULL,"1",NULL,"2020-05-09 02:44:18",NULL),
("1193","<p>Dans le protocole de communication IP :</p>
","<p>Les données sont envoyées en une seule partie.</p>
","<p>Les données sont envoyées en plusieurs parties qui suivent le même itinéraire au sein du réseau.</p>
","<p>Les données sont envoyées en plusieurs parties qui suivent des itinéraires différents au sein du réseau et arrivent à destination en respectant l’ordre de leur envoi.</p>
","<p>Les données sont envoyées en plusieurs parties qui suivent des itinéraires différents au sein du réseau et arrivent à destination dans un ordre quelconque.</p>
","D","6","27",NULL,"1",NULL,"2020-05-09 02:45:26",NULL),
("1194","
<p>Un protocole est un ensemble de …</p>
","
<p>matériels connectés entre eux</p>
","
<p>serveurs et de clients connectés entre eux</p>
","
<p>règles qui régissent les échanges entre équipements informatiques</p>
","
<p>règles qui régissent les échanges entre un système d’exploitation et les applications</p>
","C","6","27",NULL,"1",NULL,"2020-05-09 02:46:18",NULL),
("1195","
<p>On cherche à connaitre l’itinéraire vers une destination sur un réseau. On utilisera la commande :</p>
","
<p>ping</p>
","
<p>traceroute</p>
","
<p>ipconfig</p>
","
<p>arp</p>
","B","6","27",NULL,"1",NULL,"2020-05-09 02:46:53",NULL),
("1196","
<p>Quel est l’effet de la commande shell suivante ?</p>
<p><code>% cd ..</code></p>
","
<p>éjecter le CD</p>
","
<p>copier le contenu du répertoire courant dans un répertoire caché</p>
","
<p>changer le répertoire courant vers le répertoire supérieur</p>
","
<p>supprimer le répertoire courant</p>
","C","6","27",NULL,"1",NULL,"2020-05-09 02:48:13",NULL),
("1197","
<p>On exécute le script suivant :</p>
<pre><code class='python'>def calcul(a,b):
    a = a + 2
    b = b + 5
    c = a + b
    return c

a,b = 3,5
calcul(a,b)</code></pre>
<p>À la fin de cette exécution :</p>
","
<p>a vaut 3, b vaut 5 et c vaut 15</p>
","
<p>a vaut 3, b vaut 5 et c n'est pas défini</p>
","
<p>a vaut 5, b vaut 10 et c vaut 15</p>
","
<p>a vaut 5, b vaut 10 et c n'est pas défini</p>
","B","7","33",NULL,"1",NULL,"2020-05-09 02:51:25",NULL),
("1198","
<p>Lequel des langages suivants n'est pas un langage de programmation :</p>
","
<p>PHP</p>
","
<p>Javascript</p>
","
<p>HTML</p>
","
<p>Python</p>
","C","7","33",NULL,"1",NULL,"2020-05-09 02:52:00",NULL),
("1199","
<p>On définit la fonction f suivante qui prend en argument une liste t d'entiers :</p>
<pre><code class='python'>def f(t):
    n = len(t)
    for i in range(n-1):
        for j in range(i+1,n):
            if t[i] == t[j]:
                return True
    return False</code></pre>
<p>Pour quelle valeur de t, f(t) vaut-elle True ?</p>
","
<p>[[2,3], [3,4], [4,5], [2,3]]</p>
","
<p>[[2,2], [3,4], [4,5], [2,3]]</p>
","
<p>[[2,3], [3,2], [4,5], [5,4]]</p>
","
<p>[[3,3], [4,4], [5,5], [6,6]]</p>
","A","7","33",NULL,"1",NULL,"2020-05-09 02:54:23",NULL),
("1200","<p>On exécute le script suivant.</p>
<pre><code class='python'>a,b = 10,3
if a &lt; 10:
    a,b = a+2,b+a</code></pre>
<p>Quelle est la valeur de b à la fin de son exécution ?</p>
","3
","12
","13
","15
","A","7","33",NULL,"1",NULL,"2020-05-09 02:55:20",NULL),
("1201","
<p>On souhaite écrire une fonction qui renvoie le maximum d'une liste d'entiers :</p>
<pre><code class='python'>def maximum(L):
    m = L[0]
    for i in range(1,len(L)):
        if .........:
            m = L[i]
    return m</code></pre>
<p>Par quoi faut-il remplacer les pointillés pour que cette fonction produise bien le résultat attendu ?</p>
","
<p>i &gt; m</p>
","
<p>L[i] &gt; m</p>
","
<p>L[i] &gt; L[i-1]</p>
","
<p>L[i] &gt; L[i+1]</p>
","B","7","33",NULL,"1",NULL,"2020-05-09 02:57:39",NULL),
("1202","
<p>On considère la fonction suivante :</p>
<pre><code class='python'>def comparaison(a,b):
    if a &lt; b:
        return a
    else:
        return b</code></pre>
<p>Quel est le type de la valeur renvoyée par l'appel comparaison(6,5) ?</p>
","
<p>un booléen (vrai/faux)</p>
","
<p>un nombre entier</p>
","
<p>un nombre flottant</p>
","
<p>une chaîne de caractères</p>
","B","7","33",NULL,"1",NULL,"2020-05-09 02:59:25",NULL),
("1203","
<p>Quelle est la complexité du tri par sélection ?</p>
","
<p>inconnue</p>
","
<p>linéaire</p>
","
<p>quadratique</p>
","
<p>exponentielle</p>
","C","8","39",NULL,"1",NULL,"2020-05-09 03:00:34",NULL),
("1204","
<p>À quelle catégorie appartient l’algorithme classique de rendu de monnaie ?</p>
","
<p>les algorithmes de classification et d'apprentissage</p>
","
<p>les algorithmes de tri</p>
","
<p>les algorithmes gloutons</p>
","
<p>les algorithmes de mariages stables</p>
","C","8","39",NULL,"1",NULL,"2020-05-09 03:01:27",NULL),
("1205","
<p>On considère la fonction suivante :</p>
<pre><code class='python'>def comptage(phrase,lettre):
    i = 0
    for j in phrase:
        if j == lettre:
            i = i+1
    return i</code></pre>
<p>Que renvoie l'appel comptage('Vive l’informatique','e') ?</p>
","
<p>0</p>
","
<p>2</p>
","
<p>19</p>
","
<p>'e'</p>
","B","8","39",NULL,"1",NULL,"2020-05-09 03:04:26",NULL),
("1206","
<p>Pour pouvoir utiliser un algorithme de recherche par dichotomie dans une liste, quelle précondition doit être vraie ?</p>
","
<p>la liste doit être triée</p>
","
<p>la liste ne doit pas comporter de doublons</p>
","
<p>la liste doit comporter uniquement des entiers positifs</p>
","
<p>la liste doit être de longueur inférieure à 1024</p>
","A","8","39",NULL,"1",NULL,"2020-05-09 03:05:28",NULL),
("1207","
<p>Lors de l'exécution du code suivant, combien de fois l'opération a = 2*a sera-t-elle effectuée ?</p>
<pre><code class='python'>a = 1
cpt = 1
while cpt &lt; 8:
    a = 2*a
    cpt = cpt+1</code></pre>
","
<p>0</p>
","
<p>1</p>
","
<p>7</p>
","
<p>8</p>
","C","8","39",NULL,"1",NULL,"2020-05-09 03:07:15",NULL),
("1208","<br><br><p>En base 2, l'entier 2019 s'écrit :</p><br>","<br><br><p>111 1110 0011</p><br>","<br><br><p>7E3</p><br>","<br><br><p>110 0011 1111</p><br>","<br><br><p>3E7</p><br>","A","2","7",NULL,"1",NULL,"2020-03-25 16:33:02",NULL),
("1209","
<p>Parmi les quatre propositions, quelle est celle qui correspond au résultat de l'addition en écriture hexadécimale 7B692 + 4C81E ?</p>
","
<p>C8EB0</p>
","
<p>C5EB0</p>
","
<p>C7EC0</p>
","
<p>C7EB0</p>
","D","2","7",NULL,"1",NULL,"2020-05-05 00:14:10",NULL),
("1210","<br><br><p>Soit <span class='math inline'>\(n\)</span> l'entier dont la représentation binaire en complément à deux codée sur 8 bits est 0110 1110.</p><br><p>Quelle est la représentation binaire de <span class='math inline'>\(- n\)</span> ?</p><br>","<br><br><p>0001 0001</p><br>","<br><br><p>0001 0010</p><br>","<br><br><p>1001 0001</p><br>","<br><br><p>1001 0010</p><br>","D","2","7",NULL,"1",NULL,"2020-03-25 16:33:18",NULL),
("1211","<br><br><p>Sachant que l'expression not(a or b) a la valeur True, quelles peuvent être les valeurs des variables booléennes a et b&nbsp;?</p><br>","<br><br><p>True et True</p><br>","<br><br><p>False et True</p><br>","<br><br><p>True et False</p><br>","<br><br><p>False et False</p><br>","D","2","7",NULL,"1",NULL,"2020-03-25 16:33:27",NULL),
("1212","<br><br><p>Combien de bits sont nécessaires pour représenter 15 en binaire ?</p><br>","<br><br><p>2</p><br>","<br><br><p>3</p><br>","<br><br><p>4</p><br>","<br><br><p>5</p><br>","C","2","7",NULL,"1",NULL,"2020-03-25 16:33:34",NULL),
("1213","<br><br><p>Parmi les quatre propositions, quelle est celle qui correspond au résultat de l'addition en écriture binaire 1101&nbsp;1001&nbsp;+&nbsp;11&nbsp;0110 ?</p><br>","<br><br><p>1000&nbsp;1111</p><br>","<br><br><p>10 0000 1111</p><br>","<br><br><p>1&nbsp;0000&nbsp;1111</p><br>","<br><br><p>1&nbsp;1000&nbsp;0111</p><br>","C","2","7",NULL,"1",NULL,"2020-03-25 16:33:42",NULL),
("1214","
<p>On définit la liste L ainsi : <code class='python'>L = [ [1], [1,2], [1,2,3] ]</code></p>
<p>Des égalités suivantes, une seule est fausse. Laquelle ?</p>
","
<p>len(L[0]) == 1</p>
","
<p>len(L) == 6</p>
","
<p>len(L[2]) == 3</p>
","
<p>L[2][2] == 3</p>
","B","3","13",NULL,"1",NULL,"2020-05-05 00:19:24",NULL),
("1215","<br><br><p>On crée la liste suivante :</p><br><p>t = [ [1,2,3,4], [5,6,7,8], [9,10,11,12] ]</p><br><p>Que vaut t[1][2] :</p><br>","<br><br><p>2</p><br>","<br><br><p>7</p><br>","<br><br><p>10</p><br>","<br><br><p>on obtient un message d'erreur 'indexError : list index out of range'</p><br>","B","3","13",NULL,"1",NULL,"2020-03-25 16:33:58",NULL),
("1216","<br><br><p>On définit un dictionnaire : d = { 'couleur': 'vert', 'taille': 42, 'marque': 'le coq sportif'&nbsp;}</p><br><p>Quelle est la valeur de l'expression d.keys() ?</p><br>","<br><br><p>['couleur', 'taille', 'marque']</p><br>","<br><br><p>[('couleur', 'vert'), ('taille', 42), ('marque', 'le coq sportif')]</p><br>","<br><br><p>['vert', 42, 'le coq sportif']</p><br>","<br><br><p>['couleur': 'vert', 'taille': 42, 'marque': 'le coq sportif']</p><br>","A","3","13",NULL,"1",NULL,"2020-03-25 16:34:06",NULL),
("1217","
<p>On s'intéresse à la valeur 14 présente dans la liste suivante:</p>
<p><code class='python'>T = [[1,2,3,4,5], [6,7,8,9,10], [11,12,13,14,15], [16,17,18,19,20]]</code>.</p>
<p>Quelle expression vaut 14 parmi les suivantes ?</p>
","
<p>T[2][3]</p>
","
<p>T[3][4]</p>
","
<p>T[3][2] </p>
","
<p>T[4][3]</p>
","A","3","13",NULL,"1",NULL,"2020-05-05 01:08:52",NULL),
("1218","
<p>On considère le code suivant :</p>
<pre><code class='python'>t = [1, 6, 8, 3, 21]
u = [x for x in t if x &gt; 3]</code></pre>
<p>Que vaut u à la fin de son exécution ?</p>
","
<p>[1, 6, 8, 21]</p>
","
<p>[6, 8, 3, 21]</p>
","
<p>[6, 8, 21]</p>
","
<p>[1, 3, 6, 21]</p>
","C","3","13",NULL,"1",NULL,"2020-05-05 00:22:12",NULL),
("1219","
<p>Après l'affectation suivante :</p>
<pre><code class='python'>alphabet = [ 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' ]</code></pre>
<p>quelle est l'expression qui permet d'accéder à la lettre E ?</p>
","
<p>alphabet.E</p>
","
<p>alphabet['E']</p>
","
<p>alphabet[4]</p>
","
<p>alphabet[5]</p>
","C","3","13",NULL,"1",NULL,"2020-05-05 00:16:51",NULL),
("1220","<br><br><p>Soit le tableau défini de la manière suivante&nbsp;: tableau = [[1,3,4],[2,7,8],[9,10,6],[12,11,5]]</p><br><p>On souhaite accéder à la valeur&nbsp;12, on écrit pour cela&nbsp;:</p><br>","<br><br><p>tableau[4][1]</p><br>","<br><br><p>tableau[1][4]</p><br>","<br><br><p>tableau[3][0]</p><br>","<br><br><p>tableau[0][3]</p><br>","C","4","18",NULL,"1",NULL,"2020-03-25 16:34:39",NULL),
("1221","<br><br><p>On utilise habituellement un fichier d'extension csv pour quel type de données ?</p><br>","<br><br><p>des données structurées graphiquement</p><br>","<br><br><p>des données sonores</p><br>","<br><br><p>des données compressées</p><br>","<br><br><p>des données structurées en tableau</p><br>","D","4","18",NULL,"1",NULL,"2020-03-25 16:34:48",NULL),
("1222","<br><br><p>On définit la variable suivante : lettres = {'a': 1, 'b': 2, 'c': 3}.</p><br><p>Quelle est la valeur de l'expression list(lettres.keys()) ?</p><br>","<br><br><p>[a,b,c]</p><br>","<br><br><p>[1,2,3]</p><br>","<br><br><p>['a','b','c']</p><br>","<br><br><p>{'a': 1, 'b': 2, 'c': 3}</p><br>","C","4","18",NULL,"1",NULL,"2020-03-25 16:34:56",NULL),
("1223","<br><br><p>Qu'est-ce que le format de fichier CSV ?</p><br>","<br><br><p>un format de fichier mis au point par Microsoft pour Excel</p><br>","<br><br><p>un format de fichier pour décrire une base de données</p><br>","<br><br><p>un format de fichier où les données sont séparées par un caractère tel qu'une virgule</p><br>","<br><br><p>un format de fichier décrivant une page Web</p><br>","C","4","18",NULL,"1",NULL,"2020-03-25 16:35:05",NULL),
("1224","
<p>On exécute le code suivant :</p>
<pre><code class='python'>dict = { 'alexandre' : 17, 'mehdi' : 18,  'jeanne' : 16, 'charlotte' : 19, 'celina' : 18, 'noé' : 19  }
def f(dic):
    for cle, valeur in dic.items():
        if valeur &gt; 18:
            return cle</code></pre>
<p>Que renvoie l'appel f(dict) ?</p>
","
<p>19</p>
","
<p>19,19</p>
","
<p>'charlotte'</p>
","
<p>'charlotte','noé'</p>
","C","4","18",NULL,"1",NULL,"2020-05-05 00:33:55",NULL),
("1225","
<p>On considère l’extraction suivante d'une base de données des départements français. Cette extraction a ensuite été sauvegardée dans un fichier texte.</p>
<pre><code>'1','01','Ain','AIN','ain','A500'
'2','02','Aisne','AISNE','aisne','A250'
'3','03','Allier','ALLIER','allier','A460'
'4','04','Alpes-de-Haute-Provence','ALPES-DE-HAUTE-PROVENCE','alpes-de-haute-provence','A412316152'
'5','05','Hautes-Alpes','HAUTES-ALPES','hautes-alpes','H32412'</code></pre>
<p>Quel est le format de ce fichier ?</p>
","
<p>YML</p>
","
<p>XML</p>
","
<p>CSV</p>
","
<p>JSON</p>
","C","4","18",NULL,"1",NULL,"2020-05-05 00:29:24",NULL),
("1226","<br><br><p>Dans le code HTML les délimiteurs tels que &lt;body&gt; et &lt;/body&gt; s’appellent ?</p><br>","<br><br><p>des bornes</p><br>","<br><br><p>des balises</p><br>","<br><br><p>des paragraphes</p><br>","<br><br><p>des liens</p><br>","B","5","22",NULL,"1",NULL,"2020-03-25 16:35:32",NULL),
("1227","<br><br><p>Un site internet utilise une requête HTTP avec la méthode POST pour transmettre les données d'un formulaire. Laquelle des affirmations suivantes est <strong>incorrecte</strong>&nbsp;?</p><br>","<br><br><p>les données envoyées ne sont pas visibles</p><br>","<br><br><p>il est possible de transmettre des données de type binaire</p><br>","<br><br><p>les données transmises sont cryptées</p><br>","<br><br><p>il n'y a pas de restriction de longueur pour les données transmises</p><br>","C","5","22",NULL,"1",NULL,"2020-03-25 16:35:40",NULL),
("1228","
<p>Un internaute clique sur un lien qui envoie la requête HTTP suivante à un serveur :</p>
<p><code>http://jaimelaneige.com/ma_planche/traitement.php?nom=Snow&amp;prenom=Jon</code></p>
<p>Quelle est l'adresse du serveur ?</p>
","
<p>jaimelaneige</p>
","
<p>jaimelaneige.com</p>
","
<p>jaimelaneige.com/ma_planche</p>
","
<p>jaimelaneige.com/ma_planche/traitement.php</p>
","B","5","22",NULL,"1",NULL,"2020-05-09 03:10:41",NULL),
("1229","<br><br><p>Parmi les réponses suivantes, que permet d’effectuer la méthode POST du protocole HTTP&nbsp;?</p><br>","<br><br><p>Définir le style d’une page web</p><br>","<br><br><p>Pirater des données bancaire</p><br>","<br><br><p>Envoyer une page web vers le client</p><br>","<br><br><p>Envoyer les données saisies dans un formulaire HTML vers un serveur</p><br>","D","5","22",NULL,"1",NULL,"2020-03-25 16:35:57",NULL),
("1230","<p>Qu'affiche cet extrait de code HTML ?</p>
<pre>
&lt;a href='photo.html'&gt;&lt;img src='images/photo-lycee.jpg' alt='Photo du Lycée'&gt;&lt;/a&gt;
</pre>
","<p>seulement l'image contenue dans le fichier photo-lycee.jpg</p>
","<p>seulement le texte 'Photo du Lycée'</p>
","<p>l'image contenue dans le fichier photo-lycee.jpg avec le texte 'Photo du Lycée' à côté en légende</p>
","<p>la page Web photo.html</p>
","A","5","22",NULL,"1",NULL,"2020-05-05 01:11:00",NULL),
("1231","
<p>Lors de la consultation d’une page HTML contenant un bouton auquel est associée la fonction javascript suivante, que se passe-t-il quand on clique sur ce bouton ?</p>
<pre><code class='html'>function action(event) {
    this.style.color = 'red'
}</code></pre>
","
<p>le pointeur de souris devient rouge lorsqu'il arrive sur le bouton</p>
","
<p>le texte du bouton devient rouge</p>
","
<p>le texte du bouton est remplacé par le mot 'red'</p>
","
<p>le texte de la page passe en rouge</p>
","B","5","22",NULL,"1",NULL,"2020-05-05 00:36:14",NULL),
("1232","<br><br><p>On cherche à connaitre l’itinéraire vers une destination sur un réseau. On utilisera la commande :</p><br>","<br><br><p>ping</p><br>","<br><br><p>traceroute</p><br>","<br><br><p>ipconfig</p><br>","<br><br><p>arp</p><br>","B","6","27",NULL,"1",NULL,"2020-03-25 16:36:24",NULL),
("1233","
<p>Le shell Linux renvoie ce résultat à la commande ls -al :</p>
<pre><code>lrwxr--r-- 2 toto toto 807 juin 26 14:06 eclipse
drwxr-xr-x 2 toto toto 4096 juin 26 15:00 Doc_1
-rw-r-xr-x 2 toto toto 4096 juin 26 14:06 QCM
-rwxr-xr-x 2 toto toto 4096 juin 26 14:06 Doc_Travail</code></pre>
<p>Quel est le nom du fichier du répertoire courant, de taille 4096 octets, exécutable par son propriétaire ?</p>
","
<p>eclipse</p>
","
<p>Doc_1</p>
","
<p>QCM</p>
","
<p>Doc_Travail</p>
","D","6","27",NULL,"1",NULL,"2020-05-05 00:43:16",NULL),
("1234","<br><br><p>Dans la console Linux, quelle commande faut-il exécuter pour obtenir la liste des répertoires et dossiers contenus dans le répertoire courant ?</p><br>","<br><br><p>man pwd</p><br>","<br><br><p>cd pwd</p><br>","<br><br><p>ls -l</p><br>","<br><br><p>man ls -l</p><br>","C","6","27",NULL,"1",NULL,"2020-03-25 16:36:41",NULL),
("1235","<br><br><p>Dans le protocole de communication IP&nbsp;:</p><br>","<br><br><p>Les données sont envoyées en une seule partie.</p><br>","<br><br><p>Les données sont envoyées en plusieurs parties qui suivent le même itinéraire au sein du réseau.</p><br>","<br><br><p>Les données sont envoyées en plusieurs parties qui suivent des itinéraires différents au sein du réseau et arrivent à destination en respectant l’ordre de leur envoi.</p><br>","<br><br><p>Les données sont envoyées en plusieurs parties qui suivent des itinéraires différents au sein du réseau et arrivent à destination dans un ordre quelconque.</p><br>","D","6","27",NULL,"1",NULL,"2020-03-25 16:36:51",NULL),
("1236","<br><br><p>Le répertoire personnel de l'utilisateur contient deux répertoires tempo et sauve.</p><br><p>On souhaite déplacer le fichier bac.txt du repertoire tempo vers le répertoire sauve.</p><br><p>Quelle commande permet de réaliser ce déplacement ?</p><br>","<br><br><p>mkdir ~/tempo/bac.txt ~/sauve</p><br>","<br><br><p>mkdir ~/sauve ~/tempo/bac.txt</p><br>","<br><br><p>mv ~/tempo/bac.txt ~/sauve</p><br>","<br><br><p>mv ~/sauve ~/tempo/bac.txt</p><br>","C","6","27",NULL,"1",NULL,"2020-03-25 16:36:59",NULL),
("1237","<br><br><p>Quelle est la seule affirmation exacte ?</p><br>","<br><br><p>la mémoire RAM ne fonctionne qu'en mode lecture</p><br>","<br><br><p>la mémoire RAM permet de stocker des données et des programmes</p><br>","<br><br><p>une mémoire morte ne peut pas être utilisée</p><br>","<br><br><p>la mémoire RAM permet de stocker définitivement des données</p><br>","B","6","27",NULL,"1",NULL,"2020-03-25 16:37:08",NULL),
("1238","<p>La documentation de la bibliothèque random de Python précise que <code class='python'>random.randint(a,b)</code> renvoie un entier aléatoire \(N \) tel que \( a \le N \le b \).</p>
<p>Afin d’obtenir un entier choisi aléatoirement dans l’ensemble {-4 ; -2 ; 0 ; 2 ; 4}, après avoir importé la librairie random de Python, on peut utiliser l’instruction :</p>
","<p>random.randint(0,8)/2</p>
","<p>random.randint(0,8)/2 - 4</p>
","<p>random.randint(0,4)*2 - 2</p>
","<p>(random.randint(0,4) - 2) * 2</p>
","D","7","33",NULL,"1",NULL,"2020-05-05 01:06:54",NULL),
("1239","
<p>On souhaite écrire une fonction qui renvoie le maximum d'une liste d'entiers :</p>
<pre><code class='python'>def maximum(L):
    m = L[0]
    for i in range(1,len(L)):
        if .........:
            m = L[i]
    return m</code></pre>
<p>Par quoi faut-il remplacer les pointillés pour que cette fonction produise bien le résultat attendu ?</p>
","
<p>i &gt; m</p>
","
<p>L[i] &gt; m</p>
","
<p>L[i] &gt; L[i-1]</p>
","
<p>L[i] &gt; L[i+1]</p>
","B","7","33",NULL,"1",NULL,"2020-05-05 00:52:05",NULL),
("1240","
<p>Quelle est la valeur de la variable b à la fin de l'exécution du script suivant ?</p>
<pre><code class='python'>a = 2
b = 5
if a &gt; 8:
    b = 10
elif a &gt; 6:
    b = 3</code></pre>
","
<p>3</p>
","
<p>5</p>
","
<p>6</p>
","
<p>10</p>
","B","7","33",NULL,"1",NULL,"2020-05-05 00:46:45",NULL),
("1241","<p>On exécute le code suivant</p>
<pre><code class='python'>def calculPourcentage(prix,reduction):
    assert reduction &gt;= 0, 'la réduction doit être un nombre positif'
    assert reduction &lt; 100, 'la réduction doit être inférieure à 100'
    assert prix &gt; 0, 'le prix doit être un nombre strictement positif'
    
    remise = (reduction*prix)/100
    prix_remise = prix - remise
    
    return prix_remise
</code></pre>
<p>Quelle est la valeur renvoyée par l'appel <code class='python'>calculPourcentage(30,100)</code> ?</p>
","<p>AssertionError: la réduction doit être un nombre positif</p>
","<p>AssertionError: la réduction doit être inférieure à 100</p>
","<p>AssertionError: le prix doit être un nombre strictement positif</p>
","<p>70</p>
","B","7","33",NULL,"1",NULL,"2020-05-05 01:03:25",NULL),
("1242","<p>La fonction suivante calcule la racine carrée du double d’un nombre flottant.</p>
<pre><code class='python'>from math import sqrt
def racine_du_double(x):
    return sqrt(2*x)</code></pre>
<p>Quelle est la précondition sur l'argument de cette fonction ?</p>
"," 
<p>x &lt; 0</p>
","<p>x &gt;= 0</p>
","<p>2 * x &gt; 0</p>
","<p>sqrt(x) &gt;= 0</p>
","B","7","33",NULL,"1",NULL,"2020-05-05 00:48:27",NULL),
("1243","<br><br><p>On considère l'instruction suivante :</p><br><p>resultat = [0] * 7</p><br><p>Que contient la variable resultat après son exécution ?</p><br>","<br><br><p>0</p><br>","<br><br><p>[0]</p><br>","<br><br><p>[[0], [0], [0], [0], [0], [0], [0]]</p><br>","<br><br><p>[0, 0, 0, 0, 0, 0, 0]</p><br>","D","7","33",NULL,"1",NULL,"2020-03-25 16:38:02",NULL),
("1244","<br><p>On considère le code incomplet suivant qui recherche le maximum dans une liste.</p><br><pre><code>liste = [5,12,15,3,15,17,29,1]<br>iMax = 0<br>for i in range(1,len(liste)):<br>    ............<br>       iMax = i<br>print (liste[iMax])<br></code></pre><br><p>Par quoi faut-il remplacer la ligne pointillée ?</p><br>","<br><pre><code>if i > iMax:</code></pre><br>","<br><pre><code>if liste[i] > liste[iMax]:</code></pre><br>","<br><pre><code>if liste[i] > iMax:</code></pre><br>","<br><pre><code>if i > liste[iMax]:</code></pre><br>","B","8","39",NULL,"1",NULL,"2020-03-25 16:59:13",NULL),
("1245","<p>Un algorithme de recherche dichotomique sur un tableau trié de mille entiers s'exécute en 50 millisecondes.</p>
<p>Quelle est la durée approximative de son exécution sur un tableau trié d'un million d'entiers ?</p>
","<p>la même durée : environ 50 millisecondes</p>
","<p>une durée environ deux fois plus longue : environ 100 millisecondes</p>
","<p>une durée environ mille fois plus longue : environ 50 secondes</p>
","<p>une durée qui dépasserait l'année, car la complexité de l'algorithme est exponentielle</p>
","B","8","39",NULL,"1",NULL,"2020-05-09 03:13:24",NULL),
("1246","<p>On considère le code suivant de recherche d'une valeur dans une liste :</p>
<pre><code class='python'>def search(x, y):
    # x est la valeur à chercher
    # y est une liste de valeurs
    for i in range(len(y)):
        if x == y[i]:
            return i
    return None</code></pre>
<p>Quel est le coût de cet algorithme ?</p>
","<p>constant</p>
","<p>logarithmique</p>
","<p>linéaire</p>
","<p>quadratique</p>
","C","8","39",NULL,"1",NULL,"2020-05-05 01:17:42",NULL),
("1247","<p>On considère la fonction suivante :</p><br><pre><code>def trouverLettre(phrase,lettre):<br>    indexResultat = 0<br>    for i in range(len(phrase)):<br>        if phrase[i]== lettre:<br>            indexResultat = i<br>    return indexResultat<br></pre></code><br><p>Que renvoie l'appel <code>trouverLettre('Vive l’informatique','e')</code> ?</p>","<br><p>3</p><br>","<br><p>4</p><br>","<br><p>18</p><br>","<br><p>'e'</p><br>","C","8","39",NULL,"1",NULL,"2020-03-25 16:53:51",NULL),
("1248","<p>On exécute le script suivant :</p>
<pre><code class='python'>liste = [17, 12, 5, 18, 2, 7, 9, 15, 14, 20]
somme = 0
i = 0
while i &lt; len(liste):
    somme = somme + liste[i]
    i = i + 1
resultat = somme / len(liste)</code></pre>
<p>Quelle affirmation est <strong>fausse</strong> parmi les suivantes ?</p>
","<p>le corps de la boucle a été exécuté 10 fois</p>
","<p>à la fin de l'exécution la valeur de i est 9</p>
","<p>resultat contient la moyenne des éléments de liste</p>
","<p>len est une fonction</p>
","B","8","39",NULL,"1",NULL,"2020-05-05 01:15:45",NULL),
("1249","<br><br><p>Pour pouvoir utiliser un algorithme de recherche par dichotomie dans une liste, quelle précondition doit être vraie ?</p><br>","<br><br><p>la liste doit être triée</p><br>","<br><br><p>la liste ne doit pas comporter de doublons</p><br>","<br><br><p>la liste doit comporter uniquement des entiers positifs</p><br>","<br><br><p>la liste doit être de longueur inférieure à 1024</p><br>","A","8","39",NULL,"1",NULL,"2020-03-25 16:38:55",NULL),
("1250","Sachant que le répertoire courant contient les <code>fichiers fich.txt</code>, <code>mafich.txt</code> et <code>programme.py</code>, quel est le résultat de la commande  <code>ls fich*</code>  dans un shell Linux ?","<code>fich.txt  mafich.txt</code>","<code>mafich.txt</code>","<code>fich.txt</code>","<code>programme.py</code>","C","6","25",NULL,"1",NULL,"2020-03-25 16:46:22",NULL),
("1296","<h4>On considère la fonction suivante :</h4><br><br><br><pre><br>def mystere(tab1, tab2):<br>    '''reçoit deux tableaux de même longueur'<br>    for i in range(len(tab1)):<br>        if tab1[i] != tab2[i]:<br>            return False<br>    return True<br></pre><br><p>Que renvoie la fonction si l'appel est le suivant: </p><br><pre><br>mystere([12, 5, 18, 4],[12, 5, 18, 4])<br></pre><br>","3","faux","True","False","C","8","34",NULL,"1",NULL,"2020-03-26 09:24:12",NULL),
("1297","<h4>On considère un algorithme permettant de trier un tableau d'entiers par ordre croissant : </h4><br><br><br><P>A quel type tri correspond l'invariant de boucle ci-dessous : </p><br><P>tous les éléments d'indices 0 à i-1 sont déjà triés,<br><br><br>tous les éléments d'indices i à n sont de valeurs supérieures à ceux de la partie triée.<br></p><br>","tri par fusion","tri par sélection","tri par insertion","tri rapide","B","8","35",NULL,"1","1297_Selection_1.png","2020-03-26 10:18:49",NULL),
("1298","<h4>Invariant d'un algorithme de tri par sélection : </h4><br><p>On considère un algorithme de tri par sélection, dans lequel la fonction:<br><pre>echanger(tab[i], tab[j])<br></pre><br>effectue l'échange des ième et jième valeurs du tableau tab.<br></p><br><br><br><pre><br><P>nom: tri_sélection<br><br>paramètre: tab, tableau de n entiers, n>=2<br><br>Traitement:<br>pour i allant de 1 à n-1:<br>    pour j allant de i+1 à n:<br>        si tab[j] < tab[i]:<br>            echanger(tab[i], tab[j])<br>renvoyer tab<br></pre><br><b>Question: quel est l'invariant de boucle qui correspond précisément à cet algorithme ?</b>","Tous les éléments d'indice supérieur ou égal à i sont triés par ordre croissante","Tous les éléments d'indice compris entre 0 et i sont triés et les éléments d'indice supérieurs ou égal à i leurs sont tous supérieurs<br>","Tous les éléments d'indice supérieur ou égal à i sont non triés","Tous les éléments d'indice compris entre 0 et i sont triés, on ne peut rien dire sur les éléments d'indice supérieur ou égal à i","B","8","35",NULL,"1",NULL,"2020-03-26 12:15:25",NULL),
("1299","<h4>Algorithme de tri</h4><br><p>On considère un algorithme de tri dans lequel la fonction:<br><pre>echanger(tab[i], tab[j])<br></pre><br>effectue l'échange les ième et jième valeurs du tableau tab.<br></p><br><br><br><pre><br><P>nom: tri_mystere<br><br>paramètre: tab, tableau de n entiers, non trié, non vide<br><br>Traitement:<br>pour i allant de 1 à n-1:<br>    pour j allant de i+1 à n:<br>        si tab[j] < tab[i]:<br>            echanger(tab[i], tab[j])<br>renvoyer tab<br></pre><br><b>Question: quel est le type de tri qui correspond à cet algorithme ?</b>","tri par sélection","tri fusion","tri rapide","tri par insertion","A","8","35",NULL,"1",NULL,"2020-03-26 11:20:28",NULL),
("1300","<h4>Algorithme de tri</h4><br><p>On considère un algorithme de tri dans lequel la fonction:<br><pre>echanger(tab[i], tab[j])<br></pre><br>effectue l'échange les ième et jième valeurs du tableau tab.<br></p><br><br><br><pre><br><P>nom: tri_mystere<br><br>paramètre: tab, tableau de n entiers, non trié, non vide<br><br>Traitement:<br>pour i allant de 1 à n-1:<br>    pour j allant de i+1 à n:<br>        si tab[j] < tab[i]:<br>            echanger(tab[i], tab[j])<br>renvoyer tab<br></pre><br><b>Question: quel est le type de tri qui correspond à cet algorithme ?</b>","tri par sélection","tri fusion","tri rapide","tri par insertion","A","8","35",NULL,"1",NULL,"2020-03-26 11:33:47",NULL),
("1301","<h4>Invariant d'un algorithme de tri par insertion : </h4><br><p>On considère un algorithme de tri par insertion, dans lequel la fonction:<br><pre>echanger(tab[i], tab[j])<br></pre><br>effectue l'échange les ième et jième valeurs du tableau tab.<br></p><br><br><br><pre><br><P>nom: tri_insertion<br><br>paramètre: tab, tableau de n entiers, n >= 2<br><br>Traitement:<br>pour i allant de 2 à n:<br>    j = i<br>    tant que j > 1  et tab[j-1] > tab[j]:<br>        echanger(tab[j-1], tab[j])<br>        j = j-1<br>renvoyer tab<br></pre><br><b>Question: quel est l'invariant de boucle qui correspond précisément à cet algorithme ?</b>","Tous les éléments d'indice compris entre 0 et i sont triés et les éléments d'indice supérieurs ou égal à i leurs sont tous supérieurs","Tous les éléments d'indice supérieur ou égal à i sont triés par ordre croissant","Tous les éléments d'indice compris entre 0 et i sont triés, on ne peut rien dire sur les éléments d'indice supérieur ou égal à i","Tous les éléments d'indice supérieur ou égal à i sont non triés par ordre croissants","C","8","35",NULL,"1",NULL,"2020-03-26 12:16:11",NULL),
("1313","<br><p>On considère le dictionnaire</p><br><p>D={1: 3, 2: 4, 3: 5, 4: 2, 5: 1}</p><br><p>Quelle est la valeur de D[D[D[2]]] ?</p><br>","<br><p>2</p><br>","<br><p>3</p><br>","<br><p>4</p><br>","<br><p>5</p><br>","C","3","13",NULL,"1",NULL,"2020-04-01 16:36:30",NULL),
("1314","
<p>On considère la liste de listes suivante :</p>
<pre><code class='python'>tictactoe = [ ['X', 'O', 'O'],
              ['O', 'O', 'O'],
              ['O', 'O', 'X'] ]</code></pre>
<p>Quelle instruction permet d'obtenir une diagonale de 'X' ?</p>
","
<p>tictactoe[3] = 'X'</p>
","
<p>tictactoe[4] = 'X'</p>
","
<p>tictactoe[1][1] = 'X'</p>
","
<p>tictactoe[2][2] = 'X'</p>
","C","3","13",NULL,"1",NULL,"2020-05-08 21:05:47",NULL),
("1315","<br><br><p>Quelle est la valeur de l'expression [ 2*k + 1 for k in range(4) ] ?</p><br>","<br><br><p>[1,3,5,7]</p><br>","<br><br><p>[0,1,2,3]</p><br>","<br><br><p>[3,5,7,9]</p><br>","<br><br><p>[1,2,3,4]</p><br>","A","3","13",NULL,"1",NULL,"2020-03-29 18:40:22",NULL),
("1316","<br><br><p>Qu'est-ce que le CSV ?</p><br>","<br><br><p>Un langage de programmation</p><br>","<br><br><p>Un format de fichier permettant de stocker de l’information</p><br>","<br><br><p>Un algorithme permettant de rechercher une information dans un fichier</p><br>","<br><br><p>Un format de fichier permettant de définir le style d’une page web</p><br>","B","4","18",NULL,"1",NULL,"2020-03-29 18:40:31",NULL),
("1317","
<p>On a extrait les deux premières lignes de différents fichiers.</p>
<p>Déterminer celui qui est un authentique fichier CSV :</p>
","
<p>Nom,Pays,Temps<br />
Camille Muffat,France,241.45</p>
","
<p>Nom Pays Temps<br />
Camille Muffat France 241.45</p>
","
<p>[<br />
{ 'Nom': 'Camille Muffat', 'Pays': 'France', 'Temps': 241.45},</p>
","
<p>[<br />
{ Nom: 'Camille Muffat', Pays: 'France', Temps: 241.45},</p>
","A","4","18",NULL,"1",NULL,"2020-05-09 03:16:18",NULL),
("1318","<p>Quelle expression Python permet d’accéder au numéro de téléphone de Tournesol, sachant que le répertoire a été défini par l’affectation suivante :</p>
<pre><code class='python'>repertoire = [ {'nom':'Dupont', 'tel':'5234'},
               {'nom':'Tournesol', 'tel':'5248'},
               {'nom':'Dupond', 'tel':'3452'}]</code></pre>","<pre><code class='python'>repertoire['Tournesol']</code></pre>
","<pre><code class='python'>repertoire['tel'][1]</code></pre>","<pre><code class='python'>repertoire[1]['tel']</code></pre>
","<pre><code class='python'>repertoire['Tournesol'][tel]</code></pre>
","C","4","18",NULL,"1",NULL,"2020-05-12 14:14:35",NULL),
("1319","<p>On définit la fonction suivante qui prend en argument un tableau non vide d'entiers.</p>
<pre><code class='python'>def f(T):
    s = 0
    for k in T:
        if k == 8:
            s = s+1
    if s &gt; 1:
        return True
    else:
        return False</code></pre>
<p>Dans quel cas cette fonction renvoie-t-elle la valeur True ?</p>
","<p>dans le cas où 8 est présent au moins une fois dans le tableau T</p>
","<p>dans le cas où 8 est présent au moins deux fois dans le tableau T</p>
","<p>dans le cas où 8 est présent exactement une fois dans le tableau T</p>
","<p>dans le cas où 8 est présent exactement deux fois dans le tableau T</p>
","B","4","18",NULL,"1",NULL,"2020-05-08 21:08:06",NULL),
("1320","
<p>On exécute le script suivant :</p>
<pre><code class='python'>a = [1, 2, 3]
b = [4, 5, 6]
c = a + b</code></pre>
<p>Que contient la variable c à la fin de cette exécution ?</p>
","
<p>[5,7,9]</p>
","
<p>[1,4,2,5,3,6]</p>
","
<p>[1,2,3,4,5,6]</p>
","
<p>[1,2,3,5,7,9]</p>
","C","4","18",NULL,"1",NULL,"2020-05-08 21:12:12",NULL),
("1321","<p>On définit :</p>
<pre><code class='python'>T = [ {'fruit': 'banane', 'nombre': 25}, {'fruit': 'orange', 'nombre': 124},
      {'fruit': 'pomme', 'nombre': 75}, {'fruit': 'kiwi', 'nombre': 51} ]</code></pre>
<p>Quelle expression a-t-elle pour valeur le nombre de pommes ?</p>
","<p>T[2]['nombre']</p>
","<p>T[2,'nombre']</p>
","<p>T[3]['nombre']</p>
","<p>T[3,'nombre']</p>
","A","4","18",NULL,"1",NULL,"2020-05-09 03:17:15",NULL),
("1322","
<p>On considère cet extrait de fichier HTML représentant les onglets d'une barre de navigation :</p>
<pre><code class='html'>&lt;ul id='tab-nav'&gt;
    &lt;li&gt;&lt;a href='onglet1.html' class='tab-nav-active'&gt;Onglet 1&lt;/a&gt;&lt;/li&gt;
    &lt;li&gt;&lt;a href='onglet2.html'&gt;Onglet 2&lt;/a&gt;&lt;/li&gt;
    &lt;li&gt;&lt;a href='onglet3.html'&gt;Onglet 3&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</code></pre>
<p>Quel code CSS permet de mettre en bleu la couleur de fond des onglets et uniquement eux ?</p>
","
<p>tab-nav a {  background-color : blue; }</p>
","
<p>tab-nav, a {  background-color : blue; }</p>
","
<p>#tab-nav a {  background-color : blue; }</p>
","
<p>#tab-nav, a {  background-color : blue; }</p>
","C","5","22",NULL,"1",NULL,"2020-05-08 21:17:01",NULL),
("1323","
<p>Lors de la consultation d’une page HTML contenant un bouton auquel est associée la fonction suivante, que se passe-t-il quand on clique sur ce bouton ?</p>
<pre><code class='html'>function action(event) {
  this.style.color = 'blue'
}</code></pre>","
<p>le texte de la page passe en bleu</p>
","
<p>le texte du bouton passe en bleu</p>
","
<p>le texte du bouton est changé et affiche maintenant le mot 'bleu'</p>
","
<p>le pointeur de la souris devient bleu quand il arrive sur le bouton</p>
","B","5","22",NULL,"1",NULL,"2020-05-08 21:18:21",NULL),
("1324","
<p>Un fichier HTML contient la ligne suivante :<br /><code>&lt;p&gt;Coucou ! Ca va?&lt;/p&gt;</code></p>
<p>Quelle commande CSS écrire pour que le texte apparaisse en rose sur fond jaune ?</p>
","
<p>p { couleur: rose ; fond: jaune;}</p>
","
<p>&lt;p&gt; { color = pink background-color = yellow}</p>
","
<p>&lt;p&gt; { color = pink ; background-color: yellow} &lt;/p&gt;</p>
","
<p>p { color: pink ; background-color: yellow ;}</p>
","D","5","22",NULL,"1",NULL,"2020-05-08 21:20:27",NULL),
("1325","<br><br><p>Parmi les réponses suivantes, que permet d’effectuer la méthode POST du protocole HTTP&nbsp;?</p><br>","<br><br><p>Définir le style d’une page web</p><br>","<br><br><p>Pirater des données bancaire</p><br>","<br><br><p>Envoyer une page web vers le client</p><br>","<br><br><p>Envoyer les données saisies dans un formulaire HTML vers un serveur</p><br>","D","5","22",NULL,"1",NULL,"2020-03-29 18:41:59",NULL),
("1326","<p>On considère le code suivant</p>
<pre><code class='python'>def moyenne(notes) :
   somme=0
   for cpt in range(len(notes)):
      ...
   m=somme/len(notes)
   return m
</code></pre>
Par quoi faut-il remplacer le ligne en pointillé pour que cette fonction calcule la moyenne de la liste notes ?
","<pre><code class='python'>somme=somme+notes[cpt]</code></pre>","<pre><code class='python'>somme=notes[cpt]</code></pre>","<pre><code class='python'>somme=cpt</code></pre>","<pre><code class='python'>somme=somme+cpt</code></pre>","A","7","28",NULL,"1",NULL,"2020-05-12 14:28:09",NULL),
("1327","<p>Dans la console Linux, quelle commande faut-il exécuter pour effacer test0.csv ?</p><br><br>","rm test0.csv","del test0.csv","ls test0.csv","mv test0.csv","A","6","25",NULL,"1",NULL,"2020-04-03 11:04:07",NULL),
("1328","<p>Un navigateur affiche la page HTML suivante : </p>
<pre><code class='html'>&lt;html lang='fr'&gt;
&lt;head&gt;
   &lt;meta charset='utf-8'&gt;
   &lt;link rel='stylesheet' href='style.css'&gt;
   &lt;title&gt;Un bouton &lt;/title&gt;
&lt;/head&gt;
&lt;body&gt;
   &lt;button onclick='maFonction()'&gt; Cliquer ici &lt;/button&gt;
&lt;/body&gt;
&lt;script src='script.js'&gt; &lt;/script&gt;
&lt;/html&gt;
</code></pre>
Lorsqu'on clique sur le bouton, l'action déclenchée maFonction() est définie : ","dans le fichier HTML seul","dans le fichier style.css","dans une bibliothèque prédéfinie du navigateur","dans le fichier script.js","D","5","20",NULL,"1",NULL,"2020-05-12 14:24:42",NULL),
("1329","Parmi ces quatre éléments d'un formulaire, lequel est prévu pour envoyer les données saisies ?

 ","<pre><code class='html'>&lt;input name='file' type='file'/&gt;</code></pre>","<pre><code class='html'>&lt;input value='here we go !' type='submit'/&gt;</code></pre>","<pre><code class='html'>&lt;input name='email' type='email'/&gt;</code></pre>","<pre><code class='html'>&lt;input value='once again !' type='reset'/&gt;</code></pre>","B","5","20",NULL,"1",NULL,"2020-05-12 14:20:13",NULL),
("1330","<p>On exécute le code suivant :</p>
<pre><code class='python'>A = [ [1,2,3], [4,5,6], [7,8,9] ]
B = [ [0,0,0], [0,0,0], [0,0,0] ]
for i in range(3):
    for j in range(3):
        B[i][j] = A[j][i]</code></pre>
<p>Que vaut B à la fin de l'exécution ?</p>
","rien du tout, le programme déclenche une erreur d'exécution
","<pre><code class='python'>[ [3,2,1], [6,5,4], [9,8,7] ]</code></pre>

","<pre><code class='python'>[ [1,4,7], [2,5,8], [3,6,9] ]</code></pre>","<pre><code class='python'>[ [7,8,9], [4,5,6], [1,2,3] ]</code></pre>","C","3","12",NULL,"1",NULL,"2020-05-12 08:42:43",NULL),
("1331","<p>Les points \(A\), \(B\) et \(C\) sont de coordonnées \(A\,(1,5)\), \(\,B(3,4)\) et \(\,C(-2,1)\).</p><br><p>On veut créer un dictionnaire triangle dont les clés sont les noms des points et les valeurs leur couple de coordonnées.</p><br><p>Comment procéder ?</p><br>","<pre><code class='python'>triangle = ('A': (1,5), 'B': (3,4), 'C': (-2,1))</code></pre>","<pre><code class='python'>triangle = ['A': (1,5), 'B': (3,4), 'C': (-2,1)]</code></pre>","<pre><code class='python'>triangle = {'A': (1,5), 'B': (3,4), 'C': (-2,1)}</code></pre>","<pre><code class='python'>triangle = {['A',(1,5)], ['B',(3,4)], ['C',(-2,1)]}</code></pre>","C","3","9",NULL,"1",NULL,"2020-04-13 13:11:11",NULL),
("1332","<div class='md'>## Permissions</div><br><br><br><p>La commande ls -l entrée dans le terminal Linux a fait afficher les permissions d'un répertoire :</p><br><p>drwxr-xr-x</p><br><br>De quels droits dispose le groupe propriétaire du répertoire ?<br>","En lecture et en écriture","En lecture et en exécution","En écriture et en exécution","En écriture, en lecture et en exécution","B","6","25",NULL,"1",NULL,"2020-04-09 08:38:56",NULL),
("1333","<h2>Liens et image</h2><br><br><br><p>Un fichier html contient le code suivant :</p><br><pre><br>&ltimg src='Deuxcygnes.jpg' alt='Deux cygnes' usemap='#carte' /&gt<br>&ltmap name='carte'&gt<br>  &ltarea shape='rect' coords='36,10,322,302' alt='cygne 1' href='page1.html' /&gt<br>  &ltarea shape='rect' coords='326,10,604,302' alt='cygne 2' href='page2.html' /&gt<br>&lt/map&gt'<br></pre><br><p>L'image ci-dessous a un format 654x400 pixels.<br><br>Que ce passe t-il si on clique sur le cygne blanc ?</p>","Rien, l'image n'est pas cliquable.","Le navigateur ouvre la page 1.","Le navigateur ouvre la page 2.","Le navigateur pointe vers l'identifiant 'carte'.","C","5","19",NULL,"1","1333_cygnes.jpg","2020-04-09 16:57:55",NULL),
("1334","<h2>Sélecteurs</h2><br><br><br><p>On donne le code html :</p><br><pre><br>&ltp&gtpremier paragraphe&lt/p&gt<br>&ltdiv&gt&ltp&gtdeuxième paragraphe&lt/p&gt&lt/div&gt<br>&ltdiv&gttroisième paragraphe&lt/div&gt<br>&ltdiv&gt&ltarticle&gt&ltp&gtquatrième paragraphe&lt/p&gt&lt/article&gt&lt/div&gt<br></pre><br><p>ainsi que le code css :</p><br><pre><br>div > p {color : red;}<br></pre><br><p>Quel paragraphe prend la couleur rouge ?</p>","Le premier paragraphe","Le deuxième paragraphe","Le troisième paragraphe","Le quatrième paragraphe","B","5","19",NULL,"1",NULL,"2020-04-09 16:53:25",NULL),
("1335","Combien de bits faut-il au minimum pour coder le nombre décimal 4085 ?","4","12","2042","2043","B","2","2",NULL,"1",NULL,"2020-04-18 14:04:55",NULL),
("1336","<p>Quelle table de vérité correspond à l'expression <strong>(non(A) ou B) ?</strong></p><br><p>Remarque : dans les tables proposées, la première colonne donne les valeurs de A, la première ligne les valeurs de B.</p><br><br>","<table class='table_verite' border='1' width='100px'><br><tr><th>A/B</th><th>0</th><th>1</th></tr><br><tr><th>0</th><th>0</th><th>1</th></tr><br><tr><th>1</th><th>1</th><th>1</th></tr><br></table>","<table class='table_verite' border='1' width='100px'><br><tr><th>A/B</th><th>0</th><th>1</th></tr><br><tr><th>0</th><th>1</th><th>0</th></tr><br><tr><th>1</th><th>1</th><th>0</th></tr><br></table>","<table class='table_verite' border='1' width='100px'><br><tr><th>A/B</th><th>0</th><th>1</th></tr><br><tr><th>0</th><th>1</th><th>1</th></tr><br><tr><th>1</th><th>0</th><th>0</th></tr><br></table>","<table class='table_verite' border='1' width='100px'><br><tr><th>A/B</th><th>0</th><th>1</th></tr><br><tr><th>0</th><th>1</th><th>1</th></tr><br><tr><th>1</th><th>0</th><th>1</th></tr><br></table>","D","2","5",NULL,"1",NULL,"2020-04-18 14:12:41",NULL),
("1337","Un seul des réels suivants (écrits en base 10) n'a pas une écriture finie en base 2. Lequel ?","1,25","1,5","1,6","1,75","C","2","4",NULL,"1",NULL,"2020-04-18 14:14:29",NULL),
("1338","<p>On définit ainsi une liste P : </p>
<pre><code class='python'>P = [ {'nom':'Turing','prénom':'Alan','âge':28},
      {'nom':'Lovelace','prénom':'Ada','âge':27} ]</code></pre>
<p>Comment accéder à la chaîne de caractères 'Alan' ?</p>
","<pre><code class='python'>P[0]</code></pre>","<pre><code class='python'>P[1]</code></pre>","<pre><code class='python'>P[0]['prénom']</code></pre>","<pre><code class='python'>P[1]['prénom']</code></pre>","C","3","9",NULL,"1",NULL,"2020-05-12 08:48:53",NULL),
("1339","<p>On écrit la fonction suivante :</p>
<pre><code class='python'>def extreme(t, test):
    m = t[0]
    for x in t:
        if test(x,m):
            m = x
    return m</code></pre>
<p>On dispose d'une liste L dont les éléments sont des couples (nom, note).</p>
<p>Par exemple :</p>
<pre><code class='python'>L = [ ('Alice', 17),('Barnabé', 18),('Casimir', 17),('Doriane', 20),('Emilien', 15), ('Fabienne', 16) ]</code></pre>
<p>On aimerait que l'appel de fonction extreme(L, test) renvoie un couple présentant la note maximale.
Quelle définition de la fonction test peut-on utiliser ?</p>
","<pre><code class='python'>def test(a,b):
    return a[0] &lt; b[0]</code></pre>","<pre><code class='python'>def test(a,b):
    return a[0] &gt; b[0]</code></pre>","<pre><code class='python'>def test(a,b):
    return a[1] &lt; b[1]</code></pre>","<pre><code class='python'>def test(a,b):
    return a[1] &gt; b[1]</code></pre>","D","4","14",NULL,"1",NULL,"2020-05-09 03:19:37",NULL),
("1340","<p>Quelle utilisation faut-il avoir pour garantir qu'une transmission entre un client et un serveur sera chiffrée ?</p>","Lorsqu'on utilise le navigateur web Firefox","Lorsqu’on utilise la méthode POST","Lorsqu’on utilise le protocole HTTPS","Lorsqu’on utilise HTML et CSS","C","5","20",NULL,"1",NULL,"2020-04-19 09:38:32",NULL),
("1341","Comment s'appelle le service qui permet de faire le lien entre une IP et un nom de domaine ?","DNS","ARP","HTTP","Internet","A","6","24",NULL,"1",NULL,"2020-04-19 09:40:39",NULL),
("1342","Lequel de ces objets n'est pas un périphérique ?","le clavier","une clé USB","la carte graphique","la carte mère","D","6","26",NULL,"1",NULL,"2020-04-19 09:42:05",NULL),
("1343","<p>Quelle est la valeur de X/m à la fin de l'exécution du code suivant :</p>
<pre><code class='python'>L = [1,2,3,4,1,2,3,4,0,2]

X = 0
m = 0
for k in L:
    X = X + k
    m = m + 1</code></pre>","2","2.2","10","22","B","8","34",NULL,"1",NULL,"2020-05-09 03:20:48",NULL),
("1344","Quelle est la représentation binaire en complément à deux sur huit bits du nombre –3 ?","1000 0011","1111 1100","1111 1101","1 0000 0011","C","2","3",NULL,"1",NULL,"2020-04-19 09:52:33",NULL),
("1345","Un élève a écrit une fonction javascript qui détermine la moyenne des valeurs entrées par l'utilisateur dans un formulaire de sa page HTML. Il place sa fonction javascript :
","entre la balise &lt;js&gt; et la balise &lt;/js&gt;","entre la balise &lt;code&gt; et la balise &lt;/code&gt;","entre la balise &lt;script&gt; et la balise &lt;/script&gt;","entre la balise &lt;javascript&gt; et la balise &lt;/javascript&gt;","C","5","22",NULL,"1",NULL,"2020-05-09 03:25:01",NULL),
("1346","Quelle méthode doit utiliser la requête envoyée au serveur lorsque vous entrez votre identifiant et votre mot de passe dans un formulaire sécurisé ?","GET","POST","FORM","SUBMIT","B","5","21",NULL,"1",NULL,"2020-04-19 10:08:45",NULL),
("1347","Laquelle de ces écritures ne désigne pas une adresse IP ?","127.0.0.1","207.142.131.245","192.168.229.48","296.141.2.4","D","6","24",NULL,"1",NULL,"2020-04-19 10:10:56",NULL),
("1348","Dans un terminal sous Linux, quelle commande permet d'afficher la liste des fichiers du répertoire courant ?","ls","cd","mv","rm","A","6","25",NULL,"1",NULL,"2020-04-19 10:12:37",NULL),
("1349","Vous soupçonnez que des paquets se perdent entre votre ordinateur et leur destination. Quelle commande utiliseriez-vous pour trouver la source du problème efficacement ?<br>","ping","ipconfig","traceroute","nslookup","C","6","24",NULL,"1",NULL,"2020-04-19 10:14:35",NULL),
("1350","Quelles sont les quatre parties distinctes de l’architecture de Von Neumann ?","L’unité logique, l’unité de contrôle, la mémoire et les dispositifs d’entrée-sortie","L’écran, le clavier, le disque dur et le micro-processeur","Le disque dur, le micro-processeur, la carte-mère et la carte graphique","La mémoire des programmes, la mémoire des données, les entrées-sorties et l’unité logique","A","6","23",NULL,"1",NULL,"2020-04-19 10:16:54",NULL),
("1351","Quel composant électronique, inventé vers le milieu du 20e siècle, a permis le développement des ordinateurs actuels ?","le condensateur","la résistance","le transistor","la diode","C","6","27",NULL,"1",NULL,"2020-04-19 10:18:33",NULL),
("1352","<p>Karine écrit une bibliothèque Python, nommée GeomPlan, de géométrie plane dont voici un extrait :</p>
<pre><code class='python'>import math
def aireDisque(R):
    return math.pi * R**2</code></pre>
<p>Gilles utilise cette bibliothèque pour calculer l'aire d'un disque de rayon 8. Laquelle des instructions suivantes renvoie un message d'erreur ?</p>
","<pre><code class='python'>import GeomPlan
GeomPlan.aireDisque(8)</code></pre>","<pre><code class='python'>import GeomPlan
aireDisque(8)</code></pre>","<pre><code class='python'>from GeomPlan import *
aireDisque(8)</code></pre>","<pre><code class='python'>from GeomPlan import aireDisque
aireDisque(8)</code></pre>","B","7","33",NULL,"1",NULL,"2020-05-08 21:23:23",NULL),
("1353","Quelle est la représentation binaire, en complément à 2 sur 8 bits, de l'entier négatif –25 ?","0001 1001","1001 1001","1110 0110","1110 0111","D","2","3",NULL,"1",NULL,"2020-04-19 11:09:42",NULL),
("1354","<p>Quelle est l’écriture décimale de l’entier dont la représentation en binaire non signé est 0001 0101 ?</p>","15","21","111","420","B","2","2",NULL,"1",NULL,"2020-04-22 08:36:25",NULL),
("1355","<p>Dans le bloc <code class='html'>&lt;head&gt;</code> d'un fichier HTML, afin d'encoder les caractères avec le standard Unicode/UTF-8  on insère la ligne : </p><br><pre><code class='html'>&lt;meta http-equiv='Content -Type' content='text/html; charset=UTF-8'&gt;</code></pre><br><p>Pourquoi cela ?</p><br>","UTF-8 est l'encodage Linux","ASCII est une vieille norme, il est temps d'en changer","UTF-8 est une norme conçue pour permettre un affichage correct des caractères spéciaux sur tout système d'exploitation","UTF-8 est un encodage qui protège mieux contre le piratage informatique","C","2","6",NULL,"1",NULL,"2020-04-22 08:40:36",NULL),
("1356","Soient a et b deux booléens. L’expression booléenne <code class='python'> NOT(a AND b) OR a </code> est équivalente à :","False","True","NOT(b)","NOT(a) OR NOT(b)","B","2","5",NULL,"1",NULL,"2020-04-22 08:45:10",NULL),
("1357","La recherche dichotomique est un algorithme rapide qui permet de trouver ou non la présence d’un élément dans un tableau. Mais, pour l’utiliser, une contrainte est indispensable, laquelle ?","le tableau ne contient que des nombres positifs","la longueur du tableau est une puissance de 2","le tableau est trié en ordre croissant","le tableau ne contient pas la valeur 0","C","8","37",NULL,"1",NULL,"2020-04-22 08:46:59",NULL),
("1358","Une seule des affirmations suivantes est vraie :","L'algorithme des k plus proches voisins a pour but de déterminer les k plus proches voisins d'une observation dans un ensemble de données.","L'algorithme des k plus proches voisins a pour but de déterminer la classe d'une observation à partir des classes de ses k plus proches voisins.","L'algorithme des k plus proches voisins a pour but de déterminer dans un ensemble de données le sous-ensemble à k éléments qui sont les plus proches les uns des autres.","L'algorithme des k plus proches voisins a pour but de déterminer les éléments d'un ensemble de données appartenant à une même classe.","B","8","36",NULL,"1",NULL,"2020-04-22 08:48:22",NULL),
("1359","<p>La variable sequence contient une liste de lettres, éventuellement répétées, choisies parmi 'A', 'B', 'C', 'D'. On veut créer un dictionnaire effectifs associant à chaque lettre le nombre de fois qu'elle apparaît dans la liste sequence. </p><br><p>Par exemple si sequence contient ['A', 'B', 'B', 'D', 'B', 'A'],</p><br><p>effectifs doit contenir {'A':2, 'B':3, 'C':0, 'D':1}.</p><br><p>Parmi les scripts suivants, lequel réalise cet objectif ?</p><br>","<pre><code class='python'><br>effectifs = {'A':0, 'B':0, 'C':0, 'D':0}<br>for lettre in sequence:<br>    effectifs[lettre] = effectifs[lettre] + 1<br></code></pre>","<pre><code class='python'><br>effectifs = {}<br>for lettre in sequence:<br>    effectifs[lettre] = effectifs[lettre] + 1<br></code></pre>","<pre><code class='python'><br>effectifs = {'A':0, 'B':0, 'C':0, 'D':0}<br>for lettre in effectifs.keys():<br>    effectifs[lettre] = len([lettre in effectifs])<br></code></pre>","<pre><code class='python'><br>effectifs = {}<br>for lettre in effectifs.keys():<br>    effectifs[lettre] = len([lettre in effectifs])<br></code></pre>","A","3","9",NULL,"1",NULL,"2020-04-22 09:12:43",NULL),
("1360","Quel est le type de l'expression f(4) si la fonction f est définie par :<br><pre><code class='python'><br>def f(x):<br>    return (x, x**2)<br></code></pre><br><br>","un entier","un flottant","une liste","un tuple","D","3","8",NULL,"1",NULL,"2020-04-22 09:14:39",NULL),
("1361","<p>On considère la fonction suivante :</p><br><pre><code class='python'><br>def somme(tab):<br>    s = 0<br>    for i in range(len(tab)):<br>        ......<br>    return s<br></code></pre><br><p>Par quelle instruction faut-il remplacer les points de suspension pour que l'appel somme([10,11,12,13,14]) renvoie 60 ?</p><br><br><br><br>","<code class='python'>s = tab[i]</code>","<code class='python'>s = s + tab[i]</code>","<code class='python'>tab[i] = tab[i] + s</code>","<code class='python'>s = s + i</code>","B","3","10",NULL,"1",NULL,"2020-04-22 09:18:09",NULL),
("1362","On dispose d'une table tab constituée d'une liste de trois sous-listes contenant chacune quatre caractères.<br><pre><code class='python'><br>tab = [['A', 'B', 'C', 'D'],<br>       ['E', 'F', 'G', 'H'],<br>       ['I', 'J', 'K', 'L'] ]<br></code></pre><br>Parmi les propositions suivantes, laquelle permet de convertir cette table en une liste L contenant dans l'ordre, ligne par ligne, les 12 caractères de tab ?<br><pre><code class='python'><br># à la fin, on a l'égalité :<br>L == [  'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L' ]<br></code></pre><br>","<pre><code class='python'><br>L = []<br>for i in range(3):<br>    for j in range(4):<br>        L.append(tab[i][j])<br></code></pre>","<pre><code class='python'><br>L = []<br>for i in range(4):<br>    for j in range(3):<br>        L.append(tab[i][j])<br></code></pre>","<pre><code class='python'><br>L = []<br>for i in range(3):<br>    L.append(tab[i])<br></code></pre>","<pre><code class='python'><br>L = []<br>for i in range(4):<br>    L.append(tab[i])<br></code></pre>","A","3","10",NULL,"1",NULL,"2020-04-22 09:22:59",NULL),
("1363","La fonction ci-dessous prend en argument deux nombres entiers.<br><pre><code class='python'><br>def f(n1,n2):<br>    etendue = max(n1,n2)-min(n1,n2)<br>    moyenne = (n1+n2)/2<br>    return etendue,moyenne<br></code></pre><br>Quel est le type de la valeur renvoyée par un appel à cette fonction ?<br>","un entier","un réel(ou flottant)","un tuple","une liste","C","3","8",NULL,"1",NULL,"2020-04-22 09:25:08",NULL),
("1364","Un fichier CSV …","ne peut être lu que par un tableur","est l'unique format utilisé pour construire une base de données","est un fichier texte","est un format propriétaire","C","4","18",NULL,"1",NULL,"2020-04-22 09:26:19",NULL),
("1365","On considère le formulaire HTML suivant :<br><pre><code class='html'><br>&lt;form action='action.php' method='get' name='prenom'&gt;<br>    Prénom : <br>        &lt;input type='text' id='champ1' name='p'/&gt;<br>    &lt;br/&gt;<br>    &lt;input type='hidden' name='util' value='1549'/&gt;<br>      &lt;input value='Envoi du prénom' type='submit'/&gt;<br>&lt;/form&gt;<br></code></pre><br>Le prénom entré par l'utilisateur est contenu dans :<br>","l’élément dont l’attribut id est  champ1","l’élément nommé prénom","l’élément dont l’attribut value est  Envoi du prénom","l’élément dont l’attribut type est  hidden","A","5","21",NULL,"1",NULL,"2020-04-22 09:29:59",NULL),
("1366","Dans l’architecture générale de Von Neumann, la partie qui a pour rôle d’effectuer les opérations de base est :","l'unité de contrôle","la mémoire","l'unité arithmétique et logique","les dispositifs d'entrée-sortie","C","6","23",NULL,"1",NULL,"2020-04-22 09:31:23",NULL),
("1367","Parmi les adresses suivantes, laquelle est une adresse Ethernet non valide ?","8D:A9:D5:67:E6:F3","8d:a9:d5:67:e6:f3","8H:A9:D5:67:E6:F3","FF:A9:D5:67:E6:F3","C","6","24",NULL,"1",NULL,"2020-04-22 09:32:41",NULL),
("1368","On exécute le script suivant : <br><pre><code class='python'><br>a = 10<br>if a < 5:<br>    a = 20<br>elif a < 100:<br>    a = 500<br>elif a < 1000:<br>    a = 1<br>else:<br>    a = 0<br></code></pre><br>Quelle est la valeur de la variable a à la fin de cette exécution ?<br>","1","10","20","500","D","7","28",NULL,"1",NULL,"2020-04-22 09:34:22",NULL),
("1369","Voici un programme en Python :<br><pre><code class='python'><br>tableau = [8, 1, 6, 6, 9, 6, 9, 3, 8, 6, 3, 4, 9, 6, 7, 1]<br>m = -1<br>rang = -1<br>for i in range(len(tableau)):<br>    if m <= tableau[i]:<br>        m = tableau[i]<br>        rang = i<br></code></pre><br>Quelle est la valeur du couple (m,rang) à la fin de l'exécution du programme ?<br>","(1,1)","(1,15)","(9,4)","(9,12)","D","7","28",NULL,"1",NULL,"2020-04-22 09:36:24",NULL),
("1370","Le résultat de l'addition en binaire 101001 + 101 est égal au nombre binaire :","101102","010101","101110","110000","C","2","2",NULL,"1",NULL,"2020-04-22 11:41:13",NULL),
("1371","La variable x contient la valeur 3, la variable y contient la variable 4.<br>Quelle expression s'évalue en True parmi les quatre propositions suivantes ?<br>","x == 3 or y == 5","x == 3 and y == 5","x != 3 or y == 5","y < 4","A","2","5",NULL,"1",NULL,"2020-04-22 11:43:19",NULL),
("1372","On considère l'expression logique <code>(a or b) and a</code><br><p> Quelle est sa table de vérité ?</p>","<table class='table_verite' border='1' width='100px'><br><tr><th>a</th><th>b</th><th>?</th></tr><br><tr><td>False</td><td>False</td><td>False</td></tr><br><tr><td>False</td><td>True</td><td>True</td></tr><br><tr><td>True</td><td>False</td><td>True</td></tr><br><tr><td>True</td><td>True</td><td>True</td></tr><br></table>","<table class='table_verite' border='1' width='100px'><br><tr><th>a</th><th>b</th><th>?</th></tr><br><tr><td>False</td><td>False</td><td>False</td></tr><br><tr><td>False</td><td>True</td><td>False</td></tr><br><tr><td>True</td><td>False</td><td>True</td></tr><br><tr><td>True</td><td>True</td><td>True</td></tr><br></table>","<table class='table_verite' border='1' width='100px'><br><tr><th>a</th><th>b</th><th>?</th></tr><br><tr><td>False</td><td>False</td><td>False</td></tr><br><tr><td>False</td><td>True</td><td>False</td></tr><br><tr><td>True</td><td>False</td><td>False</td></tr><br><tr><td>True</td><td>True</td><td>True</td></tr><br></table>","<table class='table_verite' border='1' width='100px'><br><tr><th>a</th><th>b</th><th>?</th></tr><br><tr><td>False</td><td>False</td><td>False</td></tr><br><tr><td>False</td><td>True</td><td>False</td></tr><br><tr><td>True</td><td>False</td><td>False</td></tr><br><tr><td>True</td><td>True</td><td>False</td></tr><br></table>","B","2","5",NULL,"1",NULL,"2020-04-22 12:41:12",NULL),
("1373","La représentation en complément à deux sur 8 bits de l’entier –42 est :","–00101010","10101010","11010101","11010110","D","2","3",NULL,"1",NULL,"2020-04-22 12:43:14",NULL),
("1374","Soit une liste définie de la manière suivante : <code class='python'>liste = [18, 23, 45, 38, 12]</code><br>On exécute l’instruction <code class='python'>liste.append(45)</code>, la liste a alors pour valeur :<br>","[18, 23, 38, 12, 45]","[18, 23, 38, 12]","[45, 18, 23, 45, 38, 12]","[18, 23, 45, 38, 12, 45]","D","3","10",NULL,"1",NULL,"2020-04-22 12:45:57",NULL),
("1375","On définit ainsi une liste P : <br><pre><code class='python'><br>P = [ {'nom':'Turing','prénom':'Alan','âge':28},<br>      {'nom':'Lovelace','prénom':'Ada','âge':27} ]<br></code></pre><br>Que fait alors l'instruction <code class='python'>P[1]['âge'] = 25</code> ?<br>","elle modifie la valeur de la clé âge du deuxième élément de la liste P","elle modifie la valeur de la clé âge du premier élément de la liste P","elle donne la longueur de la liste P","elle donne la longueur du premier élément de la liste P","A","3","9",NULL,"1",NULL,"2020-04-22 12:48:58",NULL),
("1376","Quelle est la valeur de :
<pre><code class='python'>[ x - y for x in range(4) for y in range(3) if x &gt; y ]</code></pre>","[1, 2, 1, 3, 2, 1]","[1, 2, 3, 1, 2, 1]","[1, 2, 3, 3, 2, 1]","[1, 2, 1, 2, 3, 1]","A","3","11",NULL,"1",NULL,"2020-05-09 03:27:54",NULL),
("1377","Une table d’un fichier client contient le nom, le prénom et l’identifiant des clients sous la forme : 
<pre><code class='python'>clients = [('Dupont', 'Paul', 1),
           ('Durand', 'Jacques', 2),
           ('Dutronc', 'Jean', 3),
           ...]</code></pre>
<p>En supposant que plusieurs clients se prénomment Jean, que vaut la liste x après l’exécution du  code suivant ?</p>
<pre><code class='python'>x = []
for i in range(len(clients)):
    if clients[i][1] == 'Jean':
       x = clients[i]</code></pre>
","Une liste de tuples des noms, prénoms et numéros de tous les clients prénommés Jean","Une liste des numéros de tous les clients prénommés Jean","Un tuple avec le nom, prénom et numéro du premier client prénommé Jean","Un tuple avec le nom, prénom et numéro du dernier client prénommé Jean","D","3","8",NULL,"1",NULL,"2020-05-09 03:29:03",NULL),
("1378","Quel est l'Objet de base du modèle relationnel ?
","La clef primaire","La clef étrangère","La relation","L'union","C","13","48",NULL,"1",NULL,"2020-05-04 09:30:01",NULL),
("1379","Trouvez la phrase qui est correcte:
","Une relation est implémentée dans une base de données par une table.","Un tuple contient des lignes","Les termes tuple, ligne, vecteur et enregistrement sont tous synonymes."," ","A","13","48",NULL,"1",NULL,"2020-05-04 09:34:33",NULL),
("1380","<p>On dispose d’une table de données de villes européennes. On utilise ensuite l’algorithme des k-plus proches voisins pour compléter automatiquement cette base avec de nouvelles villes.</p>
<table border='1'><tr><th>Ville</th><th>Pays</th><th>Distance jusqu’à Davos</th></tr><tr><td>Berne</td><td>Suisse</td><td>180 km</td></tr><tr><td>Innsbruck</td><td>Autriche</td><td>130 km</td></tr><tr><td>Milan</td><td>Italie</td><td>150 km</td></tr><tr><td>Munich</td><td>Allemagne</td><td>200 km</td></tr><tr><td>Stuttgart</td><td>Allemagne</td><td>225 km</td></tr><tr><td>Turin</td><td>Italie</td><td>250 km</td></tr><tr><td>Zurich</td><td>Suisse</td><td>115 km</td></tr></table><p>En appliquant l’algorithme des 4 plus proches voisins, quel sera le pays prédit pour la ville de Davos ?</p>","Allemagne","Autriche","Italie","Suisse","D","8","36",NULL,"1",NULL,"2020-05-09 16:32:12",NULL),
("1381","Parmi les phrases suivantes, laquelle est vraie ?
","un algorithme glouton fournit toujours une solution optimale","un algorithme glouton est généralement moins complexe que les autres méthodes d’optimisation ","un algorithme glouton étudie tous les cas possibles pour déterminer l’optimal ","un algorithme glouton peut revenir en arrière en cas de blocage","B","8","38",NULL,"1",NULL,"2020-06-07 09:49:46",NULL),
("1382","<pre><code class='python'>
a = '2'
b = '3'
c = a + b
d = 4 * 'a'
</code></pre>
Que contiennent les variables <code>c</code> et <code>d</code>  à la fin de l’exécution du  script  Python précédent  ?
","<code>c</code> contient 23 et <code>d</code> contient 8","<code>c</code> contient 5 et <code>d</code> contient 8","<code>c</code> contient <code>'23'</code> et <code>d</code> contient <code>'aaaa'</code>","<code>c</code> contient <code>'23'</code> et <code>d</code> contient <code>'2222'</code>","C","7","28",NULL,"1",NULL,"2020-09-05 15:18:12",NULL),
("1383","<pre><code class='python'>
a = 1
b = 2
c = 3
</code></pre>
Par quelle séquence d'instructions faut-il compléter le script Python ci-dessus pour obtenir l'état :

<table><tbody><tr><td>  a</td>
<td>  b</td>
<td>  c </td>
</tr><tr><td>  2</td>
<td>  3</td>
<td> 1</td>
</tr></tbody></table>","<pre>
<code class='python'>
a = b
b = c
c = a
</code>

</pre>","<pre>
<code class='python'>
c = a
b = c
a = b
</code>

</pre>","<pre>
<code class='python'>
e = b
c = a
b = c
a = e
</code>

</pre>","<pre>
<code class='python'>
c = a + b + c
b = c - (a + b)
a = c - (a + b)
c = c - (a + b)
</code>

</pre>","D","7","28",NULL,"1",NULL,"2020-09-05 15:37:05",NULL),
("1384","<br /><p> Une entité est contenue dans </p>

","une relation","un attribut","un domaine","aucune des trois propositions","A","13","47",NULL,"1",NULL,"2020-09-06 16:14:40",NULL),
("1385","<br /><p> Une relation est : </p>

","un ensemble d'attributs","un ensemble de domaines","un ensemble d'entités","aucune des trois propositions","C","13","47",NULL,"1",NULL,"2020-09-06 16:15:09",NULL),
("1386","<br /><p>Dans le modèle relationnel un objet est représenté par :</p>
","un attribut","un domaine","un n-uplet","aucune des trois propositions","C","13","47",NULL,"1",NULL,"2020-09-06 16:19:59",NULL),
("1387","<br /><p> On considère l'ensemble suivant :</p>
<pre><code class='html'>
Joueur = {
('Messi', 'Lionel', 'Argentin', 'FC Barcelone', '33' ), 
('Neymar', 'Junior', 'Brésilien', 'PSG', '28'),
('Mbappé', 'Kilian', 'Français', 'PSG', '21')} 
</code></pre>
L'ensemble joueur est :
","une entité","un attribut","une relation","aucune des trois propositions","C","13","47",NULL,"1",NULL,"2020-09-06 17:24:15",NULL),
("1388","<br /><p> On considère l'ensemble suivant :</p>
<pre><code class='html'>
Joueur = {
('Messi', 'Lionel', 'Argentin', 'FC Barcelone', '33' ), 
('Neymar', 'Junior', 'Brésilien', 'PSG', '28'),
('Mbappé', 'Kilian', 'Français', 'PSG', '21')} 
</code></pre>
Combien d'attributs cet ensemble comporte-t-il ? ","trois","cinq","un","aucune des trois propositions","B","13","47",NULL,"1",NULL,"2020-09-06 16:41:52",NULL),
("1389","<br /><p>On considère l'ensemble suivant :</p>
<pre><code class='html'>
<em>Note</em> = {
('Fonda', 'James', 'NSI', 16), 
('Pierce', 'Nica', 'Philosophie', 13),
('Hanna', 'Vincent', 'Histoire', 11),
('Connor', 'John', 'EPS', 14)} 
</code></pre>
Quel est le schéma associé a cet ensemble ? 
","<pre><code class='html'>
<em>Note</em>(nom <b> String </b> , prenom <b> String</b>, discipline <b> String</b>, note_sur_vingt <b>Int</b>)
</code></pre>","<pre><code class='html'>
<em>Note</em>(nom <b> String </b> , prenom <b> String</b>, discipline <b> String</b>, note_sur_vingt <b>String</b>)
</code></pre>","<pre><code class='html'>
<em>Note</em>(nom <b> String </b> , prenom <b> String</b>, note_sur_vingt <b>Int</b>, discipline <b> String</b>)
</code></pre>","aucune des trois propositions","A","13","47",NULL,"1",NULL,"2020-09-06 17:10:09",NULL),
("1390","<br /><p>On considère l'ensemble suivant :</p>
<pre><code class='html'>
<em>Note</em> = {
('Fonda', 'James', 'NSI', 16), 
('Pierce', 'Nica', 'Philosophie', 13),
('Hanna', 'Vincent', 'Histoire', 11),
('Connor', 'John', 'EPS', 14),
('Ripley','Hélène', 'théatre', 16),
('Amy', 'Brenneman', 'Mathématiques', 18)} 
</code></pre>
Combien d'entités cet ensemble possède-t-il ?
","quatre","six","zéro","aucune des trois propositions","B","13","47",NULL,"1",NULL,"2020-09-06 17:18:31",NULL),
("1391","<div class='md'>## Pop</div>
<br /><p>Que vaut maliste à l'issue du code ci-dessous
</p>
<pre><code class='html'>
maliste=[ 5, 3, 2, 6, 5, 4]
maliste=maliste.pop()
</code></pre>
","4","[ 5, 3, 2, 6, 5]
","[ 5, 3, 2, 6, 5, 4]
","Le code renvoie une erreur","A","3","10",NULL,"1",NULL,"2020-09-14 11:23:17",NULL),
("1392","<div class='md'>## Remove</div>
<br /><p>Que vaut maliste a l'issue du code ci-dessous :</p>
<pre><code class='html'>
maliste=[ 5, 3, 2, 6, 5, 4]
maliste=maliste.remove(3)
</code></pre>
","[ 5, 2, 6, 5, 4]","[ 5, 3, 2, 5, 4]","None","le code renvoie une erreur","C","3","10",NULL,"1",NULL,"2020-09-14 11:28:09",NULL),
("1393","<div class='md'>## Remove</div>
<br /><p>Que vaut truc à l'issue du code ci-dessous :</p>
<pre><code class='html'>
truc=[ 5, 3, 2, 6, 5, 4]
truc.remove(3)
</code></pre>","[ 5, 3, 2, 6, 5, 4]","[ 5, 2, 6, 5, 4]","[ 5, 3, 6, 5, 4]","[ 5, 3, 2, 5, 4]","B","3","10",NULL,"1",NULL,"2020-09-14 11:30:54",NULL),
("1394","<div class='md'>## constructon de liste</div>
<br /><p>Pour créer la liste des entiers de 1 à 7 quel code utiliserez vous ?</p>

","<pre><code class='html'>
lst=[]
for i in range(7) :
    lst+=i
</code></pre>
","<pre><code class='html'>
lst=[]
for i in range(7) :
    lst=[i+1]
</code></pre>
","<pre><code class='html'>
lst=[]
for i in range(7) :
    lst.insert(i)
</code></pre>
","<pre><code class='html'>
lst=[]
for i in range(7) :
    lst.append(i+1)
</code></pre>
","D","3","10",NULL,"1",NULL,"2020-09-14 11:38:45",NULL),
("1395","<div class='md'>## Insert</div>
<br /><p>Que vaut lst à l'issue de ce code :</p>
<pre><code class='html'>

lst=[1,2,3,4,5,6]
lst.insert(2,3)
</code></pre>
","[1, 2, 3, 3, 4, 5, 6]
","[1, 2, 3, 4, 5, 6, 3]
","[1, 2, 3, 2, 4, 5, 6]
","le code renvoie une erreur","A","3","10",NULL,"1",NULL,"2020-09-14 11:42:22",NULL),
("1396","<div class='md'>## del</div>
<br /><p>Que vaut lst à l'issue de ce code :</p>
<pre><code class='html'>
lst=[1,2,3,4,5,6]
del lst[2]
</code></pre>
","[1, 3, 4, 5, 6]
","[4, 5, 6]
","[1, 2, 4, 5, 6]
","le code renvoie une erreur","C","3","10",NULL,"1",NULL,"2020-09-14 11:46:06",NULL),
("1397","<div class='md'>### Test récursif</div>

<p>On a saisi le code suivant :</p>
<pre><code class='python'>
def mystere(n):
    if n&gt;0 :
        return mystere(n-2)
    else :
        return n==0
</code></pre> 

<p>Que retourne la commande  suivante ?</p>
<pre><code class='python'>mystere(4)</code></pre>  
","0","False","True","L'exécution génère une erreur.","C","15","58",NULL,"1",NULL,"2020-09-26 15:51:15",NULL),
("1398","<div class='md'>### Fonction récursive</div>
<br /><p>Laquelle de ces fonctions retourne True lorsqu'on exécute :</p>
<pre><code class='python'>f(5)</code></pre>

","<pre><code class='python'>def f(n):
    if n==0 :
        return True
    else :
        return f(n-2)
</code></pre>","<pre><code class='python'>def f(n):
    if n&lt;=0 :
        return True
    else :
        f(n-2)
</code></pre>","<pre><code class='python'>def f(n):
    if n&lt;=0 :
        return True
    return f(n-2)
</code></pre>","<pre><code class='python'>def f(n):
    if n==0 :
        return True
    f(n-2)
</code></pre>","C","15","58",NULL,"1",NULL,"2020-09-26 15:51:38",NULL),
("1399","<div class='md'>### Affichages récursifs</div>
<br /><p>On a saisi le code suivant :</p>
<pre><code class='python'>def affiche(n):
    print(n)
    if n&gt;=0:
        affiche(n-1)
</code></pre>
Quel affichage obtient-on en exécutant <code class='python'>affiche(3)</code>","<pre><code class='python'>3
2
1
0
</code></pre>","<pre><code class='python'>0
1
2
3
</code></pre>","<pre><code class='python'>3
2
1
0
-1
</code></pre>","<pre><code class='python'>3
</code></pre>","C","15","58",NULL,"1",NULL,"2020-09-26 15:51:57",NULL),
("1400","<div class='md'>### Fonction récursive de deux variables</div>

Une seule des fonctions définies ci-dessous retourne <code class='python'>'ccccc'</code> à l'appel de  <code class='python'>replique(5,'c')</code>. Déterminer laquelle. 
","<pre><code class='python'>def replique(a,b):
    if a==1:
        return b
    else :
        return replique( a-1 , b+b)
</code></pre>","<pre><code class='python'>def replique(a,b):
    if a==1:
        return b
    elif a%2 == 0:
        return replique( a-2 , b+b)
    else :
        return b + replique( a-2 , b+b)
</code></pre>","<pre><code class='python'>def replique(a,b):
    if a==1:
        return b
    elif a%2 == 0:
        return replique( a//2 , b+b)
    else :
        return b + replique( a//2 , b+b)
</code></pre>","<pre><code class='python'>def replique(a,b):
    if a==1:
        return b
    else :
        replique( a-1 , b+b)
</code></pre>","C","15","58",NULL,"1",NULL,"2020-09-26 15:52:20",NULL),
("1401","<div class='md'>## Fonction récursive et type str </div>

<p>On a saisi le code suivant :</p>
<pre><code class='python'>def copy(n,s):
    if n==0:
        return s
    return copy(n-1, s+s)
</code></pre>
Que retourne l'instruction <code class='python'>copy(3,'A')</code> ?
","<code class='python'>'AAA'</code>","<code class='python'>'AAAAAA'</code>","<code class='python'>'AAAAAAAA'</code>","<code class='python'>'3A'</code>","C","15","58",NULL,"1",NULL,"2020-09-14 17:18:51",NULL),
("1402","<div class='md'>## Affichages récursifs</div>

<p>On a saisi le code suivant :</p>
<pre><code class='python'>def affiche(n):
    if n&gt;0:
        affiche(n-1)
    print(n)
</code></pre>

Quel affichage obtient-on en exécutant <code class='python'>affiche(3)</code>
","<pre><code class='python'>3
2
1
0
</code></pre>","<pre><code class='python'>3
2
1
</code></pre>","<pre><code class='python'>0
1
2
3
</code></pre>","<pre><code class='python'>
1
2
3
</code></pre>","C","15","58",NULL,"1",NULL,"2020-09-24 16:16:52",NULL),
("1403","<div class='md'>## Fonction récursive et type str</div>

<p>On a saisi le code suivant :</p>
<pre><code class='python'>def mystere(n,s):
    if n==0:
        return s
    return s + mystere(n-1, s)
</code></pre>

Que retourne l'instruction : <code class='python'> mystere(3,'$')</code> ?","<code class='python'>'$$$'</code>","<code class='python'>'$2$'</code>","<code class='python'>'$$$$'</code>","L'exécution déclenche une erreur","C","15","58",NULL,"1",NULL,"2020-09-23 09:30:21",NULL),
("1404","<div class='md'>## Fonction récursive et assertion</div>

<p>On a saisi le code suivant :</p>
<pre><code class='python'>def g(n):
    assert n&gt;=0
    if n==0 :
        return 1789
    else :
        return g(n-2)
</code></pre>
Qu'obtient-on en exécutant la commande <code class='python'>g(3)</code>?","<code class='python'>1789</code>","<code class='python'>1</code>","<code class='python'>AssertionError</code>","<code class='python'>RecursionError</code>","C","15","58",NULL,"1",NULL,"2020-09-23 09:41:11",NULL),
("1405","<div class='md'>## Fonction récursive de deux variables</div>

<p>On a saisi le code suivant :</p>
<pre><code class='python'>def f(a,b):
    if a == 0 :
        return b
    return f(a-1, b+1)
</code></pre>
Que retourne la commande <code class='python'>f(3,4)</code> ?
","4","5","6","7","D","15","58",NULL,"1",NULL,"2020-09-23 09:45:33",NULL),
("1406","<div class='md'>## Test récursif</div>

<p>On a saisi le code suivant :</p>
<pre><code class='python'>def mystere(n):
    if n&gt;0 :
        return mystere(n-2)
    else :
        return n==0
</code></pre>
Que retourne la commande : <code class='python'>mystere(3)</code>
","True","False","RecursionError","0","B","15","58",NULL,"1",NULL,"2020-09-23 09:54:33",NULL),
("1407","<div class='md'>## Attributs d'une instance de classe</div>

<p>On a saisi le code suivant :</p>
<pre><code class='python'>class Test:
    def __init__(self, a,b):
        self.x = a
        self.y = b

u = Test(4,5)
</code></pre>
Laquelle des expressions suivantes a pour valeur <code class='python'> 4 </code> ?
","<code class='python'>u.a</code>","<code class='python'>u.b</code>","<code class='python'>u.x</code>","<code class='python'>u.y</code>","C","12","42",NULL,"1",NULL,"2020-09-26 12:12:45",NULL),
("1408","<div class='md'>## Comparaison entre objets</div>
<p>On a saisi le code suivant :</p>
<pre><code class='python'>class Coord:
    def __init__(self, a,b):
        self.x = a
        self.y = b

u = Coord(0,5)
v = Coord(0,5)
</code></pre>
Laquelle de ces expressions est évaluée à <code class='python'>True</code>?
","<code class='python'>u == v</code>","<code class='python'>u.a == v.a and u.b == v.b</code>","<code class='python'>u.x == v.x and u.y == v.y</code>","<code class='python'>Coord(0,5) == v</code>","C","12","42",NULL,"1",NULL,"2020-09-26 12:23:16",NULL),
("1409","<div class='md'>## instance et attribut</div>

<p>On a saisi le code suivant :</p>
<pre><code class='python'>class Store:
    def __init__(self, v):
        self.value = v

a = Store(10)
a.value += 2
b = Store(a.value)
a.value += 2
</code></pre>
Quelle est la valeur de : <code class='python'>b.value</code>
","<code class='python'>a.value</code>","<code class='python'>10</code>","<code class='python'>12</code>","<code class='python'>14</code>","C","12","42",NULL,"1",NULL,"2020-09-26 12:35:08",NULL),
("1410","<div class='md'>###Attribut d'un objet</div>

<p>On a saisi le code suivant :</p>
<pre><code class='python'>class Student:
    def __init__(self, name):
        self.name = name


bob = Student('BOB')
name = 'nom'
</code></pre>
Quelle est la valeur de l'expression : <code class='python'>bob.name</code>
","<code class='python'>'name'</code>","<code class='python'>'nom'</code>","<code class='python'>'BOB'</code>","<code class='python'>'bob'</code>","C","12","42",NULL,"1",NULL,"2020-09-26 13:02:36",NULL),
("1411","<div class='md'>### Méthode et attribut</div>

<p>On a saisi le code suivant :</p>
<pre><code class='python'>class Student:
    def __init__(self, name):
        self.name = name
        
    def mystere(self, x):
        return self.name + x

james = Student('JAMES')
bond = Student('BOND')
</code></pre>
Laquelle des expressions suivantes a la valeur : <code class='python'>'JAMESBOND'</code>
","<code class='python'>mystere(james,bond.name)</code>","<code class='python'>mystere(james,bond)</code>","<code class='python'>james.mystere(bond.name)</code>","<code class='python'>james.mystere(bond)</code>","C","12","42",NULL,"1",NULL,"2020-09-26 13:25:48",NULL),
("1412","<div class='md'>###Une méthode, deux instances</div>

<p>On a saisi le code suivant :</p>
<pre><code class='python'>class Score:
    def __init__(self, value):
        self.value = value
        
    def mystere(self, a):
        return self.value &gt; a.value

x = Score(100)
y = Score(80)
</code></pre>
Laquelle des expressions suivantes est évaluée à <code class='python'>True</code> ?
"," <code class='python'>x.mystere(x)</code>"," <code class='python'>x.mystere(y)</code>"," <code class='python'>y.mystere(x)</code>"," <code class='python'>y.mystere(y)</code>","B","12","42",NULL,"1",NULL,"2020-09-26 13:37:49",NULL),
("1413","Donner le résultat de la soustraction binaire 101101 - 1011
","111000","110110","110011","100010","D","2","2",NULL,"1",NULL,"2020-09-27 13:48:34",NULL),
("1414","Donner le résultat de la soustraction binaire 1111 - 101
","1010","1000","1001","110","A","2","2",NULL,"1",NULL,"2020-09-27 13:54:39",NULL),
("1415","Quelle instruction Python permet de renvoyer le caractère correspondant au code décimal 68 ?","ord(68)","ord(0x68)","chr(68)","chr(0x68)","C","2","6",NULL,"1",NULL,"2020-09-27 14:03:55",NULL),
("1416","Quelle instruction Python permet de renvoyer le caractère correspondant au code binaire 01000010, dans la table ASCII ?","ord(int('0b' + '01000010', 2))","chr(int('0b' + '01000010', 2))","chr(int('01000010'))","chr('0b01000010')","B","2","6",NULL,"1",NULL,"2020-09-27 14:20:14",NULL),
("1417","Dans une table Unicode, avec l'encodage UTF-8 en hexadécimal, quel encodage pourrait correspondre à la lettre accentuée 'è' ?","C3 A8","43 A8","C3","A8","A","2","6",NULL,"1",NULL,"2020-09-27 14:25:16",NULL),
("1418","<p>On a saisi le code suivant :</p>
<pre><code class='python'>chaine = 'bout de texte'
mot = 'incomplet'
print(chaine + mot)
</code></pre>
Qu'affichera la console de l'interpréteur Python ?
","chaine + mot","bout de texte incomplet","bout de texteincomplet","un petit bout de texte","C","2","6",NULL,"1",NULL,"2020-09-27 14:57:34",NULL),
("1419","<p>On a saisi le code suivant :</p>
<pre><code class='python'>nombre = input('Saisir un nombre : ')
resultat = nombre + 45
print(resultat)
</code></pre>
Qu'affichera la console de l'interpréteur Python si un utilisateur saisit le nombre 12 au clavier ?","Une erreur","57","67","nombre + 45","A","2","6",NULL,"1",NULL,"2020-09-27 15:01:41",NULL),
("1420","
<b> Quelle est la représentation de 28,8625 en binaire </b>","(11100,11011)<sub>2</sub>","(11100,11011100...)<sub>2</sub>","(0,11011100...)<sub>2</sub>","(1,11011100...)<sub>2</sub>","B","2","4",NULL,"1",NULL,"2020-09-28 20:22:09",NULL),
("1421","<p>Que renvoie machin(7) si la fonction machin, écrite en langage Python est :</p>
<pre><code class='html'>
def machin(a):
    if a &lt; 7:
        return 2 * a
    else:
        return a - 1
</code></pre>","7","14","6","13","C","7","28",NULL,"1",NULL,"2020-10-03 18:01:11",NULL),
("1422","<p>Combien de fois la fonction ```print``` va-t-elle être appelée dans le script suivant :</p>
<pre><code class='html'>
x=4
while(x &lt; 10):
    print('je fais')
    x=x+2
</code></pre>
","1 fois","2 fois","3 fois","4 fois","C","7","28",NULL,"1",NULL,"2020-10-03 18:07:55",NULL),
("1423","<p>Que renvoie cette fonction ?</p>
<pre><code class='html'>
i = 0
j = 2
while j &gt; 0 :
    j = j - 1
    i = i + 1
return i
</code></pre>","i = 0","i = 1","i = 2","i = 3","C","7","28",NULL,"1",NULL,"2020-10-03 18:06:42",NULL),
("1424","<p>Combien de paramètres sont passés dans cette fonction ?</p>
<pre><code class='html'>
calcul_imc(47,1.60,51)
</code></pre>","1","2","3","4","C","7","28",NULL,"1",NULL,"2020-10-03 18:21:24",NULL),
("1425","<p>Quel est le sens de la partie '**' dans ce calcul ?</p>
<pre><code class='html'>
calcul = energie / vitesse**2
</code></pre>","une multiplication","une division","une puissance","un quotient","C","7","28",NULL,"1",NULL,"2020-10-03 18:22:17",NULL),
("1426","<p>Que donnera le code suivant :</p>
<pre><code class='html'>
a = 0
while (a &lt; 7):
    a = a + 1
    print (a)
</code></pre>

","tous les nombres de 0 à 7 (compris)","tous les nombres de 0 à 6 (compris)","tous les nombres de 1 à 7 (compris)","affiche seulement '7'","C","7","28",NULL,"1",NULL,"2020-10-03 18:09:34",NULL),
("1427","<p>Quel est le tableau <code>t</code> construit par les instructions suivantes ?</p>
<pre><code class='python'>t = [0]*5
for i in range(5):
    t[i] = 2 + i
</code></pre>
","<code class='python'>[7, 7, 7, 7, 7]</code>","<code class='python'>[5, 5, 5, 5, 5]</code>","<code class='python'>[2, 3, 4, 5, 6, 7]</code>","<code class='python'>[2, 3, 4, 5, 6]</code>","D","3","10",NULL,"1",NULL,"2020-10-05 17:14:55",NULL),
("1428","<p>Quel est le tableau <code>tab</code> défini par compréhension de la façon suivante ?</p>
<pre><code class='python'>tab = [[i - j for i in range(2)] for j in range(3)]</code></pre>","<pre><code class='python'>[[0, 1], [-1, 0], [-2, -1]]</code></pre>","<pre><code class='python'>[[0, -1], [1, 0], [2, 1]]</code></pre>","<pre><code class='python'>[[0, -1, -2], [1, 0, -1]]</code></pre>","<pre><code class='python'>[0, -1, -2, 1, 0, -1]</code></pre>","A","3","11",NULL,"1",NULL,"2020-10-05 17:49:13",NULL),
("1429","<p class='md'>On considère le tableau `notes` suivant.</p>
<pre><code class='python'>notes= [['Anne', 15], ['Marie', 16], ['Julie', 13.5]]</code></pre>
<p class='md'>Quelle série d'instructions permet d'afficher tous les prénoms et toutes les notes du tableau `notes` ?</p>","<pre><code class='python'>for i in range(3):
    for j in range(2):
        print(i, j)
</code></pre>","<pre><code class='python'>for tab in notes:
    for element in tab:
        print(element)
</code></pre>","<pre><code class='python'>for element in tab:
    print(element)
</code></pre>","<pre><code class='python'>for i in range(3):
    for j in range(2):
        print(notes[j])
</code></pre>","B","3","12",NULL,"1",NULL,"2020-10-05 17:34:28",NULL),
("1430","<br /><p>Quel est le composant de base des microprocesseurs intégrés ?</p>
","La résistance","La diode","Le thyristor","Le transistor","D","6","23",NULL,"1",NULL,"2020-10-09 22:45:24",NULL),
("1431","<br /><p>Dans un processeur dont le bus d’adresses est constitué de 16 bits, le bus de commande de 4 bits et le bus de données de 8 bits, le compteur de programme PC (Program Counter) est un registre de :</p>
","4 bits","8 bits","12 bits","16 bits","D","6","23",NULL,"1",NULL,"2020-10-10 18:40:00",NULL),
("1432","<br /><p>Les trois phases du cycle d’exécution d’une instruction sont dans le désordre : 1 : exécution de l’instruction, 2 : recherche de l’instruction à traiter, 3 : Décodage de l’instruction et recherche de l’opérande. Quel est le bon ordre ?</p>
","1, 2, 3","2, 1, 3","1, 3, 2","2, 3, 1","D","6","23",NULL,"1",NULL,"2020-10-10 18:40:37",NULL),
("1433","<br /><p>Qui a proposé le l'architecture représentée par le schéma suivant?</p>
","William Shockley","John Von Neumann","Philippe Dreyfus","Wilhelm Schickard","B","6","23",NULL,"1","1433_Von Neumann.jpg","2020-10-10 18:41:13",NULL),
("1434","<br /><p>A quelle époque a vécu John Von Neumann?</p>
","Première moitié du 19ième siècle","Deuxième moitié de 19ième siècle","Première moitié du 20ième siècle","Deuxième moitié du 20ième siècle","C","1","1",NULL,"1","1434_john-von-neumann-1903-1957-everett.jpg","2020-10-10 11:11:00",NULL),
("1435","<br /><p>Que permet l’utilisation de plusieurs cœurs dans un système microprogrammé ?</p>","Augmenter les performances de calcul","Augmenter la capacité mémoire","Décupler le nombre de périphériques","Évacuer plus de chaleur","A","6","27",NULL,"1",NULL,"2020-10-10 18:42:06",NULL),
("1436","<br /><p>Cochez la réponse vraie :</p>
","Le jeu d’instructions est l’ensemble de toutes les instructions que peut accomplir le processeur et tous les processeurs ont le même jeu d’instruction","Le jeu d’instructions est l’ensemble de toutes les instructions que peut accomplir le processeur et chaque type de processeur a son propre jeu d’instructions","Le jeu d’instructions est l’ensemble de toutes les fonctions du langage de programmation choisi et chaque type de processeur a son propre jeu d’instruction","Le jeu d’instructions est l’ensemble de toutes les fonctions du langage de programmation choisi et tous les processeurs ont le même jeu d’instruction","B","6","27",NULL,"1",NULL,"2020-10-10 11:30:12",NULL),
("1437","<br /><p>Un programme est écrit en assembleur dans un système microprogrammé dont le bus d'adresse est de 12 bits et le bus de données de 8 bits. Juste avant l’exécution de l’instruction MOVE A, H’702 le contenu du registre PC est H’013.
En vous aidant de l'extrait du jeu d'instruction de la figure suivante et en sachant que pour le registre A r=0 et pour le registre B r=1, choisissez le bon état de la mémoire.
Exemple de proposition suivante: H'700 (H'32); H'701 (H'28). La valeur H'32 (en hexadécimal) est stockée à l'adresse H'700 (en hexadécimal) et la valeur H'28 est stockée à l'adresse H'701.</p>","H’013 (H’58); H’014 (H’07); H’015 (H’02)","H’012 (H’58); H’013 (H’07); H’014 (H’02)","H’013 (H’59); H’014 (H’07); H’015 (H’02)","H’012 (H’59); H’013 (H’07); H’014 (H’02)","A","6","27",NULL,"1","1437_Jeu instruc.png","2020-10-10 12:21:01",NULL),
("1438","<br /><p>Qui est l'inventeur du mot 'Informatique'?</p>
","John Von Neumann","Philippe Dreyfus","Blaise Pascal","Albert Einstein","B","1","1",NULL,"1",NULL,"2020-10-10 12:31:49",NULL),
("1439","<br /><p>Une instruction est l’opération élémentaire que le processeur peut accomplir. Les
instructions sont codées par des nombres binaires et stockées dans la mémoire principale,
en vue d’être traitée par le processeur. Une instruction est composée de:</p>
","Deux champs: code opérande puis code opération","Deux champs: code opération puis code opérande","Un champ: code opération","Un champ: code opérande","B","6","23",NULL,"1",NULL,"2020-10-10 12:39:21",NULL),
("1440","<br /><p>Considérons le schéma d’architecture d’un système microprogrammé suivant, attribuez les bons noms aux différents blocs numérotés:</p>
","1 : Décodeur d’adresse, 2 : Unité centrale, 3 : Mémoire principale, 4 : Interface E/S","1 : Unité centrale, 2 : Décodeur d’adresse, 3 : Interface E/S, 4 : Mémoire principale","1 : Décodeur d’adresse, 2 : Mémoire principale, 3 : Interface E/S, 4 : Unité centrale","1 : Décodeur d’adresse, 2 : Interface E/S, 3 : Unité centrale, 4 : Mémoire principale","A","6","23",NULL,"1","1440_Von Neumann numéroté.png","2020-10-10 15:16:11",NULL),
("1441","<br /><p>Dans l'unité de traitement de l'unité centrale d'un système microprogrammé, quel registre contient des informations sur le résultat de la dernière opération effectuée par l’UAL ?</p>","L'accumulateur","Le registre d'instruction","Le registre PC (Program Counter)","Le registre d'état","D","6","23",NULL,"1",NULL,"2020-10-10 15:23:33",NULL),
("1442","<br /><p>Que signifie l'unité MIPS?</p>
","Millier d'instructions par seconde","Million d'instructions par seconde","Milliard d'instructions par seconde","Milli instruction par seconde","B","6","27",NULL,"1",NULL,"2020-10-10 15:29:59",NULL),
("1443","<div class='md'>## Structures de données </div>
<br /><p>En informatique, une pile (en anglais stack) est : </p>
","une structure de données fondée sur le principe «dernier arrivé, premier sorti» (ou LIFO pour Last In, First Out)","une structure de données fondée sur le principe «premier arrivé, premier sorti» (ou FIFO pour First In, First Out)","une base de données fondée sur le principe «dernier arrivé, dernier sorti» (ou LILO pour Last In, Last Out)","une base de données fondée sur le principe «premier arrivé, dernier sorti» (ou FILO pour First In, Last Out)","A","12","43",NULL,"1",NULL,"2020-10-14 16:40:11",NULL),
("1444","<div class='md'>## Structures de données </div>
<br /><p>En informatique, une file (queue en anglais ) est : </p>

","une structure de données basée sur le principe «Premier entré,premier sorti», en anglais FIFO (First In, First Out)","une structure de données basée sur le principe «Dernier entré,premier sorti», en anglais LIFO (Last In, First Out)","une base de données basée sur le principe «Dernier entré,dernier sorti», en anglais LILO (Last In, Last Out)","une base de données basée sur le principe «Premier entré,dernier sorti», en anglais FILO (First In, Last Out)","A","12","43",NULL,"1",NULL,"2020-10-14 16:46:33",NULL),
("1445","<div class='md'>## Structure de données</div>
<br /><p>En informatique, un usage courant d'une pile, par exemple, est: </p>
","Dans un navigateur web, une pile sert à mémoriser les pages Web visitées.","Les serveurs d’impression, qui doivent traiter les requêtes dans l’ordre dans lequel elles arrivent, et les insèrent dans une pile.","On utilise des piles pour mémoriser temporairement des transactions qui doivent attendre pour être traitées.","La réalisation d'un répertoire téléphonique.","A","12","43",NULL,"1",NULL,"2020-10-14 17:00:20",NULL),
("1446","<div class='md'>Structure de données</div>
<br /><p>En informatique, l'usage d'une file se fait couramment pour :</p>
","Les serveurs d’impression, qui doivent traiter les requêtes dans l’ordre dans lequel elles arrivent, et les insèrent dans une file d’attente (ou une queue).","La fonction «Annuler la frappe» (en anglais «Undo») d’un traitement de texte qui mémorise les modifications apportées au texte dans une file.","L’évaluation des expressions mathématiques en notation post-fixée (ou polonaise inverse en file).","Mémoriser temporairement des transactions qui doivent attendre dans une file pour être traitées.","A","12","43",NULL,"1",NULL,"2020-10-14 17:20:03",NULL),
("1447","<div class='md'>Structure de données</div>
<br /><p>Voici l'implémentation d'une pile en utilisant une simple liste.</p>
<p>On a saisi le code suivant :</p>
<pre><code class='html'>
def pile():
    return []
    
def vide(p):
    return p==[]
    
def empiler(p,x):
    p.append(x)
    
def depiler(p):
    assert not vide(p)
    return p.pop()

p=pile()
for i in range(7):
    empiler(p,2*i)
a=depiler(p)
print(a)
print(vide(p))
</code></pre>
Quel affichage obtient-on à la fin de l’exécution de ce script ?
","<p>12</p>
False","<p>14</p>
None","<p>12</p>
[ ]","<p>7</p>
True","A","12","43",NULL,"1",NULL,"2020-10-14 17:39:28",NULL),
("1448","<div class='md'>## Instance et attribut</div>
<pre><code class='html'>
On a saisi le code suivant :

class Bizarre:
    def __init__(self, v):
        self.value = v

    def truc(self,chose):
        return self.value+chose

a = Bizarre(5)
a.value += 2
b = a.truc(3)
b += 4
</code></pre>
Quelle est la valeur de b?
","3","8","12","14","D","12","42",NULL,"1",NULL,"2020-10-16 09:06:54",NULL),
("1449","<p><strong>Les programmes et les ordinateurs.</strong></p>
Quel est le composant qui exécute les programmes ?
","Le processeur","La RAM ou mémoire vive","Le disque dur","La ROM ou mémoire morte","A","6","23",NULL,"1",NULL,"2020-10-28 17:34:54",NULL),
("1450","<p><strong>Les programmes et les ordinateurs.</strong></p>
Dans quel composant est chargé un programme avant d'être exécuté par le processeur ?
","La RAM ou mémoire vive","La ROM ou mémoire morte","Le disque dur","Le chipset","A","6","23",NULL,"1",NULL,"2020-10-28 17:42:54",NULL),
("1451","<p><strong>Les programmes et les ordinateurs.</strong></p>
La RAM est une mémoire à accès ?
","en lecture uniquement.","en écriture uniquement.","aléatoire.","séquentiel.","C","6","23",NULL,"1",NULL,"2020-10-28 18:05:43",NULL),
("1452","<div class='md'>## On considère la base de donnée constituée des relations suivantes :</div>
<br /><p>Les clés primaires sont soulignées, les clés étrangères sont en italique</p>
<p> Le domaine de chaque attribut est écrit entre parenthèse </p>
<pre><code class='html'>
Pilote (<u>N_Pil</u> (INT), Nom_Pil (CHAR(15)), Ville_Pil (CHAR(15)))
Avion (<u>N_Av</u> (INT), Nom_Av (CHAR(15)), Capacite (INT), Ville_Av (CHAR(15)))
Vol (<u>N_Vol</u> (INT), <i>N_Pil</i> (INT), <i>N_Av</i> (INT), Ville_Dep (CHAR(15)), Ville_Arr (CHAR(15)), H_Dep (TIME), H_Arr (TIME))
</code></pre>
<div class='md'>### Quelles sont les requêtes qui ne rendent pas la demande formulée ?</div>","Obtenir la liste des noms des pilotes :
<pre><code class='html'>
SELECT DISTINCT Nom_Pil
FROM Pilote
</code></pre>","Obtenir la liste des noms des avions pilotés par le pilote dénommé 'Joe Delle' :
<pre><code class='html'>
SELECT DISTINCT Nom_Av
FROM Avion
JOIN Vol ON Avion.N_Av = Vol.N_Av
JOIN Pilote ON Pilote.N_pil = Vol.N_Pil
WHERE Nom_Pil = 'Joe Delle'
</code></pre>","Obtenir la liste des noms et capacités de tous les avions des vols au départ de 'Rome' :
<pre><code class='html'>
SELECT Nom_Av,Capacite
FROM Avion
JOIN Vol ON Avion.N_Av=Vol.N_Av
WHERE Ville_Dep = 'Rome'
</code></pre>","Obtenir les numéros de vols de tous les Airbus A320 :
<pre><code class='html'>
SELECT N_Vol
FROM vols
JOIN Avion ON Avion.Nom_Av = Vol.Nom_Av
WHERE Nom_Av = 'Airbus A320'
</code></pre>","D","13","50",NULL,"1",NULL,"2020-10-28 18:14:49",NULL),
("1453","<p><strong>Les programmes et les ordinateurs.</strong></p>
Les informations entre le processeur et la RAM (mémoire vive) circulent ?
","Du processeur vers la mémoire RAM uniquement.","De la mémoire RAM  vers le processeur uniquement.","Aucune information n'est échangée entre ces deux composants.","De la mémoire RAM  vers le processeur et du processeur vers la RAM.","D","6","23",NULL,"1",NULL,"2020-10-28 17:54:01",NULL),
("1454","<p><strong>Les programmes et les ordinateurs.</strong></p><p>
Le BIOS appelé aussi mémoire morte : </p>","est une partie du processeur.","contient les informations nécessaires pour démarrer l'ordinateur.","est effacé lorsque l'ordinateur est éteint.","est une mémoire accessible en écriture uniquement;","B","6","23",NULL,"1",NULL,"2020-10-28 18:16:09",NULL),
("1455","<p><strong>Les programmes et les ordinateurs.</strong></p>
Pour démarrer un ordinateur doit disposer au minimum d'un logiciel de type :
","navigateur","système d'exploitation","compilateur","interpréteur","B","6","23",NULL,"1",NULL,"2020-10-28 18:20:19",NULL),
("1456","<p><strong>Les programmes et les ordinateurs.</strong></p>
Que désigne le terme code source ?
","du code écrit par le programmeur.","du code binaire exécuté par la machine.","du code directement exécutable par la machine.","du code généré par un logiciel de type compilateur.","A","6","27",NULL,"1",NULL,"2020-10-28 18:31:47",NULL),
("1457","<p><strong>Langages et programmes: vocabulaire</strong></p>
Le fait de mettre 'bout à bout' deux chaines de caractères se nomme ?
 ","interprétation","compilation","indentation","concaténation","D","2","6",NULL,"1",NULL,"2020-10-28 19:27:01",NULL),
("1458","<p><strong>Langages et programmes: vocabulaire<strong></strong></strong></p>
Le langage Python est un langage ?","interprété","compilé","binaire","machine","A","7","29",NULL,"1",NULL,"2020-10-28 19:35:45",NULL),
("1459","<div class='md'>## On considère la base de donnée constituée des relations suivantes :</div>
<br /><p>Les clés primaires sont soulignées, les clés étrangères sont en italique</p>
<p> Le domaine de chaque attribut est écrit entre parenthèse </p>
<pre><code class='html'>
Client (<u>N_Cli</u> (INT), Nom (CHAR(15)), Adresse (CHAR(150)) )
Facture (<u>Num</u> (INT), <i>N_Cli</i> (INT), Date (DATE), Reglee (CHAR(1)) )
Produit (<u>Ref</u> (INT), Designation (CHAR(50)), <i>Ref_Fournisseur</i> (INT), Prix (REAL), Stock (INT) )
Details_facture (<i>Num</i> (INT), <i>Ref_Prod</i> (INT), Quantite (INT))
Fournisseur(<u>Ref</u> (INT), Nom (CHAR(15)) , Adresse (CHAR(150)) )

</code></pre>
<p> <b> L'attribut 'Reglee' de la relation 'Facture' n'a que 2 valeurs possibles : 'O' si la facture est réglée et 'N' sinon</b> </p>
<div class='md'>### Quelles sont les requêtes qui ne rendent pas la demande formulée ?</div>

","Obtenir tous les nom (désignation) et quantité des produits dont le nombre en stock est inférieur à 5 :
<pre><code class='html'>
SELECT Designation, Stock
FROM Produit
WHERE Stock &lt; 5
</code></pre>","Obtenir tous les nom des clients qui n'ont pas réglé au moins une facture :
<pre><code class='html'>
SELECT DISTINCT Nom
FROM Client
JOIN Facture ON Facture.N_Cli = Client.N_Cli
WHERE Reglee = 'N'
</code></pre>","Obtenir tous les nom des clients qui ont acheté le produit dénommé 'Disque Dur 1To Sangston' :
<pre><code class='html'>
SELECT DISTINCT Nom
FROM Client
JOIN Facture ON Facture.N_Cli = Client.N_Cli
JOIN Produit ON Facture.Ref_Prod = Produit.Ref
WHERE Produit.Designation = 'Disque Dur 1To Sangston'
</code></pre>","Obtenir le nom du client et le total de la facture référencée sous le numéro 1004006 :
<pre><code class='html'>
SELECT Nom,SUM(Prix*Quantite)
FROM Client
JOIN Facture ON Facture.N_Cli = Client.N_Cli
JOIN Details_facture ON Details_facture.Num = Facture.Num
JOIN Produit ON Details_facture.Ref_Prod = Produit.Ref
WHERE Facture.Num = 1004006
</code></pre>","C","13","50",NULL,"1",NULL,"2020-10-28 20:07:16",NULL),
("1460","<p><strong>Programmation bases Python</strong></p>
On a saisi le code suivant :
<pre>
def moy(x,y):
    return ((x+y)/2)
moyenne=moy(3,5)
</pre>
Que contient la variable moyenne à la fin de l’exécution de ce script ?
","8.0","4","4.0","Autre réponse","C","7","28",NULL,"1",NULL,"2020-10-30 15:49:12",NULL),
("1461","<p><strong>Programmation bases Python</strong></p>
On a saisi le code suivant :
<pre>
def carre(val):
    return (val*val)
def func(val):
    val=val+1
    return (val)
valeur=carre(func(5))
</pre>
Que contient la variable valeur à la fin de l’exécution de ce script ?
","25","26","36","37","C","7","28",NULL,"1",NULL,"2020-10-30 15:32:32",NULL),
("1462","<p><strong>Programmation bases Python</strong></p>
On a saisi le code suivant :
<pre>
def func(val):
    if val&gt;0:
        return (1+val)
    else:
        return (1-val)
resultat=func(3)+func(-3)
</pre>
Que contient la variable resultat à la fin de l’exécution de ce script ?
","0","2","6","8","D","7","28",NULL,"1",NULL,"2020-10-30 15:50:41",NULL),
("1463","<p><strong>Programmation bases Python</strong></p>
On a saisi le code suivant :
<pre>
def operation_1(a,b,c):
    return (a+b*c)
def operation_2(a,b,c):
    return (a*2+b)
def operation_3(a,b,c):
    return (c*3-b)
i=5
j=1
k=2
resultat_1=operation_1(i,j,k)+operation_3(k,i,j)
if i!=resultat_1:
    resultat_2=resultat_1+operation_2(k,j,i)
else:
    resultat_2=resultat_1+operation_2(k,i,j)
</pre>
Que contient la variable resultat_2 à la fin de l’exécution de ce script ?
","10","12","14","16","C","7","28",NULL,"1",NULL,"2020-10-30 15:55:28",NULL),
("1464","<p><strong>Programmation bases Python</strong></p>
On a saisi le code suivant :
<pre>
def fonct(x):
    k=1
    for i in range(1,x):
        k=k+k
    return k
resultat=fonct(5)
</pre>
Que contient la variable resultat à la fin de l’exécution de ce script ?","1","5","8","16","D","7","28",NULL,"1",NULL,"2020-10-30 15:57:04",NULL),
("1465","<p><strong>Programmation bases Python</strong></p>
On a saisi le code suivant :
<pre>
def func(val):
    if val&gt;0:
        return (1+val)
    else:
        return (1-val)
resultat=func(3,5)
</pre>
Que contient la variable resultat à la fin de l’exécution de ce script ?","0","2","5","Autre réponse","D","7","28",NULL,"1",NULL,"2020-10-30 15:57:44",NULL),
("1466","<p><strong>Programmation bases Python</strong></p>
<p>On a saisi le code suivant :</p>
<pre>
def operation_1(a,b,c):
    return (a+b*c)
def operation_2(a,b,c):
    return (a*2+b)
def operation_3(a,b,c):
    return (c*3-b)
i=1
j=2
k=7
if i&gt;5:
  resultat=operation_1(i,j,k)
elif j&lt;3:
    resultat=operation_2(j,k,i)
else:
    resultat=operation_3(k,i,j)
</pre>
Que contient la variable resultat à la fin de l’exécution de ce script ?","4","11","15","19","B","7","28",NULL,"1",NULL,"2020-10-30 15:41:54",NULL),
("1467","<div class='md'>## On considère la base de donnée constituée des relations suivantes :</div>
<br /><p>Les clés primaires sont soulignées, les clés étrangères sont en italique</p>
<p> Le domaine de chaque attribut est écrit entre parenthèse </p>
<pre><code class='html'>
mes (<u>id</u> (INT), nom (CHAR(15)), prenom (CHAR(15)), annee_naiss (INT) )
film (<u>id</u> (INT), titre_film (CHAR(15)), annee_sortie (INT), <i>idMES</i> (INT), genre (CHAR(15)), pays (CHAR(15)) )
</code></pre>
<div class='md'>
#### *mes* est la table des metteurs en scène et *IdMES* est l'id du metteur en scène
### Quelles sont les requêtes qui ne rendent pas la demande formulée ?</div>
","Obtenir la liste des différents genres de film :
<pre><code class='html'>
SELECT DISTINCT genre 
FROM film
</code></pre>","Obtenir les titres des films d'action sortis entre 1980 et 1990 et le nom de leur réalisateur :
<pre><code class='html'>
SELECT titre_film , nom
FROM mes 
JOIN film ON film.idMES=mes.id
WHERE genre='Action' AND annee_sortie BETWEEN 1980 AND 1990
</code></pre>","Obtenir le nombre de comédies :
<pre><code class='html'>
SELECT COUNT(*) 
FROM film 
WHERE genre='Comedie'
</code></pre>","Obtenir le nom et le prénom du metteur en scène du film 'Titanic' :
<pre><code class='html'>
SELECT prenom, nom 
FROM mes
WHERE titre_film='Titanic'
</code></pre>","D","13","50",NULL,"1",NULL,"2020-10-31 19:34:09",NULL),
("1468","<div class='md'>## Dictionnaire </div>
<br /><p>Que va afficher la console si on exécute le programme suivant ?</p>
<pre><code class='html'>
info={'nom':'Durand', 'prenom':'Jean','ddn':'12/12/1970'}
for i in info.values():
    print(i)
</code></pre>

","[Durand,Jean,12/12/1970]","['Durand','Jean','12/12/1970']","['nom','prenom','ddn']","<p>Durand</p>
<p>Jean</p>
<p>12/12/1970</p>","D","3","9",NULL,"1",NULL,"2020-11-08 17:59:38",NULL),
("1469","<div class='md'>### liste chaînée </div>
<p>On a saisi le code suivant :</p>
<pre><code class='python'>class Maillon: 
    def __init__(self, val, suiv):
        self.valeur = val
        self.suivant = suiv

def mystere(lst, n):
    if n == 0:
        return lst.valeur
    else:
        return mystere(lst.suivant, n-1)

lst0 = Maillon(10,Maillon(20,Maillon(30,None)))</code></pre>

Quelle est la valeur de <code class='python'>mystere(lst0, 2)</code>
","10","20","30","cette commande renvoie une erreur !","C","12","43",NULL,"1",NULL,"2020-11-09 11:34:05",NULL),
("1470","<div class='md'>### liste chaînée </div>
<p>On a saisi le code suivant :</p>
<pre><code class='python'>class Maillon: 
    def __init__(self, val, suiv):
        self.valeur = val
        self.suivant = suiv

def mystere(lst, n):
    if n == 0:
        return lst.valeur
    else:
        return mystere(lst.suivant, n-1)

a = Maillon(10,None)
b = Maillon(20,a)
c = Maillon(30,b)</code></pre>

Quelle est la valeur de <code class='python'>mystere(c, 2)</code>
","10","20","30","cette commande renvoie une erreur !","A","12","43",NULL,"1",NULL,"2020-11-09 11:40:25",NULL),
("1471","<div class='md'>### liste chaînée </div>
<p>On a saisi le code suivant :</p>
<pre><code class='python'>class Maillon: 
    def __init__(self, val, suiv):
        self.valeur = val
        self.suivant = suiv

a = Maillon(10,None)
b = Maillon(20,a)
c = Maillon(30,b)</code></pre>

Parmi les expressions suivantes, quelle est celle dont la valeur est 20 ?","a.suivant.valeur","a.valeur.suivant","c.suivant.valeur","c.valeur.suivant","C","12","42",NULL,"1",NULL,"2020-11-09 11:45:32",NULL),
("1472","<div class='md'>### liste chaînée </div>
<p>On a saisi le code suivant :</p>
<pre><code class='python'>class Maillon: 
    def __init__(self, val, suiv):
        self.valeur = val
        self.suivant = suiv

def mystere(lst):
    if lst is None:
        return ''
    else:
        return mystere(lst.suivant) + '-' + str(lst.valeur)

lst = Maillon(10,Maillon(20,Maillon(30,None)))</code></pre>

Qu'obtient-on en exécutant <code class='python'>mystere(lst)</code>","<code class='python'>-10-20-30</code>","<code class='python'>10-20-30</code>","<code class='python'>-30-20-10</code>","<code class='python'>30-20-10</code>","C","12","43",NULL,"1",NULL,"2020-11-09 12:04:07",NULL),
("1473","<div class='md'>### liste chaînée </div>
<p>On a saisi le code suivant :</p>
<pre><code class='python'>class Maillon: 
    def __init__(self, val, suiv):
        self.valeur = val
        self.suivant = suiv

def mystere(lst):
    if lst.suivant is None:
        return str(lst.valeur)
    else:
        return str(lst.valeur) + '-' +mystere(lst.suivant)

lst = Maillon(10,Maillon(20,Maillon(30,None)))</code></pre>

Qu'obtient-on en exécutant <code class='python'>mystere(lst)</code>
","<code class='python'>10-20-30</code>","<code class='python'>-10-20-30</code>","<code class='python'>30-20-10</code>","<code class='python'>-30-20-10</code>","A","12","43",NULL,"1",NULL,"2020-11-09 12:03:05",NULL),
("1474","<div class='md'>### liste chaînée </div>
<p>On a saisi le code suivant :</p>
<pre><code class='python'>class Maillon: 
    def __init__(self, val, suiv):
        self.valeur = val
        self.suivant = suiv

def mystere(lst):
    if lst.suivant is not None:
        mystere(lst.suivant)
        print(lst.valeur)

a = Maillon(10,None)
b = Maillon(20,a)
c = Maillon(30,b)</code></pre>

quel affichage obtient-on en exécutant <code class='python'>mystere(c)</code>","<pre><code class='python'>10
20
30</code></pre>","<pre><code class='python'>20
30</code></pre>","<pre><code class='python'>30
20
10</code></pre>","<pre><code class='python'>30
20</code></pre>","B","12","43",NULL,"1",NULL,"2020-11-09 12:17:24",NULL),
("1475","<p>On a saisi le code suivant :</p>
<pre><code class='python'>class Maillon: 
    def __init__(self, val, suiv):
        self.valeur = val
        self.suivant = suiv

def mystere(lst):
    if lst is  None:
        return 0
    return lst.valeur + mystere(lst.suivant)

a = Maillon(10,None)
b = Maillon(20,a)
c = Maillon(30,b)</code></pre>

quelle est la valeur de <code class='python'>mystere(c)</code>","30","50","60","0","C","12","43",NULL,"1",NULL,"2020-11-09 15:33:01",NULL),
("1476","<p>On a saisi le code suivant :</p>
<pre><code class='python'>class Maillon: 
    def __init__(self, val, suiv):
        self.valeur = val
        self.suivant = suiv

def mystere(lst):
    if lst.suivant is  None:
        return 0
    return lst.valeur + mystere(lst.suivant)

a = Maillon(10,None)
b = Maillon(20,a)
c = Maillon(30,b)</code></pre>

quelle est la valeur de <code class='python'>mystere(c)</code>","30","50","60","0","B","12","43",NULL,"1",NULL,"2020-11-09 15:37:46",NULL),
("1477","<p>On a saisi le code suivant :</p>
<pre><code class='python'>class Maillon: 
    def __init__(self, val, suiv):
        self.valeur = val
        self.suivant = suiv

def mystere(lst):
    if lst.suivant is None:
        return lst.valeur 
    return mystere(lst.suivant)

a = Maillon(10,None)
b = Maillon(20,a)
c = Maillon(30,b)</code></pre>

quelle est la valeur de <code class='python'>mystere(c)</code>","30","20","10","<code class='python'>None</code>","C","12","43",NULL,"1",NULL,"2020-11-09 15:43:32",NULL),
("1478","<p>On a saisi le code suivant :</p>
<pre><code class='python'>class Maillon: 
    def __init__(self, val, suiv):
        self.valeur = val
        self.suivant = suiv

def mystere(lst):
    if lst.suivant is not None:
        return lst.valeur 
    else :
        return 0

a = Maillon(10,None)
b = Maillon(20,a)

print(mystere(a), mystere(b) )</code></pre>

quel sera l'affichage obtenu à l'exécution de ce code?","<code class='python'>10 20</code>","<code class='python'>0 10</code>","<code class='python'>0 20</code>","30","C","12","43",NULL,"1",NULL,"2020-11-09 15:53:25",NULL),
("1479","<p>On a saisi le code suivant :</p>
<pre><code class='python'>class Maillon: 
    def __init__(self, val, suiv):
        self.valeur = val
        self.suivant = suiv

def mystere(lst):
    if lst.suivant is None:
        return ''
    return str(lst.valeur) + '-' + mystere(lst.suivant)

a = Maillon(10,None)
b = Maillon(20,a)
c = Maillon(30,b)

a.suivant = c
</code></pre>

quelle est la valeur de <code class='python'>mystere(c)</code>","<code class='python'>'30-20-10'</code>","<code class='python'>'30-20-'</code>","<code class='python'>'30-20'</code>","cette commande renvoie une <code class='python'>RecursionError</code>                    ","D","12","43",NULL,"1",NULL,"2020-11-10 09:52:51",NULL),
("1480","<div class='md'>## Système de Gestion des Base de Données (SGBD)!</div>
<br />
A quoi ne sert pas un SGBD ?

","A gérer les doublons existants","A garantir la cohérence des données (respect des domaines par exemple)","A éviter les pertes d'informations lors des pannes","A gérer les accès aux données","A","13","49",NULL,"1",NULL,"2020-11-09 20:59:33",NULL),
("1481","Qu'affiche le terminal suite à l'interprétation du code suivant :

<br /><pre><code class='html'>
phrase = 'il fait beau dehors et pourtant j'ai le nez collé à mon écran'

print('é' in phrase)
</code></pre>
","é","2","True","C'est vrai, tu devrais sortir un peu...","C","2","6",NULL,"1",NULL,"2020-11-16 11:21:53",NULL),
("1482","On a saisi le code suivant :

<pre><code class='html'>
chaine = '56842'

bidule = ''
for element in chaine:
    bidule = bidule + element
</code></pre>
Que contient la variable bidule à la fin de l’exécution de ce script ?
","56842","25","5","element","A","7","28",NULL,"1",NULL,"2020-11-16 11:22:15",NULL),
("1483","<div class='md'>#### Boucles imbriquées</div>
<p>On a saisi le code suivant :</p>
<pre><code class='html'>
a = int(input('Saisir la valeur de a : '))
b = int(input('Saisir la valeur de b : '))

for i in range(a):
    print('Début du bloc d'instructions de la première boucle')
    for j in range (b):
        print(f'i = {i} et j = {j}')
</code></pre>

<p>Que doit saisir l'utilisateur pour que le terminal affiche les lignes suivantes :</p>
<pre><code class='html'>
Début du bloc d'instructions de la première boucle
i = 0 et j = 0
i = 0 et j = 1
Début du bloc d'instructions de la première boucle
i = 1 et j = 0
i = 1 et j = 1
Début du bloc d'instructions de la première boucle
i = 2 et j = 0
i = 2 et j = 1
</code></pre>","<p>Ses deux saisies successives doivent être... </p>
<p>Saisir la valeur de a : 3</p>
<p>Saisir la valeur de b : 3</p>","<p>Ses deux saisies successives doivent être... </p>
<p>Saisir la valeur de a : 3</p>
<p>Saisir la valeur de b : 2</p>","<p>Ses deux saisies successives doivent être... </p>
<p>Saisir la valeur de a : 2</p>
<p>Saisir la valeur de b : 3</p>","<p>Ses deux saisies successives doivent être... </p>
<p>Saisir la valeur de a : 2</p>
<p>Saisir la valeur de b : 2</p>","B","7","28",NULL,"1",NULL,"2020-11-16 11:22:56",NULL),
("1484","On a saisi le code suivant :

<pre><code class='html'>
phrase1 = 'Quand te reverrai-je, pays merveilleux, '
phrase2 = 'où ceux qui s’aiment vivent à deux ? '
phrase3 = 'dans cette neige y trouverais-je un amoureux ?'

citation = phrase1 + phrase2 + phrase1 + phrase3

compteur = 0
for elt in citation:
    if elt == '-':
        compteur = compteur + 1
print(compteur)
</code></pre>
Qu'affiche le terminal à la fin de l’exécution de ce script ?","0","1","2","3","D","7","28",NULL,"1",NULL,"2020-11-16 11:23:29",NULL),
("1485","<p>On souhaite donner la représentation en virgule flottante, en simple précision, du nombre 45,59375. </p>
<p>On admet que 45,59375 est égale à (101101.10011) en binaire ?</p>
<p>1) Combien vaut le décallage ? </p>
<p>2) Que vaut la mantisse ?</p>
<p>3) Combien vaut l'exposant (en décimal) ? </p>
<p>4) Donner la représentation en virgule flottante, en simple précision, du nombre 45,59375.</p>","<p>1) 3</p>
<p>2) 0110110011</p>
<p>3) 130</p>
<p>4) 0 - 10000010 - 01101100110000000000000</p>","<p>1) 5</p>
<p>2) 0110110011</p>
<p>3) 132</p>
<p>4) 0 - 10000100 - 01101100110000000000000</p>","<p>1) -3</p>
<p>2) 10110110011</p>
<p>3) 260</p>
<p>4) 0 - 10000010 - 10110110011000000000000</p>","<p>1) -5</p>
<p>2) 10011</p>
<p>3) 122</p>
<p>4) 0 - 01111010 - 100110000000000000000</p>","B","2","4",NULL,"1",NULL,"2020-11-17 22:14:49",NULL),
("1486","<p>On souhaite donner la représentation en virgule flottante, en simple précision, du nombre -23,21875. </p>
<p>On admet que -23,21875 est égal à (10111.00111) en binaire ?</p>
<p>1) Combien vaut le décallage ? </p>
<p>2) Que vaut la mantisse ?</p>
<p>3) Combien vaut l'exposant (en décimal) ? </p>
<p>4) Donner la représentation en virgule flottante, en simple précision, du nombre -23,21875.</p>
","<p>1) -4</p>
<p>2) 00111</p>
<p>3) 130</p>
<p>4) 0 - 10000010 - 0011100000000000000000</p>","<p>1) -4</p>
<p>2) 011100111</p>
<p>3) 123</p>
<p>4) 1 - 01111011 - 0111001110000000000000</p>","<p>1) 4</p>
<p>2) 011100111</p>
<p>3) 131</p>
<p>4) 1 - 10000011 - 0111001110000000000000</p>","<p>1) 4</p>
<p>2) 00111</p>
<p>3) 131</p>
<p>4) 1 - 10000011 - 0011100000000000000000</p>","C","2","4",NULL,"1",NULL,"2020-11-17 22:26:47",NULL),
("1487","<p>On considère le nombre N ci-dessous codé en binaire simple précision :</p>
<p> N = 1 - 0110 0110 - 0011 1000 0000 0000 0000 000 </p>
<p>1) Quel est le signe du nombre N ?</p>
<p>2) Que vaut l'exposant ?</p>
<p>3) Que vaut le décallage en décimal ?</p>
<p>4) En décimal, le nombre N est égal à ...</p>","<p>1) négatif</p>
<p>2) 0011 1000</p>
<p>3) 102</p>
<p>4) N=-1,21875</p>","<p>1) négatif</p>
<p>2) 0110 0110</p>
<p>3) -25</p>
<p>4) N=-3,632 x 10^(-8)</p> ","<p>1) positif</p>
<p>2) 0011 1000</p>
<p>3) -25</p>
<p>4) N=1,21875 x 2^(-25)</p>","<p>1) négatif</p>
<p>2) 0110 0110</p>
<p>3) 102</p>
<p>4) N=-1,21875 x 2^(-25)</p>","B","2","4",NULL,"1",NULL,"2020-11-17 22:43:12",NULL),
("1488","<p> L'écriture binaire de 13,25 en virgule fixe est ...</p>","1011,010","1101.001","1101.010","101,001","C","2","4",NULL,"1",NULL,"2020-11-17 22:46:43",NULL),
("1489","<p>Parmi les nombres flotants à virgule fixe suivants, lequel est strictement supérieur à 5,125 ?</p>
","101.100","100.010","101.000","100.001","A","2","4",NULL,"1",NULL,"2020-11-17 22:53:19",NULL),
("1490","<div class='md'>## BDD -SQL</div>

<p>La municipalité de Grenoble organise tous les ans un festival de musique classique. Pour la gestion de son festival elle utilise le modèle relationnel suivant :<br /><br />
ŒUVRE(N°_de_l’œuvre, Nom_de_l’œuvre, Durée, N°_du_concert)<br />
Clé primaire : N°_de_l’œuvre<br />
Clé étrangère : N°_du_concert<br />
CONCERT(N°_du_concert, Nom_du_concert, Date_du_concert, n°_du_lieu)<br />
Clé primaire : N°_du_lieu<br /><br /><b>Quelle est la requête SQL qui permet de donner le nom du concert (Nom, Date) concernant l’œuvre 'Le Beau Danube bleu' ?</b></p>

","SELECT Nom_du_concert, Date_du_concert
FROM CONCERT, ŒUVRE
WHERE CONCERT.N°_du_concert = ŒUVRE.N°_du_concert
AND Nom_de_l'oeuvre = 'Le Beau Danube bleu';","SELECT Nom_du_concert, Date_du_concert
FROM CONCERT
WHERE Nom_de_l’œuvre = 'Le Beau Danube bleu';","SELECT Nom_du_concert, Date_du_concert
FROM CONCERT, ŒUVRE
WHERE CONCERT.N°_du_concert = ŒUVRE.N°_du_concert
AND N°_de_l'oeuvre = 'Le Beau Danube bleu';","je ne sais pas","A","13","50",NULL,"1",NULL,"2020-11-19 10:23:25",NULL),
("1491","<div class='md'>## BDD -SQL</div>

<p>La municipalité de Grenoble organise tous les ans un festival de musique classique. Pour la gestion de son festival elle utilise le modèle relationnel suivant :<br /><br />
ŒUVRE(N°_de_l’œuvre, Nom_de_l’œuvre, Durée, N°_du_concert)<br />
Clé primaire : N°_de_l’œuvre<br />
Clé étrangère : N°_du_concert<br />
CONCERT(N°_du_concert, Nom_du_concert, Date_du_concert, n°_du_lieu)<br />
Clé primaire : N°_du_lieu<br /><br /><b>Quelle est la requête SQL qui permet de donner les noms des concerts triés par ordre alphabétique ?</b></p>

","SELECT Nom_du_concert,
FROM OEUVRE
ORDER BY Nom_du_concert ASC;","SELECT Nom_du_concert,
FROM CONCERT
ORDER BY Nom_du_concert ASC;","SELECT Nom_du_concert,
FROM CONCERT
ORDER BY Nom_du_concert DESC;","SELECT Nom_du_concert,
FROM OEUVRE
ORDER BY Nom_du_concert DESC;","B","13","50",NULL,"1",NULL,"2020-11-19 11:28:54",NULL),
("1492","<p>En complément à 2 sur 8 bits, quel est l'entier relatif codé par 11110010?</p>

","242","-242","14","-14","D","2","3",NULL,"1",NULL,"2020-11-19 19:01:11",NULL),
("1493","<p>L'écriture décimale du nombre 10011101 écrit sur 8 bits en complément à 2 est :</p>
","-4","-29","-99","157","C","2","3",NULL,"1",NULL,"2020-11-19 18:04:38",NULL),
("1494","<p>L'écriture décimale du nombre 11101000 écrit sur 8 bits en complément à 2 est :</p>","232","-24","-104","-23","B","2","3",NULL,"1",NULL,"2020-11-19 18:57:38",NULL),
("1495","<p>L'écriture décimale du nombre 10001100 écrit sur 8 bits en complément à 2 est :</p>","-116","-12","140","-115","A","2","3",NULL,"1",NULL,"2020-11-19 19:00:19",NULL);
/*!40000 ALTER TABLE `questions` ENABLE KEYS */;
