#!/usr/bin/env python3
# -*- coding: utf-8 -*-
 
from setuptools import setup, find_packages
 
# notez qu'on import la lib
# donc assurez-vous que l'importe n'a pas d'effet de bord
import genumsitoAMC

setup(
    name='genumsitoAMC',
    version = genumsitoAMC.__version__,
    packages=find_packages(),
    author='Vincent-Xavier Jumel',
    author_email="vincent-xavier.jumel@lasallesaintdenis.com",
    description="Création de QCM au format AMC depuis genumsi",
    long_description=open('README.md').read(),
    include_package_data=True,
    url='https://framagit.org/',
    classifiers=[
        "Programming Language :: Python",
        "Development Status :: 1 - Planning",
        "License :: OSI Approved",
        "Natural Language :: French",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3.9",
        "Topic :: Education",
    ],
    entry_points = {
        'console_scripts': [
            'genere-qcm = genumsitoAMC.main:generer',
        ],
    },
    license='GNU-GPL'
)
